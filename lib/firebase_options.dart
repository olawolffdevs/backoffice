// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyBUH0ABLe4_m7wu33u1gc7VcIXebz-DZrI',
    appId: '1:609511349479:web:2dceb9b68d40ff8b1c1642',
    messagingSenderId: '609511349479',
    projectId: 'venver-app',
    authDomain: 'venver-app.firebaseapp.com',
    databaseURL: 'https://venver-app.firebaseio.com',
    storageBucket: 'venver-app.appspot.com',
    measurementId: 'G-86S71DSKCD',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyB1jBmK-WxfBvkhgs9F1His6ZW6Hl-F9sE',
    appId: '1:609511349479:android:4f3b7cccfad6a1441c1642',
    messagingSenderId: '609511349479',
    projectId: 'venver-app',
    databaseURL: 'https://venver-app.firebaseio.com',
    storageBucket: 'venver-app.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyBNKxx9_-iDLZhsWlkJBKowcwf4WdsV7eI',
    appId: '1:609511349479:ios:7264aebad5d0aee61c1642',
    messagingSenderId: '609511349479',
    projectId: 'venver-app',
    databaseURL: 'https://venver-app.firebaseio.com',
    storageBucket: 'venver-app.appspot.com',
    androidClientId: '609511349479-aq1uqnjr4mpca1mfngugtk6qah94k9cf.apps.googleusercontent.com',
    iosClientId: '609511349479-0v3mdj9jlcieo52o57nl2q5d81upkkam.apps.googleusercontent.com',
    iosBundleId: 'com.example.backoffice',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyBNKxx9_-iDLZhsWlkJBKowcwf4WdsV7eI',
    appId: '1:609511349479:ios:e1e30f48572855261c1642',
    messagingSenderId: '609511349479',
    projectId: 'venver-app',
    databaseURL: 'https://venver-app.firebaseio.com',
    storageBucket: 'venver-app.appspot.com',
    androidClientId: '609511349479-aq1uqnjr4mpca1mfngugtk6qah94k9cf.apps.googleusercontent.com',
    iosBundleId: 'com.example.backoffice.RunnerTests',
  );
}
