import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ContactsTaskPage extends StatefulWidget {
  const ContactsTaskPage({Key? key, this.contacts}) : super(key: key);
  final List<ContactEntity>? contacts;

  @override
  State<ContactsTaskPage> createState() => _ContactsTaskPageState();
}

class _ContactsTaskPageState extends State<ContactsTaskPage> {
  List<ContactEntity>? _contacts;
  List<ContactEntity> _contactsSelected = [];

  @override
  void initState() {
    super.initState();
    checkContacts();
    callApi();
  }

  void checkContacts() {
    if (widget.contacts != null) {
      widget.contacts?.map((e) => _contactsSelected.add(e)).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  OwText(
                    'Selecionar contatos',
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                ],
              ),
            ),
            Visibility(
              visible: _contacts != null,
              replacement: const Center(child: CircularProgressIndicator()),
              child: ListView.separated(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                ),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _contacts?.length ?? 0,
                separatorBuilder: (context, index) {
                  return const SizedBox(
                    height: 25,
                  );
                },
                itemBuilder: (context, index) {
                  return Ink(
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondaryContainer,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(15),
                      ),
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(15),
                      ),
                      onTap: () async {
                        if (_contactsSelected.contains(_contacts![index])) {
                          _contactsSelected.remove(_contacts![index]);
                        } else {
                          _contactsSelected.add(_contacts![index]);
                        }
                        setState(() {});
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(25),
                        child: Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.primary,
                                shape: BoxShape.circle,
                              ),
                              padding: const EdgeInsets.all(12),
                              child: OwText(
                                '${_contacts![index].name?.substring(0, 1)}',
                                style: Theme.of(context)
                                    .textTheme
                                    .titleLarge
                                    ?.copyWith(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .onPrimary,
                                    ),
                              ),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  OwText(
                                    '${_contacts![index].name}',
                                    style:
                                        Theme.of(context).textTheme.titleLarge,
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  OwText(
                                    "${_contacts![index].phone?.length} ${_contacts![index].phone?.length == 1 ? "telefone" : "telefones"}",
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(width: 15),
                            Checkbox(
                              value:
                                  _contactsSelected.contains(_contacts![index]),
                              onChanged: (value) {
                                setState(() {
                                  if (value == true) {
                                    _contactsSelected.add(_contacts![index]);
                                  } else {
                                    _contactsSelected.remove(_contacts![index]);
                                  }
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        icon: const Icon(
          EvaIcons.saveOutline,
        ),
        label: const Text('Salvar'),
        onPressed: () async {
          context.pop(_contactsSelected);
        },
      ),
    );
  }

  void callApi() async {
    try {
      _contacts = await Api.contacts.listByCompany();
    } catch (_) {
      // erro
    }
    setState(() {});
  }
}
