import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class TasksHomePage extends StatefulWidget {
  const TasksHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  static String route = '/tasks';
  final bool automaticallyImplyLeading;
  @override
  State<TasksHomePage> createState() => _TasksHomePageState();
}

class _TasksHomePageState extends State<TasksHomePage> {
  List<TaskEntity>? _tasks;
  String? msgError;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  OwText(
                    'Tarefas',
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const OwText(
                    'Gerencie facilmente as tarefas da sua empresa e faça muito mais.',
                  ),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 25),
              child: OwDivider(),
            ),
            Visibility(
              visible: !Valid.text(msgError),
              replacement: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    OwText(
                      'Ocorreu um erro',
                      style: Theme.of(context).textTheme.titleMedium,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 16),
                    OwText(
                      '$msgError',
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 16),
                    OwButton(
                      labelText: 'Voltar',
                      onPressed: () {
                        context.pop();
                      },
                    ),
                    OwButton.elevated(
                      labelText: 'Tentar novamente',
                      onPressed: () {
                        setState(() {
                          msgError = null;
                          _tasks = null;
                        });
                        callApi();
                      },
                    ),
                  ],
                ),
              ),
              child: Visibility(
                visible: _tasks != null,
                replacement: const Center(child: CircularProgressIndicator()),
                child: Visibility(
                  visible: _tasks?.isNotEmpty ?? false,
                  replacement: Container(
                    padding: const EdgeInsets.fromLTRB(25, 0, 25, 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          'Você ainda não cadastrou nenhuma tarefa.',
                          style: Theme.of(context).textTheme.titleMedium,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  child: OwGrid.builder(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    shrinkWrap: true,
                    spacing: 15,
                    runSpacing: 15,
                    itemCount: _tasks?.length ?? 0,
                    numbersInRowAccordingToWidgth: const [
                      680,
                      1050,
                      1400,
                      2900
                    ],
                    itemBuilder: (context, index) {
                      return Ink(
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.secondaryContainer,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(15),
                          ),
                        ),
                        child: InkWell(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(15),
                          ),
                          onTap: () async {
                            final result = await context.push(
                              '/${WhiteLabelEntity.current?.id}/dashboard/tasks/tasks/${_tasks![index].id}/details',
                              extra: _tasks![index],
                            );
                            if (result is TaskEntity) {
                              _tasks![index] = result;
                              setState(() {});
                            } else if (result == true) {
                              _tasks!.removeAt(index);
                              setState(() {});
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(25),
                            child: OwText(
                              '${_tasks?[index].description}',
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: _tasks == null
          ? const SizedBox()
          : FloatingActionButton.extended(
              heroTag: null,
              label: const Text('Nova tarefa'),
              icon: const Icon(
                Icons.add,
              ),
              onPressed: () async {
                final TaskEntity? retorno = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/tasks/add',
                );
                if (retorno is TaskEntity) {
                  _tasks?.insert(0, retorno);
                  setState(() {});
                }
              },
            ),
    );
  }

  void callApi() async {
    try {
      _tasks = await Api.tasks.listByCompany(id: '${companySM.company?.id}');
    } catch (e) {
      msgError = '$e';
    }
    setState(() {});
  }
}
