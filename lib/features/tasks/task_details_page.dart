import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class TaskDetailsPage extends StatefulWidget {
  const TaskDetailsPage({Key? key, this.task}) : super(key: key);
  final TaskEntity? task;

  @override
  State<TaskDetailsPage> createState() => _TaskDetailsPageState();
}

class _TaskDetailsPageState extends State<TaskDetailsPage> {
  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  MaskedTextController startDate = MaskedTextController(
    mask: '00-00-0000',
    cursorBehavior: CursorBehaviour.unlocked,
  );
  MaskedTextController endDate = MaskedTextController(
    mask: '00-00-0000',
    cursorBehavior: CursorBehaviour.unlocked,
  );

  DateTime? startDateTime;
  DateTime? endDateTime;

  List<ContactEntity> _contacts = [];

  @override
  void initState() {
    super.initState();
    if (widget.task != null) {
      title.text = widget.task!.title;
      description.text = widget.task!.description ?? '';
      startDate.text = widget.task!.startDate;
      endDate.text = widget.task!.endDate ?? '';
      widget.task!.contacts?.map((e) {
        _contacts.add(e);
      }).toList();
    }
  }

  @override
  void dispose() {
    title.dispose();
    description.dispose();
    startDate.dispose();
    endDate.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        actions: [
          Visibility(
            visible: widget.task != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar esta tarefa?',
                  description:
                      'Essa ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton(
                      labelText: 'Sim',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                    OwButton(
                      labelText: 'Cancelar',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.tasks.delete(id: widget.task!.id!);
                    context.pop(true);
                  } catch (_) {
                    OwBotToast.toast('Erro ao remover tarefa.');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 25,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwText(
              'Detalhes da tarefa',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            const SizedBox(height: 25),
            OwTextField(
              controller: title,
              labelText: 'Título',
            ),
            const SizedBox(height: 25),
            OwTextField(
              controller: description,
              labelText: 'Descrição',
            ),
            const SizedBox(height: 25),
            OwTextField(
              controller: startDate,
              readOnly: true,
              labelText: 'Data inicial',
              hintText: 'DD-MM-AAAA',
              keyboardType: TextInputType.phone,
              textInputAction: TextInputAction.next,
              onTap: () async {
                startDateTime = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2023),
                  lastDate: DateTime(2099),
                );
                if (startDateTime != null) {
                  setState(() {
                    startDate.text = '${DateFormat("dd/MM/yyyy").format(startDateTime!)}';
                  });
                } else {
                  startDate.clear();
                }
              },
            ),
            const SizedBox(height: 25),
            OwTextField(
              controller: endDate,
              readOnly: true,
              labelText: 'Data final',
              hintText: 'DD-MM-AAAA',
              keyboardType: TextInputType.phone,
              textInputAction: TextInputAction.next,
              onTap: () async {
                endDateTime = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2023),
                  lastDate: DateTime(2099),
                );
                if (endDateTime != null) {
                  setState(() {
                    endDate.text = '${DateFormat("dd/MM/yyyy").format(endDateTime!)}';
                  });
                } else {
                  endDate.clear();
                }
              },
            ),
            const SizedBox(height: 25),
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primaryContainer,
                borderRadius: const BorderRadius.all(
                  Radius.circular(15),
                ),
              ),
              padding: const EdgeInsets.all(25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Contatos',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  const SizedBox(height: 15),
                  Text(
                    'Vincule contatos a sua tarefa.',
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                  const SizedBox(height: 15),
                  Text(
                    _contacts.length >= 1
                        ? '${_contacts.length} contatos adicionados'
                        : 'Nenhum contato adicionado a tarefa',
                    style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  const SizedBox(height: 15),
                  OwButton(
                    labelText: 'Vincular contato',
                    onPressed: () async {
                      final retorno = await context.push(
                        '/${WhiteLabelEntity.current?.id}/dashboard/tasks/contacts',
                        extra: _contacts,
                      );
                      if (retorno is List<ContactEntity>) {
                        _contacts = retorno;
                        setState(() {});
                      }
                    },
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            OwButton(
              expanded: true,
              margin: const EdgeInsets.all(25),
              labelText: 'Salvar',
              onPressed: () async {
                if (title.text.trim().isEmpty || startDate.text.trim().isEmpty) {
                  showOwDialog(
                    context: context,
                    title: 'Existem um ou mais campos que precisam ser preenchidos.',
                  );
                } else {
                  if (widget.task != null) {
                    final TaskEntity module = widget.task!.copyWith(
                      companyId: '${companySM.company?.id}',
                      contactId: _contacts.map((e) => '${e.id}').toList(),
                      contacts: _contacts,
                      description: description.text,
                      title: title.text,
                      startDate: startDateTime!.toUtc().toString(),
                      endDate: endDateTime!.toUtc().toString(),
                      status: 'open',
                    );
                    try {
                      OwBotToast.loading();
                      await Api.tasks.update(id: widget.task!.id!, data: module);
                      OwBotToast.close();
                      context.pop(module);
                      showOwDialog(
                        context: context,
                        title: 'Tarefa atualizada com sucesso.',
                      );
                    } catch (_) {
                      OwBotToast.close();
                      showOwDialog(
                        context: context,
                        title: '${_.toString()}',
                      );
                    }
                  } else {
                    try {
                      final TaskEntity module = TaskEntity(
                        companyId: '${companySM.company?.id}',
                        contactId: _contacts.map((e) => '${e.id}').toList(),
                        contacts: _contacts,
                        description: description.text,
                        title: title.text,
                        startDate: startDateTime!.toUtc().toString(),
                        endDate: endDateTime!.toUtc().toString(),
                        status: 'open',
                      );
                      print(module.toString());
                      OwBotToast.loading();
                      final response = await Api.tasks.insert(data: module);
                      context.pop(response);
                      OwBotToast.close();
                      showOwDialog(
                        context: context,
                        title: 'Tarefa adicionada com sucesso.',
                      );
                    } catch (_) {
                      OwBotToast.close();
                      showOwDialog(
                        context: context,
                        title: '${_.toString()}',
                      );
                    }
                  }
                }
              },
            ),
            SizedBox(height: BS.bottomPadding(context))
          ],
        ),
      ),
    );
  }
}
