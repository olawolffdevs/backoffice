import 'package:backoffice/settings/start_app.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class SelectWhiteLabelPage extends StatefulWidget {
  const SelectWhiteLabelPage({super.key});

  @override
  State<SelectWhiteLabelPage> createState() => _SelectWhiteLabelPageState();
}

class _SelectWhiteLabelPageState extends State<SelectWhiteLabelPage> {
  List<WhiteLabelEntity>? list;

  @override
  void initState() {
    super.initState();
    if (WhiteLabelEntity.current != null) {
      appSM.setWhiteLabel(backoffice);
    }
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        titleSpacing: 25,
        title: Image.asset(
          'assets/images/logo-backoffice.png',
          height: 45,
          color: Theme.of(context).colorScheme.onBackground,
        ),
        actions: [
          IconButton(
            icon: const Icon(EvaIcons.logOutOutline),
            onPressed: () {
              UserDB.remover();
              userSM.user = null;
              context.go('/login');
            },
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await callApi();
        },
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              OwText(
                'Olá, ${userSM.user?.name.split(" ")[0]}',
                style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
                padding: const EdgeInsets.fromLTRB(24, 24, 24, 10),
              ),
              Visibility(
                visible: (userSM.user?.email == 'danzi@pjmei.app' || userSM.user?.email == 'whitelabel@pjmei.app'),
                child: OwButton(
                  margin: const EdgeInsets.fromLTRB(25, 0, 25, 25),
                  labelText: 'Cadastrar novo White Label',
                  leading: Icons.add,
                  onPressed: () {
                    context.push('/white-label/add');
                  },
                ),
                replacement: const SizedBox(height: 14),
              ),
              const OwDivider(),
              Visibility(
                visible: list != null,
                replacement: Container(
                  constraints: const BoxConstraints(
                    minHeight: 300,
                  ),
                  child: const Center(child: CircularProgressIndicator()),
                ),
                child: Visibility(
                  visible: list?.isNotEmpty ?? false,
                  replacement: Container(
                    margin: appSM.isWeb
                        ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                        : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                    padding: const EdgeInsets.all(25),
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Theme.of(context).colorScheme.outlineVariant,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(15)),
                    ),
                    child: const Center(
                      child: Text(
                        'Você ainda não possui acesso a nenhuma plataforma. Entre em contato com o administrador do sistema.',
                      ),
                    ),
                  ),
                  child: OwGrid.builder(
                    itemCount: list?.length ?? 0,
                    runSpacing: 12,
                    spacing: 12,
                    padding: const EdgeInsets.all(24),
                    numbersInRowAccordingToWidgth: const [600, 1200],
                    itemBuilder: (context, index) {
                      final cc = ColorScheme.fromSeed(
                        seedColor: colorConvert(list![index].style.lightColorScheme),
                      );
                      return Ink(
                        decoration: BoxDecoration(
                          color: cc.primaryContainer,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(16)),
                        ),
                        child: InkWell(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(16)),
                          onTap: () async {
                            appSM.setSelectedIndex(0);
                            appSM.setWhiteLabel(list![index]);
                            companySM.company = CompanyEntity(
                              id: '${WhiteLabelEntity.current?.setting.companyId}',
                              fantasyName: 'fantasyName',
                              corporateName: 'corporateName',
                              documentNumber: 'documentNumber',
                              documentType: 'documentType',
                              phone: 'phone',
                              email: 'email',
                              plan: null,
                              whiteLabel: list![index].id,
                            );
                            WhiteLabelEntity.current = list![index];
                            try {
                              OwBotToast.loading();
                              ecommerceSM.establishment = await Api.ecommerce.find(
                                id: '${WhiteLabelEntity.current?.setting.ecommerceId}',
                              );
                              OwBotToast.close();
                            } catch (e) {
                              OwBotToast.close();
                            }
                            context.push('/${WhiteLabelEntity.current?.id}/dashboard');
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(25),
                            child: Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image.network(
                                    '${list![index].style.image.logoSplash}',
                                    color: cc.onPrimaryContainer,
                                    height: 160,
                                  ),
                                  Text(
                                    '${list![index].name}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium
                                        ?.copyWith(
                                          color: cc.onPrimaryContainer,
                                          fontWeight: FontWeight.bold,
                                        ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      list = await Api.whiteLabel.listByUser(id: '${userSM.user?.id}');
    } catch (e) {
      //
    }
    setState(() {});
  }
}
