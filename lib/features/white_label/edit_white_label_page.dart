import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class CrudWhiteLabelPage extends StatefulWidget {
  const CrudWhiteLabelPage({
    Key? key,
    this.whiteLabel,
  }) : super(key: key);
  final WhiteLabelEntity? whiteLabel;
  @override
  State<CrudWhiteLabelPage> createState() => _CrudWhiteLabelPageState();
}

class _CrudWhiteLabelPageState extends State<CrudWhiteLabelPage>
    with SingleTickerProviderStateMixin {
  WhiteLabelEntity? whiteLabel;
  late TabController _tabController;
  Color? screenPickerColor;

  // basic
  TextEditingController uuid = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController description = TextEditingController();

  // setting
  TextEditingController companyId = TextEditingController();
  TextEditingController ecommerceId = TextEditingController();
  TextEditingController urlGooglePlay = TextEditingController();
  TextEditingController urlAppleStore = TextEditingController();
  TextEditingController urlWebApplication = TextEditingController();
  TextEditingController redirectOrigin = TextEditingController();
  MaskedTextController limitCompany = MaskedTextController(mask: '00');
  MaskedTextController limitIndividual = MaskedTextController(mask: '00');
  bool enabledCompany = false;
  bool enabledIndividual = false;
  bool forceAuthenticationStart = false;
  bool disabledPrintScreen = false;

  bool planRequired = false;
  bool companies = false;
  bool financialEducation = false;
  bool groups = false;
  bool inbox = false;
  bool knowledgeBase = false;
  bool master = false;
  bool modules = false;
  bool ecommerce = false;
  bool notifications = false;
  bool plans = false;
  bool users = false;

  // design
  TextEditingController color = TextEditingController();
  TextEditingController logoMenuWeb = TextEditingController();
  TextEditingController logoMenuMobile = TextEditingController();
  TextEditingController logoSplash = TextEditingController();
  TextEditingController favicon = TextEditingController();
  TextEditingController icon = TextEditingController();

  // links
  TextEditingController privacyPolicy = TextEditingController();
  TextEditingController termsOfUse = TextEditingController();
  TextEditingController lgpd = TextEditingController();
  TextEditingController website = TextEditingController();

  @override
  void initState() {
    super.initState();
    whiteLabel = widget.whiteLabel;
    _tabController = TabController(length: 6, vsync: this);
    if (widget.whiteLabel != null) {
      uuid.text = whiteLabel!.id;
      name.text = whiteLabel!.name;
      description.text = whiteLabel!.description;
      companyId.text = whiteLabel!.setting.companyId;
      ecommerceId.text = whiteLabel!.setting.ecommerceId;
      urlAppleStore.text = whiteLabel!.setting.linkAppleStore ?? '';
      urlGooglePlay.text = whiteLabel!.setting.linkGooglePlay ?? '';
      urlWebApplication.text = whiteLabel!.setting.urlWebApplication;
      redirectOrigin.text = whiteLabel!.setting.redirectOrigin;
      limitCompany.text = whiteLabel!.setting.environment.company.limit.toString();
      limitIndividual.text = whiteLabel!.setting.environment.individual.limit.toString();
      enabledCompany = whiteLabel!.setting.environment.company.enabled;
      enabledIndividual = whiteLabel!.setting.environment.individual.enabled;
      forceAuthenticationStart = whiteLabel!.setting.forceAuthenticationStart;
      disabledPrintScreen = whiteLabel!.setting.disabledPrintScreen;
      logoMenuWeb.text = whiteLabel!.style.image.logoMenuWeb;
      logoMenuMobile.text = whiteLabel!.style.image.logoMenuMobile;
      logoSplash.text = whiteLabel!.style.image.logoSplash;
      favicon.text = whiteLabel!.style.image.favicon;
      icon.text = whiteLabel!.style.image.icon;
      privacyPolicy.text = whiteLabel!.setting.link.privacyPolicy;
      termsOfUse.text = whiteLabel!.setting.link.termsOfUse;
      lgpd.text = whiteLabel!.setting.link.lgpd;
      website.text = whiteLabel!.setting.link.website;

      companies = whiteLabel!.setting.functionality.companies;
      financialEducation = whiteLabel!.setting.functionality.financialEducation;
      groups = whiteLabel!.setting.functionality.groups;
      inbox = whiteLabel!.setting.functionality.inbox;
      knowledgeBase = whiteLabel!.setting.functionality.knowledgeBase;
      master = whiteLabel!.setting.functionality.master;
      modules = whiteLabel!.setting.functionality.modules;
      planRequired = whiteLabel!.setting.planRequired;
      notifications = whiteLabel!.setting.functionality.notifications;
      plans = whiteLabel!.setting.functionality.plans;
      users = whiteLabel!.setting.functionality.users;

      color.text = whiteLabel!.style.lightColorScheme;

      screenPickerColor = colorConvert(whiteLabel!.style.lightColorScheme);
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: OwAppBar(
          title: const Text('Detalhes do White Label'),
          elevation: 0.5,
          preferredSize: const Size.fromHeight(
            kToolbarHeight + kTextTabBarHeight,
          ),
          bottom: TabBar(
            isScrollable: true,
            indicatorColor: Theme.of(context).colorScheme.primary,
            controller: _tabController,
            tabs: const [
              Tab(
                text: 'Básico',
              ),
              Tab(
                text: 'Configurações',
              ),
              Tab(
                text: 'Design',
              ),
              Tab(
                text: 'Links',
              ),
              Tab(
                text: 'Funcionalidades',
              ),
              Tab(
                text: 'Validações',
              ),
            ],
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          physics: const BouncingScrollPhysics(),
          children: [
            _basic(),
            _settings(),
            _design(),
            _links(),
            _functionalities(),
            const Center(child: Text('Em breve')),
          ],
        ),
        bottomNavigationBar: OwBottomAppBar(
          child: OwButton(
            margin: const EdgeInsets.all(25),
            labelText: 'Salvar',
            onPressed: _onPressed,
          ),
        ),
      ),
    );
  }

  Widget _basic() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              labelText: 'Nome da plataforma',
              controller: name,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Descrição da plataforma',
              controller: description,
            ),
            const SizedBox(height: 25),
            Row(
              children: [
                Expanded(
                  child: Text(
                    'Plano é obrigatório?',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ),
                Switch(
                  value: planRequired,
                  onChanged: (val) {
                    setState(() {
                      planRequired = val;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _settings() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              labelText:
                  'ID da empresa ${widget.whiteLabel != null ? '(somente leitura)' : ''}',
              enabled: widget.whiteLabel == null,
              controller: companyId,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText:
                  'ID da loja virtual ${widget.whiteLabel != null ? '(somente leitura)' : ''}',
              enabled: widget.whiteLabel == null,
              controller: ecommerceId,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Link da Google Play',
              controller: urlGooglePlay,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Link da Apple Store',
              controller: urlAppleStore,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText:
                  'Endereço da aplicação WEB ${widget.whiteLabel != null ? '(somente leitura)' : ''}',
              enabled: widget.whiteLabel == null,
              controller: urlWebApplication,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Endereço de redirecionamento',
              controller: redirectOrigin,
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Ambiente PJ',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: enabledCompany,
                        onChanged: (val) {
                          setState(() {
                            enabledCompany = val;
                          });
                        },
                      ),
                    ],
                  ),
                  Visibility(
                    visible: enabledCompany,
                    child: OwTextField(
                      labelText: 'Limite de PJ por usuário',
                      controller: limitCompany,
                      margin: const EdgeInsets.only(top: 2, bottom: 20),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Ambiente PF',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: enabledIndividual,
                        onChanged: (val) {
                          setState(() {
                            enabledIndividual = val;
                          });
                        },
                      ),
                    ],
                  ),
                  Visibility(
                    visible: enabledIndividual,
                    child: OwTextField(
                      labelText: 'Limite de PF por usuário',
                      controller: limitIndividual,
                      margin: const EdgeInsets.only(top: 2, bottom: 20),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Desabilitar print da tela',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: disabledPrintScreen,
                        onChanged: (val) {
                          setState(() {
                            disabledPrintScreen = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Forçar uso de biometria',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: forceAuthenticationStart,
                        onChanged: (val) {
                          setState(() {
                            forceAuthenticationStart = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _design() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ColorPicker(
              color: screenPickerColor ?? Colors.green,
              onColorChanged: (Color color) =>
                  setState(() => screenPickerColor = color),
              width: 50,
              height: 50,
              borderRadius: 50,
              pickersEnabled: const {
                ColorPickerType.both: true,
                ColorPickerType.primary: false,
                ColorPickerType.accent: false
              },
              padding: const EdgeInsets.all(25),
              heading: Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: OwText(
                  'Selecione uma cor que representará sua página',
                  style: Theme.of(context).textTheme.headlineSmall,
                ),
              ),
              subheading: OwText(
                'Selecione a variação da cor',
                style: Theme.of(context).textTheme.titleSmall,
              ),
            ),
            OwTextField(
              labelText: 'Logo do menu (celular)',
              controller: logoMenuMobile,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Logo do menu (web)',
              controller: logoMenuWeb,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Logo splash',
              controller: logoSplash,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Favicon',
              controller: favicon,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Icon',
              controller: icon,
            ),
          ],
        ),
      ),
    );
  }

  Widget _links() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              labelText: 'Termos de uso',
              controller: termsOfUse,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Política de privacidade',
              controller: privacyPolicy,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'LGPD',
              controller: lgpd,
            ),
            const SizedBox(height: 25),
            OwTextField(
              labelText: 'Website',
              controller: website,
            ),
          ],
        ),
      ),
    );
  }

  Widget _functionalities() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Empresas',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: companies,
                        onChanged: (val) {
                          setState(() {
                            companies = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Usuários',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: users,
                        onChanged: (val) {
                          setState(() {
                            users = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Educação financeira',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: financialEducation,
                        onChanged: (val) {
                          setState(() {
                            financialEducation = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Grupos',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: groups,
                        onChanged: (val) {
                          setState(() {
                            groups = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Inbox',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: inbox,
                        onChanged: (val) {
                          setState(() {
                            inbox = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Base de conhecimento',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: knowledgeBase,
                        onChanged: (val) {
                          setState(() {
                            knowledgeBase = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Master',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: master,
                        onChanged: (val) {
                          setState(() {
                            master = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Módulos',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: modules,
                        onChanged: (val) {
                          setState(() {
                            modules = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Notificações',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: notifications,
                        onChanged: (val) {
                          setState(() {
                            notifications = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 8),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                color: Theme.of(context).colorScheme.secondaryContainer,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Planos',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ),
                      Switch(
                        value: plans,
                        onChanged: (val) {
                          setState(() {
                            plans = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onPressed() async {
    if (name.text.isEmpty) {
      // e
    } else {
      whiteLabel = WhiteLabelEntity(
        id: uuid.text,
        name: name.text,
        description: description.text,
        setting: SettingWhiteLabelEntity(
          companyId: companyId.text,
          ecommerceId: ecommerceId.text,
          disabledPrintScreen: disabledPrintScreen,
          forceAuthenticationStart: forceAuthenticationStart,
          linkAppleStore: urlAppleStore.text,
          linkGooglePlay: urlGooglePlay.text,
          redirectOrigin: redirectOrigin.text,
          urlWebApplication: urlWebApplication.text,
          link: LinkWhiteLabelEntity(
            lgpd: lgpd.text,
            privacyPolicy: privacyPolicy.text,
            termsOfUse: termsOfUse.text,
            website: website.text,
          ),
          environment: EnvironmentWhiteLabelEntity(
            company: ItemEnvironmentWhiteLabelEntity(
                enabled: enabledCompany,
                limit:
                    enabledCompany ? int.tryParse(limitCompany.text) ?? 1 : 0),
            individual: ItemEnvironmentWhiteLabelEntity(
                enabled: enabledIndividual,
                limit: enabledIndividual
                    ? int.tryParse(limitIndividual.text) ?? 1
                    : 0),
          ),
          functionality: FunctionalityWhiteLabelEntity(
            companies: companies,
            financialEducation: financialEducation,
            groups: groups,
            inbox: inbox,
            knowledgeBase: knowledgeBase,
            master: master,
            modules: modules,
            notifications: notifications,
            ecommerce: ecommerce,
            plans: plans,
            users: users,
          ),
          planRequired: planRequired,
        ),
        style: StyleWhiteLabelEntity(
          darkColorScheme: screenPickerColor!.toHex(),
          lightColorScheme: screenPickerColor!.toHex(),
          image: ImageWhiteLabelEntity(
            favicon: favicon.text,
            icon: icon.text,
            logoMenuMobile: logoMenuMobile.text,
            logoMenuWeb: logoMenuWeb.text,
            logoSplash: logoSplash.text,
          ),
        ),
      );
      if (widget.whiteLabel != null) {
        try {
          OwBotToast.loading();
          await Api.whiteLabel.update(
            id: widget.whiteLabel!.id,
            data: whiteLabel!,
          );
          OwBotToast.close();
          if (mounted) {
            context.go('/home');
          }
        } catch (e) {
          OwBotToast.close();
          if (mounted) {
            showOwDialog(
              context: context,
              title: 'Ocorreu um erro',
              description: '$e',
            );
          }
        }
      } else {
        try {
          OwBotToast.loading();
          await Api.whiteLabel.insert(
            data: whiteLabel!,
          );
          OwBotToast.close();
          if (mounted) {
            context.go('/home');
          }
        } catch (e) {
          OwBotToast.close();
          if (mounted) {
            showOwDialog(
              context: context,
              title: 'Ocorreu um erro',
              description: '$e',
            );
          }
        }
      }
    }
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
