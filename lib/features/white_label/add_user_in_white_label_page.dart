import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class AddUserInWhiteLabelPage extends StatefulWidget {
  const AddUserInWhiteLabelPage({super.key, this.selectUsers = false});
  final bool selectUsers;

  @override
  State<AddUserInWhiteLabelPage> createState() =>
      _AddUserInWhiteLabelPageState();
}

class _AddUserInWhiteLabelPageState extends State<AddUserInWhiteLabelPage> {
  List<UserEntity>? users;
  List<UserEntity> selecteds = [];

  final search = TextEditingController();

  @override
  void dispose() {
    search.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    // final temp = users?.where((i) => i.name.toLowerCase())).toList();
    final temp = users
        ?.where((i) => i.name.toLowerCase().contains(
            OwFormat.removeAccentAndPonctuation(search.text.toString(),
                    removedPonctuation: ',.!?;:()[]{}}-/')
                .toLowerCase()))
        .toList();
    return Scaffold(
      appBar: OwAppBar(
        title: Text(
          widget.selectUsers
              ? 'Selecionar usuários'
              : 'Adicionar colaboradores',
        ),
      ),
      body: Visibility(
        visible: users == null,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
        replacement: Column(
          children: [
            OwTextField(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              hintText: 'Buscar',
              controller: search,
              suffixIcon: IconButton(
                onPressed: () {
                  if (Valid.text(search.text)) {
                    search.clear();
                    setState(() {});
                  }
                },
                icon: const Icon(EvaIcons.closeCircleOutline),
              ),
              onFieldSubmitted: (str) {
                setState(() {});
              },
            ),
            Expanded(
              child: ListView.separated(
                itemCount: temp?.length ?? 0,
                separatorBuilder: (context, index) {
                  return const OwDivider();
                },
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(
                      '${temp![index].name}',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    subtitle: Text(
                      '${temp[index].email}',
                    ),
                    trailing: Checkbox(
                      value: selecteds.contains(temp[index]),
                      onChanged: (bool? value) {
                        if (value == true) {
                          selecteds.add(temp[index]);
                        } else {
                          selecteds.remove(temp[index]);
                        }
                        setState(() {});
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          labelText: widget.selectUsers ? 'Concluir' : 'Adicionar pessoas',
          onPressed: () async {
            if (widget.selectUsers) {
              context.pop(selecteds.map((e) => e.id).toList());
            } else {
              try {
                OwBotToast.loading();
                await Api.whiteLabel.addUser(
                  id: '${WhiteLabelEntity.current?.id}',
                  data: selecteds.map((e) => e.id).toList(),
                );
                OwBotToast.close();
                context.pop();
                OwBotToast.toast('Cadastrados com sucesso');
              } catch (e) {
                OwBotToast.close();
                showOwDialog(
                  context: context,
                  title: 'Aconteceu um erro',
                  description: '$e',
                );
              }
            }
          },
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      users = await Api.user.listAll();
    } catch (e) {
      //
    }
    setState(() {});
  }
}
