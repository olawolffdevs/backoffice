import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class WhiteLabelHomePage extends StatefulWidget {
  const WhiteLabelHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<WhiteLabelHomePage> createState() => _WhiteLabelHomePageState();
}

class _WhiteLabelHomePageState extends State<WhiteLabelHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: Text('${WhiteLabelEntity.current?.name}'),
        actions: [
          IconButton(
            icon: const Icon(EvaIcons.edit2Outline),
            onPressed: () {
              context.push(
                '/${WhiteLabelEntity.current?.id}/dashboard/white-label/edit',
                extra: WhiteLabelEntity.current!,
              );
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 15),
            Center(
              child: Container(
                padding: const EdgeInsets.all(25),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: Theme.of(context).colorScheme.outline,
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
                child: Image.network(
                  '${WhiteLabelEntity.current?.style.image.logoMenuWeb}',
                  fit: BoxFit.fitHeight,
                  color: Theme.of(context).colorScheme.primary,
                  height: 45,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: appSM.isWeb ? 50 : 25,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(height: 25),
                  Text(
                    '${WhiteLabelEntity.current?.description}',
                    style: Theme.of(context).textTheme.bodyMedium,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            const OwDivider(),
            OwText(
              'Cores da plataforma',
              style: Theme.of(context).textTheme.titleLarge,
              padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
            ),
            OwGrid.builder(
              padding: const EdgeInsets.fromLTRB(25, 15, 25, 25),
              numbersInRowAccordingToWidgth: [
                170,
                220,
                270,
                320,
                370,
                420,
                470,
                520,
                570,
                620
              ],
              itemCount: styleWidget.length,
              itemBuilder: (context, index) {
                return CardAtalhos(
                  onPressed: () {},
                  selecionado: true,
                  labelColor: Colors.white,
                  border: BorderSide(
                    width: 1,
                    color: Theme.of(context).colorScheme.onBackground,
                  ),
                  selectedColor: returnColor(styleWidget[index], context),
                );
              },
            ),
            const OwDivider(),
            Container(
              margin: appSM.isWeb
                  ? const EdgeInsets.fromLTRB(50, 50, 50, 0)
                  : const EdgeInsets.fromLTRB(25, 25, 25, 0),
              padding: EdgeInsets.all(appSM.isWeb ? 35 : 20),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.outlineVariant,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Informações da plataforma',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(height: 15),
                  Text(
                    'Gestão PJ habilitada: ${(WhiteLabelEntity.current!.setting.environment.company.enabled) ? 'Sim' : 'Não'}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 6),
                  Text(
                    'Limite de PJ por usuário: ${WhiteLabelEntity.current?.setting.environment.company.limit}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 25),
                  Text(
                    'Gestão PF habilitada: ${(WhiteLabelEntity.current!.setting.environment.individual.enabled) ? 'Sim' : 'Não'}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 6),
                  Text(
                    'Limite de PF por usuário: ${WhiteLabelEntity.current?.setting.environment.individual.limit}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 25),
                  Text(
                    'Plano obrigatório: ${(WhiteLabelEntity.current!.setting.planRequired) ? 'Sim' : 'Não'}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                ],
              ),
            ),
            Container(
              margin: appSM.isWeb
                  ? const EdgeInsets.fromLTRB(50, 50, 50, 0)
                  : const EdgeInsets.fromLTRB(25, 25, 25, 0),
              padding: EdgeInsets.all(appSM.isWeb ? 35 : 20),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.outlineVariant,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Links relacionados',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(height: 15),
                  Text(
                    'Link do site: ${WhiteLabelEntity.current?.setting.link.website}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'Link do portal: ${WhiteLabelEntity.current?.setting.urlWebApplication}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'Política de privacidade: ${WhiteLabelEntity.current?.setting.link.privacyPolicy}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'Termos de uso: ${WhiteLabelEntity.current?.setting.link.termsOfUse}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'LGPD: ${WhiteLabelEntity.current?.setting.link.lgpd}',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 80),
          ],
        ),
      ),
    );
  }

  List<String> styleWidget = [
    'background',
    'primary',
    'primaryContainer',
    'secondary',
    'secondaryContainer',
    'tertiary',
    'tertiaryContainer',
    'error',
    'errorContainer',
    'surfaceVariant',
    'inversePrimary',
  ];

  Color returnColor(String color, BuildContext context) {
    switch (color) {
      case 'background':
        return Theme.of(context).colorScheme.background;
      case 'primary':
        return Theme.of(context).colorScheme.primary;
      case 'primaryContainer':
        return Theme.of(context).colorScheme.primaryContainer;
      case 'secondary':
        return Theme.of(context).colorScheme.secondary;
      case 'secondaryContainer':
        return Theme.of(context).colorScheme.secondaryContainer;
      case 'tertiary':
        return Theme.of(context).colorScheme.tertiary;
      case 'tertiaryContainer':
        return Theme.of(context).colorScheme.tertiaryContainer;
      case 'error':
        return Theme.of(context).colorScheme.error;
      case 'errorContainer':
        return Theme.of(context).colorScheme.errorContainer;
      case 'surfaceVariant':
        return Theme.of(context).colorScheme.surfaceVariant;
      case 'inversePrimary':
        return Theme.of(context).colorScheme.inversePrimary;
      default:
        return Theme.of(context).colorScheme.background;
    }
  }
}
