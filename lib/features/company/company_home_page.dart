import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class CompanyHomePage extends StatefulWidget {
  const CompanyHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  CompanyHomePageState createState() => CompanyHomePageState();
}

class CompanyHomePageState extends State<CompanyHomePage> with AutomaticKeepAliveClientMixin<CompanyHomePage> {
  @override
  bool get wantKeepAlive => true;

  TextEditingController search = TextEditingController();

  List<CompanyEntity>? companies;
  int count = 0;
  int? sortColumnIndex;
  bool isAscending = false;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const OwText('Empresas'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Visibility(
              visible: companies != null,
              replacement: const Center(
                child: CircularProgressIndicator(),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(right: 24, top: 12),
                        constraints: const BoxConstraints(
                          maxWidth: 290,
                        ),
                        child: OwTextField(
                          widthBorder: 2,
                          decoration: InputDecoration(
                            border: const OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(16)),
                            ),
                            fillColor: Theme.of(context).colorScheme.primaryContainer,
                            hintText: 'Buscar...',
                            suffixIcon: IconButton(
                              icon: const Icon(EvaIcons.searchOutline),
                              onPressed: () {
                                setState(() {});
                              },
                            )
                          ),
                          outlined: true,
                          controller: search,
                          onFieldSubmitted: (value) {
                            setState(() {});
                          },
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.all(24),
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.surface,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(16),
                      ),
                      border: Border.all(
                        width: 1,
                        color: Theme.of(context).colorScheme.surfaceVariant,
                      ),
                    ),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(16),
                      ),
                      child: DataTable(
                        border: const TableBorder(
                          borderRadius: const BorderRadius.all(Radius.circular(16)),
                        ),
                        headingRowColor: MaterialStatePropertyAll(
                          Theme.of(context).colorScheme.primaryContainer,
                        ),
                        sortAscending: isAscending,
                        sortColumnIndex: sortColumnIndex,
                        columns: getColumns(['Nome', 'CNPJ', 'Entrou em']),
                        rows: getRows(Valid.text(search.text) ? searchData() : companies),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Visibility(
              visible: count > (companies?.length ?? 0),
              child: OwButton(
                margin: const EdgeInsets.all(25),
                labelText: 'Carregar mais',
                onPressed: () async {
                  callApi(companies?.length);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void callApi([skip = 0]) async {
    try {
      final ListCompanyResult result = await Api.company.listAll(params: {'skip': skip});
      if (result.count > 0) {
        count = result.count;
      }
      final List<CompanyEntity> newResults = [];
      for (var element in result.company) {
        newResults.add(element);
      }
      companies = F.concatenateList(companies ?? <CompanyEntity>[], newResults) as List<CompanyEntity>;
    } catch (error) {
      OwBotToast.toast(error.toString());
    }
    setState(() {});
  }


  List<DataColumn> getColumns(List<String> columns) => columns.map((column) {
    return DataColumn(
      label: Text(column),
      onSort: onSort, 
    );
  }).toList();

  List<DataRow> getRows(List<CompanyEntity>? companies) => companies?.map((CompanyEntity company) {
    final cells = [
      company.fantasyName,
      OwFormat.cpfCnpj(company.documentNumber),
      DateFormat('dd/MM/yyyy - HH:mm').format(DateTime.parse(company.createdAt.toString()).toLocal())
    ];
    return DataRow(
      cells: getCells(cells, company),
    );
  }).toList() ?? <DataRow>[];

  List<DataCell> getCells(List<dynamic> cells, CompanyEntity company) => cells.map((cell) {
    return DataCell(
      Text(cell),
      onTap: () async {
        final result = await context.push(
          '/${WhiteLabelEntity.current?.id}/dashboard/company/${company.id}',
          extra: company,
        );
        if (result is CompanyEntity) {
          company = result;
          setState(() {});
        }
      }
    );
  }).toList();

  void onSort(int columnIndex, bool ascending) {

    if(columnIndex == 0) {
      companies!.sort((user1, user2) => compareString(ascending, user1.fantasyName, user2.fantasyName));
    } else if (columnIndex == 1) {
      companies!.sort((user1, user2) => compareString(ascending, user1.documentNumber, user2.documentNumber));
    } else {
      companies!.sort((user1, user2) => compareString(ascending, user1.createdAt!, user2.createdAt!));
    }


    setState(() {
      this.sortColumnIndex = columnIndex;
      this.isAscending = ascending;
    });
  }

  int compareString(bool ascending, String value1, String value2) {
    return ascending ? value1.compareTo(value2) : value2.compareTo(value1);
  }

  List<CompanyEntity> searchData() {

    final s = normalizarTexto(search.text);

    return companies!.where((usuario) {
      final n = normalizarTexto(usuario.fantasyName);
      final e = normalizarTexto(usuario.email);
      final d = normalizarTexto(usuario.documentNumber);

      return n.contains(s) || e.contains(s) || d.contains(s);
    }).toList();
  }

  String normalizarTexto(String texto) {
    final semAcentos = texto.toLowerCase().replaceAll(RegExp(r'[áàãâä]'), 'a')
        .replaceAll(RegExp(r'[éèêë]'), 'e')
        .replaceAll(RegExp(r'[íìîï]'), 'i')
        .replaceAll(RegExp(r'[óòõôö]'), 'o')
        .replaceAll(RegExp(r'[úùûü]'), 'u')
        .replaceAll(RegExp(r'[ç]'), 'c')
        .replaceAll(RegExp(r'[^a-z0-9]'), ''); // Remove pontuações e mantém apenas letras e números
    return semAcentos;
  }
}