import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CompanyCrudPage extends StatefulWidget {
  const CompanyCrudPage({Key? key, required this.company}) : super(key: key);
  final CompanyEntity company;
  @override
  CompanyCrudPageState createState() => CompanyCrudPageState();
}

class CompanyCrudPageState extends State<CompanyCrudPage> {
  MaskedTextController document = MaskedTextController(
    mask: '00.000.000/0000-00',
    text: '',
  );
  TextEditingController corporateName = TextEditingController();
  TextEditingController fantasyName = TextEditingController();
  MaskedTextController phone = MaskedTextController(
    mask: '(00) 00000 0000',
    cursorBehavior: CursorBehaviour.unlocked,
  );
  TextEditingController email = TextEditingController();

  String? documentError;
  String? corporateNameError;
  String? fantasyNameError;
  String? phoneError;
  String? emailError;

  @override
  void initState() {
    super.initState();
    document.text = widget.company.documentNumber;
    corporateName.text = widget.company.corporateName;
    fantasyName.text = widget.company.fantasyName;
    phone.text = widget.company.phone;
    email.text = widget.company.email;
  }

  @override
  void dispose() {
    document.dispose();
    corporateName.dispose();
    fantasyName.dispose();
    phone.dispose();
    email.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Dados da empresa'),
      ),
      body: Center(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(24),
              child: Container(
                constraints: const BoxConstraints(
                  maxWidth: 420,
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.secondaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(16)),
                  border: Border.all(
                    width: 1,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const SizedBox(height: 45),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 120,
                          height: 120,
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.primaryContainer,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(40),
                            ),
                            border: Border.all(
                              width: 1,
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          child: const Center(
                            child: Icon(
                              Icons.storefront_outlined,
                              size: 60,
                            ),
                          ),
                        ),
                      ],
                    ),
                    OwText(
                      '${widget.company.fantasyName}',
                      style: Theme.of(context).textTheme.headlineMedium,
                      padding: const EdgeInsets.fromLTRB(25, 25, 25, 10),
                      textAlign: TextAlign.center,
                    ),
                    OwText(
                      '${OwFormat.cpfCnpj(widget.company.documentNumber)}',
                      style: Theme.of(context).textTheme.bodyMedium,
                      padding: const EdgeInsets.fromLTRB(25, 0, 25, 30),
                      textAlign: TextAlign.center,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FloatingActionButton(
                          heroTag: null,
                          child: const Icon(EvaIcons.phoneOutline),
                          onPressed: () async {
                            launchUrl(Uri.parse('tel:${widget.company.phone}'));
                          },
                        ),
                        const SizedBox(width: 12),
                        FloatingActionButton(
                          heroTag: null,
                          child: const Icon(EvaIcons.emailOutline),
                          onPressed: () async {
                            launchUrl(Uri.parse('mailto:${widget.company.email}'));
                          },
                        ),
                        const SizedBox(width: 12),
                        FloatingActionButton(
                          heroTag: null,
                          child: const Icon(EvaIcons.fileTextOutline),
                          onPressed: () async {
                            context.push('/${WhiteLabelEntity.current?.id}/dashboard/empresa/${widget.company.id}/documents');
                          },
                        ),
                      ],
                    ),
                    const SizedBox(height: 35),
                    OwTextField(
                      color: Colors.transparent,
                      controller: corporateName,
                      errorText: corporateNameError,
                      margin: const EdgeInsets.fromLTRB(
                        25,
                        20,
                        25,
                        0,
                      ),
                      labelText: 'Razão Social',
                      enabled: false,
                      textCapitalization: TextCapitalization.sentences,
                      hintText: 'Nome da empresa LTDA',
                      onChanged: (_) {
                        if (corporateNameError != null) {
                          setState(() {
                            corporateNameError = null;
                          });
                        }
                      },
                    ),
                    OwTextField(
                      color: Colors.transparent,
                      controller: phone,
                      errorText: phoneError,
                      margin: const EdgeInsets.fromLTRB(
                        25,
                        20,
                        25,
                        0,
                      ),
                      labelText: 'Telefone da empresa',
                      hintText: '(00) 00000 0000',
                      enabled: false,
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      onChanged: (_) {
                        if (phone.text.length <= 13) {
                          phone.updateMask('(00) 0000 00000');
                        } else {
                          phone.updateMask('(00) 00000 0000');
                        }
                        if (phoneError != null) {
                          setState(() {
                            phoneError = null;
                          });
                        }
                      },
                    ),
                    OwTextField(
                      color: Colors.transparent,
                      controller: email,
                      errorText: emailError,
                      margin: const EdgeInsets.fromLTRB(
                        25,
                        20,
                        25,
                        0,
                      ),
                      labelText: 'E-mail da empresa',
                      hintText: 'email@empresa.com.br',
                      enabled: false,
                      keyboardType: TextInputType.emailAddress,
                      textCapitalization: TextCapitalization.none,
                      textInputAction: TextInputAction.done,
                      onChanged: (_) {
                        if (emailError != null) {
                          setState(() {
                            emailError = null;
                          });
                        }
                      },
                    ),
                    const SizedBox(height: 35),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
