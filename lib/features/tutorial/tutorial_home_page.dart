import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class TutorialHomePage extends StatefulWidget {
  const TutorialHomePage({super.key});

  @override
  State<TutorialHomePage> createState() => _TutorialHomePageState();
}

class _TutorialHomePageState extends State<TutorialHomePage> {

  List<TutorialEntity>? tutorials;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.all(24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Backoffice Tutorial',
                    style: Theme.of(context).textTheme.headlineLarge?.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'Este kit foi preparado para você por PJMEI. Aproveite para conhecer nossas funcionalidades e como gerenciar sua plataforma da melhor forma possível.',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                ],
              ),
            ),
            OwGrid.builder(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 25),
              itemBuilder: (context, index) {
                return Ink(
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.secondaryContainer,
                    borderRadius: const BorderRadius.all(Radius.circular(16)),
                  ),
                  child: InkWell(
                    onTap: () async {
                      context.push(
                        '/${WhiteLabelEntity.current?.id}/dashboard/tutorial/item',
                        extra: tutorials?[index],
                      );
                    },
                    borderRadius: const BorderRadius.all(Radius.circular(16)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 20,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          AspectRatio(
                            aspectRatio: 16 / 9,
                            child: ClipRRect(
                              borderRadius: const BorderRadius.all(Radius.circular(16)),
                              child: Image.network(
                                '${tutorials?[index].cover}',
                                width: MediaQuery.of(context).size.width,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          const SizedBox(height: 15),
                          Text(
                            '${tutorials?[index].title}',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: Theme.of(context).colorScheme.onSecondaryContainer,
                            ),
                          ),
                          const SizedBox(
                            height: 6,
                          ),
                          Text(
                            '${tutorials?[index].description}',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          const SizedBox(height: 10),
                          IgnorePointer(
                            child: CardAtalhos(
                              title: getData('${tutorials?[index].type}').$1,
                              icon: getData('${tutorials?[index].type}').$2,
                              onPressed: () {},
                              selecionado: true,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: tutorials?.length ?? 0,
              numbersInRowAccordingToWidgth: const [400, 850, 1200, 1600],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      tutorials = await Api.backofficeTutorial.list();
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: 'Tente novamente mais tarde.',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }

  (String, IconData) getData(String value) {
    if(value == 'pdf') return ('Ver PDF', EvaIcons.fileTextOutline);
    if(value == 'video') return ('Assistir', EvaIcons.filmOutline);
    return ('Saiba mais', EvaIcons.link2Outline);
  }
}