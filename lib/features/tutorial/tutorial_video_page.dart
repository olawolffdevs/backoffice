import 'dart:async';
import 'package:chewie/chewie.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class TutorialVideoDetail extends StatefulWidget {
  const TutorialVideoDetail({required this.tutorial, Key? key}) : super(key: key);
  final TutorialEntity tutorial;

  @override
  _TutorialVideoDetailState createState() => _TutorialVideoDetailState();
}

class _TutorialVideoDetailState extends State<TutorialVideoDetail> {

  late VideoPlayerController _videoPlayerController1;
  ChewieController? _chewieController;

  @override
  void initState() {
    super.initState();
    initializePlayer();
  }

  @override
  void dispose() {
    try {
      _videoPlayerController1.dispose();
      _chewieController?.dispose();
    } catch (e) {
      // e
    }
    super.dispose();
  }

  Future<void> initializePlayer() async {
    try {
      _videoPlayerController1 = VideoPlayerController.networkUrl(
        Uri.parse(widget.tutorial.url),
      );
      await _videoPlayerController1.initialize();
    } catch (e) {
      // e
    }
    _createChewieController();
    setState(() {});
  }

  void _createChewieController() {
    try {
      _chewieController = ChewieController(
        videoPlayerController: _videoPlayerController1,
        autoPlay: false,
        looping: false,
      );
    } catch (e) {
      // e
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const OwText('Tutorial'),
        actions: [
          IconButton(
            icon: const Icon(EvaIcons.externalLinkOutline),
            onPressed: () {
              launchUrl(
                Uri.parse(widget.tutorial.url),
                mode: LaunchMode.externalApplication,
              );
            },
          ),
        ],
      ),
      body: Visibility(
        visible: MediaQuery.sizeOf(context).width > 760,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 6,
              child: _video(),
            ),
            Expanded(
              flex: 4,
              child: _text(context),
            ),
          ],
        ),
        replacement: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            children: [
              _video(),
              _text(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _text(context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.all(25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          OwText(
            '${widget.tutorial.title}',
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          const SizedBox(
            height: 25,
          ),
          OwText(
            '${widget.tutorial.description}',
          ),
        ],
      ),
    );
  }

  Widget _video() {
    return Padding(
      padding: const EdgeInsets.all(25),
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: Container(
          constraints: const BoxConstraints(
            maxWidth: 600,
            maxHeight: 1000,
            minHeight: 230,
          ),
          height: MediaQuery.sizeOf(context).height * 0.7,
          child: _chewieController != null && _chewieController!.videoPlayerController.value.isInitialized
            ? ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                child: Chewie(
                  controller: _chewieController!,
                ),
              )
            : const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 20),
                  OwText('Carregando'),
                ],
              ),
        ),
      ),
    );
  }
}
