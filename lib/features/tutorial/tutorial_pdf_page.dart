import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:pdfx/pdfx.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:internet_file/internet_file.dart';
import 'package:url_launcher/url_launcher.dart';

class TutorialPdfPage extends StatefulWidget {
  const TutorialPdfPage({required this.tutorial, Key? key}) : super(key: key);
  final TutorialEntity tutorial;

  @override
  State<TutorialPdfPage> createState() => _TutorialPdfPageState();
}

class _TutorialPdfPageState extends State<TutorialPdfPage> {
  late PdfController pdfController;

  @override
  void initState() {
    super.initState();
    pdfController = PdfController(
      document: PdfDocument.openData(InternetFile.get(widget.tutorial.url)),
      initialPage: 1,
      viewportFraction: 1,
    );
  }

  @override
  void dispose() {
    pdfController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Tutorial em PDF'),
        actions: [
          IconButton(
            icon: const Icon(EvaIcons.externalLinkOutline),
            onPressed: () {
              launchUrl(
                Uri.parse(widget.tutorial.url),
                mode: LaunchMode.externalApplication,
              );
            },
          ),
        ],
      ),
      body: Stack(
        children: [
          const Center(child: CircularProgressIndicator()),
          PdfView(
            controller: pdfController,
            renderer: (PdfPage page) => page.render(
              width: page.width * 2,
              height: page.height * 2,
              format: PdfPageImageFormat.jpeg,
              backgroundColor: '#FFFFFF',
            ),
          ),
        ],
      ),
    );
  }
}
