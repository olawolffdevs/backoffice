import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:url_launcher/url_launcher.dart';


class OrderOptions extends StatefulWidget {
  const OrderOptions(this.order, this.notifyParent, {super.key});
  final OrderEcommerceEntity order;
  final Function notifyParent;

  @override
  State<OrderOptions> createState() => _OrderOptionsState(order, notifyParent);
}

class _OrderOptionsState extends State<OrderOptions> {
  _OrderOptionsState(this.order, this.notifyParent);
  OrderEcommerceEntity order;
  final Function notifyParent;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: const BorderRadius.all(Radius.circular(15)),
        ),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.only(bottom: 20, left: 25, top: 20),
                child: OwText(
                  'Mais ações',
                  style: TextStyle(fontSize: 19, fontWeight: FontWeight.w700),
                ),
              ),
              Container(
                  color: Theme.of(context).secondaryHeaderColor, height: 1),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: const OwText('Encaminhar no WhatsApp'),
                      onTap: () async {
                        final concatenate = StringBuffer();
                        order.products.forEach((produtoPedido) {
                          final String produto = '${produtoPedido.quantity}x ${produtoPedido.product.name} - ${OwFormat.toRealCurrency(produtoPedido.amount)}%0A';
                          concatenate.write(produto);
                          if (Valid.validateText(produtoPedido.observation)) {
                            concatenate.write(
                                '*Observação*: ${produtoPedido.observation}%0A');
                          }
                        });

                        // final String pedido = "📝 *PEDIDO*%0ARealizado às ${DateFormat("HH:mm - dd/MM/yyyy").format(DateTime.parse(order.createdAt.toString()).toLocal())}%0AStatus: ${order.orderHistoric[0].status}%0ANº Pedido: %23${order.orderId}%0A%0A🛍️ *ITENS*%0A${concatenate.toString()}%0A";
                        // final String pagamento = "🤑 *PAGAMENTO*%0AForma de Pagamento: ${order.paymentMethod}%0AStatus do Pagamento: ${order.paymentStatus}%0ATotal: ${OwFormat.toRealCurrency(order.total)}%0A${((double.tryParse(order.change.toString()) ?? 0) > 0) ? "Troco para ${OwFormat.toRealCurrency(order.change)}" : "Sem troco"}%0A%0A";
                        // final String entrega = '🛵 *ENTREGA*%0AForma de Entrega: ${OwFormat.capsFirst(order.serviceLocation)}%0ALocal de Entrega: ${order.address?.toString()}%0ATaxa de Entrega: ${OwFormat.toRealCurrency(order.deliveryFee)}%0A%0A';
                        final String cliente = '😝 *CLIENTE*%0ANome: ${order.user?.name.toString()}%0ATelefone: ${OwFormat.phoneNumber(order.user?.phone)}%0A%0A';
                        const String footer = 'Pedido realizado pelo Venver! 🛒😀';
                        final String link = 'https://api.whatsapp.com/send?phone=55${order.user?.phone.toString()}&text=$order${order.deliveryFee}${order.paymentMethod}$cliente$footer';
                        launchUrl(
                          Uri.parse(link),
                          mode: LaunchMode.externalApplication,
                        );
                      },
                      leading: Icon(
                        EvaIcons.cornerDownRightOutline,
                        color: Theme.of(context).primaryTextTheme.bodyLarge?.color,
                      ),
                    ),
                    ListTile(
                      title: const OwText('Imprimir pedido'),
                      onTap: () async {},
                      leading: Icon(
                        EvaIcons.printerOutline,
                        color:
                            Theme.of(context).primaryTextTheme.bodyLarge?.color,
                      ),
                      trailing: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 12, vertical: 3),
                        decoration: const BoxDecoration(
                            color: Color(0xff007a55),
                            borderRadius:
                                BorderRadius.all(Radius.circular(50))),
                        child: const OwText('Em Breve',
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                    order.orderHistoric[0].status.toString() ==
                            'Não atendido'
                        ? ListTile(
                            title: const OwText('Aceitar pedido'),
                            onTap: () async {
                              await atualizarStatusPedido(
                                  'Em andamento', context);
                            },
                            leading: Icon(
                              EvaIcons.doneAll,
                              color: Theme.of(context)
                                  .primaryTextTheme
                                  .bodyLarge
                                  ?.color,
                            ),
                          )
                        : const SizedBox(),
                        order.paymentStatus == 'Aguardando Pagamento' 
                          && order.paymentMethod.toString().toLowerCase() == 'presencial' 
                          && order.orderHistoric[0].status.toString() != 'Cancelado' 
                          && order.orderHistoric[0].status.toString() != 'Não atendido'
                        ? ListTile(
                            title: const OwText('Confirmar pagamento'),
                            onTap: () async {
                            },
                            leading: Icon(
                              Icons.monetization_on_outlined,
                              color: Theme.of(context)
                                  .primaryTextTheme
                                  .bodyLarge
                                  ?.color,
                            ),
                          )
                        : const SizedBox(),
                    order.orderHistoric[0].status.toString() ==
                                'Em andamento' ||
                            order.orderHistoric[0].status.toString() ==
                                'Em entrega' ||
                            order.orderHistoric[0].status.toString() ==
                                'Para retirada'
                        ? ListTile(
                            title: const OwText('Finalizar'),
                            onTap: () async {
                              await atualizarStatusPedido(
                                  'Finalizado', context);
                            },
                            leading: Icon(
                              Icons.check_circle_outline,
                              color: Theme.of(context)
                                  .primaryTextTheme
                                  .bodyLarge
                                  ?.color,
                            ),
                          )
                        : const SizedBox(),
                    order.orderHistoric[0].status.toString() ==
                            'Em andamento'
                        ? ListTile(
                            title: OwText(
                              order.serviceLocation.toLowerCase() == 'delivery'
                                  ? 'Saiu para entrega'
                                  : 'Disponível para retirada',
                            ),
                            onTap: () async {
                              await atualizarStatusPedido(
                                  order.serviceLocation.toLowerCase() == 'delivery'
                                      ? 'Em entrega'
                                      : 'Para retirada',
                                  context);
                            },
                            leading: Icon(
                              Icons.motorcycle_outlined,
                              color: Theme.of(context)
                                  .primaryTextTheme
                                  .bodyLarge
                                  ?.color,
                            ),
                          )
                        : const SizedBox(),
                    order.orderHistoric[0].status.toString() != 'Não atendido' &&
                            order.orderHistoric[0].status.toString() != 'Cancelado' &&
                            order.orderHistoric[0].status.toString() != 'Finalizado'
                        ? ListTile(
                            title: const OwText('Cancelar pedido', style: TextStyle(color: Colors.red)),
                            onTap: () async {
                              cancelarPedido(context);
                            },
                            leading: const Icon(EvaIcons.close, color: Colors.red),
                          )
                        : const SizedBox(),
                    order.orderHistoric[0].status.toString() == 'Não atendido'
                        ? ListTile(
                            title: const OwText('Rejeitar pedido', style: TextStyle(color: Colors.red)),
                            onTap: () async {
                              cancelarPedido(context, cancelarRejeitar: 'Rejeitar');
                            },
                            leading: const Icon(EvaIcons.close, color: Colors.red),
                          )
                        : const SizedBox(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> atualizarStatusPedido(String status, context) async {

  }

  void cancelarPedido(context, {String cancelarRejeitar = 'Cancelar'}) async {
    String? cancelarRejeitarConteudo;
    if (cancelarRejeitar == 'Cancelar') {
      cancelarRejeitarConteudo = 'cancelados';
    } else {
      cancelarRejeitarConteudo = 'rejeitados';
    }
    final bool verificarCancelamento = await showOwDialog(
      context: context,
      title: '$cancelarRejeitar pedido?',
      description: 'Pedidos $cancelarRejeitarConteudo não podem ser alterados.',
      buttons: [
        OwButton(
          labelText: 'Sim',
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
        OwButton(
          labelText: 'Não',
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
      ],
    );

    if (verificarCancelamento == true) {
      atualizarStatusPedido('Cancelado', context);
    } else {
      Navigator.pop(context);
    }
  }
}