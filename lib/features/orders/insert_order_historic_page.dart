import 'package:backoffice/features/orders/new_date_order_page.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:intl/intl.dart';

class InsertOrderHistoricPage extends StatefulWidget {
  const InsertOrderHistoricPage(this.order, {super.key});
  final OrderEcommerceEntity order;

  @override
  State<InsertOrderHistoricPage> createState() => _InsertOrderHistoricPageState();
}

class _InsertOrderHistoricPageState extends State<InsertOrderHistoricPage> {

  String? selected;
  String? newDate;
  TextEditingController observation = TextEditingController();

  @override
  void dispose() {
    observation.dispose();
    super.dispose();
  }

  Map<String, List<String>> status = {
    'created': [
      'confirmed',
      'cancelled',
    ],
    'confirmed': [
      'reschedule',
      'outForDelivery',
      'readyForPickup',
      'finished',
      'cancelled',
    ],
    'reschedule': [
      'reschedule',
      'finished',
      'cancelled',
    ],
    'cancelled': [],
    'finished': [],
    'outForDelivery': [
      'finished',
      'cancelled',
    ],
    'readyForPickup': [
      'finished',
      'cancelled',
    ],
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: Text('Nº ${widget.order.orderId}'),
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwText(
              'Status atual do pedido',
              style: Theme.of(context).textTheme.headlineMedium,
              padding: const EdgeInsets.fromLTRB(25, 25, 25, 15),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 25),
              child: HighlightMessageCard(
                color: ColorSystem(context: context),
                text: '${statusCurrent(widget.order.orderHistoric[0].status)}',
              ),
            ),
            const OwDivider(),
            ListView.builder(
              itemCount: status[widget.order.orderHistoric[0].status]!.length,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                final item = status[widget.order.orderHistoric[0].status]!;
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          selected = item[index];
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.all(25),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                '${nextStatus(item[index])}',
                                style: Theme.of(context).textTheme.titleMedium,
                              ),
                            ),
                            const SizedBox(width: 25),
                            IgnorePointer(
                              child: Radio(
                                value: item[index],
                                groupValue: selected,
                                onChanged: (value) {},
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const OwDivider(),
                  ],
                );
              },
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              padding: const EdgeInsets.symmetric(horizontal: 12),
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primaryContainer,
                borderRadius: const BorderRadius.all(Radius.circular(14)),
              ),
              child: OwTextField(
                margin: const EdgeInsets.all(0),
                hideBorder: true,
                controller: observation,
                color: Colors.transparent,
                labelText: 'Observação (Opcional)',
                hintText: 'Você pode digitar aqui...',
              ),
            ),
            Visibility(
              visible: selected == 'reschedule' && newDate != null,
              child: Container(
                margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
                padding: const EdgeInsets.symmetric(horizontal: 12),
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.primaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(14)),
                ),
                child: InkWell(
                  onTap: () async {
                    final result = await openLinkPage(
                      context,
                      OpenLinkPageParams.basic(
                        child: NewDateOrderPage(widget.order),
                      ),
                    );
                    if(result is String) {
                      newDate = result;
                      setState(() {});
                    }
                  },
                  child: OwTextField(
                    margin: const EdgeInsets.all(0),
                    hideBorder: true,
                    enabled: false,
                    color: Colors.transparent,
                    labelText: 'Nova data (reagendamento)',
                    hintText: '${DateFormat("dd/MM/yyyy • HH:mm").format(DateTime.parse(newDate ?? '2020-01-01').toLocal())}',
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Visibility(
        visible: status[widget.order.orderHistoric[0].status]!.length > 0,
        child: OwBottomAppBar(
          child: OwButton(
            margin: const EdgeInsets.all(25),
            labelText: selected == 'reschedule' && newDate == null
              ? 'Selecionar nova data'
              : 'Atualizar status',
            onPressed: () async {
              if(!Valid.text(selected)) {
                showOwDialog(
                  context: context,
                  title: 'Ops, faltou preencher algo',
                  description: 'Selecione uma das opções da lista',
                  buttons: [
                    OwButton.text(
                      labelText: 'Fechar',
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                );
              } else if (selected == 'reschedule' && newDate == null) {
                final result = await openLinkPage(
                  context,
                  OpenLinkPageParams.basic(
                    child: NewDateOrderPage(widget.order),
                  ),
                );
                if(result is String) {
                  newDate = result;
                  setState(() {});
                }
              } else {
                try {
                  OwBotToast.loading();
                  final result = await Api.ecommerce.orders.historic.create(
                    orderId: '${widget.order.id}',
                    status: '$selected',
                    description: observation.text,
                    startDate: newDate,
                  );
                  OwBotToast.close();
                  Navigator.pop(context, result);
                } catch(e) {
                  OwBotToast.close();
                  showOwDialog(
                    context: context,
                    title: 'Ops, aconteceu um erro',
                    description: '$e',
                    buttons: [
                      OwButton.text(
                        labelText: 'Fechar',
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  );
                }
              }
            },
          ),
        ),
      ),
    );
  }

  String statusCurrent(String? value) {
    return switch (value) {
      'created' => 'Aguardando confirmação',
      'cancelled' => 'Pedido cancelado',
      'reschedule' => 'Pedido remarcado',
      'available' => 'Pedido disponível',
      'confirmed' => 'Pedido confirmado',
      'finished' => 'Pedido finalizado',
      'outForDelivery' => 'Saiu para entrega',
      'readyForPickup' => 'Pronto para retirada',
      _ => value ?? 'Inválido',
    };
  }

  String nextStatus(String? value) {
    return switch (value) {
      'created' => 'Aguardando confirmação',
      'cancelled' => 'Cancelar pedido',
      'reschedule' => 'Remarcar pedido',
      'available' => 'Disponível',
      'confirmed' => 'Confirmar pedido',
      'finished' => 'Finalizar pedido',
      'outForDelivery' => 'Saiu para entrega',
      'readyForPickup' => 'Pronto para retirada',
      _ => value ?? 'Inválido',
    };
  }
}
