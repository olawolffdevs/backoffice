import 'package:backoffice/features/orders/order_detail_page.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class OrdersHomePage extends StatefulWidget {
  const OrdersHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<OrdersHomePage> createState() => _OrdersHomePageState();
}

class _OrdersHomePageState extends State<OrdersHomePage> {
  List<OrderEcommerceEntity>? orders;
  List<OrderEcommerceEntity>? ordersFilter;
  String? errorMsg;
  bool showTextInput = false;
  TextEditingController search = TextEditingController();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();

  List<String> statusOrder = [
    'created',
    'cancelled',
    'reschedule',
    'available',
    'confirmed',
    'finished',
    'outForDelivery',
    'readyForPickup',
  ];

  List<String> statusOrderSelected = [];

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        titleSpacing: 25,
        title: const Text('Pedidos e agendamentos'),
        centerTitle: false,
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                showTextInput = !showTextInput;
              });
            },
            icon: const Icon(EvaIcons.searchOutline),
          ),
          IconButton(
            onPressed: () {
              _refreshIndicatorKey.currentState?.show();
            },
            icon: const Icon(EvaIcons.refreshOutline),
          ),
        ],
      ),
      body: Visibility(
        visible: Valid.text(errorMsg),
        child: hasError(),
        replacement: Visibility(
          visible: orders == null,
          child: isLoading(),
          replacement: Visibility(
            visible: orders?.isEmpty ?? true,
            child: isEmpty(),
            replacement: isNotEmpty(),
          ),
        ),
      ),
    );
  }

  Widget isEmpty() {
    return Center(
      child: RefreshIndicator(
        onRefresh: () async {
          await callApi();
        },
        key: _refreshIndicatorKey,
        child: SingleChildScrollView(
          padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Image.asset(
                'assets/images/chat.png',
                height: 250,
              ),
              const SizedBox(height: 25),
              const OwText(
                'Você ainda não recebeu nenhum pedido ou agendamento',
                style: TextStyle(fontWeight: FontWeight.w800, fontSize: 20),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 15),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 35),
                child: OwText(
                  'Assim que receber seu primeiro pedido você poderá acompanhar por aqui.',
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(height: 25)
            ],
          ),
        ),
      ),
    );
  }

  Widget isNotEmpty() {
    return RefreshIndicator(
      onRefresh: () async {
        await callApi();
      },
      key: _refreshIndicatorKey,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Visibility(
              visible: showTextInput,
              child: OwTextField(
                controller: search,
                hintText: 'Pesquisar por nome ou nº do pedido',
                margin: const EdgeInsets.fromLTRB(25, 10, 25, 25),
                textInputAction: TextInputAction.done,
                onChanged: (value) {
                  setState(() {
                    updateShowedOrders();
                  });
                },
                onFieldSubmitted: (value) {
                  setState(() {
                    updateShowedOrders();
                  });
                },
              ),
            ),
            ScrollConfiguration(
              behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                PointerDeviceKind.touch,
                PointerDeviceKind.mouse,
              }),
              child: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 25, right: 17),
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: statusOrder
                      .map((e) => CardAtalhos(
                            title: convertEnumsOrderStatus(e),
                            onPressed: () {
                              if (statusOrderSelected.contains(e)) {
                                statusOrderSelected.remove(e);
                              } else {
                                statusOrderSelected.add(e);
                              }
                              setState(() {
                                updateShowedOrders();
                              });
                            },
                            selecionado: statusOrderSelected.contains(e),
                          ))
                      .toList(),
                ),
              ),
            ),
            new OwGrid.builder(
              spacing: 15,
              runSpacing: 15,
              padding: const EdgeInsets.all(25),
              numbersInRowAccordingToWidgth: [550, 950, 1600, 1950],
              itemCount: ordersFilter?.length ?? 0,
              itemBuilder: (context, index) {
                final List<String> destaques = [
                  '${convertEnumsOrderStatus(ordersFilter![index].orderHistoric[0].status)}',
                ];
                if (ordersFilter![index].total > 0) {
                  destaques.add('${ordersFilter![index].paymentStatus}');
                }
                return new Material(
                  type: MaterialType.transparency,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.primaryContainer,
                      borderRadius: const BorderRadius.all(Radius.circular(14)),
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(14)),
                      onTap: () async {
                        await openLinkPage(
                          context,
                          OpenLinkPageParams.basic(
                            child: OrderDetailPage(
                              ordersFilter![index],
                              () {
                                setState(() {});
                              },
                            ),
                          ),
                        );
                        setState(() {});
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 24,
                          vertical: 20,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    '${ordersFilter![index].user?.name}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium
                                        ?.copyWith(
                                          fontWeight: FontWeight.bold,
                                        ),
                                  ),
                                ),
                                const SizedBox(width: 12),
                                Text(
                                  '#${ordersFilter![index].orderId}',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    'Feito em ${DateFormat("dd/MM/yyyy HH:mm").format(DateTime.parse(ordersFilter![index].createdAt ?? '2020-01-01').toLocal())}',
                                    style:
                                        Theme.of(context).textTheme.bodySmall,
                                  ),
                                ),
                                const SizedBox(width: 12),
                                Text(
                                  '${convertEnumsServiceLocation(ordersFilter![index].serviceLocation)}',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                              ],
                            ),
                            const SizedBox(height: 6),
                            Wrap(
                              spacing: 8,
                              runSpacing: 8,
                              children: destaques
                                  .map((String e) => HighlightMessageCard(
                                        color: ColorSystem(context: context),
                                        text: (e),
                                      ))
                                  .toList(),
                            ),
                            const SizedBox(height: 6),
                            Visibility(
                              visible:
                                  Valid.text(ordersFilter![index].startDate),
                              child: Text(
                                'Agendado para ${DateFormat("dd/MM/yyyy HH:mm").format(DateTime.parse(ordersFilter![index].startDate ?? '2020-01-01').toLocal())}',
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget hasError() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 35),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          OwText(
            'Poxa, tivemos um erro aqui',
            style: Theme.of(context).textTheme.titleMedium,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 16),
          OwText(
            '$errorMsg',
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 16),
          OwButton(
            labelText: 'Voltar',
            onPressed: () {
              context.pop();
            },
          ),
          OwButton.elevated(
            labelText: 'Tentar novamente',
            onPressed: () {
              setState(() {
                errorMsg = null;
              });
              callApi();
            },
          ),
        ],
      ),
    );
  }

  Widget isLoading() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  bool _showOrder(OrderEcommerceEntity item) {
    if (Valid.text(search.text)) {
      return OwFormat.removeAccentsAndPunctuation('${item.user?.name}')
              .contains(search.text) ||
          OwFormat.removeAccentsAndPunctuation('${item.orderId}')
              .contains(search.text);
    }
    return (statusOrderSelected.isEmpty) ||
        statusOrderSelected.contains(item.orderHistoric[0].status);
  }

  void updateShowedOrders() {
    ordersFilter = [];
    orders?.forEach((element) {
      if (_showOrder(element)) {
        if (ordersFilter == null) {
          ordersFilter = [];
        }
        ordersFilter!.add(element);
      }
    });
  }

  Future<void> callApi() async {
    try {
      orders = await Api.ecommerce.orders.listByEcommerce(
        id: '${WhiteLabelEntity.current?.setting.ecommerceId}',
      );
      ordersFilter = orders;
    } catch (e) {
      errorMsg = '$e';
    }
    setState(() {});
  }

  String convertEnumsPaymentStatus(String value) {
    return switch (value) {
      'paid' => 'Pago',
      'processing' => 'Processando pagamento',
      'refunded' => 'Pagamento reembolsado',
      'waiting_payment' => 'Aguardando pagamento',
      'pending_refund' => 'Aguardando reembolso',
      'refused' => 'Pagamento recusado',
      'chargedback' => 'Transação sofreu chargeback',
      'analyzing' => 'Pagamento em análise',
      'pending_review' => 'Pagamento em análise',
      _ => value,
    };
  }

  String convertEnumsOrderStatus(String? value) {
    return switch (value) {
      'created' => 'Aguardando confirmação',
      'confirmed' => 'Confirmado',
      'available' => 'Disponível',
      'outForDelivery' => 'Saiu para entrega',
      'readyForPickup' => 'Pronto para retirada',
      'reschedule' => 'Reagendado',
      'cancelled' => 'Cancelado',
      'finished' => 'Finalizado',
      _ => value ?? 'Inválido',
    };
  }

  String convertEnumsServiceLocation(String? value) {
    return switch (value) {
      'takeout' => 'No meu local',
      'delivery' => 'No cliente',
      'online' => 'Online',
      _ => value ?? 'Inválido',
    };
  }
}
