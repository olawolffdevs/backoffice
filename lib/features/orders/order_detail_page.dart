import 'package:backoffice/features/orders/order_historic_page.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:url_launcher/url_launcher.dart';

class OrderDetailPage extends StatefulWidget {
  const OrderDetailPage(this.order, this.notifyParent, {super.key});
  final OrderEcommerceEntity order;
  final Function() notifyParent;

  @override
  State<OrderDetailPage> createState() => _OrderDetailPageState();
}

class _OrderDetailPageState extends State<OrderDetailPage> {
  TextEditingController description = TextEditingController();

  @override
  void initState() {
    super.initState();
    description.text = widget.order.description ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: Text('Nº ${widget.order.orderId}'),
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            tooltip: 'Histórico do pedido',
            icon: const Icon(Icons.pending_actions_outlined),
            onPressed: () async {
              await openLinkPage(
                context,
                OpenLinkPageParams.basic(
                    child: OrderHistoricPage(widget.order)),
              );
              setState(() {});
            },
          ),
        ],
      ),
      body: Center(
        child: Container(
          constraints: const BoxConstraints(
            maxWidth: 600,
          ),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 45),
                Text(
                  '${widget.order.user?.name}',
                  style: Theme.of(context).textTheme.headlineSmall?.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 8),
                Text(
                  'Pedido feito às ${DateFormat("HH:mm • dd/MM/yyyy").format(DateTime.parse(widget.order.createdAt ?? '2020-01-01').toLocal())}',
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 45),
                Material(
                  type: MaterialType.transparency,
                  color: Colors.transparent,
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 24),
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(14)),
                      border: Border.all(
                        width: 1,
                        color: Theme.of(context).colorScheme.primaryContainer,
                      ),
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(14)),
                      onTap: () async {
                        await openLinkPage(
                          context,
                          OpenLinkPageParams.basic(
                              child: OrderHistoricPage(widget.order)),
                        );
                        setState(() {});
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '${convertEnumsOrderStatus(widget.order.orderHistoric[0].status)}',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onPrimaryContainer,
                                  ),
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              'Atualizado às ${DateFormat("HH:mm • dd/MM/yyyy").format(DateTime.parse(widget.order.orderHistoric[0].createdAt).toLocal())}',
                              style: Theme.of(context).textTheme.bodySmall,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: Valid.text(widget.order.startDate),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 25),
                      child: HighlightMessageCard(
                        color: ColorSystem(context: context),
                        text:
                            'Agendado para ${DateFormat("dd/MM/yyyy • HH:mm").format(DateTime.parse(widget.order.startDate ?? '2020-01-01').toLocal())}',
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        tooltip: 'Telefone',
                        icon: const Icon(EvaIcons.phoneOutline),
                        onPressed: () async {
                          launchUrl(
                              Uri.parse('tel:${widget.order.user?.phone}'));
                        },
                      ),
                      IconButton(
                        tooltip: 'E-mail',
                        icon: const Icon(EvaIcons.emailOutline),
                        onPressed: () async {
                          launchUrl(
                              Uri.parse('mailto:${widget.order.user?.email}'));
                        },
                      ),
                      // IconButton(
                      //   tooltip: 'Chat',
                      //   icon: const Icon(EvaIcons.messageCircleOutline),
                      //   onPressed: () async {
                      //     final DateTime nowTime = DateTime.now();
                      //     final date = Timestamp.fromDate(nowTime);
                      //     final current = ParticipantChat(
                      //       name: '${WhiteLabelEntity.current?.name}',
                      //       id: '${WhiteLabelEntity.current?.setting.ecommerceId}',
                      //       type: 'ecommerce',
                      //       createdDate: Timestamp.fromDate(nowTime),
                      //     );
                      //     final List<String> ids = [
                      //       '${current.id}',
                      //       '${widget.order.user?.id}',
                      //       '${widget.order.id}',
                      //     ].toSet().toList();
                      //     ids.sort();
                      //     final String chatId = hashList(ids);
                      //     final additionalChat = AdditionalChat(
                      //       orderId: '${widget.order.id}',
                      //       params: {},
                      //     );
                      //     context.push(
                      //       '/${WhiteLabelEntity.current?.id}/inbox/chat/$chatId',
                      //       extra: <String, dynamic>{
                      //         'chatId': chatId,
                      //         'current': current,
                      //         'title': 'Pedido nº ${widget.order.orderId}',
                      //         'origin': 'order',
                      //         'additionalChat': additionalChat,
                      //         'participants': [
                      //           ParticipantChat(
                      //             createdDate: date,
                      //             id: '${widget.order.user?.id}',
                      //             name: '${widget.order.user?.name}',
                      //             type: 'user',
                      //           ),
                      //           current,
                      //         ],
                      //         'chat': ,
                      //         'key': ValueKey<Object>(Key(chatId)),
                      //       },
                      //     );
                      //   },
                      // ),
                    ],
                  ),
                ),
                Visibility(
                  visible: widget.order.serviceLocation == 'online' &&
                      widget.order.orderHistoric[0].status != 'created' &&
                      widget.order.orderHistoric[0].status != 'cancelled' &&
                      widget.order.orderHistoric[0].status != 'finished',
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const SizedBox(height: 45),
                      const OwDivider(),
                      OwText(
                        'Descrição para acesso online',
                        padding: const EdgeInsets.fromLTRB(25, 35, 25, 15),
                        style:
                            Theme.of(context).textTheme.headlineSmall?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                      ),
                      OwText(
                        '${widget.order.type == 'product' ? 'A forma de entrega' : 'O local do serviço'} é online; portanto você precisa informar no campo abaixo como seu cliente terá acesso ao produto/serviço.',
                        padding: const EdgeInsets.fromLTRB(25, 0, 25, 25),
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.primaryContainer,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(14)),
                        ),
                        child: OwTextField(
                          margin: const EdgeInsets.all(0),
                          hideBorder: true,
                          controller: description,
                          color: Colors.transparent,
                          labelText:
                              'Como seu cliente terá acesso ao produto/serviço?',
                          hintText: 'Você pode digitar aqui...',
                          onChanged: (value) {
                            setState(() {});
                          },
                        ),
                      ),
                      Visibility(
                        visible: widget.order.description !=
                            (Valid.text(description.text)
                                ? description.text
                                : null),
                        child: OwButton(
                          margin: const EdgeInsets.fromLTRB(25, 15, 25, 0),
                          labelText: 'Salvar alterações',
                          onPressed: () async {
                            try {
                              OwBotToast.loading();
                              await Api.ecommerce.orders.update(
                                orderId: '${widget.order.id}',
                                description: (Valid.text(description.text)
                                    ? description.text
                                    : null),
                              );
                              OwBotToast.close();
                              widget.order.description = description.text;
                              OwBotToast.toast(
                                  'Descrição atualizada com sucesso');
                              setState(() {});
                            } catch (e) {
                              OwBotToast.close();
                              showOwDialog(
                                context: context,
                                title: 'Ops, aconteceu um erro',
                                description: '$e',
                              );
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 45),
                const OwDivider(),
                _items(),
                const SizedBox(height: 30),
                const OwDivider(),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      OwText(
                        widget.order.type == 'product'
                            ? 'Forma de entrega'
                            : 'Local do serviço',
                        padding: const EdgeInsets.fromLTRB(25, 35, 25, 15),
                        style:
                            Theme.of(context).textTheme.headlineSmall?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                      ),
                      OwText(
                        '${convertEnumsServiceLocation(widget.order.serviceLocation)}',
                        padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                      Visibility(
                        visible: widget.order.serviceLocation != 'online',
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            OwText(
                              '${(widget.order.address?.toBasic())}',
                              padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                            OwText(
                              '${(widget.order.address?.city)} - ${(widget.order.address?.state)}',
                              padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                            OwText(
                              '${(widget.order.address?.zipCode)}',
                              padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                        replacement: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            OwText(
                              'Agendado para ${DateFormat("dd/MM/yyyy • HH:mm").format(DateTime.parse(widget.order.startDate ?? '2020-01-01').toLocal())}',
                              padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 30),
                const OwDivider(),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      OwText(
                        'Resumo de valores',
                        padding: const EdgeInsets.fromLTRB(25, 35, 25, 15),
                        style:
                            Theme.of(context).textTheme.headlineSmall?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                        child: Row(
                          children: [
                            Expanded(
                              child: OwText(
                                'Subtotal',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                            const SizedBox(width: 10),
                            OwText(
                              '${paraReal(widget.order.subtotal, retornoGratis: false)}',
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                        child: Row(
                          children: [
                            Expanded(
                              child: OwText(
                                'Taxa de entrega',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                            const SizedBox(width: 10),
                            OwText(
                              '${paraReal(widget.order.deliveryFee, retornoGratis: false)}',
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                        child: Row(
                          children: [
                            Expanded(
                              child: OwText(
                                'Taxa de serviço',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                            const SizedBox(width: 10),
                            OwText(
                              '${paraReal(widget.order.serviceFee, retornoGratis: false)}',
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                        child: Row(
                          children: [
                            Expanded(
                              child: OwText(
                                'Total',
                                style: Theme.of(context).textTheme.titleMedium,
                              ),
                            ),
                            const SizedBox(width: 10),
                            OwText(
                              '${paraReal(widget.order.total, retornoGratis: false)}',
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 30),
                const OwDivider(),
                Visibility(
                  visible: widget.order.total > 0,
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        OwText(
                          'Forma de pagamento',
                          padding: const EdgeInsets.fromLTRB(25, 35, 25, 15),
                          style: Theme.of(context)
                              .textTheme
                              .headlineSmall
                              ?.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                          child: Row(
                            children: [
                              Expanded(
                                child: OwText(
                                  'Status do pagamento',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                              ),
                              const SizedBox(width: 10),
                              OwText(
                                '${convertEnumsPaymentStatus(widget.order.paymentStatus)}',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                          child: Row(
                            children: [
                              Expanded(
                                child: OwText(
                                  'Tipo de pagamento',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                              ),
                              const SizedBox(width: 10),
                              OwText(
                                '${convertEnumsPaymentType(widget.order.paymentType)}',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                          child: Row(
                            children: [
                              Expanded(
                                child: OwText(
                                  'Método de pagamento',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                              ),
                              const SizedBox(width: 10),
                              OwText(
                                '${convertEnumsPaymentMethod(widget.order.paymentMethod)}',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ],
                          ),
                        ),
                        Visibility(
                          visible: Valid.text(
                              widget.order.last4DigitsOfTheCreditCard),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                            child: Row(
                              children: [
                                Expanded(
                                  child: OwText(
                                    'Cartão de crédito',
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                                ),
                                const SizedBox(width: 10),
                                OwText(
                                  '•••• ${(widget.order.last4DigitsOfTheCreditCard)}',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: Valid.text(widget.order.pix),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(25, 0, 25, 6),
                            child: Row(
                              children: [
                                Expanded(
                                  child: OwText(
                                    'Pix',
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                                ),
                                const SizedBox(width: 10),
                                OwText(
                                  '${(widget.order.pix)}',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(height: 30),
                        const OwDivider(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _items() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          OwText(
            '${widget.order.products.length} ${widget.order.products.length > 1 ? 'items' : 'item'}',
            padding: const EdgeInsets.fromLTRB(25, 45, 25, 4),
            style: Theme.of(context).textTheme.titleMedium,
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: widget.order.products.length,
            itemBuilder: (context, index) {
              final product = widget.order.products[index];
              return Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 16),
                child: Ink(
                  child: InkWell(
                    onTap: () {},
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Row(
                          children: [
                            HighlightMessageCard(
                              color: ColorSystem(context: context),
                              text: '${product.quantity}',
                            ),
                            const SizedBox(width: 15),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${product.product.name}',
                                    maxLines: 2,
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                ],
                              ),
                            ),
                            Visibility(
                              visible: (product.amount > 0),
                              child: Padding(
                                padding: const EdgeInsets.only(top: 6),
                                child: Text(
                                  '${paraReal(product.product.amountWithDiscount)}',
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall
                                      ?.copyWith(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Visibility(
                          visible: Valid.text(product.observation),
                          child: Container(
                            margin: const EdgeInsets.only(top: 10),
                            padding: const EdgeInsets.all(6),
                            decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(6)),
                              color: Theme.of(context)
                                  .colorScheme
                                  .primaryContainer,
                            ),
                            child: Text(
                              'Obs.: ${product.observation}',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onPrimaryContainer,
                                  ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}

String convertEnumsOrderStatus(String? value) {
  return switch (value) {
    'created' => 'Aguardando confirmação',
    'cancelled' => 'Cancelado',
    'reschedule' => 'Reagendado',
    'available' => 'Disponível',
    'confirmed' => 'Confirmado',
    'finished' => 'Finalizado',
    'outForDelivery' => 'Saiu para entrega',
    'readyForPickup' => 'Pronto para retirada',
    _ => value ?? 'Inválido',
  };
}

String convertEnumsPaymentStatus(String? value) {
  return switch (value) {
    'paid' => 'Pago',
    'processing' => 'Processando pagamento',
    'refunded' => 'Pagamento reembolsado',
    'waiting_payment' => 'Aguardando pagamento',
    'pending_refund' => 'Aguardando reembolso',
    'refused' => 'Pagamento recusado',
    'chargedback' => 'Transação sofreu chargeback',
    'analyzing' => 'Pagamento em análise',
    'pending_review' => 'Pagamento em análise',
    _ => value ?? 'Inválido',
  };
}

String convertEnumsPaymentType(String? value) {
  return switch (value) {
    'personal' => 'Presencial',
    'presential' => 'Presencial',
    'online' => 'Online',
    _ => value ?? 'Inválido',
  };
}

String convertEnumsPaymentMethod(String? value) {
  return switch (value) {
    'pix' => 'PIX',
    'creditCard' => 'Cartão de Crédito',
    'money' => 'Dinheiro',
    _ => value ?? 'Inválido',
  };
}

IconData convertEnumsPaymentMethodIcons(String? value) {
  return switch (value) {
    'pix' => Icons.pix_outlined,
    'money' => Icons.payments_outlined,
    _ => EvaIcons.creditCardOutline,
  };
}

String convertEnumsServiceLocation(String? value) {
  return switch (value) {
    'takeout' => 'No local',
    'delivery' => 'No meu endereço',
    'online' => 'Online',
    _ => value ?? 'Inválido',
  };
}