import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:table_calendar/table_calendar.dart';

class NewDateOrderPage extends StatefulWidget {
  const NewDateOrderPage(this.order, {super.key});
  final OrderEcommerceEntity order;

  @override
  State<NewDateOrderPage> createState() => _NewDateOrderPageState();
}

class _NewDateOrderPageState extends State<NewDateOrderPage> {

  ProductEcommerceEntity? product;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    if(product == null) {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Scaffold(
      appBar: OwAppBar(
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            boxService(),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          labelText: 'Salvar e fechar',
          onPressed: () {
            final selected = DateTime(focusedDay.year, focusedDay.month, focusedDay.day, focusedTime!.hour, focusedTime!.minute);
            Navigator.pop(context, selected.toIso8601String());
          },
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      product = await Api.ecommerce.products.find(
        id: widget.order.products[0].product.id,
      );
      generate();
    } catch (e) {
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: '$e',
        buttons: [
          OwButton.text(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }

  List<ScheduleEntity> generateSchedule = [];
  DateTime focusedDay = DateTime.now().add(const Duration(days: 1));
  DateTime? focusedTime = DateTime.now();

  void generate() {
    generateSchedule = ScheduleGenerator.generateSchedule(
      cronograma: (product?.schedules ?? []).where((e) => e.dayWeek == focusedDay.weekday).toList(),
      interval: product?.breakBetweenSessions ?? 30,
      durationService: (product?.serviceDuration ?? 30).toInt(),
    );
  }

  Widget boxService() {
    if (widget.order.type == 'product' || (product?.schedules?.isEmpty ?? true)) {
      return const SizedBox();
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(height: 40),
        OwText(
          'Data',
          padding: const EdgeInsets.symmetric(horizontal: 25),
          style: Theme.of(context).textTheme.titleLarge?.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 15),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18),
          child: TableCalendar(
            firstDay: DateTime.now().add(const Duration(days: 1)),
            lastDay: DateTime.now().add(const Duration(days: 15)),
            currentDay: focusedDay,
            focusedDay: focusedDay,
            locale: 'pt_BR',
            headerVisible: false,
            calendarFormat: CalendarFormat.twoWeeks,
            daysOfWeekHeight: 45,
            calendarStyle: CalendarStyle(
              outsideTextStyle: TextStyle(
                color: Theme.of(context).colorScheme.onBackground,
              ),
              outsideDecoration: BoxDecoration(
                color: Theme.of(context).colorScheme.background,
                borderRadius: const BorderRadius.all(
                  Radius.circular(80),
                ),
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.onBackground,
                ),
              ),
              disabledTextStyle: TextStyle(
                color: Theme.of(context).colorScheme.onErrorContainer,
              ),
              disabledDecoration: BoxDecoration(
                color: Theme.of(context).colorScheme.errorContainer,
                borderRadius: const BorderRadius.all(
                  Radius.circular(80),
                ),
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.error,
                ),
              ),
              todayTextStyle: TextStyle(
                color: Theme.of(context).colorScheme.onPrimary,
              ),
              todayDecoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primary,
                borderRadius: const BorderRadius.all(
                  Radius.circular(8),
                ),
              ),
              defaultDecoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(80),
                ),
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.onBackground,
                ),
              ),
              weekendDecoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(80),
                ),
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.onBackground,
                ),
              ),
            ),
            onDaySelected: (selectedDay, _) {
              setState(() {
                focusedDay = selectedDay;
                focusedTime = null;
                generate();
              });
            },
          ),
        ),
        const SizedBox(height: 40),
        OwText(
          'Horário',
          padding: const EdgeInsets.symmetric(horizontal: 25),
          style: Theme.of(context).textTheme.titleLarge?.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 15),
        Visibility(
          visible: generateSchedule.isNotEmpty,
          replacement: Container(
            margin: const EdgeInsets.symmetric(horizontal: 25),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.primaryContainer,
              borderRadius: const BorderRadius.all(
                Radius.circular(15),
              ),
            ),
            padding: const EdgeInsets.all(25),
            child: Text(
              'Nenhum horário disponível para esta data. Tente escolher um outro dia.',
              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                color: Theme.of(context).colorScheme.onPrimaryContainer,
              ),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              alignment: WrapAlignment.spaceBetween,
              spacing: 4,
              runSpacing: 1,
              children: generateSchedule.map((e) => FilterChip(
                selected: focusedTime == DateTime.parse(e.startTime),
                label: Text('${DateFormat("HH:mm", "pt_BR").format(DateTime.parse(e.startTime))}'),
                showCheckmark: false,
                selectedColor: Theme.of(context).colorScheme.primary,
                shape: RoundedRectangleBorder(
                  borderRadius: focusedTime == DateTime.parse(e.startTime)
                    ? const BorderRadius.all(Radius.circular(8))
                    : const BorderRadius.all(Radius.circular(50))
                ),
                labelStyle: TextStyle(
                  color: focusedTime == DateTime.parse(e.startTime)
                    ? Theme.of(context).colorScheme.onPrimary
                    : Theme.of(context).colorScheme.onBackground,
                ),
                shadowColor: Colors.transparent,
                onSelected: (bool value) {
                  setState(() {
                    focusedTime = DateTime.parse(e.startTime);
                  });
                },
              )).toList(),
            ),
          ),
        ),
        const SizedBox(height: 45),
      ],
    );
  }

  List<int> findDaysWithNoSchedule(List<ScheduleEntity> scheduleList) {
    // Crie uma lista de todos os dias da semana (de 1 a 7)
    final List<int> allDays = List.generate(7, (index) => index + 1);

    // Extraia os dias da lista de agendamentos
    final List<int> scheduledDays = scheduleList.map((schedule) => schedule.dayWeek).toSet().toList();

    // Encontre os dias sem agendamentos
    final List<int> daysWithNoSchedule = allDays.where((day) => !scheduledDays.contains(day)).toList();

    return daysWithNoSchedule;
  }
}

class ScheduleGenerator {
  static List<ScheduleEntity> generateSchedule({
    required List<ScheduleEntity> cronograma,
    required int interval,
    required int durationService,
  }) {
    final List<ScheduleEntity> generatedSchedule = [];
    for (var schedule in cronograma) {
      DateTime startDateTime = DateTime.parse('2023-01-0${schedule.dayWeek} ${schedule.startTime}');
      final DateTime endDateTime = DateTime.parse('2023-01-0${schedule.dayWeek} ${schedule.endTime}');
      while (startDateTime.isBefore(endDateTime)) {
        generatedSchedule.add(ScheduleEntity(
          dayWeek: schedule.dayWeek,
          startTime: startDateTime.toLocal().toIso8601String(),
          endTime: startDateTime.add(Duration(minutes: interval)).toLocal().toIso8601String(),
        ));
        if(startDateTime.add(Duration(minutes: interval + durationService)).isBefore(endDateTime)) {
          startDateTime = startDateTime.add(Duration(minutes: interval));
        } else {
          startDateTime = startDateTime.add(Duration(minutes: interval + durationService));
        }
      }
    }
    generatedSchedule.sort((a, b) => a.startTime.compareTo(b.startTime));
    return generatedSchedule;
  }
}