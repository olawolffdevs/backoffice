import 'package:backoffice/features/orders/insert_order_historic_page.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class OrderHistoricPage extends StatefulWidget {
  const OrderHistoricPage(this.order, {super.key});
  final OrderEcommerceEntity order;

  @override
  State<OrderHistoricPage> createState() => _OrderHistoricPageState();
}

class _OrderHistoricPageState extends State<OrderHistoricPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: Text('Nº ${widget.order.orderId}'),
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwText(
              'Atualizações sobre esse pedido',
              style: Theme.of(context).textTheme.headlineMedium,
              padding: const EdgeInsets.fromLTRB(25, 25, 25, 10),
            ),
            ListView.separated(
              itemCount: widget.order.orderHistoric.length,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              separatorBuilder: (context, index) {
                return const OwDivider();
              },
              itemBuilder: (context, index) {
                final item = widget.order.orderHistoric[index];
                return Container(
                  padding: const EdgeInsets.all(25),
                  child: Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Theme.of(context).colorScheme.primaryContainer,
                        ),
                        child: const Icon(Icons.pending_actions_outlined),
                      ),
                      const SizedBox(width: 25),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              '${convertEnumsOrderStatus(item.status)}',
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                            const SizedBox(height: 4),
                            Text(
                              '${DateFormat("HH:mm • dd/MM/yyyy").format(DateTime.parse(item.createdAt).toLocal())}',
                            ),
                            Visibility(
                              visible: Valid.text(item.description),
                              child: OwText(
                                'Observação: ${item.description}',
                                padding: const EdgeInsets.only(top: 6),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: widget.order.orderHistoric[0].status == 'cancelled' || widget.order.orderHistoric[0].status == 'finished'
          ? const SizedBox()
          : OwBottomAppBar(
              child: OwButton(
                margin: const EdgeInsets.all(25),
                labelText: 'Adicionar nova atualização',
                onPressed: () async {
                  final result = await openLinkPage(
                    context,
                    OpenLinkPageParams.basic(
                      child: InsertOrderHistoricPage(widget.order),
                    ),
                  );
                  if (result is OrderHistoricEntity) {
                    widget.order.orderHistoric.insert(0, result);
                    setState(() {});
                  }
                },
              ),
            ),
    );
  }

  String convertEnumsOrderStatus(String? value) {
    return switch (value) {
      'created' => 'Pedido criado',
      'cancelled' => 'Cancelado',
      'reschedule' => 'Reagendado',
      'available' => 'Disponível',
      'confirmed' => 'Confirmado',
      'finished' => 'Finalizado',
      'outForDelivery' => 'Saiu para entrega',
      'readyForPickup' => 'Pronto para retirada',
      _ => value ?? 'Inválido',
    };
  }
}
