import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool dynamicColor = false;
  bool soundEnabled = false;
  bool isLoading = true;

  String? theme;

  late SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    verificar();
  }

  void verificar() async {
    prefs = await SharedPreferences.getInstance();

    if (prefs.containsKey('ativo')) {
      theme = prefs.getBool('ativo')! ? 'Escuro' : 'Claro';
    } else {
      theme = 'Sistema';
    }

    dynamicColor = appSM.dynamicColor;
    soundEnabled = appSM.soundEnabled;

    isLoading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(),
      body: OwVisibility(
        visible: isLoading,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
        replacement: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    OwText(
                      'Configurações',
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    OwText(
                      'Tema',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Ink(
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(15),
                              ),
                              color: appSM.themeMode == ThemeMode.light
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.primaryContainer,
                            ),
                            child: InkWell(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(15),
                              ),
                              onTap: () {
                                appSM.setThemeMode(ThemeMode.light);
                                prefs.setBool('ativo', false);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(16),
                                child: Icon(
                                  EvaIcons.sunOutline,
                                  color: appSM.themeMode == ThemeMode.light
                                    ? Theme.of(context).colorScheme.onPrimary
                                    : Theme.of(context).colorScheme.onPrimaryContainer,
                                ),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 25),
                        Expanded(
                          child: Ink(
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(15),
                              ),
                              color: appSM.themeMode == ThemeMode.dark
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.primaryContainer,
                            ),
                            child: InkWell(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(15),
                              ),
                              onTap: () {
                                appSM.setThemeMode(ThemeMode.dark);
                                prefs.setBool('ativo', true);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(16),
                                child: Icon(
                                  EvaIcons.moonOutline,
                                  color: appSM.themeMode == ThemeMode.dark
                                    ? Theme.of(context).colorScheme.onPrimary
                                    : Theme.of(context).colorScheme.onPrimaryContainer,
                                ),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 25),
                        Expanded(
                          child: Ink(
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(15),
                              ),
                              color: appSM.themeMode == ThemeMode.system
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.primaryContainer,
                            ),
                            child: InkWell(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(15),
                              ),
                              onTap: () {
                                appSM.setThemeMode(ThemeMode.system);
                                prefs.remove('ativo');
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(16),
                                child: Icon(
                                  Icons.brightness_6_outlined,
                                  color: appSM.themeMode == ThemeMode.system
                                    ? Theme.of(context).colorScheme.onPrimary
                                    : Theme.of(context).colorScheme.onPrimaryContainer,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 10),
              InkWell(
                onTap: () async {
                  setState(() {
                    dynamicColor = !dynamicColor;
                  });
                  appSM.setDynamicColor(!dynamicColor);
                  prefs.setBool('dynamicColor', dynamicColor);
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: 20,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            OwText(
                              'Cor dinâmica',
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                            const SizedBox(height: 6),
                            const OwText(
                              'Usar cor do meu papel de parede, quando meu dispositivo for suportado.',
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 25),
                      Switch(
                        onChanged: (valor) async {
                          setState(() {
                            dynamicColor = !dynamicColor;
                          });
                          appSM.setDynamicColor(!dynamicColor);
                          prefs.setBool('dynamicColor', dynamicColor);
                        },
                        value: dynamicColor,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: OwText(
                  'Adicionais',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ),
              const SizedBox(height: 10),
              InkWell(
                onTap: () async {
                  appSM.setDataSavingMode(!soundEnabled);
                  setState(() {
                    soundEnabled = !soundEnabled;
                  });
                  prefs.setBool('soundEnabled', soundEnabled);
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: 20,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            OwText(
                              'Som de notificação',
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                            const SizedBox(height: 6),
                            const OwText(
                              'Receber aviso sonoro ao receber notificações de novas mensagens.',
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 25),
                      Switch(
                        onChanged: (valor) async {
                          setState(() {
                            appSM.setSoundEnabled(!soundEnabled);
                            soundEnabled = !soundEnabled;
                          });
                          prefs.setBool('soundEnabled', soundEnabled);
                        },
                        value: soundEnabled,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}
