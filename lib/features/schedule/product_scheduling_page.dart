import 'package:intl/intl.dart';

import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:flutter/material.dart';

class ProductSchedulingPage extends StatefulWidget {
  const ProductSchedulingPage(this.product, {super.key});
  final ProductEcommerceEntity? product;
  @override
  _ProductSchedulingPageState createState() => _ProductSchedulingPageState();
}

class _ProductSchedulingPageState extends State<ProductSchedulingPage> {
  List<Map<String, dynamic>> horarioFuncionamento = [];
  List<ScheduleEntity> cronogramasOriginal = [];
  List<ScheduleEntity> cronogramasTemp = [];
  List<String> diasDaSemana = [
    'Domingo',
    'Segunda',
    'Terça',
    'Quarta',
    'Quinta',
    'Sexta',
    'Sábado'
  ];

  @override
  void initState() {
    super.initState();
    getSchedules();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () async {
              final retorno = await context.push(
                '/${WhiteLabelEntity.current?.setting.ecommerceId}/product/schedule/add',
                extra: widget.product,
              );
              if (retorno is List<ScheduleEntity>) {
                cronogramasTemp.addAll(retorno);
                atualizarHorarios(cronogramasTemp);
              }
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(
          horizontal: 25,
          vertical: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwText(
              'Cronograma',
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            OwText(
              'Defina os horários disponíveis ${widget.product == null ? 'do estabelecimento' : 'do serviço'} para que seus usuários possam realizar pedidos/agendamentos.',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            const SizedBox(height: 25),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: horarioFuncionamento.length,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    ListTile(
                      contentPadding: const EdgeInsets.all(0),
                      title: OwText(
                        horarioFuncionamento[index]['dia_semana'],
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Visibility(
                      visible: horarioFuncionamento[index]['horarios'].length > 0,
                      child: ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: horarioFuncionamento[index]['horarios'].length,
                        itemBuilder: (context, indexHorario) {
                          return ListTile(
                            contentPadding: const EdgeInsets.only(left: 20),
                            title: OwText(
                              "${horarioFuncionamento[index]["horarios"][indexHorario]}",
                            ),
                            trailing: IconButton(
                              icon: Icon(
                                Icons.delete_outline,
                                color: Theme.of(context).colorScheme.primary,
                              ),
                              onPressed: () async {
                                final confirmar = await showOwDialog(
                                  context: context,
                                  title: 'Deseja apagar este horário?',
                                  description: 'Essa ação não poderá ser desfeita',
                                  buttons: [
                                    OwButton.text(
                                      labelText: 'Remover',
                                      onPressed: () {
                                        Navigator.pop(context, true);
                                      },
                                    ),
                                    OwButton.text(
                                      labelText: 'Manter',
                                      onPressed: () {
                                        Navigator.pop(context, false);
                                      },
                                    ),
                                  ],
                                );
                                if (confirmar == true) {
                                  try {
                                    final String start = '${horarioFuncionamento[index]['horarios'][indexHorario].toString().split(" ")[0]}';
                                    final String end = '${horarioFuncionamento[index]['horarios'][indexHorario].toString().split(" ")[2]}';
                                    final ScheduleEntity schedule = cronogramasTemp.where((e) {
                                      return diasDaSemana.indexOf(horarioFuncionamento[index]['dia_semana']) == e.dayWeek
                                      && (DateTime.parse(e.startTime).toLocal()).toString().contains(start.replaceAll('h', ':'))
                                      && (DateTime.parse(e.endTime).toLocal()).toString().contains(end.replaceAll('h', ':'));
                                    }).first;
                                    if(Valid.text(schedule.id)) {
                                      OwBotToast.loading();
                                      await Api.ecommerce.schedule.delete(id: '${schedule.id}');
                                      OwBotToast.close();
                                    }                                    
                                    ((horarioFuncionamento[index]['horarios']) as List).removeAt(indexHorario);
                                    cronogramasTemp.remove(schedule);
                                    atualizarHorarios(cronogramasTemp);
                                  } catch (e) {
                                    OwBotToast.close();
                                    showOwDialog(
                                      context: context,
                                      title: 'Ops',
                                      description: '$e',
                                      buttons: [
                                        OwButton(
                                          labelText: 'Fechar',
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ],
                                    );
                                  }
                                }
                              },
                            ),
                          );
                        },
                      ),
                      replacement: const ListTile(
                        contentPadding: EdgeInsets.only(left: 20),
                        title: OwText('Nenhum horário cadastrado neste dia'),
                      ),
                    ),
                  ],
                );
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          onPressed: () async {
            try {
              final List<ScheduleEntity> temp = [];
              cronogramasTemp.forEach((element) {
                if(!Valid.text(element.id)) {
                  temp.add(element);
                }
              });
              if(temp.isEmpty) {
                context.pop(cronogramasTemp);
              } else {
                if (widget.product != null) {
                  await Api.ecommerce.schedule.insert(productId: '${widget.product?.id}', data: temp);
                } else{
                  await Api.ecommerce.schedule.insert(ecommerceId: '${WhiteLabelEntity.current?.setting.ecommerceId}', data: temp);
                }
                context.pop(cronogramasTemp);
              }
            } catch (e) {
              showOwDialog(
                context: context,
                title: 'Ops',
                description: '$e',
                buttons: [
                  OwButton(
                    labelText: 'Fechar',
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            }
          },
          margin: EdgeInsets.fromLTRB(25, 25, 25, MediaQuery.viewInsetsOf(context).bottom + 25),
          labelText: 'Atualizar cronograma',
        ),
      ),
    );
  }

  String weekConverter(int day) {
    switch (day) {
      case 1:
        return 'Segunda';
      case 2:
        return 'Terça';
      case 3:
        return 'Quarta';
      case 4:
        return 'Quinta';
      case 5:
        return 'Sexta';
      case 6:
        return 'Sábado';
      default:
        return 'Domingo';
    }
  }

  void atualizarHorarios(List<ScheduleEntity> schedules) {
    horarioFuncionamento = [];
    for (int i = 0; i < diasDaSemana.length; i++) {
      final List<String> time = schedules.where((element) => element.dayWeek == i).toList().map((e) {
        final startTemp = DateTime.tryParse(e.startTime) ?? DateTime(2023, 1, 1, int.parse(e.startTime.split(':')[0]), int.parse(e.startTime.split(':')[1]));
        final endTemp = DateTime.tryParse(e.endTime) ?? DateTime(2023, 1, 1, int.parse(e.endTime.split(':')[0]), int.parse(e.endTime.split(':')[1]));
        return '${DateFormat("HH:mm").format(startTemp.toLocal())} até ${DateFormat("HH:mm").format(endTemp.toLocal())}';
      }).toList();
      horarioFuncionamento.add({'dia_semana': '${diasDaSemana[i]}', 'horarios': time});
    }
    if (mounted) {
      setState(() {});
    }
  }

  Future<void> getSchedules() async {
    if (widget.product != null) {
      cronogramasTemp = widget.product?.schedules ?? [];
    } else {
      try {
        OwBotToast.loading();
        cronogramasTemp = await Api.ecommerce.schedule.listByEcommerce(id: '${WhiteLabelEntity.current?.setting.ecommerceId}');
      } catch (e) {
        OwBotToast.close();
      }
    }
    cronogramasOriginal = cronogramasTemp;
    atualizarHorarios(cronogramasTemp);
  }
}