import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class AddProductSchedulePage extends StatefulWidget {
  const AddProductSchedulePage(this.product);
  final ProductEcommerceEntity? product;

  @override
  _AddProductSchedulePageState createState() => _AddProductSchedulePageState();
}

class _AddProductSchedulePageState extends State<AddProductSchedulePage> {
  ScrollController controller = ScrollController();

  MaskedTextController abre = new MaskedTextController(
    mask: '00h00',
    cursorBehavior: CursorBehaviour.unlocked,
  );
  MaskedTextController fecha = new MaskedTextController(
    mask: '00h00',
    cursorBehavior: CursorBehaviour.unlocked,
  );

  bool domingo = false;
  bool segunda = false;
  bool terca = false;
  bool quarta = false;
  bool quinta = false;
  bool sexta = false;
  bool sabado = false;

  List horarios = [
    '00h00',
    '00h30',
    '01h00',
    '01h30',
    '02h00',
    '02h30',
    '03h00',
    '03h30',
    '04h00',
    '04h30',
    '05h00',
    '05h30',
    '06h00',
    '06h30',
    '07h00',
    '07h30',
    '08h00',
    '08h30',
    '09h00',
    '09h30',
    '10h00',
    '10h30',
    '11h00',
    '11h30',
    '12h00',
    '12h30',
    '13h00',
    '13h30',
    '14h00',
    '14h30',
    '15h00',
    '15h30',
    '16h00',
    '16h30',
    '17h00',
    '17h30',
    '18h00',
    '18h30',
    '19h00',
    '19h30',
    '20h00',
    '20h30',
    '21h00',
    '21h30',
    '22h00',
    '22h30',
    '23h00',
    '23h30'
  ];

  int horaInicio = 0;
  int minutoInicio = 0;
  int horaTermino = 23;
  int minutoTermino = 59;

  DateTime inicio = DateTime.now();
  DateTime termino = DateTime.now();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    abre.dispose();
    fecha.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: OwText(
          'Adicionar Turno'.toUpperCase(),
        ),
      ),
      body: SingleChildScrollView(
        controller: controller,
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 3),
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 5),
              padding: const EdgeInsets.all(20),
              width: MediaQuery.sizeOf(context).width,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                color: Theme.of(context).colorScheme.tertiaryContainer,
              ),
              child: Center(
                child: OwText(
                  'Adicione um horário e seus respectivos dias de funcionamento neste horário. Você pode adicionar vários horários diferentes.',
                  style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                        color:
                            Theme.of(context).colorScheme.onTertiaryContainer,
                      ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            const SizedBox(height: 20),
            Container(
              width: MediaQuery.sizeOf(context).width,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        OwText(
                          'Abertura',
                          style: Theme.of(context).textTheme.titleLarge,
                          textAlign: TextAlign.center,
                        ),
                        const OwText('HH:MM')
                      ],
                    ),
                    flex: 1,
                  ),
                  const SizedBox(width: 20),
                  Expanded(
                    child: Column(
                      children: [
                        OwText(
                          'Fechamento',
                          style: Theme.of(context).textTheme.titleLarge,
                          textAlign: TextAlign.center,
                        ),
                        const OwText('HH:MM')
                      ],
                    ),
                    flex: 1,
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.sizeOf(context).width,
              height: 180,
              child: Row(
                children: [
                  ScrollConfiguration(
                    behavior:
                        ScrollConfiguration.of(context).copyWith(dragDevices: {
                      PointerDeviceKind.touch,
                      PointerDeviceKind.mouse,
                    }),
                    child: Expanded(
                      child: CupertinoTheme(
                        data: CupertinoThemeData(
                          brightness: Theme.of(context).brightness,
                        ),
                        child: CupertinoDatePicker(
                          mode: CupertinoDatePickerMode.time,
                          use24hFormat: true,
                          minuteInterval: 1,
                          initialDateTime:
                              DateTime(2023, 1, 1, horaInicio, minutoInicio),
                          onDateTimeChanged: (DateTime dt) {
                            inicio = dt;
                          },
                        ),
                      ),
                      flex: 1,
                    ),
                  ),
                  const SizedBox(width: 15),
                  ScrollConfiguration(
                    behavior:
                        ScrollConfiguration.of(context).copyWith(dragDevices: {
                      PointerDeviceKind.touch,
                      PointerDeviceKind.mouse,
                    }),
                    child: Expanded(
                      child: CupertinoTheme(
                        data: CupertinoThemeData(
                          brightness: Theme.of(context).brightness,
                        ),
                        child: CupertinoDatePicker(
                          mode: CupertinoDatePickerMode.time,
                          use24hFormat: true,
                          minuteInterval: 1,
                          initialDateTime:
                              DateTime(2023, 1, 1, horaTermino, minutoTermino),
                          onDateTimeChanged: (DateTime dt) {
                            int segundos = 0;
                            if (dt.minute == 59) segundos = 59;
                            termino = dt.copyWith(
                              second: segundos,
                            );
                          },
                        ),
                      ),
                      flex: 1,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 15),
            ListTile(
              contentPadding: const EdgeInsets.all(0),
              title: const OwText('Domingo'),
              trailing: Switch(
                value: domingo,
                onChanged: (valor) {
                  setState(() {
                    domingo = valor;
                  });
                },
              ),
            ),
            ListTile(
              contentPadding: const EdgeInsets.all(0),
              title: const OwText('Segunda'),
              trailing: Switch(
                value: segunda,
                onChanged: (valor) {
                  setState(() {
                    segunda = valor;
                  });
                },
              ),
            ),
            ListTile(
              contentPadding: const EdgeInsets.all(0),
              title: const OwText('Terça'),
              trailing: Switch(
                value: terca,
                onChanged: (valor) {
                  setState(() {
                    terca = valor;
                  });
                },
              ),
            ),
            ListTile(
              contentPadding: const EdgeInsets.all(0),
              title: const OwText('Quarta'),
              trailing: Switch(
                value: quarta,
                onChanged: (valor) {
                  setState(() {
                    quarta = valor;
                  });
                },
              ),
            ),
            ListTile(
              contentPadding: const EdgeInsets.all(0),
              title: const OwText('Quinta'),
              trailing: Switch(
                value: quinta,
                onChanged: (valor) {
                  setState(() {
                    quinta = valor;
                  });
                },
              ),
            ),
            ListTile(
              contentPadding: const EdgeInsets.all(0),
              title: const OwText('Sexta'),
              trailing: Switch(
                value: sexta,
                onChanged: (valor) {
                  setState(() {
                    sexta = valor;
                  });
                },
              ),
            ),
            ListTile(
              contentPadding: const EdgeInsets.all(0),
              title: const OwText('Sábado'),
              trailing: Switch(
                value: sabado,
                onChanged: (valor) {
                  setState(() {
                    sabado = valor;
                  });
                },
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          onPressed: () async {
            final String startTime = inicio.toLocal().toIso8601String();
            final String endTime = termino.toLocal().toIso8601String();

            // ! Validar se esse inicio e termino estão entre os cronogramas já existentes em cronogramasTemp
            if (!domingo &&
                !segunda &&
                !terca &&
                !quarta &&
                !quinta &&
                !sexta &&
                !sabado) {
              OwBotToast.toast('Escolha um dia para este horário');
            } else if (termino.hour == 0 && termino.minute == 0) {
              OwBotToast.toast(
                  'Horário de término inválido\n(${endTime.substring(0, 5)})');
            } else if (termino.compareTo(inicio) <= 0) {
              OwBotToast.toast(
                  'Horário inválido\n(${startTime.substring(0, 5)} - ${endTime.substring(0, 5)})');
            } else {
              final List<ScheduleEntity> cronogramas = [];
              final startTemp = DateTime.now()
                  .copyWith(
                    hour: inicio.toLocal().hour,
                    minute: inicio.toLocal().minute,
                  )
                  .toUtc()
                  .toIso8601String();
              final endTemp = DateTime.now()
                  .copyWith(
                    hour: termino.toLocal().hour,
                    minute: termino.toLocal().minute,
                  )
                  .toUtc()
                  .toIso8601String();
              if (domingo) {
                final ScheduleEntity cronograma = ScheduleEntity(
                  dayWeek: 0,
                  startTime: startTemp,
                  endTime: endTemp,
                );
                cronogramas.add(cronograma);
              }
              if (segunda) {
                final ScheduleEntity cronograma = ScheduleEntity(
                  dayWeek: 1,
                  startTime: startTemp,
                  endTime: endTemp,
                );
                cronogramas.add(cronograma);
              }
              if (terca) {
                final ScheduleEntity cronograma = ScheduleEntity(
                  dayWeek: 2,
                  startTime: startTemp,
                  endTime: endTemp,
                );
                cronogramas.add(cronograma);
              }
              if (quarta) {
                final ScheduleEntity cronograma = ScheduleEntity(
                  dayWeek: 3,
                  startTime: startTemp,
                  endTime: endTemp,
                );
                cronogramas.add(cronograma);
              }
              if (quinta) {
                final ScheduleEntity cronograma = ScheduleEntity(
                  dayWeek: 4,
                  startTime: startTemp,
                  endTime: endTemp,
                );
                cronogramas.add(cronograma);
              }
              if (sexta) {
                final ScheduleEntity cronograma = ScheduleEntity(
                  dayWeek: 5,
                  startTime: startTemp,
                  endTime: endTemp,
                );
                cronogramas.add(cronograma);
              }
              if (sabado) {
                final ScheduleEntity cronograma = ScheduleEntity(
                  dayWeek: 6,
                  startTime: startTemp,
                  endTime: endTemp,
                );
                cronogramas.add(cronograma);
              }
              context.pop(cronogramas);
            }
          },
          labelText: 'Adicionar turno',
        ),
      ),
    );
  }
}
