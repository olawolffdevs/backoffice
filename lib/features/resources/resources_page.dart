import 'package:backoffice/features/home/home_dashboard_page.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ResourcesPage extends StatefulWidget {
  const ResourcesPage({super.key});

  @override
  State<ResourcesPage> createState() => _ResourcesPageState();
}

class _ResourcesPageState extends State<ResourcesPage> {
  List<ItemFunctionalities> functionalities = [];

  @override
  void initState() {
    super.initState();
    list();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: false,
        title: const Text('Recursos'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _listFunctionalities(),
        ],
      ),
    );
  }

  Widget _listFunctionalities() {
    return LayoutBuilder(builder: (context, constraints) {
      return Material(
        child: Container(
          padding: appSM.isWeb
              ? const EdgeInsets.fromLTRB(50, 50, 50, 0)
              : const EdgeInsets.fromLTRB(25, 25, 25, 0),
          constraints: constraints,
          child: OwGrid.builder(
            constraints: constraints,
            itemCount: functionalities.length,
            numbersInRowAccordingToWidgth: const [
              150,
              325,
              500,
              650,
              900,
              1250,
              1500,
              1800,
              2100
            ],
            itemBuilder: (context, index) {
              return RoundCard(
                color: ColorSystem(context: context, type: 'primaryContainer'),
                spotlight: const SizedBox(),
                title: functionalities[index].title,
                child: Icon(
                  functionalities[index].icon,
                  color: Theme.of(context).colorScheme.primary,
                ),
                onPressed: () async {
                  context.push(functionalities[index].route);
                },
              );
            },
          ),
        ),
      );
    });
  }

  void list() {
    final item = WhiteLabelEntity.current?.setting.functionality;
    if (item?.plans ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Planos',
            icon: EvaIcons.gridOutline,
            route: '/${WhiteLabelEntity.current?.id}/dashboard/plans'),
      );
    }
    if (item?.companies ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Empresas',
            icon: EvaIcons.briefcaseOutline,
            route: '/${WhiteLabelEntity.current?.id}/dashboard/company'),
      );
    }
    if (item?.users ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Usuários',
            icon: EvaIcons.personOutline,
            route: '/${WhiteLabelEntity.current?.id}/dashboard/user'),
      );
    }
    if (item?.modules ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Módulos',
            icon: EvaIcons.gridOutline,
            route: '/${WhiteLabelEntity.current?.id}/dashboard/modules'),
      );
    }
    if (item?.notifications ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Notificações',
            icon: EvaIcons.bellOutline,
            route: '/${WhiteLabelEntity.current?.id}/dashboard/notifications'),
      );
    }
    if (item?.knowledgeBase ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Base de conhecimento',
            icon: EvaIcons.questionMarkCircleOutline,
            route: '/${WhiteLabelEntity.current?.id}/dashboard/knowledge-base'),
      );
    }
    if (item?.financialEducation ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Educação Financeira',
            icon: EvaIcons.videoOutline,
            route:
                '/${WhiteLabelEntity.current?.id}/dashboard/financial-education'),
      );
    }
    if (item?.groups ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Grupos',
            icon: EvaIcons.atOutline,
            route: '/${WhiteLabelEntity.current?.id}/dashboard/groups'),
      );
    }
    if (item?.ecommerce ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Produtos',
            icon: EvaIcons.gridOutline,
            route: '/${WhiteLabelEntity.current?.id}/ecommerce/product/home'),
      );
    }
    if (item?.ecommerce ?? false) {
      functionalities.add(
        ItemFunctionalities(
            title: 'Categorias',
            icon: EvaIcons.gridOutline,
            route:
                '/${WhiteLabelEntity.current?.id}/ecommerce/product/category'),
      );
    }
    functionalities.add(
      ItemFunctionalities(
          title: 'Gestão de mídia',
          icon: EvaIcons.imageOutline,
          route: '/${WhiteLabelEntity.current?.id}/dashboard/media-manager'),
    );
  }
}
