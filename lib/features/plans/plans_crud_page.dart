import 'package:backoffice/ui/components/alerta_simples.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class PlansCrudPage extends StatefulWidget {
  const PlansCrudPage({this.plan, Key? key}) : super(key: key);
  final PlanEntity? plan;
  @override
  State<PlansCrudPage> createState() => _PlansCrudPageState();
}

class _PlansCrudPageState extends State<PlansCrudPage> {
  TextEditingController name = TextEditingController();
  TextEditingController description = TextEditingController();
  MoneyMaskedTextController amount = MoneyMaskedTextController();

  String interval = 'month';
  String intervalCount = '1';
  String type = 'user';

  @override
  void initState() {
    super.initState();
    if (widget.plan != null) {
      name.text = widget.plan!.name;
      description.text = widget.plan!.description ?? '';
      amount.text = (widget.plan!.price).toString();
      interval = widget.plan!.interval;
      intervalCount = widget.plan!.intervalCount.toString();
      type = widget.plan!.type.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes do plano'),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Visibility(
              visible: widget.plan != null,
              child: OwButton.outline(
                margin: const EdgeInsets.fromLTRB(25, 10, 25, 0),
                labelText: 'Copiar ID do plano',
                onPressed: () {
                  Clipboard.setData(ClipboardData(text: widget.plan!.id ?? ''));
                  OwBotToast.toast('Id copiado.');
                },
              ),
            ),
            OwTextField(
              controller: name,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Nome do plano',
              hintText: 'Digite aqui...',
            ),
            OwTextField(
              controller: description,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Descrição do plano',
              hintText: 'Digite aqui...',
              maxLines: 8,
            ),
            OwTextField(
              controller: amount,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Valor do preço',
              hintText: 'Digite aqui...',
              keyboardType: TextInputType.phone,
            ),
            const SizedBox(height: 25),
            const OwDivider(),
            const SizedBox(height: 25),
            OwText(
              'Intervalo da cobrança',
              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 10),
            ),
            OwText(
              'Frequência da recorrência. Valores possíveis: dia, semana, mês ou ano.',
              style: Theme.of(context).textTheme.bodyMedium,
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 10),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Wrap(
                runSpacing: 8,
                spacing: 8,
                children: ['day', 'week', 'month', 'year']
                    .map((e) => FilterChip(
                          label: Text('$e'),
                          selected: e == interval,
                          onSelected: (value) {
                            setState(() {
                              interval = e;
                            });
                          },
                        ))
                    .toList(),
              ),
            ),
            const SizedBox(height: 25),
            OwText(
              'Número de intervalos entre cada cobrança da assinatura.',
              style: Theme.of(context).textTheme.bodyMedium,
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 10),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Wrap(
                runSpacing: 8,
                spacing: 8,
                children: ['1', '2', '6', '15', '30']
                    .map((e) => FilterChip(
                          label: Text('$e'),
                          selected: e == intervalCount,
                          onSelected: (value) {
                            setState(() {
                              intervalCount = e;
                            });
                          },
                        ))
                    .toList(),
              ),
            ),
            const SizedBox(height: 25),
            OwText(
              'Tipo do plano. Para PF ou PJ.',
              style: Theme.of(context).textTheme.bodyMedium,
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 10),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Wrap(
                runSpacing: 8,
                spacing: 8,
                children: ['user', 'company']
                    .map((e) => FilterChip(
                          label: Text('$e'),
                          selected: e == type,
                          onSelected: (value) {
                            setState(() {
                              type = e;
                            });
                          },
                        ))
                    .toList(),
              ),
            ),
            const SizedBox(height: 25),
            Visibility(
              visible: widget.plan != null,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const OwDivider(),
                  // InkWell(
                  //   onTap: () async {
                  //     await context.push(
                  //       '/${WhiteLabelEntity.current?.id}/dashboard/plans/${widget.plan!.id}/items',
                  //       extra: widget.plan!,
                  //     );
                  //   },
                  //   child: Container(
                  //     padding: const EdgeInsets.symmetric(
                  //       horizontal: 25,
                  //       vertical: 25,
                  //     ),
                  //     child: Row(
                  //       children: [
                  //         Expanded(
                  //           child: Column(
                  //             crossAxisAlignment: CrossAxisAlignment.stretch,
                  //             children: [
                  //               Text(
                  //                 'Itens do plano',
                  //                 style: Theme.of(context).textTheme.titleLarge,
                  //               ),
                  //               const SizedBox(height: 6),
                  //               const Text('Defina os itens de cada plano.'),
                  //             ],
                  //           ),
                  //         ),
                  //         const SizedBox(width: 25),
                  //         const Icon(Icons.keyboard_arrow_right),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  // const OwDivider(),
                  Container(
                    padding: const EdgeInsets.all(25),
                    margin: const EdgeInsets.all(25),
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondaryContainer,
                      borderRadius: const BorderRadius.all(Radius.circular(15)),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          "É plano promocional? ${widget.plan?.promotionalPlan == true ? "Sim" : "Não"}",
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        const SizedBox(height: 15),
                        Text(
                          'Dias de teste gratuito do plano: ${widget.plan?.trialPeriodDays ?? 0}',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        const SizedBox(height: 15),
                        Text(
                          'Formas de pagamento:',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        ListView.builder(
                          itemCount: widget.plan?.paymentMethods?.length ?? 0,
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Text(
                              '${widget.plan?.paymentMethods?[index]}',
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwText(
              'A cobrança vai ser a cada $intervalCount [$interval].',
              padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
            ),
            OwButton(
              margin: const EdgeInsets.fromLTRB(25, 10, 25, 25),
              labelText: 'Salvar',
              onPressed: _onPressed,
            ),
          ],
        ),
      ),
    );
  }

  void _onPressed() async {
    if (name.text.trim().isEmpty || description.text.trim().isEmpty) {
      showDialog(
        context: context,
        builder: (context) => const AlertaSimples(
          'Existem um ou mais campos que precisam ser marcados.',
        ),
      );
    } else {
      if (widget.plan != null) {
        final PlanEntity notification = widget.plan!.copyWith(
          name: name.text.trim(),
          description: description.text.trim(),
          price: int.parse(OwFormat.removerCaracteres(amount.text, '.,-')),
        );

        try {
          OwBotToast.loading();
          final response = await Api.plans.update(
            id: notification.id!,
            data: notification,
          );
          if (mounted) {
            context.pop(response);
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Plano atualizado com sucesso.',
              ),
            );
          }
        } catch (_) {
          OwBotToast.close();
          if (mounted) {
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Ocorreu um erro ao atualizar o plano.',
              ),
            );
          }
        }
      } else {
        final PlanEntity notification = PlanEntity(
          name: name.text.trim(),
          description: description.text.trim(),
          price: int.parse(OwFormat.removerCaracteres(amount.text, '.,-')),
          interval: interval,
          intervalCount: int.parse(intervalCount),
          type: type,
        );
        try {
          final response = await Api.plans.add(data: notification);
          if (mounted) {
            context.pop(response);
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Plano cadastrado com sucesso.',
              ),
            );
          }
        } catch (e) {
          if (mounted) {
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Ocorreu um erro ao cadastrar o plano.',
              ),
            );
          }
        }
      }
    }
  }
}
