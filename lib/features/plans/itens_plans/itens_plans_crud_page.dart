import 'package:backoffice/ui/components/alerta_simples.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ItensPlansCrudPage extends StatefulWidget {
  const ItensPlansCrudPage({super.key, this.item, required this.planId});
  final PlanItemEntity? item;
  final String planId;

  @override
  State<ItensPlansCrudPage> createState() => _ItensPlansCrudPageState();
}

class _ItensPlansCrudPageState extends State<ItensPlansCrudPage> {
  TextEditingController name = TextEditingController();
  TextEditingController index = TextEditingController();
  bool isVisible = true;

  @override
  void initState() {
    super.initState();
    if (widget.item != null) {
      name.text = widget.item!.description;
      index.text = (widget.item!.index ?? 0).toString();
      isVisible = widget.item!.icon.toString().contains('checkmark');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes do item'),
        actions: [
          Visibility(
            visible: widget.item != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertaSimples(
                      'Deseja apagar este item?',
                      conteudo:
                          'Essa ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                      textoBotaoPrimario: 'apagar',
                      funcaoBotaoPrimario: () {
                        Navigator.pop(context, true);
                      },
                      textoBotaoSecundario: 'manter',
                    );
                  },
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.plans.items.delete(id: widget.item!.id!);
                    if (mounted) {
                      context.go('/${WhiteLabelEntity.current?.id}/dashboard');
                    }
                  } catch (_) {
                    OwBotToast.toast('Erro ao remover item.');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              controller: name,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Nome do item',
            ),
            const SizedBox(
              height: 25,
            ),
            const OwDivider(),
            InkWell(
              onTap: () async {
                setState(() {
                  isVisible = !isVisible;
                });
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 25,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            'O item possui no plano?',
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                          const SizedBox(height: 6),
                          const Text('Caso contrário ficará um ícone de X.'),
                        ],
                      ),
                    ),
                    const SizedBox(width: 25),
                    Switch(
                      onChanged: (valor) async {
                        setState(() {
                          isVisible = !isVisible;
                        });
                      },
                      value: isVisible,
                    ),
                  ],
                ),
              ),
            ),
            const OwDivider(),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          labelText: 'Salvar',
          onPressed: _onPressed,
        ),
      ),
    );
  }

  void _onPressed() async {
    if (name.text.trim().isEmpty) {
      showDialog(
        context: context,
        builder: (context) => const AlertaSimples(
          'Existem um ou mais campos que precisam ser preenchidos.',
        ),
      );
    } else {
      if (widget.item != null) {
        final PlanItemEntity notification = widget.item!.copyWith(
          description: name.text.trim(),
          icon: isVisible
              ? 'EvaIcons.checkmarkCircle2Outline'
              : 'EvaIcons.closeCircleOutline',
          index: int.tryParse(index.text) ?? 0,
          planId: widget.planId,
        );

        try {
          OwBotToast.loading();
          final response = await Api.plans.items.update(
            id: notification.id!,
            data: notification,
          );
          if (mounted) {
            context.pop(response);
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Item atualizado com sucesso.',
              ),
            );
          }
        } catch (_) {
          OwBotToast.close();
          if (mounted) {
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Ocorreu um erro ao atualizar o item.',
              ),
            );
          }
        }
      } else {
        final PlanItemEntity notification = PlanItemEntity(
          description: name.text.trim(),
          icon: isVisible
              ? 'EvaIcons.checkmarkCircle2Outline'
              : 'EvaIcons.closeCircleOutline',
          index: int.tryParse(index.text) ?? 0,
          planId: widget.planId,
        );
        try {
          final response = await Api.plans.items.add(data: notification);
          if (mounted) {
            context.pop(response);
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Item cadastrado com sucesso.',
              ),
            );
          }
        } catch (e) {
          if (mounted) {
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Ocorreu um erro ao cadastrar o item.',
              ),
            );
          }
        }
      }
    }
  }
}
