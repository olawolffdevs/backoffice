import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:flutter/material.dart';

class ItensPlansHomePage extends StatefulWidget {
  const ItensPlansHomePage({super.key, required this.plan});
  final PlanEntity plan;
  @override
  State<ItensPlansHomePage> createState() => _ItensPlansHomePageState();
}

class _ItensPlansHomePageState extends State<ItensPlansHomePage> {
  List<PlanItemEntity>? planItems;

  @override
  void initState() {
    super.initState();
    planItems = widget.plan.planItem;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Itens do plano'),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListView.separated(
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
              itemCount: planItems?.length ?? 0,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              separatorBuilder: (context, index) {
                return const SizedBox(height: 15);
              },
              itemBuilder: (context, index) {
                return Ink(
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.secondaryContainer,
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                  ),
                  child: InkWell(
                    onTap: () async {
                      final result = await context.push(
                        '/${WhiteLabelEntity.current?.id}/dashboard/plans/${widget.plan.id}/items/${planItems?[index].id}',
                        extra: planItems?[index],
                      );
                      if (result is PlanItemEntity) {
                        planItems![index] = result;
                        setState(() {});
                      }
                    },
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 25, vertical: 15),
                      child: Row(
                        children: [
                          Icon(IconAdapter.getIcon(planItems![index].icon)),
                          const SizedBox(width: 25),
                          Expanded(
                            child: Text('${planItems?[index].description}'),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        icon: const Icon(
          Icons.add,
        ),
        label: const Text('Novo item'),
        onPressed: () async {
          final retorno = await context.push(
            '/${WhiteLabelEntity.current?.id}/dashboard/plans/${widget.plan.id}/items/new',
          );
          if (retorno is PlanItemEntity) {
            planItems!.insert(0, retorno);
            setState(() {});
          }
        },
      ),
    );
  }
}
