import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class PlansHomePage extends StatefulWidget {
  const PlansHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  PlansHomePageState createState() => PlansHomePageState();
}

class PlansHomePageState extends State<PlansHomePage> {
  List<PlanEntity>? plans;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Planos'),
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        icon: const Icon(
          Icons.add,
        ),
        label: const Text('Novo plano'),
        onPressed: () async {
          final retorno = await context.push(
            '/${WhiteLabelEntity.current?.id}/dashboard/plans/new',
          );
          if (retorno is PlanEntity) {
            plans!.insert(0, retorno);
            setState(() {});
          }
        },
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Visibility(
              visible: plans != null,
              replacement: Container(
                constraints: const BoxConstraints(
                  minHeight: 250,
                ),
                child: const Center(
                  child: CircularProgressIndicator(),
                ),
              ),
              child: Visibility(
                visible: plans?.isNotEmpty ?? false,
                replacement: Container(
                  margin: appSM.isWeb
                      ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                      : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  padding: const EdgeInsets.all(25),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Theme.of(context).colorScheme.outlineVariant,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                  ),
                  child: const Center(
                    child: Text(
                        'Você ainda não cadastrou nenhum plano. Adicione o primeiro para vincular as funcionalidades.'),
                  ),
                ),
                child: ListView.separated(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  padding: appSM.isWeb
                      ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                      : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  itemCount: plans?.length ?? 0,
                  separatorBuilder: (context, index) {
                    return const SizedBox(height: 25);
                  },
                  itemBuilder: (context, index) {
                    return Ink(
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(15)),
                        color: Theme.of(context).colorScheme.secondaryContainer,
                      ),
                      child: InkWell(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(15)),
                        onTap: () async {
                          final result = await context.push(
                            '/${WhiteLabelEntity.current?.id}/dashboard/plans/${plans![index].id}',
                            extra: plans![index],
                          );
                          if (result is PlanEntity) {
                            plans![index] = result;
                            setState(() {});
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(25),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                plans![index].name,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Text(
                                '${plans![index].description ?? 'Sem descrição'}',
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Text(
                                OwFormat.toRealCurrency(
                                  plans![index].price,
                                  isInt: true,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void callApi([skip = 0]) async {
    try {
      plans = await Api.plans.list(query: {'skip': plans?.length ?? 0});
    } catch (_) {}
    setState(() {});
  }
}
