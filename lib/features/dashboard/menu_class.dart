import 'package:flutter/material.dart';

class MenuSectionWidget {
  MenuSectionWidget({
    required this.icon,
    required this.title,
    required this.isExpanded,
    required this.items,
  });
  IconData icon;
  String title;
  bool isExpanded;
  List<MenuItemWidget> items;
}

class MenuItemWidget {
  MenuItemWidget({
    required this.title,
    required this.icon,
    required this.page,
    this.count,
  });
  String? count;
  String title;
  IconData icon;
  Widget page;
}
