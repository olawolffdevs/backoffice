import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class MenuWebWidget extends StatelessWidget {
  const MenuWebWidget({super.key, this.sections});
  final List<MenuSection>? sections;

  @override
  Widget build(BuildContext context) {
    return sections?.isNotEmpty ?? false
        ? Container(
            width: 260,
            decoration: BoxDecoration(
              border: Border(
                right: BorderSide(
                  width: 1,
                  color: Theme.of(context).colorScheme.secondaryContainer,
                ),
              ),
            ),
            constraints: const BoxConstraints(maxWidth: 400, minWidth: 140),
            height: MediaQuery.sizeOf(context).height,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MenuWebSide(sections: sections!),
                ],
              ),
            ),
          )
        : const SizedBox();
  }
}

class MenuSection {
  MenuSection({required this.title, required this.items});
  final String title;
  final List<MenuItem> items;
}

class MenuItem {
  MenuItem({
    required this.title,
    this.isExpanded = false,
    this.icon,
    this.notificationCount,
    this.subItems = const [],
    this.selected = false,
    this.onPressed,
    this.page,
  });
  bool isExpanded;
  final String title;
  final IconData? icon;
  final int? notificationCount;
  final List<MenuItem> subItems;
  final bool selected;
  final Widget? page;
  final Function()? onPressed;
}

class MenuWebSide extends StatefulWidget {
  const MenuWebSide({super.key, required this.sections});
  final List<MenuSection> sections;

  @override
  State<MenuWebSide> createState() => _MenuWebSideState();
}

class _MenuWebSideState extends State<MenuWebSide> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: widget.sections.length,
      itemBuilder: (context, index) {
        final section = widget.sections[index];
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 24, 24, 8),
              child: Text(
                section.title,
                style: Theme.of(context).textTheme.titleSmall?.copyWith(
                      fontWeight: FontWeight.w300,
                    ),
              ),
            ),
            ListView.builder(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: section.items.length,
              itemBuilder: (context, indexItem) {
                final item = section.items[indexItem];
                return Observer(
                  builder: (context) {
                    return Ink(
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(Radius.circular(16)),
                        color: item.isExpanded
                            ? Theme.of(context).colorScheme.secondaryContainer
                            : appSM.selectedIndex == _getIndex(item)
                                ? Theme.of(context).colorScheme.primary.withOpacity(.15)
                                : null,
                      ),
                      child: InkWell(
                        borderRadius: const BorderRadius.all(Radius.circular(16)),
                        onTap: () {
                          if (item.subItems.isNotEmpty) {
                            setState(() {
                              item.isExpanded = !item.isExpanded;
                            });
                          } else {
                            appSM.setSelectedIndex(_getIndex(item));
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(16),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  if (item.icon != null)
                                    Icon(
                                      item.icon,
                                      color: Theme.of(context).colorScheme.onSurface,
                                    ),
                                  const SizedBox(width: 12),
                                  Expanded(
                                    child: Text(
                                      item.title,
                                      style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                            fontWeight: FontWeight.bold,
                                            color: Theme.of(context).colorScheme.onSurface,
                                          ),
                                    ),
                                  ),
                                  if (item.subItems.isNotEmpty)
                                    Icon(
                                      item.isExpanded ? Icons.keyboard_arrow_up_rounded : Icons.keyboard_arrow_down_rounded,
                                      color: Theme.of(context).colorScheme.onSurface,
                                    ),
                                ],
                              ),
                              if (item.subItems.isNotEmpty && item.isExpanded)
                                ListView.builder(
                                  padding: const EdgeInsets.symmetric(horizontal: 16),
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: item.subItems.length,
                                  itemBuilder: (context, indexSubItem) {
                                    final subItem = item.subItems[indexSubItem];
                                    return Padding(
                                      padding: const EdgeInsets.only(top: 8),
                                      child: Observer(
                                        builder: (context) {
                                          return Ink(
                                            decoration: BoxDecoration(
                                              borderRadius: const BorderRadius.all(Radius.circular(16)),
                                              color: subItem.isExpanded
                                                  ? Theme.of(context).colorScheme.secondaryContainer
                                                  : appSM.selectedIndex == _getIndex(subItem)
                                                    ? Theme.of(context).colorScheme.primary.withOpacity(.15)
                                                    : null,
                                            ),
                                            child: InkWell(
                                              borderRadius: const BorderRadius.all(Radius.circular(16)),
                                              onTap: () {
                                                if (subItem.subItems.isNotEmpty) {
                                                  setState(() {
                                                    subItem.isExpanded = !subItem.isExpanded;
                                                  });
                                                } else {
                                                  appSM.setSelectedIndex(_getIndex(subItem));
                                                }
                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.all(16),
                                                child: Row(
                                                  children: [
                                                    if (subItem.icon != null)
                                                      Icon(
                                                        subItem.icon,
                                                        color: Theme.of(context).colorScheme.onSurface,
                                                      ),
                                                    const SizedBox(width: 12),
                                                    Expanded(
                                                      child: Text(
                                                        subItem.title,
                                                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                                              fontWeight: FontWeight.bold,
                                                              color: Theme.of(context).colorScheme.onSurface,
                                                            ),
                                                      ),
                                                    ),
                                                    if (subItem.subItems.isNotEmpty)
                                                      Icon(
                                                        subItem.isExpanded ? Icons.keyboard_arrow_up_rounded : Icons.keyboard_arrow_down_rounded,
                                                        color: Theme.of(context).colorScheme.onSurface,
                                                      ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        }
                                      ),
                                    );
                                  },
                                ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ],
        );
      },
    );
  }

  int _getIndex(MenuItem item) {
    int index = 0;
    for (var section in widget.sections) {
      for (var menuItem in section.items) {
        if (menuItem == item) {
          return index;
        }
        for (var subItem in menuItem.subItems) {
          index++;
          if (subItem == item) {
            return index;
          }
        }
        index++;
      }
    }
    return -1;
  }
}

class MenuDashboardMobile extends StatefulWidget {
  const MenuDashboardMobile({
    Key? key,
    required this.menu,
  }) : super(key: key);
  final List<MenuSection> menu;

  @override
  State<MenuDashboardMobile> createState() => _MenuDashboardMobileState();
}

class _MenuDashboardMobileState extends State<MenuDashboardMobile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(),
      body: ListView.builder(
        itemCount: widget.menu.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              if(index != 0) const OwDivider(),
              InkWell(
                // onTap: () {
                //   menu[index].isExpanded = !menu[index].isExpanded;
                // },
                child: Container(
                  padding: const EdgeInsets.fromLTRB(24, 24, 24, 18),
                  child: OwText(
                    widget.menu[index].title,
                    style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                      color: Theme.of(context).colorScheme.onSurfaceVariant,
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.menu[index].items.isNotEmpty,
                child: const Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // Column(
                    //   children: widget.menu[index].items.map((e) {
                    //     return Observer(builder: (context) {
                    //       return MenuWebButtonWidget(
                    //         label: e.title,
                    //         icon: e.icon,
                    //         selected: getIndex(e) == appSM.selectedIndex,
                    //         onPressed: () {
                    //           appSM.setSelectedIndex(getIndex(e));
                    //           Navigator.pop(context);
                    //         },
                    //       );
                    //     });
                    //   }).toList(),
                    // ),
                    const SizedBox(height: 12),
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }

  int getIndex(MenuItem child) {
    int index = 0;
    for (var i = 0; i < widget.menu.length; i++) {
      for (var j = 0; j < widget.menu[i].items.length; j++) {
        if (widget.menu[i].items[j] == child) {
          return index;
        }
        index++;
      }
    }
    return 0;
  }
}