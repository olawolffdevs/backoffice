import 'package:backoffice/features/company/company_home_page.dart';
import 'package:backoffice/features/contacts/contacts_home_page.dart';
import 'package:backoffice/features/course/instructor_home_page.dart';
import 'package:backoffice/features/financial_education/financial_education_home_page.dart';
import 'package:backoffice/features/groups/groups_home_page.dart';
import 'package:backoffice/features/home/home_dashboard_page.dart';
import 'package:backoffice/features/inbox/inbox_home_page.dart';
import 'package:backoffice/features/knowledge_base/knowledge_base_home_page.dart';
import 'package:backoffice/features/modules/modules_home_page.dart';
import 'package:backoffice/features/notifications/notifications_home_page.dart';
import 'package:backoffice/features/orders/orders_home_page.dart';
import 'package:backoffice/features/plans/plans_home_page.dart';
import 'package:backoffice/features/product_category/product_category_home_page.dart';
import 'package:backoffice/features/products/products_home_page.dart';
import 'package:backoffice/features/statistics/statistics_page.dart';
import 'package:backoffice/features/tasks/tasks_home_page.dart';
import 'package:backoffice/features/team/team_page.dart';
import 'package:backoffice/features/users/users_home_page.dart';
import 'package:backoffice/features/vouchers/vouchers_home_page.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:lazy_load_indexed_stack/lazy_load_indexed_stack.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import '../course/category_home_page.dart';
import '../course/course_home_page.dart';
import '../documents/documents_home_page.dart';
import '../ecommerce/ecommerce_page.dart';
import '../ecommerce/max_participants_by_schedule_page.dart';
import '../white_label/white_label_home_page.dart';
import 'app_bar_widget.dart';
import 'menu_web_widget.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);
  @override
  DashboardPageState createState() => DashboardPageState();
}

class DashboardPageState extends State<DashboardPage> with AutomaticKeepAliveClientMixin<DashboardPage> {
  @override
  bool get wantKeepAlive => true;

  final List<MenuSection> sections = [
    MenuSection(
      title: 'DASHBOARD',
      items: [
        MenuItem(
          title: 'Início',
          icon: EvaIcons.homeOutline,
          page: const HomeDashboardPage(automaticallyImplyLeading: false),
        ),
        MenuItem(
          title: 'Estatísticas',
          icon: EvaIcons.barChart2Outline,
          page: const StatisticsPage(automaticallyImplyLeading: false),
        ),
      ],
    ),
    MenuSection(
      title: 'CRM',
      items: [
        MenuItem(
          title: 'Usuários',
          icon: EvaIcons.personOutline,
          page: const UsersHomePage(automaticallyImplyLeading: false),
        ),
        MenuItem(
          title: 'Empresas',
          icon: EvaIcons.briefcaseOutline,
          page: const CompanyHomePage(automaticallyImplyLeading: false),
        ),
        MenuItem(
          title: 'Conversas',
          icon: EvaIcons.messageCircleOutline,
          page: const InboxHomePage(automaticallyImplyLeading: false),
        ),
        MenuItem(
          title: 'Notificações',
          icon: EvaIcons.bellOutline,
          page: const NotificationHomePage(automaticallyImplyLeading: false),
        ),
        MenuItem(
          title: '${WhiteLabelEntity.current?.name}',
          icon: EvaIcons.pantoneOutline,
          subItems: [
            MenuItem(
              title: 'Editar',
              icon: EvaIcons.edit2Outline,
              page: const WhiteLabelHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Equipe',
              icon: EvaIcons.peopleOutline,
              page: const TeamPage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Documentos internos',
              icon: EvaIcons.fileOutline,
              page: const DocumentsHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Árvore de links',
              icon: EvaIcons.link2Outline,
              page: const Center(child: Text('Em breve')),
            ),
            MenuItem(
              title: 'Tarefas',
              icon: EvaIcons.listOutline,
              page: const TasksHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Contatos',
              icon: EvaIcons.attach2Outline,
              page: const ContactsHomePage(automaticallyImplyLeading: false),
            ),
          ],
        ),
      ],
    ),
    MenuSection(
      title: 'Funcionalidades',
      items: [
        MenuItem(
          title: 'Módulos',
          icon: EvaIcons.cubeOutline,
          page: const ModulesHomePage(automaticallyImplyLeading: false),
        ),
        MenuItem(
          title: 'Base de Conhecimento',
          icon: EvaIcons.fileTextOutline,
          page: const KnowledgeBaseHomePage(automaticallyImplyLeading: false),
          subItems: [
            MenuItem(
              title: 'Categorias',
              icon: EvaIcons.gridOutline,
            ),
            MenuItem(
              title: 'Perguntas e respostas',
              icon: EvaIcons.fileTextOutline,
            ),
          ],
        ),
        MenuItem(
          title: 'Educação Financeira',
          icon: EvaIcons.videoOutline,
          page: const FinancialEducationHomePage(automaticallyImplyLeading: false),
          subItems: [
            MenuItem(
              title: 'Categorias',
              icon: EvaIcons.gridOutline,
            ),
            MenuItem(
              title: 'Vídeos',
              icon: EvaIcons.videoOutline,
            ),
          ],
        ),
        MenuItem(
          title: 'Cursos',
          icon: EvaIcons.bookOpenOutline,
          subItems: [
            MenuItem(
              title: 'Estudantes',
              icon: EvaIcons.personOutline,
              page: const Center(child: Text('Em breve')),
            ),
            MenuItem(
              title: 'Instrutores',
              icon: EvaIcons.personOutline,
              page: const InstructorCoursesHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Cursos',
              icon: EvaIcons.bookOutline,
              page: const CoursesHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Categorias',
              icon: EvaIcons.gridOutline,
              page: const CategoriesCoursesHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Pagamentos',
              icon: EvaIcons.creditCardOutline,
              page: const Center(child: Text('Em breve')),
            ),
          ],
        ),
        MenuItem(
          title: 'Planos e Preços',
          icon: EvaIcons.flagOutline,
          subItems: [
            MenuItem(
              title: 'Planos',
              icon: EvaIcons.awardOutline,
              page: const PlansHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Assinaturas',
              icon: EvaIcons.syncOutline,
              page: const Center(child: Text('Em breve')),
            ),
            MenuItem(
              title: 'Cobranças',
              icon: EvaIcons.archiveOutline,
              page: const Center(child: Text('Em breve')),
            ),
            MenuItem(
              title: 'Cupons',
              icon: EvaIcons.giftOutline,
              page: const VouchersHomePage(automaticallyImplyLeading: false),
            ),
          ],
        ),
        MenuItem(
          title: 'Vendas e Agendamentos',
          icon: EvaIcons.percentOutline,
          subItems: [
            MenuItem(
              title: 'Produtos e serviços',
              icon: EvaIcons.shoppingBagOutline,
              page: const ProductsHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Categorias',
              icon: EvaIcons.gridOutline,
              page: const ProductCategoryHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Histórico',
              icon: EvaIcons.inboxOutline,
              page: const OrdersHomePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Ajustes',
              icon: EvaIcons.clockOutline,
              page: const MaxParticipantsBySchedulePage(automaticallyImplyLeading: false),
            ),
            MenuItem(
              title: 'Estabelecimento',
              icon: EvaIcons.edit2Outline,
              page: const PanelEcommerceHomePage(automaticallyImplyLeading: false),
            ),
          ],
        ),
        MenuItem(
          title: 'Grupos',
          icon: EvaIcons.layersOutline,
          page: const GroupsHomePage(automaticallyImplyLeading: false),
        ),
      ],
    ),
    MenuSection(
      title: 'Outros',
      items: [
        MenuItem(
          title: 'Configurações',
          icon: EvaIcons.settings2Outline,
        ),
        MenuItem(
          title: 'Roadmap',
          icon: EvaIcons.layoutOutline,
        ),
        MenuItem(
          title: 'Tutoriais',
          icon: EvaIcons.bookOutline,
        ),
      ],
    ),
  ];

  @override
  Widget build(BuildContext context) {
    super.build(context);
    checkScreenSizeChange(context);
    return Observer(builder: (context) {
      return Scaffold(
        body: Column(
          children: [
            Visibility(
              visible: appSM.isWeb,
              child: const AppBarCompanyWidget(),
            ),
            Expanded(
              child: Row(
                children: [
                  Visibility(
                    visible: appSM.isWeb,
                    child: MenuWebWidget(
                      sections: sections,
                    ),
                  ),
                  Expanded(
                    child: LayoutBuilder(
                      builder: (context, constraints) {
                        return MediaQuery(
                          data: MediaQueryData(
                            padding: MediaQuery.of(context).viewPadding,
                            size: Size(
                              constraints.maxWidth,
                              constraints.maxHeight,
                            ),
                          ),
                          child: Observer(
                            builder: (context) {
                              return LazyLoadIndexedStack(
                                index: appSM.selectedIndex,
                                children: screens(),
                              );
                            }
                          ),
                        );
                      }
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        floatingActionButton: Observer(
          builder: (context) {
            return Visibility(
              visible: !appSM.isWeb,
              child: Padding(
                padding: const EdgeInsets.only(top: 4, right: 16),
                child: IconButton(
                  onPressed: () async {
                    await openLinkPage(
                      context,
                      OpenLinkPageParams.basic(
                        child: MenuDashboardMobile(
                          menu: sections,
                        ),
                      ),
                    );
                  },
                  icon: const Icon(Icons.menu_outlined),
                ),
              ),
            );
          },
        ),
      );
    });
  }

  List<Widget> screens() {
    final _screens = <Widget>[];
    for (var section in sections) {
      for (var item in section.items) {
        _screens.add(item.page ?? const SizedBox());
        _addSubItems(item, _screens);
      }
    }
    return _screens;
  }

  void _addSubItems(MenuItem item, List<Widget> screens) {
    for (var subItem in item.subItems) {
      screens.add(subItem.page ?? const SizedBox());
      if (subItem.subItems.isNotEmpty) {
        _addSubItems(subItem, screens); // Chamada recursiva para subitens
      }
    }
  }
}