import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../ui/components/icon_notification.dart';

class AppBarCompanyWidget extends StatefulWidget {
  const AppBarCompanyWidget({Key? key}) : super(key: key);
  @override
  State<AppBarCompanyWidget> createState() => _AppBarCompanyWidgetState();
}

class _AppBarCompanyWidgetState extends State<AppBarCompanyWidget> {
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      if (!appSM.isWeb) {
        return const SizedBox();
      }
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.viewPaddingOf(context).top,
            ),
            width: MediaQuery.sizeOf(context).width,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: Theme.of(context).colorScheme.secondaryContainer,
                ),
              ),
              color: Theme.of(context).colorScheme.surface,
            ),
            child: IntrinsicHeight(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  const SizedBox(width: 25),
                  Image.asset(
                    'assets/images/logo-backoffice.png',
                    fit: BoxFit.fitHeight,
                    height: 45,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  const SizedBox(width: 25),
                  _switchWhiteLabelButton(),
                  Expanded(
                    child: Container(),
                  ),
                  _tutorial(),
                  _roadmap(),
                  _view(),
                  _settings(),
                  const IconNotification(),
                  _logout(),
                  const SizedBox(width: 25),
                ],
              ),
            ),
          ),
        ],
      );
    });
  }

  Widget _switchWhiteLabelButton() {
    return Tooltip(
      message: 'Plataforma',
      child: Container(
        constraints: const BoxConstraints(
          minWidth: 50,
          maxWidth: 200,
        ),
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(
              Theme.of(context).colorScheme.secondaryContainer,
            ),
            elevation: MaterialStateProperty.all(0),
            padding: MaterialStateProperty.all(EdgeInsets.zero),
            shape: MaterialStateProperty.all(
              const ContinuousRectangleBorder(
                side: BorderSide.none,
              ),
            ),
          ),
          onPressed: () {
            context.go('/home');
          },
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            constraints: const BoxConstraints(
              minHeight: 56,
              maxWidth: 250,
            ),
            child: IntrinsicHeight(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.network(
                    '${WhiteLabelEntity.current?.style.image.logoMenuWeb}',
                    fit: BoxFit.fitHeight,
                    height: 45,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  const SizedBox(width: 15),
                  Center(
                    child: Icon(
                      EvaIcons.swapOutline,
                      size: 20,
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _settings() {
    return Tooltip(
      message: 'Configurações',
      child: IconButton(
        icon: const Icon(EvaIcons.settings2Outline),
        onPressed: () {
          context.push('/settings');
        },
      ),
    );
  }

  Widget _view() {
    return Tooltip(
      message: 'Visualizar plataforma',
      child: IconButton(
        icon: const Icon(EvaIcons.externalLinkOutline),
        onPressed: () {
          launchUrl(
            Uri.parse('${WhiteLabelEntity.current?.setting.urlWebApplication}'),
            mode: LaunchMode.externalApplication,
          );
        },
      ),
    );
  }

  Widget _tutorial() {
    return Tooltip(
      message: 'Tutorial da plataforma',
      child: IconButton(
        icon: const Icon(EvaIcons.bookOutline),
        onPressed: () {
          context.push('/${WhiteLabelEntity.current?.id}/dashboard/tutorial');
        },
      ),
    );
  }

  Widget _roadmap() {
    return Tooltip(
      message: 'Roteiro do produto',
      child: IconButton(
        icon: const Icon(Icons.newspaper_outlined),
        onPressed: () {
          context.push('/${WhiteLabelEntity.current?.id}/dashboard/roadmap');
        },
      ),
    );
  }

  Widget _logout() {
    return Tooltip(
      message: 'Sair da conta',
      child: IconButton(
        icon: const Icon(EvaIcons.logOutOutline),
        onPressed: () {
          UserDB.remover();
          userSM.user = null;
          context.go('/login');
        },
      ),
    );
  }
}
