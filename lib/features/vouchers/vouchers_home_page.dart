import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class VouchersHomePage extends StatefulWidget {
  const VouchersHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<VouchersHomePage> createState() => _VouchersHomePageState();
}

class _VouchersHomePageState extends State<VouchersHomePage> {

  TextEditingController search = TextEditingController();
  
  List<VoucherEntity>? vouchers;
  int? sortColumnIndex;
  bool isAscending = false;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const Text('Cupons'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Visibility(
              visible: vouchers != null,
              replacement: Container(
                constraints: const BoxConstraints(
                  minHeight: 250,
                ),
                child: const Center(
                  child: CircularProgressIndicator(),
                ),
              ),
              child: Visibility(
                visible: vouchers?.isNotEmpty ?? false,
                replacement: Container(
                  margin: appSM.isWeb
                    ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                    : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  padding: const EdgeInsets.all(25),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Theme.of(context).colorScheme.outlineVariant,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                  ),
                  child: const Center(
                    child: Text(
                      'Você ainda não cadastrou nenhum plano. Adicione o primeiro para vincular as funcionalidades.'
                    ),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(right: 24, top: 12),
                          constraints: const BoxConstraints(
                            maxWidth: 290,
                          ),
                          child: OwTextField(
                            widthBorder: 2,
                            decoration: InputDecoration(
                              border: const OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(16)),
                              ),
                              fillColor: Theme.of(context).colorScheme.primaryContainer,
                              hintText: 'Buscar...',
                              suffixIcon: IconButton(
                                icon: const Icon(EvaIcons.searchOutline),
                                onPressed: () {
                                  setState(() {});
                                },
                              )
                            ),
                            outlined: true,
                            controller: search,
                            onFieldSubmitted: (value) {
                              setState(() {});
                            },
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.all(24),
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.surface,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(16),
                        ),
                        border: Border.all(
                          width: 1,
                          color: Theme.of(context).colorScheme.surfaceVariant,
                        ),
                      ),
                      child: ClipRRect(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(16),
                        ),
                        child: DataTable(
                          border: const TableBorder(
                            borderRadius: const BorderRadius.all(Radius.circular(16)),
                          ),
                          headingRowColor: MaterialStatePropertyAll(
                            Theme.of(context).colorScheme.primaryContainer,
                          ),
                          sortAscending: isAscending,
                          sortColumnIndex: sortColumnIndex,
                          columns: getColumns(['Nome', 'Código', 'Tipo', 'Valor']),
                          rows: getRows(Valid.text(search.text) ? searchData() : vouchers),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: vouchers == null ? null : FloatingActionButton.extended(
        heroTag: null,
        icon: const Icon(
          Icons.add,
        ),
        label: const Text('Novo cupom'),
        onPressed: () async {
          final retorno = await context.push('/${WhiteLabelEntity.current?.id}/dashboard/vouchers/new');
          if (retorno is VoucherEntity) {
            vouchers!.insert(0, retorno);
            setState(() {});
          }
        },
      ),
    );
  }

  void callApi([skip = 0]) async {
    try {
      vouchers = await Api.vouchers.list(params: {});
    } catch (_) {
      //
    }
    setState(() {});
  }

  List<DataColumn> getColumns(List<String> columns) => columns.map((column) {
    return DataColumn(
      label: Text(column),
      onSort: onSort, 
    );
  }).toList();

  List<DataRow> getRows(List<VoucherEntity>? users) => users?.map((VoucherEntity user) {
    final cells = [
      user.name,
      user.code,
      user.discount.type,
      user.discount.value,
    ];
    return DataRow(
      cells: getCells(cells, user),
    );
  }).toList() ?? <DataRow>[];

  List<DataCell> getCells(List<dynamic> cells, VoucherEntity user) => cells.map((cell) {
    return DataCell(
      Text('$cell'),
      onTap: () async {
        final result = await context.push(
          '/${WhiteLabelEntity.current?.id}/dashboard/vouchers/${user.id}',
          extra: user,
        );
        if (result is VoucherEntity) {
          final int value = vouchers?.indexWhere((element) {
            return element.id == user.id;
          }) ?? -1;
          if (value != -1) {
            vouchers![value] = result.copyWith();
          }
          setState(() {});
        }
      }
    );
  }).toList();

  List<VoucherEntity> searchData() {

    final s = normalizarTexto(search.text);

    return vouchers!.where((usuario) {
      final n = normalizarTexto(usuario.name);
      final c = normalizarTexto(usuario.code);
      final t = normalizarTexto(usuario.discount.type);

      return n.contains(s) || c.contains(s) || t.contains(s);
    }).toList();
  }

  void onSort(int columnIndex, bool ascending) {

    if(columnIndex == 0) {
      vouchers!.sort((user1, user2) => compareString(ascending, user1.name, user2.name));
    } else if (columnIndex == 1) {
      vouchers!.sort((user1, user2) => compareString(ascending, user1.code, user2.code));
    } else if (columnIndex == 2) {
      vouchers!.sort((user1, user2) => compareString(ascending, user1.discount.type, user2.discount.type));
    } else {
      vouchers!.sort((user1, user2) => compareString(ascending, '${user1.discount.value}', '${user2.discount.value}'));
    }

    setState(() {
      this.sortColumnIndex = columnIndex;
      this.isAscending = ascending;
    });
  }

  int compareString(bool ascending, String value1, String value2) {
    return ascending ? value1.compareTo(value2) : value2.compareTo(value1);
  }

  String normalizarTexto(String texto) {
    final semAcentos = texto.toLowerCase().replaceAll(RegExp(r'[áàãâä]'), 'a')
        .replaceAll(RegExp(r'[éèêë]'), 'e')
        .replaceAll(RegExp(r'[íìîï]'), 'i')
        .replaceAll(RegExp(r'[óòõôö]'), 'o')
        .replaceAll(RegExp(r'[úùûü]'), 'u')
        .replaceAll(RegExp(r'[ç]'), 'c')
        .replaceAll(RegExp(r'[^a-z0-9]'), ''); // Remove pontuações e mantém apenas letras e números
    return semAcentos;
  }
}
