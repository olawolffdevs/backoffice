import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:flutter/material.dart';

import '../select/select_plan_page.dart';

class VouchersCrudPage extends StatefulWidget {
  const VouchersCrudPage({this.voucher, Key? key}) : super(key: key);
  final VoucherEntity? voucher;

  @override
  State<VouchersCrudPage> createState() => _VouchersCrudPageState();
}

class _VouchersCrudPageState extends State<VouchersCrudPage> {

  TextEditingController name = TextEditingController();
  TextEditingController code = TextEditingController();
  TextEditingController planId = TextEditingController();
  String? discountType = 'money';
  TextEditingController discountValue = TextEditingController();
  TextEditingController cycles = TextEditingController();

  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();

  TextEditingController limit = TextEditingController();
  TextEditingController limitPerUser = TextEditingController();

  PlanEntity? plan;

  bool isVisible = true;

  @override
  void initState() {
    super.initState();
    if (widget.voucher != null) {
      name.text = widget.voucher!.name;
      code.text = widget.voucher!.code;
      discountValue.text = (widget.voucher?.discount.value ?? 0).toString();
      discountType = widget.voucher?.discount.type ?? 'money';
      planId.text = widget.voucher!.planId ?? '';
      startDate.text = widget.voucher!.startDate;
      endDate.text = widget.voucher!.endDate;
      limit.text = (widget.voucher!.limit ?? 0).toString();
      limitPerUser.text = (widget.voucher!.limitPerUser ?? 0).toString();
      isVisible = widget.voucher!.isVisible;
      cycles.text = (widget.voucher!.cycles ?? 0).toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: Text('Detalhes do cupom'.toUpperCase()),
        actions: [
          Visibility(
            visible: widget.voucher != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar este cupom?',
                  description: 'Esta ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                    OwButton(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.vouchers.delete(id: '${widget.voucher?.id}');
                    context.go('/');
                  } catch (_) {
                    OwBotToast.toast('Erro ao remover cupom.');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              controller: name,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Nome',
              hintText: 'Digite aqui...',
              helperText: 'Um nome privado para ajudar na identificação.',
            ),
            OwTextField(
              controller: code,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Código do cupom',
              hintText: 'Digite aqui...',
              maxLength: 12,
              maxLines: 8,
            ),
            const SizedBox(height: 25),
            const OwDivider(),
            OwDropdown(
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              value: discountType,
              labelText: 'Tipo de desconto',
              optionsList: const ['money', 'percentage'],
              onChanged: ((value) {
                setState(() {
                  discountType = value;
                });
              }),
            ),
            OwTextField(
              controller: discountValue,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Valor do desconto',
              hintText: '0',
              keyboardType: TextInputType.phone,
            ),
            const SizedBox(height: 25),
            const OwDivider(),
            OwTextField(
              onTap: () async {
                startDate = await date(startDate);
              },
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Data de início',
              controller: startDate,
              keyboardType: TextInputType.phone,
              textCapitalization: TextCapitalization.none,
            ),
            OwTextField(
              onTap: () async {
                endDate = await date(endDate);
              },
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Data de término',
              controller: endDate,
              keyboardType: TextInputType.phone,
              textCapitalization: TextCapitalization.none,
            ),
            const SizedBox(height: 25),
            const OwDivider(),
            OwTextField(
              controller: cycles,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Ciclos',
              keyboardType: TextInputType.phone,
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(25, 15, 25, 0),
              child: Column(
                children: [
                  Text(
                    'O ciclo define a quantidade de vezes que o benefício sera aplicado.',
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Ex: Se o cupom for de 100% de desconto e o charges for 3, teriamos 3 meses gratuito em cima do plano definido no cupom.',
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            const OwDivider(),
            OwTextField(
              controller: limit,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Limite',
              keyboardType: TextInputType.phone,
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(25, 15, 25, 0),
              child: Column(
                children: [
                  Text(
                    'O limite define a quantidade de vezes que o cupom poderá ser usado.',
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Ex: Se o cupom tiver limite igual a 3, só poderá ser usado 3 vezes.',
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            const OwDivider(),
            OwTextField(
              controller: limitPerUser,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Limite por usuário',
              keyboardType: TextInputType.phone,
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(25, 15, 25, 0),
              child: Column(
                children: [
                  Text(
                    'O limite por usuário define a quantidade de vezes que o cupom poderá ser usado por uma mesma pessoa.',
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Ex: Se o cupom tiver limite por usuário igual a 1, só poderá ser usado 1 vez pelo cliente.',
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 25,
            ),
            const OwDivider(),
            InkWell(
              onTap: () async {
                setState(() {
                  isVisible = !isVisible;
                });
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 25,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            'O cupom é visível?',
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                          const SizedBox(height: 6),
                          const Text('Caso contrário não ficará disponível para escolher.'),
                        ],
                      ),
                    ),
                    const SizedBox(width: 25),
                    Switch(
                      onChanged: (valor) async {
                        setState(() {
                          isVisible = !isVisible;
                        });
                      },
                      value: isVisible,
                    ),
                  ],
                ),
              ),
            ),
            const OwDivider(),
            InkWell(
              onTap: () async {
                final result = await openLinkPage(context, OpenLinkPageParams.basic(child: const SelectPlanPage()));
                if (result is PlanEntity) {
                  plan = result;
                  planId.text = result.id ?? '';
                  setState(() {});
                }
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 25,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            Valid.text(planId.text)
                              ? 'Plano aplicado'
                              : 'Atrelar cupom a qual plano?',
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                          const SizedBox(height: 6),
                          Text(Valid.text(planId.text)
                            ? 'Id do plano: ${planId.text}'
                            : 'Clique para selecionar um plano.'),
                        ],
                      ),
                    ),
                    const SizedBox(width: 25),
                    const Icon(Icons.keyboard_arrow_right),
                  ],
                ),
              ),
            ),
            const OwDivider(),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          labelText: 'Salvar',
          onPressed: _onPressed,
        ),
      ),
    );
  }

  void _onPressed() async {
    if (!Valid.text(name.text) || !Valid.text(code.text) || !Valid.text(discountValue.text) || !Valid.text(startDate.text) || !Valid.text(endDate.text)) {
      showOwDialog(
        context: context,
        title: 'Ops, calma lá',
        description: 'Você não preencheu alguns dados necessários.',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    } else {
      if (widget.voucher != null) {
        final VoucherEntity item = widget.voucher!.copyWith(
          name: name.text.trim(),
          code: code.text.trim(),
          discount: VoucherDiscountEntity(
            type: '$discountType',
            value: int.parse(OwFormat.removerCaracteres(discountValue.text, '.,-')),
          ),
          cycles: int.parse(OwFormat.removerCaracteres(cycles.text, '.,-')),
          isVisible: isVisible,
          limit: int.parse(OwFormat.removerCaracteres(limit.text, '.,-')),
          limitPerUser: int.parse(OwFormat.removerCaracteres(limitPerUser.text, '.,-')),
          planId: planId.text.trim(),
          startDate: startDate.text,
          endDate: endDate.text,
        );
        try {
          OwBotToast.loading();
          final response = await Api.vouchers.update(
            id: '${widget.voucher?.id}',
            data: item,
          );
          OwBotToast.close();
          OwBotToast.toast('Cupom atualizado com sucesso.');
          context.pop(response);
        } catch (e) {
          OwBotToast.close();
          showOwDialog(
            context: context,
            title: 'Ocorreu um erro',
            description: '$e',
            buttons: [
              OwButton(
                labelText: 'Fechar',
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
      } else {
        final VoucherEntity item = VoucherEntity(
          name: name.text.trim(),
          code: code.text.trim(),
          discount: VoucherDiscountEntity(
            type: '$discountType',
            value: int.parse(OwFormat.removerCaracteres(discountValue.text, '.,-')),
          ),
          cycles: !Valid.text(cycles.text) ? null : int.parse(OwFormat.removerCaracteres(cycles.text, '.,-')),
          isVisible: isVisible,
          limit: !Valid.text(limit.text) ? null : int.parse(OwFormat.removerCaracteres(limit.text, '.,-')),
          limitPerUser: !Valid.text(limitPerUser.text) ? null : int.parse(OwFormat.removerCaracteres(limitPerUser.text, '.,-')),
          planId: planId.text.trim(),
          startDate: startDate.text,
          endDate: endDate.text,
        );
        try {
          OwBotToast.loading();
          final response = await Api.vouchers.insert(data: item);
          OwBotToast.close();
          OwBotToast.toast('Cupom cadastrado com sucesso.');
          context.pop(response);
        } catch (e) {
          OwBotToast.close();
          showOwDialog(
            context: context,
            title: 'Ocorreu um erro',
            description: '$e',
            buttons: [
              OwButton(
                labelText: 'Fechar',
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
      }
    }
  }

  Future<TextEditingController> date(controller) async {
    int year = DateTime.now().year;
    int month = DateTime.now().month;
    int day = DateTime.now().day;
    if (controller.text.isNotEmpty && controller.text.length == 10) {
      try {
        day = int.parse(controller.text.toString().substring(0, 2));
        month = int.parse(controller.text.toString().substring(3, 5));
        year = int.parse(controller.text.toString().substring(6));
      } catch (_) {}
    }
    showDatePicker(
      context: context,
      initialDate: DateTime(year, month, day),
      firstDate: DateTime(1950),
      lastDate: DateTime(2100),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            dialogBackgroundColor: Theme.of(context).scaffoldBackgroundColor,
            // colorScheme: Theme.of(context).colorScheme.copyWith(
            //   background: Theme.of(context).colorScheme.,
            //   primary: Theme.of(context).colorScheme.primary,
            //   onPrimary: Theme.of(context).colorScheme.onPrimary,
            //   onSurface: Theme.of(context).colorScheme.onSurface,
            // ),
            // textButtonTheme: TextButtonThemeData(
            //   style: TextButton.styleFrom(
            //     foregroundColor: Theme.of(context).colorScheme.secondary,
            //   ),
            // ),
          ),
          child: child!,
        );
      },
    ).then((value) {
      if (value != null) {
        controller.text = value.toUtc().toString();
      }
      return controller;
    });
    return controller;
  }
}
