import 'package:backoffice/ui/components/alerta_simples.dart';
import 'package:backoffice/features/products/product_category_page.dart';
import 'package:backoffice/features/schedule/product_scheduling_page.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class UpdateProductPage extends StatefulWidget {
  const UpdateProductPage(this.product, this.notifyParent, {super.key});
  final ProductEcommerceEntity product;
  final Function() notifyParent;

  @override
  State<UpdateProductPage> createState() =>
      _UpdateProductPageState(product, notifyParent);
}

class _UpdateProductPageState extends State<UpdateProductPage> {
  _UpdateProductPageState(this.product, this.notifyParent);
  ProductEcommerceEntity product;
  late Function() notifyParent;
  List<ScheduleEntity>? newSchedule;

  List productOptions = [];
  List productAditionalInfo = [];
  List<ImageEntity> productImages = [];

  String? imagem;
  TabController? _tabController;

  TextEditingController productName = TextEditingController();
  TextEditingController quantidadeEstoque = TextEditingController();
  TextEditingController productDescription = TextEditingController();
  TextEditingController alergenicos = TextEditingController();
  int serveQuantos = 0;
  int serveQuantosTemp = 0;

  MoneyMaskedTextController amount =
      MoneyMaskedTextController(decimalSeparator: '.', thousandSeparator: '');
  MoneyMaskedTextController precoDeVenda =
      MoneyMaskedTextController(decimalSeparator: '.', thousandSeparator: '');
  MoneyMaskedTextController discountValue =
      MoneyMaskedTextController(decimalSeparator: '.', thousandSeparator: '');

  MaskedTextController serviceDuration =
      MaskedTextController(mask: '000', text: '');
  MaskedTextController breakBetweenSessions =
      MaskedTextController(mask: '000', text: '');
  MaskedTextController maxParticipantsByService =
      MaskedTextController(mask: '000', text: '');

  List<FocusNode> fnListAtualizar = FN.initialilzeFocusNodeList(6);
  List<FocusNode> fnListDisponibilidade = FN.initialilzeFocusNodeList(2);

  bool desconto = false;
  bool mostrar = false;
  bool productVisibility = false;
  bool highlight = false;
  bool tonamesa = false;
  bool legalAge = false;
  bool estoque = false;
  bool quantidade = false;
  // bool hasSchedules = false;
  bool takeout = false;
  bool delivery = false;
  bool online = false;
  bool acceptScheduling = false;

  String inventario = 'Em estoque';
  String opcao = '%';

  String? errorNome;
  String? errorDescricao;
  String? errorPreco;

  // List<ProductCategoryEcommerceEntity> _productCategories = [];
  // int _currentCategoryIndex = 0;

  @override
  void initState() {
    super.initState();
    notifyParent = widget.notifyParent;
    product = widget.product;

    amount.text = '${product.amount.toStringAsFixed(2)}';
    productDescription.text = product.description;
    legalAge = product.legalAge;
    productVisibility = product.visibility;
    highlight = product.highlight;
    productName.text = product.name;
    discountValue.text = product.discountValue.toString();
    serviceDuration.text = product.serviceDuration.toString();
    breakBetweenSessions.text = product.breakBetweenSessions.toString();
    maxParticipantsByService.text = product.maxParticipantsByService.toString();

    if (product.type == 'service') {
      acceptScheduling = true;
    }

    if (product.servicesMethods.contains('takeout')) {
      takeout = true;
    }

    if (product.servicesMethods.contains('online')) {
      online = true;
    }

    if (product.servicesMethods.contains('delivery')) {
      delivery = true;
    }

    setState(() {});
  }

  void dispose() {
    super.dispose();
    FN.disposeFocusNodeList(fnListAtualizar);
    FN.disposeFocusNodeList(fnListDisponibilidade);
    productName.dispose();
    amount.dispose();
    productDescription.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: WillPopScope(
        onWillPop: () async {
          widget.notifyParent();
          context.pop();
          return true;
        },
        child: Scaffold(
          appBar: OwAppBar(
            elevation: 0.5,
            preferredSize:
                const Size.fromHeight(kToolbarHeight + kTextTabBarHeight),
            bottom: TabBar(
              isScrollable: true,
              indicatorColor: Theme.of(context).colorScheme.primary,
              controller: _tabController,
              tabs: [
                const Tab(
                  text: 'Detalhes',
                ),
                const Tab(
                  text: 'Disponibilidade',
                ),
                const Tab(
                  text: 'Agendamento',
                ),
                // const Tab(
                //   text: 'Complementos',
                // ),
                // const Tab(
                //   text: 'Informações extras',
                // ),
                // const Tab(
                //   text: 'Imagens',
                // ),
              ],
            ),
            title: OwText(
              'Editar Produto'.toUpperCase(),
              style: const TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 15,
              ),
            ),
            centerTitle: true,
            actions: [
              Tooltip(
                message: 'Deletar item',
                child: IconButton(
                  icon: const Icon(EvaIcons.trashOutline),
                  onPressed: () async {
                    final confirmar = await showOwDialog(
                      context: context,
                      title: 'Deseja apagar esse produto?',
                      description: 'Essa ação não poderá ser desfeita',
                      buttons: [
                        OwButton.text(
                          labelText: 'Remover',
                          onPressed: () {
                            Navigator.pop(context, true);
                          },
                        ),
                        OwButton.text(
                          labelText: 'Manter',
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                        ),
                      ],
                    );
                    if (confirmar == true) {
                      try {
                        OwBotToast.loading();
                        await Api.ecommerce.products.delete(id: product.id);
                        OwBotToast.close();
                        context.pop(true);
                      } catch (e) {
                        OwBotToast.close();
                      }
                    }
                  },
                ),
              ),
            ],
          ),
          body: TabBarView(
            controller: _tabController,
            physics: const BouncingScrollPhysics(),
            children: [
              detalhesScreen(),
              disponibilidadeScreen(),
              hasSchedulescreen(),
              // complementosScreen(),
              // informacoesExtrasScreen(),
              // imagensScreen(),
            ],
          ),
          bottomNavigationBar: OwBottomAppBar(
            child: OwButton(
              onPressed: () async {
                await atualizarPedido();
              },
              margin: EdgeInsets.fromLTRB(
                25,
                25,
                25,
                MediaQuery.viewInsetsOf(context).bottom + 25,
              ),
              labelText: 'Atualizar produto',
            ),
          ),
        ),
      ),
    );
  }

  Widget detalhesScreen() {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(
        vertical: 25,
      ),
      physics: const BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          OwTextField(
            margin: const EdgeInsets.symmetric(
              horizontal: 25,
              vertical: 10,
            ),
            controller: productName,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            labelText: 'Nome do produto',
            focusNode: fnListAtualizar[0],
            onFieldSubmitted: (_) {
              FN.nextFn(context, fnListAtualizar[1]);
            },
            onChanged: (valor) {
              errorNome = null;
              setState(() {});
            },
          ),
          OwTextField(
            margin: const EdgeInsets.symmetric(
              horizontal: 25,
              vertical: 10,
            ),
            controller: productDescription,
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            labelText: 'Descrição do produto',
            maxLines: 12,
            focusNode: fnListAtualizar[1],
            onFieldSubmitted: (_) {
              FN.nextFn(context, fnListAtualizar[2]);
            },
            onChanged: (valor) {
              errorDescricao = null;
              setState(() {});
            },
          ),
          Container(
            margin: const EdgeInsets.symmetric(
              horizontal: 25,
              vertical: 10,
            ),
            child: Row(
              children: [
                Expanded(
                  child: OwTextField(
                    controller: amount,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    labelText: 'Preço',
                    focusNode: fnListAtualizar[2],
                    onFieldSubmitted: (_) {
                      if (desconto) {
                        FN.nextFn(context, fnListAtualizar[3]);
                      } else {
                        FN.unfocusFn(context);
                      }
                    },
                    onChanged: (valor) {
                      errorNome = null;
                      if (desconto) {
                        calcular();
                      } else {
                        precoDeVenda.text = valor;
                      }
                      setState(() {});
                    },
                  ),
                ),
                const SizedBox(width: 20),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    const OwText('Desconto'),
                    Switch(
                      value: desconto,
                      onChanged: (valor) {
                        if (valor) {
                          setState(() {
                            precoDeVenda.text = amount.text;
                            desconto = valor;
                          });
                        } else {
                          setState(() {
                            desconto = valor;
                            precoDeVenda.text = amount.text;
                            discountValue.text = '0.00';
                          });
                        }
                      },
                    )
                  ],
                )
              ],
            ),
          ),
          desconto == true
              ? Container(
                  margin: const EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: 10,
                  ),
                  width: MediaQuery.sizeOf(context).width,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: OwTextField(
                              controller: discountValue,
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.done,
                              labelText: 'Desconto',
                              focusNode: fnListAtualizar[3],
                              onFieldSubmitted: (_) {
                                FN.unfocusFn(context);
                              },
                              onChanged: (valor) {
                                if (desconto) {
                                  calcular();
                                }
                              },
                            ),
                          ),
                          const SizedBox(width: 20),
                          DropdownButton(
                            value: opcao,
                            onChanged: (String? valor) {
                              setState(() {
                                opcao = valor ?? 'R\$';
                              });
                              if (desconto) calcular();
                            },
                            items: <String>['%', 'R\$'].map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: OwText(value),
                              );
                            }).toList(),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      OwTextField(
                        controller: precoDeVenda,
                        keyboardType: TextInputType.number,
                        labelText: 'Preço de Venda',
                        enabled: false,
                        onChanged: (valor) {
                          if (desconto) {
                            calcular();
                          }
                        },
                      ),
                    ],
                  ),
                )
              : const SizedBox(),
          Container(
            height: 1,
            color: Theme.of(context).colorScheme.secondaryContainer,
            margin: const EdgeInsets.only(
              top: 15,
              bottom: 15,
            ),
          ),
        ],
      ),
    );
  }

  Widget disponibilidadeScreen() {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(
        vertical: 25,
      ),
      physics: const BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListTile(
            contentPadding: const EdgeInsets.symmetric(
              horizontal: 25,
              vertical: 10,
            ),
            title: OwText(
              'Deixar item visível?',
              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            subtitle:
                const OwText('Deseja exibir o produto/serviço para o usuário'),
            trailing: Switch(
              value: productVisibility,
              onChanged: (valor) {
                setState(() {
                  productVisibility = valor;
                });
              },
            ),
          ),
          ListTile(
            contentPadding: const EdgeInsets.symmetric(
              horizontal: 25,
              vertical: 10,
            ),
            title: OwText(
              'Produto para maior de idade?',
              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            subtitle: const OwText(
                'O produto/serviço só aparecerá se o usuário for maior de idade'),
            trailing: Switch(
              value: legalAge,
              onChanged: (valor) {
                setState(() {
                  legalAge = valor;
                });
              },
            ),
          ),
          ListTile(
            contentPadding: const EdgeInsets.symmetric(
              horizontal: 25,
              vertical: 10,
            ),
            title: OwText(
              'Colocar produto em destaque?',
              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            subtitle: const OwText(
                'O produto/serviço aparecerá em um lugar localizado no topo da tela'),
            trailing: Switch(
              value: highlight,
              onChanged: (valor) {
                setState(() {
                  highlight = valor;
                });
              },
            ),
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(25, 50, 25, 15),
            child: OwText(
              'Categoria do produto',
              style: TextStyle(
                fontSize: 19,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Ink(
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primaryContainer,
                borderRadius: const BorderRadius.all(Radius.circular(15)),
              ),
              child: InkWell(
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                onTap: () async {
                  final ProductCategoryEcommerceEntity? result =
                      await showCupertinoModalBottomSheet(
                    context: context,
                    builder: (context) => const ProductCategoryPage(),
                  );
                  if (result != null) {
                    product.productCategory = result;
                  }
                  setState(() {});
                },
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          '${product.productCategory?.name ?? '  Selecionar categoria'}',
                          style:
                              Theme.of(context).textTheme.titleMedium?.copyWith(
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onPrimaryContainer,
                                  ),
                        ),
                      ),
                      const SizedBox(width: 15),
                      Icon(
                        EvaIcons.arrowForwardOutline,
                        color: Theme.of(context).colorScheme.onPrimaryContainer,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget informacoesExtrasScreen() {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(
        vertical: 25,
      ),
      physics: const BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 25,
            ),
            child: OwText(
              'Informações Extras',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          (productAditionalInfo.length == 0)
              ? Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.secondaryContainer,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(
                        10,
                      ),
                    ),
                  ),
                  margin: const EdgeInsets.symmetric(
                    horizontal: 25,
                  ),
                  padding: const EdgeInsets.all(
                    20,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      const OwText('Seu produto não tem informações adicionais',
                          style: TextStyle(
                              fontWeight: FontWeight.w800, fontSize: 20),
                          textAlign: TextAlign.center),
                      const SizedBox(height: 20),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 35),
                        child: OwText(
                          'Adicione informações adicionais ao produto para facilitar e orientar seus clientes. Ex.: Informações nutricionais, detalhes da peça',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(height: 20),
                      OwButton.outline(
                        labelText: 'Adicionar informação adicional',
                        onPressed: () async {
                          final dynamic resultado = await context.push(
                            '/empresa/${companySM.company?.id}/ecommerce/${ecommerceSM.establishment?.id}/product/${widget.product.id}/additionals/add',
                          );
                          if (resultado != null) {
                            productAditionalInfo.add(resultado);
                          }
                          setState(() {});
                        },
                      ),
                    ],
                  ),
                )
              : Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 25,
                  ),
                  alignment: Alignment.center,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const OwText(
                        'Configure as informações adicionais do seu produto. Por exemplo: Informações nutricionais',
                      ),
                      const SizedBox(height: 25),
                      OwButton.outline(
                        labelText: 'Adicionar mais informações',
                        onPressed: () async {
                          final dynamic resultado = await context.push(
                            '/empresa/${companySM.company?.id}/ecommerce/${ecommerceSM.establishment?.id}/product/${widget.product.id}/additionals/add',
                          );
                          if (resultado != null) {
                            productAditionalInfo.add(resultado);
                          }
                          setState(() {});
                        },
                      ),
                      ListView.separated(
                          padding: const EdgeInsets.only(top: 20),
                          scrollDirection: Axis.vertical,
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: productAditionalInfo.length,
                          separatorBuilder: (BuildContext context, int index) {
                            return Container(
                              height: 1,
                              margin: const EdgeInsets.symmetric(vertical: 10),
                            );
                          },
                          itemBuilder: (context, index) {
                            return ListTile(
                              title: OwText(productAditionalInfo[index]
                                  .titulo
                                  .toString()),
                              subtitle: OwText(
                                  '${productAditionalInfo[index].descricao.toString()}',
                                  maxLines: 2),
                              leading: Container(
                                padding: const EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1,
                                    color: Theme.of(context)
                                        .appBarTheme
                                        .actionsIconTheme!
                                        .color!,
                                  ),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(8),
                                  ),
                                ),
                                child: const Icon(EvaIcons.listOutline),
                              ),
                              trailing:
                                  const Icon(EvaIcons.arrowIosForwardOutline),
                              onTap: () async {
                                final dynamic resultado = await context.push(
                                  '/empresa/${companySM.company?.id}/ecommerce/${ecommerceSM.establishment?.id}/product/${widget.product.id}/additionals/${productAditionalInfo[index].id}/edit',
                                  extra: productAditionalInfo[index],
                                );
                                if (resultado != null) {
                                  if (resultado == 'remover') {
                                    productAditionalInfo.removeAt(index);
                                  } else {
                                    productAditionalInfo[index] = resultado;
                                  }
                                }
                                setState(() {});
                              },
                            );
                          }),
                      const SizedBox(height: 20),
                    ],
                  ))
        ],
      ),
    );
  }

  // Widget imagensScreen() {
  //   return SingleChildScrollView(
  //     padding: const EdgeInsets.symmetric(
  //       vertical: 25,
  //     ),
  //     physics: const BouncingScrollPhysics(),
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.stretch,
  //       children: [
  //         Container(
  //           padding: const EdgeInsets.symmetric(
  //             horizontal: 25,
  //           ),
  //           child: OwText(
  //             'Imagens',
  //             style: Theme.of(context).textTheme.headlineLarge,
  //           ),
  //         ),
  //         const SizedBox(
  //           height: 15,
  //         ),
  //         Row(
  //           mainAxisSize: MainAxisSize.min,
  //           children: [
  //             const SizedBox(
  //               width: 25,
  //             ),
  //             CardAtalhos(
  //               title: 'Dica de foto',
  //               icon: Icons.flash_on,
  //               onPressed: () {
  //                 context.push('/empresa/${companySM.company?.id}/ecommerce/${ecommerceSM.establishment?.id}/product/${widget.product.id}/tips');
  //               },
  //               selecionado: true,
  //             ),
  //           ],
  //         ),
  //         const SizedBox(
  //           height: 15,
  //         ),
  //         Container(
  //           margin: const EdgeInsets.symmetric(
  //             horizontal: 25,
  //           ),
  //           child: Column(
  //             crossAxisAlignment: CrossAxisAlignment.stretch,
  //             children: [
  //               const OwText('Tire uma foto ou escolha uma imagem da galeria para mostrar seu produto.'),
  //               const SizedBox(height: 20),
  //               ListView.builder(
  //                 shrinkWrap: true,
  //                 physics: const NeverScrollableScrollPhysics(),
  //                 itemCount: productImages.length,
  //                 itemBuilder: (context, indexImagem) {
  //                   return Column(
  //                     mainAxisSize: MainAxisSize.min,
  //                     crossAxisAlignment: CrossAxisAlignment.stretch,
  //                     children: [
  //                       Container(
  //                         margin: const EdgeInsets.only(bottom: 20),
  //                         width: MediaQuery.sizeOf(context).width,
  //                         decoration: BoxDecoration(
  //                           borderRadius: const BorderRadius.all(
  //                             Radius.circular(8),
  //                           ),
  //                           border: Border.all(
  //                             width: 2,
  //                             color: Theme.of(context).cardColor,
  //                           ),
  //                         ),
  //                         child: Stack(
  //                           children: [
  //                             ClipRRect(
  //                               borderRadius: BorderRadius.circular(8),
  //                               child: CachedNetworkImage(
  //                                 imageUrl:
  //                                     productImages[indexImagem].url ?? '',
  //                                 fit: BoxFit.fitWidth,
  //                                 progressIndicatorBuilder:
  //                                     (context, url, downloadProgress) =>
  //                                         Center(
  //                                   child: CircularProgressIndicator(
  //                                       value: downloadProgress.progress),
  //                                 ),
  //                                 errorWidget: (context, url, error) =>
  //                                     const Icon(Icons.error),
  //                               ),
  //                             ),
  //                             Positioned(
  //                               bottom: 10,
  //                               right: 10,
  //                               child: GestureDetector(
  //                                 onTap: () async {

  //                                 },
  //                                 child: Container(
  //                                   height: 50,
  //                                   width: 50,
  //                                   decoration: BoxDecoration(
  //                                     borderRadius: const BorderRadius.all(
  //                                         Radius.circular(50)),
  //                                     color:
  //                                         Theme.of(context).colorScheme.primary,
  //                                     boxShadow: [
  //                                       BoxShadow(
  //                                         color: Colors.black.withOpacity(.6),
  //                                         blurRadius: 15,
  //                                       ),
  //                                     ],
  //                                   ),
  //                                   child: const Center(
  //                                     child: Icon(
  //                                       Icons.delete_outline,
  //                                       color: Colors.white,
  //                                     ),
  //                                   ),
  //                                 ),
  //                               ),
  //                             )
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   );
  //                 },
  //               ),
  //               Container(
  //                 child: GestureDetector(
  //                   onTap: () async {

  //                   },
  //                   child: DottedBorder(
  //                     color: Theme.of(context)
  //                         .appBarTheme
  //                         .actionsIconTheme!
  //                         .color!,
  //                     borderType: BorderType.RRect,
  //                     strokeWidth: 1,
  //                     strokeCap: StrokeCap.round,
  //                     dashPattern: [17, 8],
  //                     radius: const Radius.circular(10.0),
  //                     child: Container(
  //                       decoration: BoxDecoration(
  //                         color:
  //                             Theme.of(context).colorScheme.secondaryContainer,
  //                         borderRadius:
  //                             const BorderRadius.all(Radius.circular(10.0)),
  //                       ),
  //                       width: MediaQuery.sizeOf(context).width * 1,
  //                       height: 100,
  //                       child: Center(
  //                         child: OwText(
  //                           'Adicionar imagem',
  //                           style: TextStyle(
  //                             color: Theme.of(context)
  //                                 .appBarTheme
  //                                 .actionsIconTheme!
  //                                 .color,
  //                           ),
  //                         ),
  //                       ),
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }

  Widget hasSchedulescreen() {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(
        vertical: 25,
      ),
      physics: const BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListTile(
            contentPadding: const EdgeInsets.symmetric(
              horizontal: 25,
              vertical: 10,
            ),
            title: OwText(
              'Este item pode receber agendamento?',
              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            subtitle: const OwText(
                'Caso o usuário possa selecionar um dia e horário para realizar o serviço, habilite a opção'),
            trailing: Switch(
              value: acceptScheduling,
              onChanged: (valor) async {
                try {
                  OwBotToast.loading();
                  await Api.ecommerce.products.update(
                    id: '${product.id}',
                    data: product.copyWith(
                      type: valor ? 'service' : 'product',
                    ),
                  );
                  acceptScheduling = valor;
                  if (mounted) {
                    setState(() {});
                  }
                  OwBotToast.close();
                } catch (e) {
                  OwBotToast.close();
                }
              },
            ),
          ),
          const SizedBox(height: 25),
          const OwDivider(),
          const SizedBox(height: 25),
          acceptScheduling
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 25,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          OwText(
                            'Detalhes do serviço',
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge
                                ?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                          const SizedBox(height: 15),
                          const OwText(
                            'Informações de como funciona o serviço. Essas informações são visíveis para os clientes',
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 15),
                    OwTextField(
                      controller: serviceDuration,
                      labelText: 'Duração do serviço (em minutos)',
                      helperText: 'O serviço dura x minutos para ser concluído',
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      margin: const EdgeInsets.symmetric(horizontal: 25),
                    ),
                    const SizedBox(height: 35),
                    OwTextField(
                      controller: breakBetweenSessions,
                      labelText: 'Tempo entre sessões (em minutos)',
                      helperText: 'A cada x minutos terá uma opção de agendamento',
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      margin: const EdgeInsets.symmetric(horizontal: 25),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 25,
                      ),
                      child: OwText(
                        'Agendamento simutâneo',
                        style: Theme.of(context)
                                .textTheme
                                .titleLarge
                                ?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                      ),
                    ),
                    OwTextField(
                      controller: maxParticipantsByService,
                      labelText: 'Máximo de agendamento em um mesmo horário',
                      helperText: 'Esse serviço consegue atender x clientes por vez',
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.done,
                      margin: const EdgeInsets.symmetric(
                        horizontal: 25,
                      ),
                      onFieldSubmitted: (_) {
                        FN.unfocusFn(context);
                      },
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 25,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          OwText(
                            'Local do agendamento',
                            style: Theme.of(context).textTheme.titleLarge?.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 10),
                          const OwText(
                            'Selecione quais opções você deseja dar ao seus usuários.',
                          ),
                        ],
                      ),
                    ),
                    ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 25,
                        vertical: 10,
                      ),
                      title: OwText(
                        'No meu endereço',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: const OwText(
                        'O usuário virá até o meu endereço',
                      ),
                      trailing: Switch(
                        value: takeout,
                        onChanged: (e) {
                          setState(() {
                            takeout = e;
                          });
                        },
                      ),
                    ),
                    ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 25,
                        vertical: 10,
                      ),
                      title: OwText(
                        'No endereço do cliente',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: const OwText(
                        'Eu aceito ir até o endereço do cliente',
                      ),
                      trailing: Switch(
                        value: delivery,
                        onChanged: (e) {
                          setState(() {
                            delivery = e;
                          });
                        },
                      ),
                    ),
                    ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 25,
                        vertical: 10,
                      ),
                      title: OwText(
                        'Atendimento online',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: const OwText(
                        'O agendamento pode ser realizado de forma online',
                      ),
                      trailing: Switch(
                        value: online,
                        onChanged: (e) {
                          setState(() {
                            online = e;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(25, 45, 25, 15),
                      child: OwText(
                        'Horários disponíveis para agendamento',
                        style: Theme.of(context)
                                .textTheme
                                .titleLarge
                                ?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                      ),
                    ),
                    OwButton.outline(
                      margin: const EdgeInsets.symmetric(
                        horizontal: 25,
                      ),
                      onPressed: () async {
                        final List<ScheduleEntity>? schedules =
                            await showCupertinoModalBottomSheet(
                          context: context,
                          builder: (context) => ProductSchedulingPage(product),
                        );

                        newSchedule = schedules;

                        setState(() {});
                      },
                      labelText: 'Visualizar horários',
                    ),
                  ],
                )
              : const SizedBox(),
        ],
      ),
    );
  }

  // Future<void> callApiCategories() async {
  //   try{
  //     OwBotToast.loading();
  //     _productCategories = await Api.ecommerce.productCategory.listByEcommerce(id: '${WhiteLabelEntity.current?.setting.ecommerceId}');
  //     OwBotToast.close();
  //   }
  //   catch(e){
  //     OwBotToast.close();
  //   }
  //   if (this.mounted){
  //     setState(() {});
  //   }
  // }

  Future<void> atualizarPedido() async {
    bool valido = true;
    final List<String> _errors = [];
    if (productName.text.length < 4) {
      errorNome = 'Nome do product muito curto';
      _errors.add('Nome do product muito curto');
      valido = false;
    }
    if (productDescription.text.length < 5) {
      errorDescricao = 'Descrição muito curta';
      _errors.add('Descrição muito curta');
      valido = false;
    }

    final List<String> localDeAtendimento = [];
    if (takeout) {
      localDeAtendimento.add('takeout');
    }
    if (delivery) {
      localDeAtendimento.add('Dinâmico');
    }
    if (online) {
      localDeAtendimento.add('Online');
    }

    // if (_productCategories.isEmpty){
    //   _errors.add('Categoria inválida');
    // }

    if (acceptScheduling) {
      if (!Valid.validateText(serviceDuration.text) ||
          num.tryParse((serviceDuration.text)) == 0) {
        valido = false;
        _errors.add('Defina uma duração do serviço.');
      }
      if (!Valid.validateText(breakBetweenSessions.text) ||
          num.tryParse((breakBetweenSessions.text)) == 0) {
        valido = false;
        _errors.add('Defina o tempo entre sessões.');
      }
      if (!Valid.validateText(maxParticipantsByService.text) ||
          num.tryParse((maxParticipantsByService.text)) == 0) {
        valido = false;
        _errors.add('Defina o número máximo de participantes.');
      }
      if (localDeAtendimento.isEmpty) {
        valido = false;
        _errors.add('Escolha pelo menos uma forma de atendimento.');
      }
      if (product.schedules?.isEmpty ?? false) {
        valido = false;
        _errors.add('Seu horário de atendimento para esse produto está vazio.');
      }
    }

    if (valido) {
      // final ProductCategoryEcommerceEntity _category = _productCategories.where((element) => element.name == _productCategories[_currentCategoryIndex].name).first;

      final List<String> methods = [];

      if (takeout) {
        methods.add('takeout');
      }

      if (online) {
        methods.add('online');
      }

      if (online) {
        methods.add('delivery');
      }

      try {
        OwBotToast.loading();
        await Api.ecommerce.products.update(
            id: product.id,
            data: product.copyWith(
              id: product.id,
              amount: num.parse(amount.text),
              description: productDescription.text,
              legalAge: legalAge,
              visibility: productVisibility,
              highlight: highlight,
              name: productName.text,
              // productCategory: _category,
              discountValue: desconto ? discountValue.numberValue : null,
              type: acceptScheduling ? 'service' : 'product',
              serviceDuration: num.parse(serviceDuration.text),
              breakBetweenSessions: int.parse(breakBetweenSessions.text),
              maxParticipantsByService:
                  int.parse(maxParticipantsByService.text),
              itemQuantity: serveQuantos,
              schedules: newSchedule != null ? newSchedule : product.schedules,
              servicesMethods: methods,
            ));

        OwBotToast.close();

        if (this.mounted) {
          context.pop();
        }
      } catch (e) {
        OwBotToast.close();
      }
      notifyParent();
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return AlertaSimples(
            'Ops, falta preencher algumas informações.',
            conteudo: _errors
                .map((e) => '# $e\n')
                .toString()
                .replaceAll('(', '')
                .replaceAll(')', '')
                .replaceAll(',', ''),
          );
        },
      );
    }
  }

  void calcular() {
    final dynamic valorNormal = amount.text;
    dynamic desconto2 = discountValue.text;
    dynamic venda;
    switch (opcao) {
      case '%':
        if ((num.tryParse(desconto2.toString()) ?? 0) > 100) {
          desconto2 = '100';
        }
        venda = ((double.parse(valorNormal.toString()) *
                double.parse(desconto2.toString())) /
            100);
        venda = double.parse(valorNormal.toString()) - venda;
        setState(() {
          precoDeVenda.text = venda.toStringAsFixed(2);
        });
        break;
      case 'R\$':
        if ((num.tryParse(desconto2.toString()) ?? 0) >
            (num.tryParse(valorNormal.toString()) ?? 0)) {
          desconto2 = valorNormal;
        }
        venda = double.parse(valorNormal.toString()) -
            double.parse(desconto2.toString());
        setState(() {
          precoDeVenda.text = venda.toStringAsFixed(2);
        });
        break;
    }
  }
}
