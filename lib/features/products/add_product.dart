import 'package:backoffice/features/products/product_category_page.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class AddProductPage extends StatefulWidget {
  const AddProductPage(this.notifyParent, {super.key});
  final Function() notifyParent;

  @override
  State<AddProductPage> createState() => _AddProductPageState(notifyParent);
}

class _AddProductPageState extends State<AddProductPage> {
  _AddProductPageState(this.notifyParent);
  late Function() notifyParent;
  TextEditingController productName = TextEditingController();
  TextEditingController quantity = TextEditingController();
  MoneyMaskedTextController productAmount =
      MoneyMaskedTextController(decimalSeparator: '.', thousandSeparator: '');
  TextEditingController productDescription = TextEditingController();
  MoneyMaskedTextController precoDeVenda =
      MoneyMaskedTextController(decimalSeparator: '.', thousandSeparator: '');
  MoneyMaskedTextController productDiscount =
      MoneyMaskedTextController(decimalSeparator: '.', thousandSeparator: '');
  bool desconto = false;
  bool visibilidade = false;
  bool alugar = false;
  bool highlights = false;
  bool service = false;
  bool estoque = false;
  bool legalAge = false;
  String opcao = '%';
  String inventario = 'Em estoque';

  String? errorNome;
  String? errorDescricao;
  String? errorPreco;

  List<FocusNode> fnList = FN.initialilzeFocusNodeList(7);

  ProductCategoryEcommerceEntity? category;

  @override
  void initState() {
    super.initState();
    notifyParent = widget.notifyParent;
  }

  void dispose() {
    FN.disposeFocusNodeList(fnList);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      // ignore: missing_return
      onWillPop: () async {
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        appBar: OwAppBar(
          title: OwText(
            'Adicionar Produto'.toUpperCase(),
            style: const TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w500,
            ),
          ),
          leading: IconButton(
            icon: const Icon(Icons.close),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            IconButton(
              onPressed: () async {
                // await openModalPage(context, const HelpHomePage());
              },
              icon: const Icon(
                EvaIcons.questionMarkCircleOutline,
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    OwTextField(
                      controller: productName,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      labelText: 'Nome do Produto',
                      errorText: errorNome,
                      autofocus: true,
                      focusNode: fnList[0],
                      onFieldSubmitted: (_) {
                        FN.nextFn(context, fnList[1]);
                      },
                      onChanged: (valor) {
                        errorNome = null;
                        setState(() {});
                      },
                    ),
                    const SizedBox(height: 15),
                    OwTextField(
                      controller: productDescription,
                      keyboardType: TextInputType.multiline,
                      textInputAction: TextInputAction.newline,
                      labelText: 'Descrição do Produto',
                      errorText: errorDescricao,
                      maxLines: 12,
                      focusNode: fnList[1],
                      onFieldSubmitted: (_) {
                        FN.nextFn(context, fnList[2]);
                      },
                      onChanged: (valor) {
                        errorDescricao = null;
                        setState(() {});
                      },
                    ),
                    const SizedBox(height: 15),
                    Row(
                      children: [
                        Expanded(
                            child: OwTextField(
                          controller: productAmount,
                          keyboardType: TextInputType.number,
                          labelText: 'Preço',
                          textInputAction: TextInputAction.next,
                          errorText: errorPreco,
                          focusNode: fnList[2],
                          onFieldSubmitted: (_) {
                            if (desconto) {
                              FN.nextFn(context, fnList[3]); // desconto
                            } else if (estoque) {
                              FN.nextFn(context, fnList[4]); // estoque
                            } else {
                              FN.unfocusFn(context);
                            }
                          },
                          onChanged: (valor) {
                            errorPreco = null;
                            if (desconto) {
                              calcular();
                            } else {
                              precoDeVenda.text = valor;
                            }
                            setState(() {});
                          },
                        )),
                        const SizedBox(width: 15),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            const OwText('Desconto'),
                            Switch(
                              value: desconto,
                              onChanged: (valor) {
                                if (valor) {
                                  setState(() {
                                    precoDeVenda.text = productAmount.text;
                                    desconto = valor;
                                  });
                                } else {
                                  setState(() {
                                    desconto = valor;
                                    precoDeVenda.text = productAmount.text;
                                    productDiscount.text = '0.00';
                                  });
                                }
                              },
                            )
                          ],
                        )
                      ],
                    ),
                    desconto == true
                        ? Container(
                            margin: const EdgeInsets.only(top: 15),
                            width: MediaQuery.sizeOf(context).width,
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: OwTextField(
                                        controller: productDiscount,
                                        keyboardType: TextInputType.number,
                                        textInputAction: TextInputAction.done,
                                        labelText: 'Desconto',
                                        focusNode: fnList[3],
                                        onFieldSubmitted: (_) {
                                          FN.nextFn(context, fnList[4]);
                                        },
                                        onChanged: (valor) {
                                          if (desconto) {
                                            calcular();
                                          }
                                        },
                                      ),
                                    ),
                                    const SizedBox(width: 20),
                                    DropdownButton(
                                      value: opcao,
                                      onChanged: (String? valor) {
                                        setState(() {
                                          opcao = valor ?? 'R\$';
                                        });
                                        if (desconto) {
                                          FN.nextFn(context, fnList[4]);
                                          calcular();
                                        }
                                      },
                                      items: <String>['%', 'R\$']
                                          .map((String value) {
                                        return new DropdownMenuItem<String>(
                                          value: value,
                                          child: OwText(value),
                                        );
                                      }).toList(),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 15),
                                OwTextField(
                                  controller: precoDeVenda,
                                  keyboardType: TextInputType.number,
                                  labelText: 'Preço de Venda',
                                  // enableInteractive: false,
                                  enabled: false,
                                  focusNode: fnList[4],
                                  onFieldSubmitted: (_) {
                                    if (estoque) {
                                      FN.nextFn(context,
                                          fnList[5]); // quantity estoque
                                    } else {
                                      FN.unfocusFn(context);
                                    }
                                  },
                                  onChanged: (valor) {
                                    if (desconto) calcular();
                                  },
                                )
                              ],
                            ),
                          )
                        : const SizedBox(),
                    const SizedBox(height: 10),
                    ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 5,
                      ),
                      title: const OwText('Para idade de 18 anos'),
                      trailing: Switch(
                        value: legalAge,
                        onChanged: (valor) {
                          setState(() {
                            legalAge = valor;
                          });
                        },
                      ),
                    ),
                    ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 5,
                      ),
                      title: const OwText('Produto para alugar'),
                      trailing: Switch(
                        value: alugar,
                        onChanged: (valor) {
                          setState(() {
                            alugar = valor;
                          });
                        },
                      ),
                    ),
                    ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 5,
                      ),
                      title: const OwText('Produto em highlights'),
                      trailing: Switch(
                        value: highlights,
                        onChanged: (valor) {
                          setState(() {
                            highlights = valor;
                          });
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const OwDivider(),
                    const SizedBox(
                      height: 20,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 6,
                      ),
                      child: OwText(
                        'Categoria',
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    OwButton(
                      labelText: category == null
                          ? 'Selecione a categoria'
                          : '${category?.name}',
                      onPressed: () async {
                        final ProductCategoryEcommerceEntity? result =
                            await showCupertinoModalBottomSheet(
                          context: context,
                          builder: (context) => const ProductCategoryPage(),
                        );

                        if (result != null) {
                          category = result;
                        }
                        setState(() {});
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: OwButton(
          onPressed: () async {
            try {
              if (isValid()) {
                final result = await Api.ecommerce.products.insert(
                    name: productName.text,
                    ecommerceId:
                        '${WhiteLabelEntity.current?.setting.ecommerceId}',
                    description: productDescription.text,
                    type: 'product',
                    productCategoryId: category?.id ?? '',
                    highlight: highlights,
                    legalAge: legalAge,
                    amount: num.parse(productAmount.text));

                context.pop();

                await context.push(
                    '/${WhiteLabelEntity.current?.id}/ecommerce/product/${result.id}',
                    extra: {'product': result, 'notifyParent': () {}});
              }
            } catch (e) {
              showOwDialog(
                context: context,
                title: 'Houve um erro ao tentar cadastrar este produto.',
              );
            }
          },
          margin: EdgeInsets.fromLTRB(
            25,
            25,
            25,
            MediaQuery.viewInsetsOf(context).bottom + 25,
          ),
          labelText: 'Adicionar produto',
        ),
      ),
    );
  }

  bool isValid() {
    final List<String> _errors = [];

    if (!Valid.validateText(productName.text, length: 5)) {
      _errors.add('Nome do produto muito curto');
    }
    if (!Valid.validateText(productDescription.text, length: 7)) {
      _errors.add('Descrição do produto muito curta');
    }
    if (category == null) {
      _errors.add('Selecione a categoria');
    }

    if (_errors.isNotEmpty) {
      showOwDialog(
        context: context,
        title:
            'Houve um ou mais erros ao tentar cadastrar este produto.\n ${_errors.join('\n')}',
      );
    }

    return _errors.isEmpty;
  }

  void calcular() {
    final dynamic valorNormal = productAmount.text;
    dynamic desconto2 = productDiscount.text;
    dynamic venda;
    switch (opcao) {
      case '%':
        if ((num.tryParse(desconto2.toString()) ?? 0) > 100) {
          desconto2 = '100';
        }
        venda =
            ((double.parse(valorNormal) * double.parse(desconto2.toString())) /
                100);
        venda = double.parse(valorNormal.toString()) - venda;
        setState(() {
          precoDeVenda.text = venda.toStringAsFixed(2);
        });
        break;
      case 'R\$':
        if ((num.tryParse(desconto2.toString()) ?? 0) >
            (num.tryParse(valorNormal.toString()) ?? 0)) {
          desconto2 = valorNormal;
        }
        venda = double.parse(valorNormal.toString()) -
            double.parse(desconto2.toString());
        setState(() {
          precoDeVenda.text = venda.toStringAsFixed(2);
        });
        break;
    }
  }
}
