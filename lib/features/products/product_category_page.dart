import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ProductCategoryPage extends StatefulWidget {
  const ProductCategoryPage({super.key});

  @override
  State<ProductCategoryPage> createState() => _ProductCategoryPageState();
}

class _ProductCategoryPageState extends State<ProductCategoryPage> {
  List<ProductCategoryEcommerceEntity> _productCategories = [];

  @override
  void initState() {
    super.initState();
    callApiCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        leading: IconButton(
          icon: const Icon(EvaIcons.arrowIosDownwardOutline),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            tooltip: 'Adicionar novo item',
            icon: const Icon(Icons.add),
            onPressed: () async {
              await context.push('/${WhiteLabelEntity.current?.id}/ecommerce/product/category/add');
              await callApiCategories();
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwText(
              'Escolher categoria',
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: _productCategories.map((e) {
                return TextButton(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: OwText(
                      '${e.name}',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ),
                  onPressed: () async {
                    Navigator.pop(context, e);
                  },
                );
              }).toList(),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> callApiCategories() async {
    try {
      OwBotToast.loading();
      _productCategories = await Api.ecommerce.productCategory.listByEcommerce(
          id: '${WhiteLabelEntity.current?.setting.ecommerceId}');
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
    }
    if (this.mounted) {
      setState(() {});
    }
  }
}
