import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ProductsHomePage extends StatefulWidget {
  const ProductsHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<ProductsHomePage> createState() => _ProductsHomePageState();
}

class _ProductsHomePageState extends State<ProductsHomePage> {
  List<ProductEcommerceEntity>? products;
  List<ProductEcommerceEntity>? showedProducts;
  List<ProductCategoryEcommerceEntity>? categories;
  String? errorMsg;
  bool onlyVisibility = false;
  ProductCategoryEcommerceEntity? _currentCategory;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        centerTitle: false,
        titleSpacing: 25,
        title: const Text('Produtos e serviços'),
        actions: [
          IconButton(
            tooltip: 'Adicionar novo item',
            icon: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6),
              child: Row(
                children: [
                  const Icon(Icons.add),
                  const SizedBox(width: 4),
                  Text(
                    'Novo',
                    style: Theme.of(context).textTheme.titleSmall?.copyWith(
                          color: Theme.of(context).colorScheme.primary,
                        ),
                  ),
                ],
              ),
            ),
            onPressed: () async {
              await context.push(
                  '/${WhiteLabelEntity.current?.id}/ecommerce/product/add');
              await callApiProducts();
            },
          ),
        ],
      ),
      body: Visibility(
        visible: Valid.text(errorMsg),
        child: hasError(),
        replacement: Visibility(
          visible: categories == null,
          child: isLoading(),
          replacement: Visibility(
            visible: categories?.isEmpty ?? true,
            child: isEmpty(),
            replacement: isNotEmpty(),
          ),
        ),
      ),
    );
  }

  Widget isEmpty() {
    return Center(
      child: SingleChildScrollView(
        padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image.asset(
              'assets/images/chat.png',
              height: 250,
            ),
            const SizedBox(height: 25),
            const OwText(
              'Comece sua jornada: Cadastre sua primeira categoria do catálogo',
              style: TextStyle(fontWeight: FontWeight.w800, fontSize: 20),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 15),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 35),
              child: OwText(
                'Você está prestes a dar o primeiro passo na construção da sua presença aqui. Nossa plataforma aguarda ansiosamente pelo seu primeiro passo.',
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 25)
          ],
        ),
      ),
    );
  }

  Widget isNotEmpty() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          OwText(
            'Filtros',
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
            padding: const EdgeInsets.fromLTRB(25, 25, 25, 15),
          ),
          ScrollConfiguration(
            behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
              PointerDeviceKind.touch,
              PointerDeviceKind.mouse,
            }),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              padding: const EdgeInsets.fromLTRB(25, 0, 20, 10),
              child: Row(
                children: (categories ?? []).map((e) {
                  return CardAtalhos(
                    title: '${e.name}',
                    selecionado: _currentCategory == e,
                    onPressed: () {
                      _currentCategory = _currentCategory == e ? null : e;
                      updateShowedOrders();
                    },
                  );
                }).toList(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 25),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Center(
                  child: CardAtalhos(
                    title: 'Apenas vísivel',
                    icon: EvaIcons.eyeOutline,
                    selecionado: onlyVisibility,
                    onPressed: () {
                      onlyVisibility = !onlyVisibility;
                      updateShowedOrders();
                    },
                  ),
                ),
              ],
            ),
          ),
          const OwDivider(),
          OwText(
            'Produtos',
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
            padding: const EdgeInsets.fromLTRB(25, 25, 25, 15),
          ),
          OwGrid.builder(
            shrinkWrap: true,
            itemCount: showedProducts?.length ?? 0,
            padding: const EdgeInsets.symmetric(horizontal: 25),
            spacing: 25,
            runSpacing: 25,
            numbersInRowAccordingToWidgth: [500, 850, 1150, 1450],
            itemBuilder: (context, index) {
              return Material(
                color: Theme.of(context).colorScheme.primaryContainer,
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                child: InkWell(
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                  onTap: () async {
                    await context.push(
                      '/${WhiteLabelEntity.current?.id}/ecommerce/product/${showedProducts?[index].id}',
                      extra: {
                        'product': showedProducts?[index],
                        'notifyParent': () {}
                      },
                    );
                    await callApiProducts();
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          '${showedProducts![index].name}',
                          style:
                              Theme.of(context).textTheme.titleMedium?.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                        ),
                        Text(
                          '${paraReal(showedProducts![index].amountWithDiscount)}',
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium
                              ?.copyWith(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                        ),
                        const SizedBox(height: 6),
                        HighlightMessageCard(
                          color: ColorSystem(context: context),
                          text:
                              '${showedProducts![index].visibility ? 'Vísivel' : 'Oculto'}',
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
          const SizedBox(height: 30),
        ],
      ),
    );
  }

  Widget hasError() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 35),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          OwText(
            'Poxa, tivemos um erro aqui',
            style: Theme.of(context).textTheme.titleMedium,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 16),
          OwText(
            '$errorMsg',
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 16),
          OwButton(
            labelText: 'Voltar',
            onPressed: () {
              context.pop();
            },
          ),
          OwButton.elevated(
            labelText: 'Tentar novamente',
            onPressed: () {
              setState(() {
                errorMsg = null;
              });
              callApi();
            },
          ),
        ],
      ),
    );
  }

  Widget isLoading() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Future<void> callApi() async {
    final apis = <Future<dynamic>>[];
    if (products == null) {
      apis.add(callApiProducts());
    }
    if (categories == null) {
      apis.add(callApiCategories());
    }
    await Future.wait(apis);
    if (mounted) {
      setState(() {});
    }
  }

  Future<void> callApiProducts() async {
    try {
      products = await Api.ecommerce.products.listByEcommerce(
        id: '${WhiteLabelEntity.current?.setting.ecommerceId}',
      );
      showedProducts = products ?? [];
    } catch (e) {
      errorMsg = '$e';
    }
    setState(() {});
  }

  Future<void> callApiCategories() async {
    try {
      categories = await Api.ecommerce.productCategory.listByEcommerce(
        id: '${WhiteLabelEntity.current?.setting.ecommerceId}',
      );
    } catch (e) {
      errorMsg = '$e';
    }
    setState(() {});
  }

  void updateShowedOrders() {
    if (products != null) {
      showedProducts = [];
      products?.forEach((element) {
        if (_showOrder(element)) {
          showedProducts!.add(element);
        }
      });
    }
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {});
    });
  }

  bool _showOrder(ProductEcommerceEntity item) {
    bool isValid = false;
    if (_currentCategory == item.productCategory) {
      isValid = true;
    } else {
      if (_currentCategory == null) {
        isValid = true;
      } else {
        return false;
      }
    }
    if (onlyVisibility) {
      if (item.visibility) {
        isValid = true;
      } else {
        isValid = false;
      }
    }
    return isValid;
  }
}
