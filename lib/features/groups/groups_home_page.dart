import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class GroupsHomePage extends StatefulWidget {
  const GroupsHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  GroupsHomePageState createState() => GroupsHomePageState();
}

class GroupsHomePageState extends State<GroupsHomePage> {
  List<GroupEntity>? groups;
  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const Text('Grupos'),
      ),
      floatingActionButton: groups == null
          ? null
          : FloatingActionButton.extended(
              heroTag: null,
              icon: const Icon(
                Icons.add,
              ),
              label: const Text('Novo grupo'),
              onPressed: () async {
                final retorno = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/groups/new',
                );
                if (retorno is GroupEntity) {
                  groups!.insert(0, retorno);
                  setState(() {});
                }
              },
            ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Visibility(
              visible: groups != null,
              replacement: Container(
                constraints: const BoxConstraints(
                  minHeight: 250,
                ),
                child: const Center(
                  child: CircularProgressIndicator(),
                ),
              ),
              child: Visibility(
                visible: groups?.isNotEmpty ?? false,
                replacement: Container(
                  margin: appSM.isWeb
                      ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                      : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  padding: const EdgeInsets.all(25),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Theme.of(context).colorScheme.outlineVariant,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                  ),
                  child: const Center(
                    child: Text('Você ainda não cadastrou nenhum grupo. Adicione o primeiro para vincular os usuários.'),
                  ),
                ),
                child: OwGrid.builder(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  shrinkWrap: true,
                  spacing: 15,
                  runSpacing: 15,
                  itemCount: groups?.length ?? 0,
                  numbersInRowAccordingToWidgth: const [
                    680,
                    1050,
                    1400,
                    2900
                  ],
                  itemBuilder: (context, index) {
                    return Ink(
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(15)),
                        color: Theme.of(context).colorScheme.secondaryContainer,
                      ),
                      child: InkWell(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(15)),
                        onTap: () async {
                          final result = await context.push(
                            '/${WhiteLabelEntity.current?.id}/dashboard/groups/${groups![index].id}',
                            extra: groups![index],
                          );
                          if (result is GroupEntity) {
                            groups![index] = result;
                            setState(() {});
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(25),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                groups![index].name,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                ),
                              ),
                              const SizedBox(height: 8),
                              Text(
                                '${groups![index].participants.length} participantes',
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void callApi([skip = 0]) async {
    try {
      groups = await Api.groups.list(query: {'skip': groups?.length ?? 0});
    } catch (_) {
      //
    }
    setState(() {});
  }
}
