import 'package:backoffice/ui/components/alerta_simples.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class GroupsCrudPage extends StatefulWidget {
  const GroupsCrudPage({this.group, Key? key}) : super(key: key);
  final GroupEntity? group;
  @override
  State<GroupsCrudPage> createState() => _GroupsCrudPageState();
}

class _GroupsCrudPageState extends State<GroupsCrudPage> {
  TextEditingController name = TextEditingController();
  List<String> users = [];

  @override
  void initState() {
    super.initState();
    if (widget.group != null) {
      name.text = widget.group!.name;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes do grupo'),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              controller: name,
              margin: const EdgeInsets.fromLTRB(25, 10, 25, 0),
              labelText: 'Nome do grupo',
            ),
            Padding(
              padding: const EdgeInsets.all(25),
              child: box(
                title: 'Usuários',
                children: [
                  Center(
                    child: Text(
                      '${users.length} usuários',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                  ),
                  OwButton(
                    margin: const EdgeInsets.only(top: 15),
                    labelText: 'Trocar',
                    onPressed: () async {
                      final result = await context.push(
                        '/${WhiteLabelEntity.current?.id}/dashboard/select/users',
                        extra: users,
                      );
                      if (result is List<String>) {
                        users = result;
                        setState(() {});
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          labelText: 'Salvar',
          onPressed: _onPressed,
        ),
      ),
    );
  }

  void _onPressed() async {
    if (name.text.trim().isEmpty) {
      showDialog(
        context: context,
        builder: (context) => const AlertaSimples(
          'Existem um ou mais campos que precisam ser marcados.',
        ),
      );
    } else {
      if (widget.group != null) {
        final GroupEntity notification = widget.group!.copyWith(
          name: name.text.trim(),
          participants: users,
        );

        try {
          OwBotToast.loading();
          final response = await Api.groups.update(
            id: notification.id!,
            data: notification,
          );
          if (mounted) {
            context.pop(response);
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Grupo atualizado com sucesso.',
              ),
            );
          }
        } catch (_) {
          OwBotToast.close();
          if (mounted) {
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Ocorreu um erro ao atualizar o grupo.',
              ),
            );
          }
        }
      } else {
        final GroupEntity notification = GroupEntity(
          name: name.text.trim(),
          isBlocked: [],
          participants: users,
        );
        try {
          final response = await Api.groups.add(
            data: notification,
          );
          if (mounted) {
            context.pop(response);
            showDialog(
              context: context,
              builder: (context) => const AlertaSimples(
                'Grupo cadastrado com sucesso.',
              ),
            );
          }
        } catch (e) {
          if (mounted) {
            showOwDialog(
              context: context,
              title: 'Ocorreu um erro',
              description: '$e',
            );
          }
        }
      }
    }
  }

  Widget box({required String title, required List<Widget> children}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.primaryContainer,
            borderRadius: const BorderRadius.vertical(
              top: Radius.circular(10),
            ),
          ),
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
            vertical: 10,
          ),
          child: Text(
            title,
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                  color: Theme.of(context).colorScheme.onPrimaryContainer,
                ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.secondaryContainer,
            borderRadius: const BorderRadius.vertical(
              bottom: Radius.circular(10),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: children,
            ),
          ),
        )
      ],
    );
  }
}
