import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class SelectUsersPage extends StatefulWidget {
  const SelectUsersPage({
    Key? key,
    required this.users,
  }) : super(key: key);
  final List<String> users;

  @override
  State<SelectUsersPage> createState() => SelectUsersPageState();
}

class SelectUsersPageState extends State<SelectUsersPage> {

  List<UserEntity>? users;
  List<String> usersSelected = [];

  @override
  void initState() {
    super.initState();
    callApi();
    usersSelected = widget.users;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 25,
                vertical: 10,
              ),
              child: Text(
                'Selecione os usuários',
                style: Theme.of(context).textTheme.headlineLarge,
              ),
            ),
            const SizedBox(height: 15),
            Visibility(
              visible: users != null,
              replacement: const Center(
                child: CircularProgressIndicator(),
              ),
              child: ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.symmetric(horizontal: 25),
                itemCount: users?.length ?? 0,
                separatorBuilder: (context, index) {
                  return const SizedBox(height: 25);
                },
                itemBuilder: (context, index) {
                  return Ink(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(15)),
                      color: Theme.of(context).colorScheme.secondaryContainer,
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(15)),
                      onTap: () async {
                        setState(() {
                          if(usersSelected.toString().contains(users![index].id)) {
                            usersSelected.remove(users![index].id);
                          } else {
                            usersSelected.add(users![index].id);
                          }
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(25),
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Text(
                                    users![index].name,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Checkbox(
                              value: usersSelected.toString().contains(users![index].id), 
                              onChanged: (value) {
                                setState(() {
                                  if(usersSelected.toString().contains(users![index].id)) {
                                    usersSelected.remove(users![index].id);
                                  } else {
                                    usersSelected.add(users![index].id);
                                  }
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: null,
        child: const Icon(EvaIcons.saveOutline),
        onPressed: () {
          Navigator.pop(context, usersSelected);
        },
      ),
    );
  }

  void callApi([skip = 0]) async {
    try {
      users = await Api.user.listAll();
    } catch (_) {
      //
    }
    setState(() {});
  }
}