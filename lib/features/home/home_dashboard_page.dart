import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../ui/components/box_message_not_read.dart';

class HomeDashboardPage extends StatefulWidget {
  const HomeDashboardPage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<HomeDashboardPage> createState() => _HomeDashboardPageState();
}

class _HomeDashboardPageState extends State<HomeDashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appSM.isWeb ? null : OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        actions: [
          _tutorial(),
          _roadmap(),
          _view(),
          _settings(),
          _logout(),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            border: MediaQuery.of(context).size.width > 1050
                ? Border(
                    right: BorderSide(
                      width: 2,
                      color: Theme.of(context).colorScheme.secondaryContainer,
                    ),
                  )
                : null,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Olá, ${userSM.user?.name.split(" ")[0]}!',
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const SizedBox(width: 15),
                    CardAtalhos(
                      title: 'Acessar',
                      icon: EvaIcons.externalLinkOutline,
                      onPressed: () {
                        launchUrl(
                          Uri.parse('${WhiteLabelEntity.current?.setting.urlWebApplication}'),
                          mode: LaunchMode.externalApplication,
                        );
                      },
                    ),
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
                child: BoxMessageNotRead(),
              ),
              Container(
                margin: const EdgeInsets.all(24),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16),
                  ),
                  color: Theme.of(context).colorScheme.surface,
                  border: Border.all(
                    width: 1,
                    color: Theme.of(context).colorScheme.surfaceContainerHighest,
                  ),
                ),
                padding: const EdgeInsets.only(top: 24, bottom: 32),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        'Análises',
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const SizedBox(height: 12),
                    const BasicStatisticsWidget(),
                    const SizedBox(height: 25),
                    OwGrid.builder(
                      itemCount: 3,
                      padding: const EdgeInsets.symmetric(
                        vertical: 0,
                        horizontal: 25,
                      ),
                      numbersInRowAccordingToWidgth: [600, 601],
                      itemBuilder: (context, index) {
                        if (index == 0) {
                          return const StatisticsPeriodWidget(
                            title: 'Novos pedidos',
                            type: 'orders',
                          );
                        } else if (index == 1) {
                          return const StatisticsPeriodWidget(
                            title: 'Novos usuários',
                            type: 'users',
                          );
                        }
                        return const StatisticsPeriodWidget(
                          title: 'Novas empresas',
                          type: 'companies',
                        );
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }

  Widget _settings() {
    return Tooltip(
      message: 'Configurações',
      child: IconButton(
        icon: const Icon(EvaIcons.settings2Outline),
        onPressed: () {
          context.push('/settings');
        },
      ),
    );
  }

  Widget _view() {
    return Tooltip(
      message: 'Visualizar plataforma',
      child: IconButton(
        icon: const Icon(EvaIcons.externalLinkOutline),
        onPressed: () {
          launchUrl(
            Uri.parse('${WhiteLabelEntity.current?.setting.urlWebApplication}'),
            mode: LaunchMode.externalApplication,
          );
        },
      ),
    );
  }

  Widget _tutorial() {
    return Tooltip(
      message: 'Tutorial da plataforma',
      child: IconButton(
        icon: const Icon(EvaIcons.bookOutline),
        onPressed: () {
          context.push('/${WhiteLabelEntity.current?.id}/dashboard/tutorial');
        },
      ),
    );
  }

  Widget _roadmap() {
    return Tooltip(
      message: 'Roteiro do produto',
      child: IconButton(
        icon: const Icon(Icons.newspaper_outlined),
        onPressed: () {
          context.push('/${WhiteLabelEntity.current?.id}/dashboard/roadmap');
        },
      ),
    );
  }

  Widget _logout() {
    return Tooltip(
      message: 'Trocar ambiente',
      child: IconButton(
        icon: const Icon(EvaIcons.swapOutline),
        onPressed: () {
          context.go('/');
        },
      ),
    );
  }
}

class StatisticsPeriodWidget extends StatefulWidget {
  const StatisticsPeriodWidget({
    super.key,
    required this.title,
    required this.type,
  });
  final String title;
  final String type;
  @override
  State<StatisticsPeriodWidget> createState() => _StatisticsPeriodWidgetState();
}

class _StatisticsPeriodWidgetState extends State<StatisticsPeriodWidget> {
  StatisticsPeriodEntity? statisticsPeriodEntity;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.outlineVariant,
        border: Border.all(
          width: 1,
          color: Theme.of(context).colorScheme.outlineVariant,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      padding: const EdgeInsets.all(20),
      child: Visibility(
        visible: statisticsPeriodEntity != null,
        replacement: const Center(child: CircularProgressIndicator()),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('${widget.title}'),
            const SizedBox(height: 6),
            Text(
              '${statisticsPeriodEntity?.current} nessa semana',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 4),
            Text('${statisticsPeriodEntity?.before} na semana passada'),
          ],
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      if (widget.type == 'companies') {
        statisticsPeriodEntity = await Api.statistics.companyByPeriod(params: {
          'period': 7,
        });
      } else if (widget.type == 'users') {
        statisticsPeriodEntity = await Api.statistics.userByPeriod(params: {
          'period': 7,
        });
      } else {
        statisticsPeriodEntity = await Api.statistics.scheduleByPeriod(params: {
          'period': 7,
        });
      }
    } catch (e) {
      //
    }
    setState(() {});
  }
}

class ItemFunctionalities {
  ItemFunctionalities({
    required this.title,
    required this.icon,
    required this.route,
  });
  String title;
  IconData icon;
  String route;
}

class BasicStatisticsWidget extends StatefulWidget {
  const BasicStatisticsWidget({super.key});

  @override
  State<BasicStatisticsWidget> createState() => _BasicStatisticsWidgetState();
}

class _BasicStatisticsWidgetState extends State<BasicStatisticsWidget> {
  BasicStatisticsEntity? basic;

  List temp = [];

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    if (basic == null) {
      return Container(
        height: 120,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return OwGrid.builder(
      padding: const EdgeInsets.symmetric(
        vertical: 0,
        horizontal: 25,
      ),
      itemCount: temp.length,
      numbersInRowAccordingToWidgth: [650, 1600, 1601],
      itemBuilder: (context, index) {
        return Material(
          shadowColor: Colors.transparent,
          color: Colors.transparent,
          child: Ink(
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                color: Theme.of(context).colorScheme.surfaceVariant,
              ),
              color: Theme.of(context).colorScheme.onPrimary,
              borderRadius: const BorderRadius.all(
                Radius.circular(15),
              ),
            ),
            child: InkWell(
              borderRadius: const BorderRadius.all(
                Radius.circular(15),
              ),
              onTap: () {
                context.push(temp[index]['route']);
              },
              child: Padding(
                padding: const EdgeInsets.all(22),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            '${temp[index]['title']}',
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge
                                ?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                        ),
                        const SizedBox(width: 15),
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            color: Theme.of(context)
                                .colorScheme
                                .onPrimaryContainer,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          child: Center(
                            child: Icon(
                              temp[index]['icon'],
                              color: Theme.of(context)
                                  .colorScheme
                                  .primaryContainer,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Text(
                      '${temp[index]['count']}',
                      style:
                          Theme.of(context).textTheme.headlineLarge?.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> callApi() async {
    try {
      basic = await Api.statistics.basic();
      temp = [
        {
          'title': 'Empresas',
          'icon': EvaIcons.briefcaseOutline,
          'count': basic?.companies ?? 0,
          'route': '/${WhiteLabelEntity.current?.id}/dashboard/company'
        },
        {
          'title': 'Usuários',
          'icon': EvaIcons.peopleOutline,
          'count': basic?.users ?? 0,
          'route': '/${WhiteLabelEntity.current?.id}/dashboard/user'
        },
        {
          'title': 'Produtos',
          'icon': EvaIcons.shoppingBagOutline,
          'count': basic?.products ?? 0,
          'route':'/${WhiteLabelEntity.current?.id}/ecommerce/product/home',
        },
        {
          'title': 'Pedidos',
          'icon': EvaIcons.archiveOutline,
          'count': basic?.schedules ?? 0,
          'route': '/${WhiteLabelEntity.current?.id}/ecommerce/orders/home',
        },
      ];
    } catch (e) {
      //
    }
    setState(() {});
  }
}
