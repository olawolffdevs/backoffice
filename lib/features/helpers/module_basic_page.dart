// import 'package:backoffice/teste.dart';
// import 'package:flutter/material.dart';

// import '../../components/menu_widget.dart';
// import 'navigation_widget.dart';

// class ModuleBasicPage extends StatefulWidget {
//   final SettingsModel settingsModel;
//   final int index;
//   final Widget child;
//   final String? title;
//   final String? description;
//   final Widget? floatingActionButton;
//   final bool onlyChild;
//   final bool scroolling;
//   final FloatingActionButtonLocation? floatingActionButtonLocation;
//   final Widget? appBarWidget;

//   const ModuleBasicPage({
//     Key? key,
//     required this.settingsModel,
//     required this.index,
//     required this.child,
//     this.floatingActionButton,
//     this.title,
//     this.description,
//     this.onlyChild = false,
//     this.scroolling = true,
//     this.floatingActionButtonLocation,
//     this.appBarWidget,
//   }) : super(key: key);

//   @override
//   _ModuleBasicPageState createState() => _ModuleBasicPageState();
// }

// class _ModuleBasicPageState extends State<ModuleBasicPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: PreferredSize(
//         preferredSize: const Size.fromHeight(80.0),
//         child: MenuWidget(
//           title: widget.settingsModel.name,
//           appBarWidget: widget.appBarWidget,
//         ),
//       ),
//       body: SizedBox(
//         width: MediaQuery.sizeOf(context).width,
//         height: MediaQuery.sizeOf(context).height,
//         child: Row(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             NavigationWidget(
//               index: widget.index,
//               menu: widget.settingsModel.menu,
//               web: true,
//             ),
//             Expanded(
//               child: widget.onlyChild
//                   ? widget.child
//                   : widget.scroolling
//                       ? SingleChildScrollView(
//                           padding: const EdgeInsets.symmetric(
//                             vertical: 25,
//                           ),
//                           physics: const BouncingScrollPhysics(),
//                           child: body())
//                       : body(),
//             ),
//           ],
//         ),
//       ),
//       floatingActionButton: widget.floatingActionButton,
//       floatingActionButtonLocation: widget.floatingActionButtonLocation,
//       bottomNavigationBar: NavigationWidget(
//         index: widget.index,
//         menu: widget.settingsModel.menu,
//         web: false,
//       ),
//     );
//   }

//   Widget body() {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.stretch,
//       children: [
//         Visibility(
//           visible: widget.title != null,
//           child: Padding(
//             padding: const EdgeInsets.only(
//               bottom: 10,
//               left: 25,
//               right: 25,
//             ),
//             child: Text(
//               widget.title ?? "",
//               style: Theme.of(context).textTheme.displayLarge,
//             ),
//           ),
//         ),
//         Visibility(
//           visible: widget.description != null,
//           child: Padding(
//             padding: const EdgeInsets.only(
//               bottom: 25,
//               left: 25,
//               right: 25,
//               top: 15,
//             ),
//             child: Text(
//               widget.description ?? "",
//               style: Theme.of(context).textTheme.bodyLarge,
//             ),
//           ),
//         ),
//         widget.child,
//       ],
//     );
//   }
// }
