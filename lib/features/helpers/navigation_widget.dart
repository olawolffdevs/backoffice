// import 'package:flutter/material.dart';
// import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
// import '../../../domain/entities/menu_item_entity.dart';

// class NavigationWidget extends StatefulWidget {
//   const NavigationWidget(
//       {Key? key, required this.index, required this.web, required this.menu})
//       : super(key: key);
//   final int index;
//   final bool web;
//   final List<MenuItemEntity> menu;

//   @override
//   _NavigationWidgetState createState() => _NavigationWidgetState();
// }

// class _NavigationWidgetState extends State<NavigationWidget> {
//   @override
//   Widget build(BuildContext context) {
//     if (widget.menu.length < 2) {
//       return const SizedBox();
//     }
//     if (widget.web) {
//       return MediaQuery.sizeOf(context).width >= 580
//           ? _navigationRail()
//           : const SizedBox();
//     } else {
//       return MediaQuery.sizeOf(context).width < 580
//           ? _navigationBar()
//           : const SizedBox();
//     }
//   }

//   Widget _navigationBar() {
//     return NavigationBar(
//       onDestinationSelected: (int index) {
//         context.push(widget.menu[index].route);
//       },
//       destinations: widget.menu.map<NavigationDestination>((e) {
//         return NavigationDestination(
//           icon: Icon(e.icon),
//           selectedIcon: Icon(e.selectedIcon),
//           label: e.title,
//         );
//       }).toList(),
//       selectedIndex: widget.index,
//     );
//   }

//   Widget _navigationRail() {
//     return Material(
//       color: Theme.of(context).colorScheme.background,
//       child: Container(
//         width: 245,
//         color: Theme.of(context).colorScheme.secondaryContainer.withOpacity(.2),
//         constraints: const BoxConstraints(maxWidth: 400, minWidth: 140),
//         height: MediaQuery.sizeOf(context).height,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Expanded(
//               child: SingleChildScrollView(
//                 child: ListView.separated(
//                   padding: const EdgeInsets.all(25),
//                   shrinkWrap: true,
//                   physics: const NeverScrollableScrollPhysics(),
//                   itemCount: widget.menu.length,
//                   separatorBuilder: (context, index) {
//                     return const SizedBox(height: 10);
//                   },
//                   itemBuilder: (context, index) {
//                     return ButtonMenuWeb(
//                       label: widget.menu[index].title,
//                       icon: widget.menu[index].icon,
//                       selected: widget.index == index,
//                       onPressed: () {
//                         context.push(widget.menu[index].route);
//                       },
//                     );
//                   },
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class ButtonMenuWeb extends StatelessWidget {
//   final String label;
//   final IconData icon;
//   final bool selected;
//   final Function() onPressed;
//   const ButtonMenuWeb({
//     Key? key,
//     required this.label,
//     required this.icon,
//     this.selected = false,
//     required this.onPressed,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return ClipRRect(
//       borderRadius: const BorderRadius.all(
//         Radius.circular(15),
//       ),
//       child: ElevatedButton(
//         style: ButtonStyle(
//           backgroundColor: MaterialStateProperty.all(
//             selected
//                 ? Theme.of(context).colorScheme.secondaryContainer
//                 : Colors.transparent,
//           ),
//           elevation: MaterialStateProperty.all(0),
//           padding: MaterialStateProperty.all(EdgeInsets.zero),
//         ),
//         onPressed: onPressed,
//         child: Container(
//           padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             mainAxisSize: MainAxisSize.max,
//             children: [
//               Icon(
//                 icon,
//                 color: selected
//                     ? Theme.of(context).colorScheme.onSecondaryContainer
//                     : Theme.of(context).colorScheme.onSurfaceVariant,
//               ),
//               const SizedBox(
//                 width: 16,
//               ),
//               Text(
//                 label,
//                 style: TextStyle(
//                   fontWeight: FontWeight.w800,
//                   color: selected
//                       ? Theme.of(context).colorScheme.onSecondaryContainer
//                       : Theme.of(context).colorScheme.onSurfaceVariant,
//                 ),
//               ),
//               const SizedBox(
//                 width: 20,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
