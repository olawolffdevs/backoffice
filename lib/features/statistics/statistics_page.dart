import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class StatisticsPage extends StatefulWidget {
  const StatisticsPage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<StatisticsPage> createState() => _StatisticsPageState();
}

class _StatisticsPageState extends State<StatisticsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const Text('Estatísticas'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwText(
              'Use os dados desses relatórios para tomar melhores decisões de negócios',
              style: Theme.of(context).textTheme.bodyMedium,
              padding: const EdgeInsets.fromLTRB(25, 15, 25, 13),
            ),
            Container(
              margin: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(16),
                ),
                color: Theme.of(context).colorScheme.surface,
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                ),
              ),
              padding: const EdgeInsets.only(top: 24, bottom: 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  sectionTitle(
                    title: 'Pedidos e agendamentos',
                  ),
                  itemListTile(
                    title: 'por período de tempo',
                    description:
                        'Compare a quantidade de pedidos recebidos em um determinado período do tempo.',
                    onTap: () {
                      context.push(
                          '/${WhiteLabelEntity.current?.id}/dashboard/statistics/basic/by-period/sales');
                    },
                  ),
                  itemListTile(
                    title: 'por dia da semana',
                    description:
                        'Qual dia da semana seus usuários mais buscam entrar para fazer pedidos/agendamentos.',
                    onTap: () {
                      context.push(
                          '/${WhiteLabelEntity.current?.id}/dashboard/statistics/most/booked-weekdays');
                    },
                  ),
                  itemListTile(
                    title: 'por forma de entrega',
                    description:
                        'Qual a forma de entrega preferida dos seus usuários.',
                    onTap: () {
                      context.push(
                          '/${WhiteLabelEntity.current?.id}/dashboard/statistics/most/service-location-most-schedule');
                    },
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(16),
                ),
                color: Theme.of(context).colorScheme.surface,
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                ),
              ),
              padding: const EdgeInsets.only(top: 24, bottom: 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  sectionTitle(
                    title: 'Produtos e serviços',
                  ),
                  itemListTile(
                    title: 'mais solicitados',
                    description:
                        'Compare a quantidade de pedidos recebidos em um determinado período do tempo.',
                    onTap: () {
                      context.push(
                          '/${WhiteLabelEntity.current?.id}/dashboard/statistics/most/products-most-schedule');
                    },
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(16),
                ),
                color: Theme.of(context).colorScheme.surface,
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                ),
              ),
              padding: const EdgeInsets.only(top: 24, bottom: 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  sectionTitle(
                    title: 'Categorias',
                  ),
                  itemListTile(
                    title: 'mais vendidas',
                    description:
                        'Compare a quantidade de pedidos recebidos em um determinado período do tempo.',
                    onTap: () {
                      context.push(
                          '/${WhiteLabelEntity.current?.id}/dashboard/statistics/most/categories-most-schedule');
                    },
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(16),
                ),
                color: Theme.of(context).colorScheme.surface,
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                ),
              ),
              padding: const EdgeInsets.only(top: 24, bottom: 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  sectionTitle(
                    title: 'Empresas',
                  ),
                  itemListTile(
                    title: 'Novas empresas por período',
                    description:
                        'Compare a quantidade de empresas novas em um determinado período do tempo.',
                    onTap: () {
                      context.push(
                          '/${WhiteLabelEntity.current?.id}/dashboard/statistics/basic/by-period/companies');
                    },
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(16),
                ),
                color: Theme.of(context).colorScheme.surface,
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                ),
              ),
              padding: const EdgeInsets.only(top: 24, bottom: 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  sectionTitle(
                    title: 'Usuários',
                  ),
                  itemListTile(
                    title: 'Novos usuários por período',
                    description:
                        'Compare a quantidade de usuários novos em um determinado período do tempo.',
                    onTap: () {
                      context.push(
                          '/${WhiteLabelEntity.current?.id}/dashboard/statistics/basic/by-period/users');
                    },
                  ),
                ],
              ),
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),
    );
  }

  Widget sectionTitle({required String title, String? description}) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          OwText(
            title,
            style: Theme.of(context).textTheme.titleLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          Visibility(
            visible: Valid.text(description),
            child: OwText(
              '$description',
              padding: const EdgeInsets.only(top: 6),
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          ),
        ],
      ),
    );
  }

  Widget divider() {
    return Container(
      margin: const EdgeInsets.only(top: 12),
      height: 10,
      width: MediaQuery.of(context).size.width,
      color: Theme.of(context).colorScheme.secondaryContainer,
    );
  }

  Widget itemListTile(
      {required String title,
      required String description,
      required Function() onTap}) {
    return Material(
      shadowColor: Colors.transparent,
      color: Colors.transparent,
      child: ListTile(
        onTap: onTap,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 25,
          vertical: 12,
        ),
        title: Text(
          '$title',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(
                fontWeight: FontWeight.bold,
              ),
        ),
        subtitle: Text('$description'),
        trailing: const Icon(EvaIcons.arrowIosForwardOutline),
      ),
    );
  }
}
