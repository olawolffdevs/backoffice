import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class SalesByPeriodPage extends StatefulWidget {
  const SalesByPeriodPage({super.key});

  @override
  State<SalesByPeriodPage> createState() => _SalesByPeriodPageState();
}

class _SalesByPeriodPageState extends State<SalesByPeriodPage> {
  String currentPeriod = 'Hoje';
  String beforePeriod = 'Ontem';
  String selectedPeriod = 'Hoje';
  double difference = 0;
  StatisticsPeriodEntity? result;
  int period = 1;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Vendas por período'),
      ),
      body: result == null
        ? const Center(child: CircularProgressIndicator())
        : SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ScrollConfiguration(
              behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                PointerDeviceKind.touch,
                PointerDeviceKind.mouse,
              }),
              child: SingleChildScrollView(
                padding: const EdgeInsets.fromLTRB(25, 10, 15, 10),
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    CardAtalhos(
                      title: 'Hoje',
                      selecionado: selectedPeriod == 'Hoje',
                      onPressed: () {
                        setState(() {
                          currentPeriod = 'Hoje';
                          beforePeriod = 'Ontem';
                          selectedPeriod = 'Hoje';
                          period = 1;
                        });
                        callApi();
                      },
                    ),
                    CardAtalhos(
                      title: 'Semana',
                      selecionado: selectedPeriod == 'Semana',
                      onPressed: () {
                        setState(() {
                          currentPeriod = 'Essa semana';
                          beforePeriod = 'Semana passada';
                          selectedPeriod = 'Semana';
                          period = 7;
                        });
                        callApi();
                      },
                    ),
                    CardAtalhos(
                      title: 'Mês',
                      selecionado: selectedPeriod == 'Mês',
                      onPressed: () {
                        setState(() {
                          currentPeriod = 'Esse mês';
                          beforePeriod = 'Mês passado';
                          selectedPeriod = 'Mês';
                          period = 30;
                        });
                        callApi();
                      },
                    ),
                    CardAtalhos(
                      title: 'Semestre',
                      selecionado: selectedPeriod == 'Semestre',
                      onPressed: () {
                        setState(() {
                          currentPeriod = 'Esse semestre';
                          beforePeriod = 'Semestre passado';
                          selectedPeriod = 'Semestre';
                          period = 180;
                        });
                        callApi();
                      },
                    ),
                    CardAtalhos(
                      title: 'Anual',
                      selecionado: selectedPeriod == 'Anual',
                      onPressed: () {
                        setState(() {
                          currentPeriod = 'Últimos 365 dias';
                          beforePeriod = 'Ano anterior';
                          selectedPeriod = 'Anual';
                          period = 365;
                        });
                        callApi();
                      },
                    ),
                  ],
                ),
              ),
            ),
            divider(),
            _period(
              title: 'Período atual',
              tag: '$currentPeriod',
              value: result?.current ?? 0,
            ),
            divider(),
            _period(
              title: 'Período passado',
              tag: '$beforePeriod',
              value: result?.before ?? 0,
            ),
            divider(),
            Container(
              decoration: BoxDecoration(
                color: difference >= 0 ? Theme.of(context).colorScheme.secondaryContainer : Theme.of(context).colorScheme.errorContainer,
                borderRadius: const BorderRadius.all(
                  Radius.circular(16),
                ),
              ),
              margin: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
              padding: const EdgeInsets.all(25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    difference >= 0 ? EvaIcons.barChart2Outline : EvaIcons.barChart,
                    color: difference >= 0 ? Theme.of(context).colorScheme.onSecondaryContainer : Theme.of(context).colorScheme.onErrorContainer,
                  ),
                  const SizedBox(height: 10),
                  Text(
                    difference >= 0 ? 'Aumento de' : 'Diminuição de',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: difference >= 0 ? Theme.of(context).colorScheme.onSecondaryContainer : Theme.of(context).colorScheme.onErrorContainer,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    '${difference.toStringAsFixed(2)}%',
                    style: Theme.of(context).textTheme.displayMedium?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: difference >= 0 ? Theme.of(context).colorScheme.onSecondaryContainer : Theme.of(context).colorScheme.onErrorContainer,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'em relação ao período passado',
                    style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                      color: difference >= 0 ? Theme.of(context).colorScheme.onSecondaryContainer : Theme.of(context).colorScheme.onErrorContainer,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      result = await Api.statistics.scheduleByPeriod(
        params: {
          'period': period,
        }
      );
      if(result!.current == 0 && result!.before == 0) {
        difference = 0;
      } else if(result!.before == 0) {
        difference = 100;
      } else {
        difference = (((result!.current - result!.before) / result!.before) * 100);
      }
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      OwBotToast.toast('$e');
    }
    setState(() {});
  }

  Widget divider() {
    return Container(
      margin: const EdgeInsets.only(top: 12, bottom: 15),
      height: 10,
      width: MediaQuery.of(context).size.width,
      color: Theme.of(context).colorScheme.secondaryContainer,
    );
  }

  Widget _period({required String title, required String tag, required int value}) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          OwText(
            '$title ($tag)',
            padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
              fontWeight: FontWeight.w400,
            ),
          ),
          OwText(
            '$value vendas',
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 10),
            style: Theme.of(context).textTheme.headlineLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
