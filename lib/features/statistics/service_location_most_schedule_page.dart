import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ServiceLocationMostSchedulePage extends StatefulWidget {
  const ServiceLocationMostSchedulePage({super.key});

  @override
  State<ServiceLocationMostSchedulePage> createState() => _ServiceLocationMostSchedulePageState();
}

class _ServiceLocationMostSchedulePageState extends State<ServiceLocationMostSchedulePage> {
  String selectedPeriod = 'Todo';
  List<StatisticsMostItemEntity>? result;
  int period = 1000;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Forma de entrega mais usadas'),
      ),
      body: result == null
        ? const Center(child: CircularProgressIndicator())
        : SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ScrollConfiguration(
              behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                PointerDeviceKind.touch,
                PointerDeviceKind.mouse,
              }),
              child: SingleChildScrollView(
                padding: const EdgeInsets.fromLTRB(25, 10, 15, 10),
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    CardAtalhos(
                      title: 'Todo o período',
                      selecionado: true,
                      onPressed: () {
                        setState(() {
                          selectedPeriod = 'Todo';
                          period = 1000;
                        });
                        callApi();
                      },
                    ),
                  ],
                ),
              ),
            ),
            divider(),
            ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: result?.length ?? 0,
              separatorBuilder: (context, index) {
                return const OwDivider();
              },
              itemBuilder: (context, index) {
                return ListTile(
                  leading: CircleAvatar(
                    child: Text('${index + 1}'),
                  ),
                  title: Text(convert(result![index].name)),
                  subtitle: Text('${result![index].count} vezes'),
                );
              },
            ),
            divider(),
          ],
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      result = await Api.statistics.serviceLocationMostSchedule(
        params: {}
      );
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      OwBotToast.toast('$e');
    }
    setState(() {});
  }

  Widget divider() {
    return Container(
      margin: const EdgeInsets.only(top: 12, bottom: 15),
      height: 10,
      width: MediaQuery.of(context).size.width,
      color: Theme.of(context).colorScheme.secondaryContainer,
    );
  }

  String convert(String value) {
    switch (value) {
      case 'online':
        return 'Pedidos online (internet)';
      case 'delivery':
        return 'Pedidos para entregar (endereço do cliente)';
      case 'takeout':
        return 'Pedidos no meu endereço (endereço da empresa)';
    }
    return '';
  }
}
