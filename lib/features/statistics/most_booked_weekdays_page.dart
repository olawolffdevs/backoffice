import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class MostBookedWeekdaysPage extends StatefulWidget {
  const MostBookedWeekdaysPage({super.key});

  @override
  State<MostBookedWeekdaysPage> createState() => _MostBookedWeekdaysPageState();
}

class _MostBookedWeekdaysPageState extends State<MostBookedWeekdaysPage> {
  String selectedPeriod = 'Todo';
  List<StatisticsMostBookedWeekdaysEntity>? result;
  int period = 1000;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Forma de entrega mais usadas'),
      ),
      body: result == null
        ? const Center(child: CircularProgressIndicator())
        : SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ScrollConfiguration(
              behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                PointerDeviceKind.touch,
                PointerDeviceKind.mouse,
              }),
              child: SingleChildScrollView(
                padding: const EdgeInsets.fromLTRB(25, 10, 15, 10),
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    CardAtalhos(
                      title: 'Todo o período',
                      selecionado: true,
                      onPressed: () {
                        setState(() {
                          selectedPeriod = 'Todo';
                          period = 1000;
                        });
                        callApi();
                      },
                    ),
                  ],
                ),
              ),
            ),
            divider(),
            ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: result?.length ?? 0,
              separatorBuilder: (context, index) {
                return const OwDivider();
              },
              itemBuilder: (context, index) {
                return ListTile(
                  contentPadding: const EdgeInsets.symmetric(horizontal: 25),
                  title: Text('${convert(result![index].day)}'),
                  subtitle: Text('${result![index].count} pedidos realizados'),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      result = await Api.statistics.mostBookedWeekdays(
        params: {}
      );
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      OwBotToast.toast('$e');
    }
    setState(() {});
  }

  Widget divider() {
    return Container(
      margin: const EdgeInsets.only(top: 12, bottom: 15),
      height: 10,
      width: MediaQuery.of(context).size.width,
      color: Theme.of(context).colorScheme.secondaryContainer,
    );
  }

  String convert(int value) {
    switch (value) {
      case 0:
        return 'Domingo';
      case 1:
        return 'Segunda-feira';
      case 2:
        return 'Terça-feira';
      case 3:
        return 'Quarta-feira';
      case 4:
        return 'Quinta-feira';
      case 5:
        return 'Sexta-feira';
      case 6:
        return 'Sábado';
    }
    return '';
  }
}
