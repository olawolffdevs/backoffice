import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ProductsMostSchedulePage extends StatefulWidget {
  const ProductsMostSchedulePage({super.key});

  @override
  State<ProductsMostSchedulePage> createState() => _ProductsMostSchedulePageState();
}

class _ProductsMostSchedulePageState extends State<ProductsMostSchedulePage> {
  String selectedPeriod = 'Todo';
  List<StatisticsMostItemEntity>? result;
  int period = 1000;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Produtos mais vendidos'),
      ),
      body: result == null
        ? const Center(child: CircularProgressIndicator())
        : SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ScrollConfiguration(
              behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                PointerDeviceKind.touch,
                PointerDeviceKind.mouse,
              }),
              child: SingleChildScrollView(
                padding: const EdgeInsets.fromLTRB(25, 10, 15, 10),
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    CardAtalhos(
                      title: 'Todo o período',
                      selecionado: true,
                      onPressed: () {
                        setState(() {
                          selectedPeriod = 'Todo';
                          period = 1000;
                        });
                        callApi();
                      },
                    ),
                  ],
                ),
              ),
            ),
            divider(),
            ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: result?.length ?? 0,
              separatorBuilder: (context, index) {
                return const OwDivider();
              },
              itemBuilder: (context, index) {
                return ListTile(
                  leading: CircleAvatar(
                    child: Text('${index + 1}'),
                  ),
                  title: Text(result![index].name),
                  subtitle: Text('${result![index].count} itens vendidos'),
                );
              },
            ),
            divider(),
          ],
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      result = await Api.statistics.productsMostSchedule(
        params: {}
      );
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      OwBotToast.toast('$e');
    }
    setState(() {});
  }

  Widget divider() {
    return Container(
      margin: const EdgeInsets.only(top: 12, bottom: 15),
      height: 10,
      width: MediaQuery.of(context).size.width,
      color: Theme.of(context).colorScheme.secondaryContainer,
    );
  }
}
