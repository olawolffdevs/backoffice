import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class UsersCrudPage extends StatefulWidget {
  const UsersCrudPage({Key? key, this.user}) : super(key: key);
  final UserEntity? user;
  @override
  UsersCrudPageState createState() => UsersCrudPageState();
}

class UsersCrudPageState extends State<UsersCrudPage> {
  TextEditingController name = TextEditingController();
  MaskedTextController phone = MaskedTextController(
    mask: '(00) 00000 0000',
    cursorBehavior: CursorBehaviour.unlocked,
  );
  MaskedTextController documentNumber = MaskedTextController(
    mask: '000.000.000-00',
    cursorBehavior: CursorBehaviour.unlocked,
  );
  MaskedTextController birth = MaskedTextController(
    mask: '00-00-0000',
    cursorBehavior: CursorBehaviour.unlocked,
  );
  TextEditingController email = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.user != null) {
      name.text = widget.user!.name;
      phone.text = widget.user!.phone ?? '';
      documentNumber.text = widget.user!.documentNumber ?? '';
      email.text = widget.user!.email ?? '';
      birth.updateText(
        DateFormat('dd/MM/yyyy')
            .format(DateTime.parse(widget.user?.birth ?? '')),
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
    name.dispose();
    phone.dispose();
    documentNumber.dispose();
    email.dispose();
    birth.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes do usuário'),
      ),
      body: Center(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(24),
              child: Container(
                constraints: const BoxConstraints(
                  maxWidth: 420,
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.secondaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(16)),
                  border: Border.all(
                    width: 1,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const SizedBox(height: 45),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 120,
                          height: 120,
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.primaryContainer,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(40),
                            ),
                            border: Border.all(
                              width: 1,
                              color: Theme.of(context).colorScheme.primary,
                            ),
                          ),
                          child: const Center(
                            child: Icon(
                              EvaIcons.personOutline,
                              size: 60,
                            ),
                          ),
                        ),
                      ],
                    ),
                    OwText(
                      '${widget.user?.name}',
                      style: Theme.of(context).textTheme.headlineMedium,
                      padding: const EdgeInsets.fromLTRB(25, 25, 25, 10),
                      textAlign: TextAlign.center,
                    ),
                    OwText(
                      '${OwFormat.cpfCnpj(widget.user?.documentNumber)}',
                      style: Theme.of(context).textTheme.bodyMedium,
                      padding: const EdgeInsets.fromLTRB(25, 0, 25, 45),
                      textAlign: TextAlign.center,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FloatingActionButton(
                          heroTag: null,
                          child: const Icon(EvaIcons.phoneOutline),
                          onPressed: () async {
                            launchUrl(Uri.parse('tel:${widget.user?.phone}'));
                          },
                        ),
                        const SizedBox(width: 12),
                        FloatingActionButton(
                          heroTag: null,
                          child: const Icon(EvaIcons.emailOutline),
                          onPressed: () async {
                            launchUrl(Uri.parse('mailto:${widget.user?.email}'));
                          },
                        ),
                        // TODO
                        // const SizedBox(width: 12),
                        // FloatingActionButton(
                        //   heroTag: null,
                        //   child: const Icon(EvaIcons.messageCircleOutline),
                        //   onPressed: () async {
                        //     //
                        //   },
                        // ),
                      ],
                    ),
                    const SizedBox(height: 25),
                    OwTextField(
                      color: Colors.transparent,
                      margin: const EdgeInsets.fromLTRB(25, 20, 25, 0),
                      enabled: false,
                      controller: phone,
                      labelText: 'Telefone',
                      hintText: 'Escreva aqui...',
                      onChanged: (_) {
                        if (phone.text.length <= 13) {
                          phone.updateMask('(00) 0000 00000');
                        } else {
                          phone.updateMask('(00) 00000 0000');
                        }
                        setState(() {});
                      },
                    ),
                    OwTextField(
                      color: Colors.transparent,
                      margin: const EdgeInsets.fromLTRB(25, 20, 25, 0),
                      enabled: false,
                      controller: email,
                      labelText: 'Email',
                      hintText: 'Escreva aqui...',
                    ),
                    OwTextField(
                      color: Colors.transparent,
                      margin: const EdgeInsets.fromLTRB(25, 20, 25, 0),
                      controller: birth,
                      enabled: false,
                      labelText: 'Data de Nascimento',
                      hintText: 'Escreva aqui...',
                    ),
                    const SizedBox(height: 35),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
