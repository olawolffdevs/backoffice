import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import 'upload_content_file.dart';

class ContentDetailPage extends StatefulWidget {
  const ContentDetailPage({
    Key? key,
    this.content,
    required this.sectionId,
  }) : super(key: key);
  final CourseContentEntity? content;
  final String sectionId;

  @override
  State<ContentDetailPage> createState() => _ContentDetailPageState();
}

class _ContentDetailPageState extends State<ContentDetailPage> {

  late TextEditingController nameController;
  late TextEditingController descriptionController;
  TextEditingController resumo = TextEditingController();
  TextEditingController index = TextEditingController();
  String type = 'article';

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(text: widget.content?.title ?? '');
    descriptionController = TextEditingController(text: widget.content?.description ?? '');
    resumo = TextEditingController(text: widget.content?.body ?? '');
    index.text = (widget.content?.index ?? 0).toString();
    type = widget.content?.type ?? 'article';
  }

  @override
  void dispose() {
    nameController.dispose();
    descriptionController.dispose();
    index.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes do Conteúdo'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Visibility(
            visible: widget.content != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar este conteúdo?',
                  description: 'Esta ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                    OwButton(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.courses.deleteCourseContent(id: widget.content!.id!);
                    if (mounted) {
                      OwBotToast.close();
                      Navigator.pop(context, true);
                    }
                  } catch (e) {
                    OwBotToast.close();
                    OwBotToast.toast('$e');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              labelText: 'Título do conteúdo',
              controller: nameController,
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
            ),
            const SizedBox(height: 16),
            OwTextField(
              labelText: 'Descrição',
              controller: descriptionController,
              maxLines: 5,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 0),
            ),
            sectionTitle(
              title: 'Posição na tela',
              description: 'Defina um index - número inteiro - para definir a posição em que o módulo irá aparecer na tela.',
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.remove),
                    onPressed: () {
                      setState(() {
                        final value = int.tryParse(index.text) ?? 2;
                        if (value > 1) {
                          index.text = '${value - 1}';
                        }
                      });
                    },
                  ),
                  Expanded(
                    child: OwTextField(
                      margin: const EdgeInsets.symmetric(horizontal: 16),
                      hintText: 'Digite aqui...',
                      controller: index,
                      onChanged: (valor) {
                        setState(() {});
                      },
                    ),
                  ),
                  IconButton(
                    icon: const Icon(Icons.add),
                    onPressed: () {
                      setState(() {
                        final value = int.tryParse(index.text) ?? 1;
                        index.text = '${value + 1}';
                      });
                    },
                  ),
                ],
              ),
            ),
            if(widget.content == null) ...[
              Padding(
                padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
                child: Row(
                  children: [
                    FilterChip(
                      label: const Text('Artigo'),
                      selected: type == 'article',
                      onSelected: (_) {
                        setState(() {
                          type = 'article';
                        });
                      },
                    ),
                    const SizedBox(width: 8),
                    FilterChip(
                      label: const Text('Vídeo'),
                      selected: type != 'article',
                      onSelected: (_) {
                        setState(() {
                          type = 'video';
                        });
                      },
                    ),
                  ],
                ),
              ),
            ],
            if(type == 'article') ...[
              OwTextField(
                controller: resumo,
                margin: const EdgeInsets.fromLTRB(24, 24, 24, 24),
                labelText: 'Resposta completa >',
                hintText: 'Escreva aqui...',
                readOnly: true,
                onTap: () async {
                  final result = await context.push(
                    '/${WhiteLabelEntity.current?.id}/dashboard/html-editor',
                    extra: resumo.text,
                  );
                  if (result is String) {
                    resumo.text = result;
                    setState(() {});
                  }
                },
              ),
            ],
            if(type == 'video' && widget.content != null && widget.content!.videoStatus == null || widget.content!.videoStatus == 'failed') ...[
              OwButton.secondary(
                labelText: 'Adicionar vídeo',
                margin: const EdgeInsets.fromLTRB(24, 24, 24, 24),
                onPressed: () async {
                  final result = await openModalPage(
                    context,
                    const UploadFilePage(
                      mediaTypeCategory: MediaTypeCategory.video,
                    ),
                  );
                  if(result == true) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  }
                },
              ),
            ],
            if(type == 'video' && widget.content!.videoStatus != null && widget.content!.videoStatus != 'failed') ...[
              Padding(
                padding: const EdgeInsets.fromLTRB(24, 24, 24, 24),
                child: HighlightMessageCard(
                  color: ColorSystem(context: context),
                  text: widget.content!.videoStatus,
                ),
              ),
            ],
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          labelText: 'Salvar categoria',
          margin: const EdgeInsets.all(24),
          onPressed: () async {
            try {
              OwBotToast.loading();
              if(widget.content == null) {
                final result = await Api.courses.insertCourseContent(
                  title: nameController.text,
                  description: descriptionController.text,
                  index: int.parse(index.text),
                  sectionId: '${widget.sectionId}',
                  type: type,
                );
                OwBotToast.close();
                OwBotToast.toast('Conteúdo cadastrado com sucesso');
                Navigator.pop(context, result);
              } else {
                final result = await Api.courses.updateCourseContent(
                  id: '${widget.content!.id}',
                  body: widget.content!.copyWith(
                    title: nameController.text,
                    description: descriptionController.text,
                    index: int.parse(index.text),
                    type: type,
                  ),
                );
                OwBotToast.close();
                OwBotToast.toast('Conteúdo atualizado com sucesso');
                Navigator.pop(context, result);
              }
            } catch (e) {
              OwBotToast.close();
              if (mounted) {
                showOwDialog(
                  context: context,
                  title: 'Ocorreu um erro',
                  description: '$e',
                  buttons: [
                    OwButton(
                      labelText: 'Fechar',
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                );
              }
            }
          },
        ),
      ),
    );
  }

  Widget sectionTitle({required String title, String? description}) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 25, 25, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          OwText(
            title,
            style: Theme.of(context).textTheme.titleLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          Visibility(
            visible: Valid.text(description),
            child: OwText(
              '$description',
              padding: const EdgeInsets.only(top: 6),
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          ),
        ],
      ),
    );
  }
}