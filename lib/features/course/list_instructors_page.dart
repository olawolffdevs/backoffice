import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ListInstructorsPage extends StatefulWidget {
  const ListInstructorsPage({super.key});

  @override
  State<ListInstructorsPage> createState() => _ListInstructorsPageState();
}

class _ListInstructorsPageState extends State<ListInstructorsPage> {

  List<CourseInstructorEntity>? users;
  List<CourseInstructorEntity> selecteds = [];

  final search = TextEditingController();

  @override
  void dispose() {
    search.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    final temp = users?.where((i) => i.name.toLowerCase().contains(OwFormat.removeAccentAndPonctuation(search.text.toString(),removedPonctuation: ',.!?;:()[]{}}-/').toLowerCase())).toList();
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Selecionar instrutores'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Visibility(
        visible: users == null,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
        replacement: Column(
          children: [
            OwTextField(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              hintText: 'Buscar',
              controller: search,
              suffixIcon: IconButton(
                onPressed: () {
                  if (Valid.text(search.text)) {
                    search.clear();
                    setState(() {});
                  }
                },
                icon: const Icon(EvaIcons.closeCircleOutline),
              ),
              onFieldSubmitted: (str) {
                setState(() {});
              },
            ),
            Expanded(
              child: ListView.separated(
                itemCount: temp?.length ?? 0,
                separatorBuilder: (context, index) {
                  return const OwDivider();
                },
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(
                      '${temp![index].name}',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    subtitle: Text(
                      '${temp[index].email}',
                    ),
                    trailing: Checkbox(
                      value: selecteds.contains(temp[index]),
                      onChanged: (bool? value) {
                        if (value == true) {
                          selecteds.add(temp[index]);
                        } else {
                          selecteds.remove(temp[index]);
                        }
                        setState(() {});
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          labelText: 'Adicionar instrutores',
          onPressed: () async {
            Navigator.pop(context, selecteds);
          },
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      users = await Api.courses.listCourseInstructorDashboard();
    } catch (e) {
      //
    }
    setState(() {});
  }
}