import 'package:backoffice/features/course/category_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class CategoriesCoursesHomePage extends StatefulWidget {
  const CategoriesCoursesHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<CategoriesCoursesHomePage> createState() => _CategoriesCoursesHomePageState();
}

class _CategoriesCoursesHomePageState extends State<CategoriesCoursesHomePage> with AutomaticKeepAliveClientMixin<CategoriesCoursesHomePage> {
  @override
  bool get wantKeepAlive => true;

  List<CourseCategoryEntity>? categories;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const OwText('Categorias'),
      ),
      body: categories == null
        ? const Center(child: CircularProgressIndicator())
        : categories!.isEmpty
            ? const Center(child: Text('Nenhuma categoria disponível'))
            : OwGrid.builder(
                numbersInRowAccordingToWidgth: [430, 730, 900, 1250, 1600],
                padding: const EdgeInsets.all(16),
                itemCount: categories!.length,
                itemBuilder: (context, index) {
                  final course = categories![index];
                  return Ink(
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondaryContainer,
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      onTap: () async {
                        final result = await openLinkPage(
                          context,
                          OpenLinkPageParams.basic(
                            child: CategoryCourseDetailPage(
                              category: course,
                            ),
                          ),
                        );
                        if (result is CourseCategoryEntity) {
                          categories![index] = result;
                          setState(() {});
                        } else if (result == true) {
                          categories!.removeAt(index);
                          setState(() {});
                        }
                      },
                      child: Container(
                        margin: const EdgeInsets.all(24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              course.name,
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                            const SizedBox(height: 5),
                            Text(
                              course.description ?? 'Sem descrição',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        icon: const Icon(
          Icons.add,
        ),
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.onPrimary,
        label: Text(
          'Nova categoria',
          style: TextStyle(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        onPressed: () async {
          final result = await openLinkPage(
            context,
            OpenLinkPageParams.basic(
              child: const CategoryCourseDetailPage(),
            ),
          );
          if (result is CourseCategoryEntity) {
            categories ??= [];
            categories!.add(result);
            setState(() {});
          }
        },
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      categories = await Api.courses.listCourseCategoryDashboard(
        query: {'skip': categories?.length ?? 0},
      );
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: '$e',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }
}