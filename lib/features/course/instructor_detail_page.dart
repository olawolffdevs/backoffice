import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class InstructorDetailPage extends StatefulWidget {
  const InstructorDetailPage({super.key, this.instructor});
  final CourseInstructorEntity? instructor;

  @override
  State<InstructorDetailPage> createState() => _InstructorDetailPageState();
}

class _InstructorDetailPageState extends State<InstructorDetailPage> {

  late TextEditingController nameController;
  late TextEditingController avatarController;
  late TextEditingController biographyController;
  late TextEditingController emailController;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(text: widget.instructor?.name ?? '');
    avatarController = TextEditingController(text: widget.instructor?.avatar ?? '');
    biographyController = TextEditingController(text: widget.instructor?.biography ?? '');
    emailController = TextEditingController(text: widget.instructor?.email ?? '');
  }

  @override
  void dispose() {
    nameController.dispose();
    avatarController.dispose();
    biographyController.dispose();
    emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes do Instrutor'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Visibility(
            visible: widget.instructor != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar este instrutor?',
                  description: 'Esta ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                    OwButton(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.courses.deleteCourseInstructor(id: widget.instructor!.id!);
                    if (mounted) {
                      OwBotToast.close();
                      Navigator.pop(context, true);
                    }
                  } catch (e) {
                    OwBotToast.close();
                    OwBotToast.toast('$e');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if(widget.instructor != null) ...[
              const SizedBox(height: 24),
              Center(
                child: Container(
                  width: 65,
                  height: 65,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(65)),
                    child: Image.network(
                      '${widget.instructor?.avatar}',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              OwText(
                '${widget.instructor?.name}',
                textAlign: TextAlign.center,
                padding: const EdgeInsets.fromLTRB(24, 16, 24, 24),
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ],
            OwTextField(
              labelText: 'Nome do instrutor',
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
              controller: nameController,
            ),
            OwTextField(
              labelText: 'Avatar',
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
              controller: avatarController,
            ),
            OwTextField(
              labelText: 'Biografia',
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
              controller: biographyController,
              maxLines: 5,
            ),
            OwTextField(
              labelText: 'Email',
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
              controller: emailController,
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          labelText: 'Salvar instrutor',
          margin: const EdgeInsets.all(24),
          onPressed: () async {
            try {
              OwBotToast.loading();
              if(widget.instructor == null) {
                final result = await Api.courses.insertCourseInstructor(
                  name: nameController.text,
                  avatar: avatarController.text,
                  biography: biographyController.text,
                  email: emailController.text,
                );
                OwBotToast.close();
                OwBotToast.toast('Instrutor cadastrado com sucesso');
                Navigator.pop(context, result);
              } else {
                final result = await Api.courses.updateCourseInstructor(
                  id: '${widget.instructor!.id}',
                  body: widget.instructor!.copyWith(
                    name: nameController.text,
                    avatar: avatarController.text,
                    biography: biographyController.text,
                    email: emailController.text,
                  ),
                );
                OwBotToast.close();
                OwBotToast.toast('Instrutor atualizado com sucesso');
                Navigator.pop(context, result);
              }
            } catch (e) {
              OwBotToast.close();
              if (mounted) {
                showOwDialog(
                  context: context,
                  title: 'Ocorreu um erro',
                  description: '$e',
                  buttons: [
                    OwButton(
                      labelText: 'Fechar',
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                );
              }
            }
          },
        ),
      ),
    );
  }
}