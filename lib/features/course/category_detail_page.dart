import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class CategoryCourseDetailPage extends StatefulWidget {
  const CategoryCourseDetailPage({super.key, this.category});
  final CourseCategoryEntity? category;

  @override
  State<CategoryCourseDetailPage> createState() => _CategoryCourseDetailPageState();
}

class _CategoryCourseDetailPageState extends State<CategoryCourseDetailPage> {
  late TextEditingController nameController;
  late TextEditingController descriptionController;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(text: widget.category?.name ?? '');
    descriptionController = TextEditingController(text: widget.category?.description ?? '');
  }

  @override
  void dispose() {
    nameController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes da Categoria'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Visibility(
            visible: widget.category != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar esta categoria?',
                  description: 'Esta ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                    OwButton(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.courses.deleteCourseCategory(id: widget.category!.id!);
                    if (mounted) {
                      OwBotToast.close();
                      Navigator.pop(context, true);
                    }
                  } catch (e) {
                    OwBotToast.close();
                    OwBotToast.toast('$e');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              labelText: 'Nome da categoria',
              controller: nameController,
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
            ),
            const SizedBox(height: 16),
            OwTextField(
              labelText: 'Descrição',
              controller: descriptionController,
              maxLines: 5,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 0),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          labelText: 'Salvar categoria',
          margin: const EdgeInsets.all(24),
          onPressed: () async {
            try {
              OwBotToast.loading();
              if(widget.category == null) {
                final result = await Api.courses.insertCourseCategory(
                  name: nameController.text,
                  description: descriptionController.text,
                );
                OwBotToast.close();
                OwBotToast.toast('Categoria cadastrada com sucesso');
                Navigator.pop(context, result);
              } else {
                final result = await Api.courses.updateCourseCategory(
                  id: '${widget.category!.id}',
                  body: widget.category!.copyWith(
                    name: nameController.text,
                    description: descriptionController.text,
                  ),
                );
                OwBotToast.close();
                OwBotToast.toast('Categoria atualizada com sucesso');
                Navigator.pop(context, result);
              }
            } catch (e) {
              OwBotToast.close();
              if (mounted) {
                showOwDialog(
                  context: context,
                  title: 'Ocorreu um erro',
                  description: '$e',
                  buttons: [
                    OwButton(
                      labelText: 'Fechar',
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                );
              }
            }
          },
        ),
      ),
    );
  }
}
