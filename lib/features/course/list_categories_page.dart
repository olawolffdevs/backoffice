import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ListCategoriesPage extends StatefulWidget {
  const ListCategoriesPage({super.key});

  @override
  State<ListCategoriesPage> createState() => _ListCategoriesPageState();
}

class _ListCategoriesPageState extends State<ListCategoriesPage> {

  List<CourseCategoryEntity>? categories;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Selecione uma categoria'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body:  categories == null
        ? const Center(child: CircularProgressIndicator())
        : categories!.isEmpty
            ? const Center(child: Text('Nenhuma categoria disponível'))
            : OwGrid.builder(
                padding: const EdgeInsets.all(16),
                itemCount: categories!.length,
                itemBuilder: (context, index) {
                  final course = categories![index];
                  return Ink(
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondaryContainer,
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      onTap: () async {
                        Navigator.pop(context, course);
                      },
                      child: Container(
                        margin: const EdgeInsets.all(24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              course.name,
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                            const SizedBox(height: 5),
                            Text(
                              course.description ?? 'Sem descrição',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      categories = await Api.courses.listCourseCategoryDashboard(
        query: {'skip': categories?.length ?? 0},
      );
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: '$e',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }

}