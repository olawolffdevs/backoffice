import 'package:backoffice/features/course/instructor_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class InstructorCoursesHomePage extends StatefulWidget {
  const InstructorCoursesHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;

  @override
  State<InstructorCoursesHomePage> createState() => _InstructorCoursesHomePageState();
}

class _InstructorCoursesHomePageState extends State<InstructorCoursesHomePage> with AutomaticKeepAliveClientMixin<InstructorCoursesHomePage> {
  @override
  bool get wantKeepAlive => true;

  List<CourseInstructorEntity>? instructors;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const OwText('Instrutores'),
      ),
      body: instructors == null
        ? const Center(child: CircularProgressIndicator())
        : instructors!.isEmpty
            ? const Center(child: Text('Nenhum instrutor disponível'))
            : OwGrid.builder(
                numbersInRowAccordingToWidgth: [430, 730, 900, 1250, 1600],
                padding: const EdgeInsets.all(16),
                itemCount: instructors!.length,
                itemBuilder: (context, index) {
                  final course = instructors![index];
                  return Ink(
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondaryContainer,
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      onTap: () async {
                        final result = await openLinkPage(
                          context,
                          OpenLinkPageParams.basic(
                            child: InstructorDetailPage(
                              instructor: course,
                            ),
                          ),
                        );
                        if (result is CourseInstructorEntity) {
                          instructors![index] = result;
                          setState(() {});
                        } else if (result == true) {
                          instructors!.removeAt(index);
                          setState(() {});
                        }
                      },
                      child: Container(
                        margin: const EdgeInsets.all(24),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Container(
                                width: 65,
                                height: 65,
                                child: ClipRRect(
                                  borderRadius: const BorderRadius.all(Radius.circular(65)),
                                  child: Image.network(
                                    course.avatar,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 12),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    course.name,
                                    style: Theme.of(context).textTheme.titleMedium,
                                  ),
                                  const SizedBox(height: 5),
                                  Text(
                                    course.biography,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: Theme.of(context).textTheme.bodyMedium,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        icon: const Icon(
          Icons.add,
        ),
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.onPrimary,
        label: Text(
          'Novo instrutor',
          style: TextStyle(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        onPressed: () async {
          final result = await openLinkPage(
            context,
            OpenLinkPageParams.basic(
              child: const InstructorDetailPage(),
            ),
          );
          if (result is CourseInstructorEntity) {
            instructors ??= [];
            instructors!.add(result);
            setState(() {});
          }
        },
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      instructors = await Api.courses.listCourseInstructorDashboard(
        query: {'skip': instructors?.length ?? 0},
      );
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: '$e',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }
}
