import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import 'list_categories_page.dart';
import 'list_instructors_page.dart';

class CourseDetailPage extends StatefulWidget {
  const CourseDetailPage({
    Key? key,
    this.course,
  }) : super(key: key);
  final CourseEntity? course;

  @override
  State<CourseDetailPage> createState() => _CourseDetailPageState();
}

class _CourseDetailPageState extends State<CourseDetailPage> {

  late TextEditingController titleController;
  late TextEditingController coverController;
  late TextEditingController descriptionController;
  CourseCategoryEntity? categoryId;
  bool isFree = false;
  List<CourseInstructorEntity> instructors = [];

  @override
  void initState() {
    super.initState();
    titleController = TextEditingController(text: widget.course?.title ?? '');
    coverController = TextEditingController(text: widget.course?.cover ?? '');
    descriptionController = TextEditingController(text: widget.course?.description ?? '');
    categoryId = widget.course?.category;
    instructors = widget.course?.instructors ?? [];
  }

  @override
  void dispose() {
    titleController.dispose();
    coverController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes do Curso'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Visibility(
            visible: widget.course != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar este instrutor?',
                  description: 'Esta ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                    OwButton(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.courses.deleteCourse(id: widget.course!.id!);
                    if (mounted) {
                      OwBotToast.close();
                      Navigator.pop(context, true);
                    }
                  } catch (e) {
                    OwBotToast.close();
                    OwBotToast.toast('$e');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if(widget.course != null) ...[
              const SizedBox(height: 24),
              Center(
                child: Container(
                  width: 65,
                  height: 65,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(65)),
                    child: Image.network(
                      '${widget.course?.cover}',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              OwText(
                '${widget.course?.title}',
                textAlign: TextAlign.center,
                padding: const EdgeInsets.fromLTRB(24, 16, 24, 24),
                style: Theme.of(context).textTheme.titleMedium,
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     FloatingActionButton(
              //       heroTag: null,
              //       child: const Icon(EvaIcons.personOutline),
              //       onPressed: () async {
              //         // launchUrl(Uri.parse('tel:${widget.company.phone}'));
              //       },
              //     ),
              //     const SizedBox(width: 12),
              //     FloatingActionButton(
              //       heroTag: null,
              //       child: const Icon(EvaIcons.fileTextOutline),
              //       onPressed: () async {
              //         // launchUrl(Uri.parse('mailto:${widget.company.email}'));
              //       },
              //     ),
              //   ],
              // ),
              // const SizedBox(height: 24,),
            ],
            OwTextField(
              labelText: 'Título do curso',
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
              controller: titleController,
            ),
            OwTextField(
              labelText: 'Capa do curso',
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
              controller: coverController,
            ),
            OwTextField(
              labelText: 'Descrição do curso',
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
              controller: descriptionController,
              maxLines: 5,
            ),
            const SizedBox(height: 24),
            const OwDivider(),
            ListTile(
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 24,
                vertical: 2,
              ),
              title: Text(
                'Categoria do Curso',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: const Icon(Icons.keyboard_arrow_right_outlined),
              subtitle: Text(categoryId == null ? 'Selecione uma categoria' : '${categoryId?.name}'),
              onTap: () async {
                final result = await openLinkPage(
                  context,
                  OpenLinkPageParams.basic(
                    child: const ListCategoriesPage(),
                  ),
                );
                if (result is CourseCategoryEntity) {
                  categoryId = result;
                  setState(() {});
                }
              },
            ),
            const OwDivider(),
            ListTile(
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 24,
                vertical: 2,
              ),
              title: Text(
                'Instrutores do Curso',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: const Icon(Icons.keyboard_arrow_right_outlined),
              subtitle: Text(instructors.isEmpty ? 'Selecione um ou mais instrutor' : 'Instrutores selecionados (${instructors.length})'),
              onTap: () async {
                final result = await openLinkPage(
                  context,
                  OpenLinkPageParams.basic(
                    child: const ListInstructorsPage(),
                  ),
                );
                if (result is List<CourseInstructorEntity>) {
                  instructors = result;
                  setState(() {});
                }
              },
            ),
            const OwDivider(),
            const SizedBox(height: 24),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          labelText: 'Salvar instrutor',
          margin: const EdgeInsets.all(24),
          onPressed: () async {
            try {
              OwBotToast.loading();
              if(widget.course == null) {
                final result = await Api.courses.insertCourse(
                  cover: coverController.text,
                  description: descriptionController.text,
                  title: titleController.text,
                  categoryId: '${categoryId?.id}',
                  isFree: isFree,
                  instructors: instructors.map((e) => e.id!).toList(),
                );
                OwBotToast.close();
                OwBotToast.toast('Curso cadastrado com sucesso');
                Navigator.pop(context, result);
              } else {
                final result = await Api.courses.updateCourse(
                  id: '${widget.course!.id}',
                  body: widget.course!.copyWith(
                    cover: coverController.text,
                    description: descriptionController.text,
                    title: titleController.text,
                    category: categoryId,
                    isFree: isFree,
                    instructors: instructors,
                  ),
                );
                OwBotToast.close();
                OwBotToast.toast('Curso atualizado com sucesso');
                Navigator.pop(context, result);
              }
            } catch (e) {
              OwBotToast.close();
              if (mounted) {
                showOwDialog(
                  context: context,
                  title: 'Ocorreu um erro',
                  description: '$e',
                  buttons: [
                    OwButton(
                      labelText: 'Fechar',
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                );
              }
            }
          },
        ),
      ),
    );
  }
}