import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import 'content_detail_page.dart';

class ContentsCoursesHomePage extends StatefulWidget {
  const ContentsCoursesHomePage({
    Key? key,
    required this.section,
  }) : super(key: key);
  final CourseSectionEntity section;

  @override
  State<ContentsCoursesHomePage> createState() => _ContentsCoursesHomePageState();
}

class _ContentsCoursesHomePageState extends State<ContentsCoursesHomePage> {

  List<CourseContentEntity>? courses;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const OwText('Conteúdos da seção'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body:  courses == null
        ? const Center(child: CircularProgressIndicator())
        : courses!.isEmpty
        ? const Center(child: Text('Nenhum conteúdo disponível'))
        : OwGrid.builder(
            numbersInRowAccordingToWidgth: [430, 730, 900, 1250, 1600],
            padding: const EdgeInsets.all(16),
            itemCount: courses!.length,
            itemBuilder: (context, index) {
              final course = courses![index];
              return Ink(
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.secondaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(16)),
                ),
                child: InkWell(
                  borderRadius: const BorderRadius.all(Radius.circular(16)),
                  onTap: () async {
                    final result = await openLinkPage(
                      context,
                      OpenLinkPageParams.basic(
                        child: ContentDetailPage(
                          content: course,
                          sectionId: '${widget.section.id}',
                        ),
                      ),
                    );
                    if (result is CourseContentEntity) {
                      courses![index] = result;
                      setState(() {});
                    } else if (result == true) {
                      courses!.removeAt(index);
                      setState(() {});
                    }
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              course.title,
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                            const SizedBox(height: 4),
                            Text(
                              course.description,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                            const SizedBox(height: 4),
                            HighlightMessageCard(
                              color: ColorSystem(context: context),
                              text: course.type == 'video' ? 'Vídeo' : 'Artigo',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        icon: const Icon(
          Icons.add,
        ),
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.onPrimary,
        label: Text(
          'Novo conteúdo',
          style: TextStyle(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        onPressed: () async {
          final result = await openLinkPage(
            context,
            OpenLinkPageParams.basic(
              child: ContentDetailPage(
                sectionId: '${widget.section.id}',
              ),
            ),
          );
          if (result is CourseContentEntity) {
            courses ??= [];
            courses!.add(result);
            setState(() {});
          }
        },
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      courses = await Api.courses.listCourseContent(
        id: '${widget.section.id}',
        query: {'skip': courses?.length ?? 0},
      );
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: '$e',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }
}