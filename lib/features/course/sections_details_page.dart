import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class SectionsCoursesDetailsPage extends StatefulWidget {
  const SectionsCoursesDetailsPage({
    Key? key,
    required this.courseId,
    this.section,
  }) : super(key: key);
  final CourseSectionEntity? section;
  final String courseId;

  @override
  State<SectionsCoursesDetailsPage> createState() => _SectionsCoursesDetailsPageState();
}

class _SectionsCoursesDetailsPageState extends State<SectionsCoursesDetailsPage> {

  late TextEditingController nameController;
  late TextEditingController descriptionController;
  bool isVisible = false;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(text: widget.section?.title ?? '');
    descriptionController = TextEditingController(text: widget.section?.description ?? '');
    isVisible = widget.section?.isVisible ?? false;
  }

  @override
  void dispose() {
    nameController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes da Seção'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Visibility(
            visible: widget.section != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar esta seção?',
                  description: 'Esta ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                    OwButton(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.courses.deleteCourseSection(id: widget.section!.id!);
                    if (mounted) {
                      OwBotToast.close();
                      Navigator.pop(context, true);
                    }
                  } catch (e) {
                    OwBotToast.close();
                    OwBotToast.toast('$e');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              labelText: 'Nome da seção',
              controller: nameController,
              margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
            ),
            const SizedBox(height: 16),
            OwTextField(
              labelText: 'Descrição',
              controller: descriptionController,
              maxLines: 5,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 0),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          labelText: 'Salvar categoria',
          margin: const EdgeInsets.all(24),
          onPressed: () async {
            try {
              OwBotToast.loading();
              if(widget.section == null) {
                final result = await Api.courses.insertCourseSection(
                  title: nameController.text,
                  description: descriptionController.text,
                  courseId: widget.courseId,
                  index: 0,
                );
                OwBotToast.close();
                OwBotToast.toast('Seção cadastrada com sucesso');
                Navigator.pop(context, result);
              } else {
                final result = await Api.courses.updateCourseSection(
                  id: '${widget.section!.id}',
                  body: widget.section!.copyWith(
                    title: nameController.text,
                    description: descriptionController.text,
                    isVisible: isVisible,
                  ),
                );
                OwBotToast.close();
                OwBotToast.toast('Seção atualizada com sucesso');
                Navigator.pop(context, result);
              }
            } catch (e) {
              OwBotToast.close();
              if (mounted) {
                showOwDialog(
                  context: context,
                  title: 'Ocorreu um erro',
                  description: '$e',
                  buttons: [
                    OwButton(
                      labelText: 'Fechar',
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                );
              }
            }
          },
        ),
      ),
    );
  }
}