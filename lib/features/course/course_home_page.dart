import 'package:backoffice/features/course/sections_home_page.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import 'course_detail_page.dart';

class CoursesHomePage extends StatefulWidget {
  const CoursesHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<CoursesHomePage> createState() => _CoursesHomePageState();
}

class _CoursesHomePageState extends State<CoursesHomePage> with AutomaticKeepAliveClientMixin<CoursesHomePage> {
  @override
  bool get wantKeepAlive => true;

  List<CourseEntity>? courses;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const OwText('Cursos'),
      ),
      body: courses == null
        ? const Center(child: CircularProgressIndicator())
        : courses!.isEmpty
            ? const Center(child: Text('Nenhum curso disponível'))
            : OwGrid.builder(
                numbersInRowAccordingToWidgth: [430, 730, 900, 1250, 1600],
                padding: const EdgeInsets.all(16),
                itemCount: courses!.length,
                itemBuilder: (context, index) {
                  final course = courses![index];
                  return Ink(
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondaryContainer,
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                      onTap: () async {
                        final result = await openLinkPage(
                          context,
                          OpenLinkPageParams.basic(
                            child: CourseDetailPage(
                              course: course,
                            ),
                          ),
                        );
                        if (result is CourseEntity) {
                          courses![index] = result;
                        } else if (result == true) {
                          courses!.removeAt(index);
                          setState(() {});
                        }
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          AspectRatio(
                            aspectRatio: 16/9,
                            child: Container(
                              height: 85,
                              child: ClipRRect(
                                borderRadius: const BorderRadius.all(Radius.circular(16)),
                                child: Image.network(
                                  course.cover,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 12),
                          Padding(
                            padding: const EdgeInsets.all(24),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Text(
                                  course.title,
                                  style: Theme.of(context).textTheme.titleMedium,
                                ),
                                const SizedBox(height: 8),
                                Text(
                                  '${course.instructors?.length} instrutores',
                                  style: Theme.of(context).textTheme.titleMedium,
                                ),
                                const SizedBox(height: 8),
                                Material(
                                  color: Colors.transparent,
                                  type: MaterialType.transparency,
                                  child: CardAtalhos(
                                    title: 'Ver seções',
                                    selecionado: true,
                                    onPressed: () {
                                      openLinkPage(
                                        context,
                                        OpenLinkPageParams.basic(
                                          child: SectionsCoursesHomePage(
                                            courseId: '${course.id}',
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        icon: const Icon(
          Icons.add,
        ),
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.onPrimary,
        label: Text(
          'Novo curso',
          style: TextStyle(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        onPressed: () async {
          final result = await openLinkPage(
            context,
            OpenLinkPageParams.basic(
              child: const CourseDetailPage(),
            ),
          );
          if (result is CourseEntity) {
            courses ??= [];
            courses!.add(result);
            setState(() {});
          }
        },
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      courses = await Api.courses.listCoursesDashboard(
        query: {'skip': courses?.length ?? 0},
      );
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: '$e',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }
}
