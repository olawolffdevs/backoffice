import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class SendNotificationPage extends StatefulWidget {
  const SendNotificationPage({
    Key? key,
    required this.notification,
  }) : super(key: key);
  final NotificationEntity notification;

  @override
  State<SendNotificationPage> createState() => _SendNotificationPageState();
}

class _SendNotificationPageState extends State<SendNotificationPage> {
  String type = 'allUsers';
  List<String> ids = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Enviar notificação'),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(25, 10, 25, 25),
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primaryContainer,
                borderRadius: const BorderRadius.all(Radius.circular(24)),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    children: [
                      Icon(
                        EvaIcons.bellOutline,
                        color: Theme.of(context).colorScheme.onPrimaryContainer,
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          '${WhiteLabelEntity.current!.name}',
                          style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).colorScheme.onPrimaryContainer,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 12),
                  Text(
                    '${widget.notification.title}',
                    style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).colorScheme.onPrimaryContainer,
                    ),
                  ),
                  const SizedBox(height: 4),
                  Text(
                    '${widget.notification.subtitle}',
                    style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                      color: Theme.of(context).colorScheme.onPrimaryContainer,
                    ),
                  ),
                  Visibility(
                    visible: Valid.text(widget.notification.link),
                    child: Text(
                      'Link para ${widget.notification.link}',
                      style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                        color: Theme.of(context).colorScheme.onPrimaryContainer,
                      ),
                    ),
                  )
                ],
              ),
            ),
            const OwDivider(),
            OwDropdown(
              margin: const EdgeInsets.all(25),
              optionsList: [
                '${enumsNotifications('individual')}',
                // '${enumsNotifications('group')}',
                '${enumsNotifications('allUsers')}',
              ],
              value: enumsNotifications(type),
              onChanged: (p0) {
                type = (p0!);
                if(type != 'Para usuários específicos') {
                  ids = [];
                }
                setState(() {});
              },
            ),
            const OwDivider(),
            Visibility(
              visible: enumsNotificationsRevers(type) == 'group',
              child: InkWell(
                onTap: () async {},
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: 25,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              'Grupos',
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                            const SizedBox(height: 6),
                            const Text(
                              'Envie notificações para grupos específicos.',
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 25),
                      const Text('Em breve'),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: enumsNotificationsRevers(type) == 'individual',
              child: InkWell(
                onTap: () async {
                  final result = await context.push(
                    '/${WhiteLabelEntity.current?.id}/dashboard/notifications/${widget.notification.id}/users',
                  );
                  if (result is List<String>) {
                    ids = result;
                  }
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: 25,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              'Usuários específicos',
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                            const SizedBox(height: 6),
                            Text(
                              'Envie notificações para determinados usuários do sistema.',
                              style: Theme.of(context).textTheme.bodySmall,
                            ),
                            const SizedBox(height: 8),
                            Text(
                              '${ids.length} selecionados',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 25),
                      const Icon(Icons.keyboard_arrow_right_outlined),
                    ],
                  ),
                ),
              ),
            ),
            const OwDivider(),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(24),
          height: 55,
          labelText: 'Enviar notificação',
          leading: EvaIcons.paperPlaneOutline,
          onPressed: () async {
            try {
              OwBotToast.loading();
              Api.notifications.send(
                id: '${widget.notification.id}',
                ids: ids,
                type: enumsNotificationsRevers(type),
              );
              OwBotToast.close();
              OwBotToast.toast('Notificação enviada');
              context.pop();
            } catch (e) {
              OwBotToast.close();
              if (mounted) {
                showOwDialog(
                  context: context,
                  title: 'Ocorreu um erro',
                  description: '$e',
                  buttons: [
                    OwButton(
                      labelText: 'Fechar',
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                );
              }
            }
          },
        ),
      ),
    );
  }

  String enumsNotifications(String value) {
    switch (value) {
      case 'individual':
        return 'Para usuários específicos';
      case 'group':
        return 'Para grupos';
      case 'allUsers':
        return 'Para todos';
      default:
        return value;
    }
  }

  String enumsNotificationsRevers(String value) {
    switch (value) {
      case 'Para usuários específicos':
        return 'individual';
      case 'Para grupos':
        return 'group';
      case 'Para todos':
        return 'allUsers';
      default:
        return value;
    }
  }
}
