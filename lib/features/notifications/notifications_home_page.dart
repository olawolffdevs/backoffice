import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class NotificationHomePage extends StatefulWidget {
  const NotificationHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  NotificationHomePageState createState() => NotificationHomePageState();
}

class NotificationHomePageState extends State<NotificationHomePage> {
  List<NotificationEntity>? notifications;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const Text('Notificações'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Visibility(
              visible: notifications != null,
              replacement: Container(
                constraints: const BoxConstraints(
                  minHeight: 250,
                ),
                child: const Center(
                  child: CircularProgressIndicator(),
                ),
              ),
              child: Visibility(
                visible: notifications?.isNotEmpty ?? false,
                replacement: Container(
                  margin: appSM.isWeb
                      ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                      : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  padding: const EdgeInsets.all(25),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Theme.of(context).colorScheme.outlineVariant,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                  ),
                  child: const Center(
                    child: Text('Você ainda não cadastrou nenhuma notificação. Adicione a primeira clicando no botão localizado no canto inferior da tela.'),
                  ),
                ),
                child: OwGrid.builder(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  shrinkWrap: true,
                  spacing: 15,
                  runSpacing: 15,
                  itemCount: notifications?.length ?? 0,
                  numbersInRowAccordingToWidgth: const [
                    680,
                    1050,
                    1400,
                    2900
                  ],
                  itemBuilder: (context, index) {
                    return Ink(
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(15)),
                        color: Theme.of(context).colorScheme.secondaryContainer,
                      ),
                      child: InkWell(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(15)),
                        onTap: () async {
                          final result = await context.push(
                            '/${WhiteLabelEntity.current?.id}/dashboard/notifications/${notifications![index].id}',
                            extra: notifications![index],
                          );
                          if (result is NotificationEntity) {
                            notifications![index] = result;
                            setState(() {});
                          }
                        },
                        child: Container(
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                notifications![index].title,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                ),
                              ),
                              const SizedBox(height: 6),
                              Text(
                                notifications![index].subtitle,
                              ),
                              const SizedBox(height: 12),
                              OwButton.outline(
                                labelText: 'Enviar notificação',
                                leading: EvaIcons.paperPlaneOutline,
                                onPressed: () async {
                                  await context.push(
                                    '/${WhiteLabelEntity.current?.id}/dashboard/notifications/${notifications![index].id}/send',
                                    extra: notifications![index],
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: notifications == null
          ? null
          : FloatingActionButton.extended(
              heroTag: null,
              icon: const Icon(
                Icons.add,
              ),
              label: const Text('Nova notificação'),
              onPressed: () async {
                final retorno = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/notifications/new',
                );
                if (retorno is NotificationEntity) {
                  notifications!.insert(0, retorno);
                  setState(() {});
                }
              },
            ),
    );
  }

  void callApi([skip = 0]) async {
    try {
      notifications = await Api.notifications.list(
        query: {'skip': notifications?.length ?? 0},
      );
    } catch (_) {
      //
    }
    setState(() {});
  }
}
