import 'package:backoffice/ui/components/alerta_simples.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:url_launcher/url_launcher.dart';

class NotificationCrudPage extends StatefulWidget {
  const NotificationCrudPage({this.notification, Key? key}) : super(key: key);
  final NotificationEntity? notification;
  @override
  State<NotificationCrudPage> createState() => _NotificationCrudPageState();
}

class _NotificationCrudPageState extends State<NotificationCrudPage> {
  TextEditingController title = TextEditingController();
  TextEditingController subtitle = TextEditingController();
  TextEditingController link = TextEditingController();
  bool send = false;

  List<String> reservados = [
    '#companyId',
    '#companyFantasyName',
    '#companyCorporateName',
    '#companyDocument',
    '#userId',
    '#userName',
    '#userFirstName',
    '#userDocument',
    '#storeId',
    '#storeName',
    '#moduleId',
  ];

  @override
  void initState() {
    super.initState();
    if (widget.notification != null) {
      title.text = widget.notification!.title;
      subtitle.text = widget.notification!.subtitle;
      link.text = widget.notification!.link ?? '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: Text('Notificação'.toUpperCase()),
        actions: [
          Visibility(
            visible: widget.notification != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertaSimples(
                      'Deseja apagar esta notificação?',
                      conteudo:
                          'Essa ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                      textoBotaoPrimario: 'apagar',
                      funcaoBotaoPrimario: () {
                        context.pop(true);
                      },
                      textoBotaoSecundario: 'manter',
                    );
                  },
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.notifications
                        .delete(id: widget.notification!.id!);
                    if (mounted) {
                      context.go('/${WhiteLabelEntity.current?.id}/dashboard');
                    }
                  } catch (e) {
                    OwBotToast.toast('Erro ao remover notificação.');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              controller: title,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Título',
              hintText: 'Escreva aqui...',
              onChanged: (value) {
                setState(() {});
              },
              maxLength: 48,
            ),
            OwTextField(
              controller: subtitle,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Subtítulo',
              hintText: 'Escreva aqui...',
              onChanged: (value) {
                setState(() {});
              },
              maxLines: 8,
              maxLength: 128,
            ),
            OwTextField(
              controller: link,
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Link (Opcional)',
              keyboardType: TextInputType.text,
              hintText: 'Escreva aqui...',
              onChanged: (value) {
                setState(() {});
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwButton.outline(
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              labelText: 'Enviar notificação',
              enable: checkSendNotification(),
              leading: EvaIcons.paperPlaneOutline,
              onPressed: () async {
                await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/notifications/${widget.notification?.id}/send',
                  extra: widget.notification,
                );
              },
            ),
            OwButton(
              margin: const EdgeInsets.fromLTRB(25, 6, 25, 25),
              labelText: 'Salvar notificação',
              enable: !checkSendNotification(),
              onPressed: _onPressed,
            ),
          ],
        ),
      ),
    );
  }

  void _onPressed() async {
    if (link.text.isNotEmpty) {
      if (!await canLaunchUrl(Uri.parse(link.text))) {
        showOwDialog(
          context: context,
          title: 'Informação errada',
          description: 'Verifique o link informado.',
          buttons: [
            OwButton(
              labelText: 'Fechar',
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    }
    if (title.text.trim().isEmpty || subtitle.text.trim().isEmpty) {
      showOwDialog(
        context: context,
        title: 'Faltando informações',
        description: 'Título e/ou subtítulo inválidos.',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    } else {
      if (widget.notification != null) {
        final NotificationEntity notification = widget.notification!.copyWith(
          title: title.text.trim(),
          subtitle: subtitle.text.trim(),
          link: link.text.trim(),
        );
        try {
          OwBotToast.loading();
          final response = await Api.notifications.update(
            id: notification.id!,
            data: notification,
          );
          if (mounted) {
            OwBotToast.close();
            context.pop(response);
            showOwDialog(
              context: context,
              title: 'Notificação atualizada com sucesso.',
            );
          }
        } catch (e) {
          OwBotToast.close();
          if (mounted) {
            showOwDialog(
              context: context,
              title: '$e',
            );
          }
        }
      } else {
        final NotificationEntity notification = NotificationEntity(
          title: title.text.trim(),
          subtitle: subtitle.text.trim(),
          link: link.text.trim(),
          send: send,
        );
        try {
          OwBotToast.loading();
          final response = await Api.notifications.add(data: notification);
          OwBotToast.close();
          if (mounted) {
            context.pop(response);
            showOwDialog(
              context: context,
              title: 'Notificação cadastrada com sucesso.',
            );
          }
        } catch (e) {
          OwBotToast.close();
          if (mounted) {
            showOwDialog(
              context: context,
              title: '$e',
            );
          }
        }
      }
    }
  }

  bool checkSendNotification() {
    if (widget.notification == null) return false;
    if (widget.notification!.title != title.text) return false;
    if (widget.notification!.subtitle != subtitle.text) return false;
    if (widget.notification!.link != link.text) return false;
    return true;
  }
}
