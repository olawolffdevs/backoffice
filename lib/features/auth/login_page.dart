import 'dart:convert';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();

  late FocusNode _focus;

  bool hideSecret = true;

  String? emailError;
  String? passwordError;

  @override
  void initState() {
    super.initState();
    _focus = FocusNode();
  }

  @override
  void dispose() {
    _focus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(36),
            width: MediaQuery.sizeOf(context).width,
            constraints: const BoxConstraints(
              maxWidth: 550,
            ),
            child: Padding(
              padding: EdgeInsets.only(
                top: kToolbarHeight + MediaQuery.paddingOf(context).top,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Image.asset(
                      'assets/images/logo-backoffice.png',
                      fit: BoxFit.fitHeight,
                      height: 45,
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const SizedBox(height: 8,),
                  Center(
                    child: HighlightMessageCard(
                      text: '${Environment.current!.environmentType == EnvironmentType.development ? 'Sandbox' : 'Produção'} | 3.3.0',
                      color: ColorSystem(context: context),
                    ),
                  ),
                  const SizedBox(height: 40,),
                  OwTextField(
                    controller: email,
                    errorText: emailError,
                    keyboardType: TextInputType.emailAddress,
                    textCapitalization: TextCapitalization.none,
                    hintText: 'Digite seu email aqui...',
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  OwTextField(
                    controller: password,
                    errorText: passwordError,
                    obscureText: hideSecret,
                    hintText: 'Digite sua senha aqui...',
                    suffixIcon: IconButton(
                      icon: Icon(
                        hideSecret
                          ? EvaIcons.eyeOutline
                          : EvaIcons.eyeOffOutline,
                      ),
                      onPressed: () {
                        setState(() {
                          hideSecret = !hideSecret;
                        });
                      },
                    ),
                  ),
                  OwButton(
                    onPressed: () {
                      _onPressed(context);
                    },
                    margin: const EdgeInsets.only(top: 40,),
                    labelText: 'Acessar plataforma',
                    height: 55,
                    expanded: true,
                  ),
                  const SizedBox(height: 35),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onPressed(BuildContext context) async {
    if (email.text.isEmpty) {
      emailError = 'Digite um e-mail válido.';
    } else {
      emailError = null;
    }
    if (password.text.length < 6) {
      passwordError = 'Digite uma senha válida.';
    } else {
      passwordError = null;
    }
    setState(() {});
    if (emailError == null && passwordError == null) {
      try {
        OwBotToast.loading();
        final String? passwordCrypt = await encriptarTexto(password.text.toString());
        final UserEntity user = await Api.auth.login(
          email: email.text,
          password: '$passwordCrypt',
        );
        userSM.setUser(user);
        await UserDB.inserir(jsonEncode(user.toMap()));
        if (mounted) {
          context.go('/home');
        }
      } catch (e) {
        OwBotToast.close();
        OwBotToast.toast(e.toString());
      }
    }
  }
}
