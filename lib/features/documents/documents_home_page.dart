import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:internet_file/internet_file.dart';
import 'package:intl/intl.dart';
import 'package:pdfx/pdfx.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import 'document_send_page.dart';

class DocumentsHomePage extends StatefulWidget {
  const DocumentsHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
    this.companyId,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  final String? companyId;
  @override
  State<DocumentsHomePage> createState() => _DocumentsHomePageState();
}

class _DocumentsHomePageState extends State<DocumentsHomePage> {
  List<DocumentEntity>? documents;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const Text('Todos os documentos'),
      ),
      body: Visibility(
        visible: documents == null,
        child: const Center(child: CircularProgressIndicator()),
        replacement: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(height: 25),
              Visibility(
                visible: documents?.isNotEmpty ?? false,
                replacement: Container(
                  margin: appSM.isWeb
                      ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                      : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  padding: const EdgeInsets.all(25),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Theme.of(context).colorScheme.outlineVariant,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                  ),
                  child: const Center(
                    child: Text('Você ainda não possui nenhum documento. Adicione o primeiro para visualizar aqui.'),
                  ),
                ),
                child: OwGrid.builder(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  shrinkWrap: true,
                  spacing: 15,
                  runSpacing: 15,
                  itemCount: documents?.length ?? 0,
                  numbersInRowAccordingToWidgth: const [680, 1050, 1400, 2900],
                  itemBuilder: (context, index) {
                    return Container(
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(Radius.circular(15)),
                        color: Theme.of(context).colorScheme.secondaryContainer,
                      ),
                      child: Material(
                        color: Colors.transparent,
                        type: MaterialType.transparency,
                        child: InkWell(
                          borderRadius: const BorderRadius.all(Radius.circular(15)),
                          onTap: () async {
                            openLinkPage(
                              context,
                              OpenLinkPageParams.basic(
                                child: PdfViewsWidget(
                                  document: documents![index],
                                ),
                              ),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(25),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Text(
                                  '${documents![index].title ?? [documents![index].title]}',
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ),
                                ),
                                const SizedBox(height: 4),
                                Text(
                                  '${DateFormat("dd/MM/yyyy • HH:mm").format(DateTime.parse(documents![index].createdAt!).toLocal())}',
                                ),
                                const SizedBox(height: 24),
                                IgnorePointer(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Theme.of(context).colorScheme.primaryContainer,
                                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                                    ),
                                    constraints: const BoxConstraints(
                                      maxHeight: 320,
                                    ),
                                    child: PdfViewsWidget(
                                      document: documents![index],
                                      hideAppBar: true,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        label: const Text('Enviar documento'),
        icon: const Icon(Icons.add),
        onPressed: () async {
          await openLinkPage(
            context,
            OpenLinkPageParams.basic(
              child: UploadPage(
                companyId: widget.companyId ?? '${WhiteLabelEntity.current?.setting.companyId}'
              ),
            ),
          );
          callApi();
        },
      ),
    );
  }

  Future<void> callApi() async {
    try {
      documents = await Api.documents.listByCompany(
        id: widget.companyId ?? '${WhiteLabelEntity.current?.setting.companyId}'
      );
    } catch (e) {
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: 'Tente novamente mais tarde.',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }
}

class PdfViewsWidget extends StatefulWidget {
  const PdfViewsWidget({
    Key? key,
    required this.document,
    this.hideAppBar = false,
  }) : super(key: key);
  final DocumentEntity document;
  final bool hideAppBar;

  @override
  State<PdfViewsWidget> createState() => _PdfViewsWidgetState();
}

class _PdfViewsWidgetState extends State<PdfViewsWidget> {

  late PdfControllerPinch pdfController;

  @override
  void initState() {
    super.initState();
    pdfController = PdfControllerPinch(
      document: PdfDocument.openData(InternetFile.get(widget.document.url)),
      initialPage: 1,
      viewportFraction: 1,
    );
  }

  @override
  void dispose() {
    pdfController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.hideAppBar ? Theme.of(context).colorScheme.primaryContainer : null,
      appBar: widget.hideAppBar ? null : OwAppBar(
        backgroundColor: widget.hideAppBar ? Theme.of(context).colorScheme.primaryContainer : null,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(EvaIcons.close),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Clipboard.setData(ClipboardData(
                text: '${widget.document.url}',
              )).then((value) {
                OwBotToast.toast('Link do documento copiado.');
              });
            },
            icon: const Icon(EvaIcons.copyOutline),
          ),
        ],
      ),
      body: Stack(
        children: [
          const Center(child: CircularProgressIndicator()),
          PdfViewPinch(
            controller: pdfController,
            onDocumentLoaded: (document) {
              if(!widget.hideAppBar) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text('Páginas totais: ${document.pagesCount}')),
                );
              }
            },
            onPageChanged: (page) {},
          ),
        ],
      ),
    );
  }
}