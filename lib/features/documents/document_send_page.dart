import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:file_picker/file_picker.dart';
import 'package:http_parser/http_parser.dart';

class UploadPage extends StatefulWidget {
  const UploadPage({super.key, required this.companyId});
  final String companyId;
  @override
  _UploadPageState createState() => _UploadPageState();
}

class _UploadPageState extends State<UploadPage> {

  double? width;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      width = 250;
      animated(true);
    });
  }

  void animated([t]) {
    try {
      setState(() {
        if (width == 250) {
          width = 180;
        } else {
          width = 250;
        }
      });
      Future.delayed(
          t ? const Duration(milliseconds: 500) : const Duration(seconds: 2),
          () {
        animated(false);
      });
    } catch (e) {}
  }


  String category = 'documento';

  Future<void> selectAndUploadFile({
    required String category,
    required String companyId,
  }) async {

    final FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
      withData: true,
    );

    if (result != null) {

      final Uint8List? fileBytes = result.files.single.bytes;
      final String filename = result.files.single.name;

      if (fileBytes != null) {
        try {
          OwBotToast.loading();
          final fileUploaded = await Api.upload.upload(
            fileBytes,
            contentType: MediaType('application', 'pdf'),
            filename: filename,
            isPublic: false,
          );
          OwBotToast.close();
          try {
            OwBotToast.loading();
            final doc = await Api.documents.add(
              data: DocumentEntity(
                category: category,
                companyId: companyId,
                url: fileUploaded.url,
                title: filename,
              ),
            );
            OwBotToast.close();
            OwBotToast.toast('Documento adicionado com sucesso.');
            Navigator.pop(context, doc);
          } catch (e) {
            OwBotToast.close();
            showOwDialog(
              context: context,
              title: 'Ocorreu um erro ao adicionar documento',
              description: '$e',
              buttons: [
                OwButton(
                  labelText: 'Fechar',
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            );
          }
        } catch (e) {
          OwBotToast.close();
          showOwDialog(
            context: context,
            title: 'Ocorreu um erro ao enviar arquivo',
            description: '$e',
            buttons: [
              OwButton(
                labelText: 'Fechar',
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
      }
    } else {
      OwBotToast.toast('Nenhum arquivo selecionado');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                AnimatedContainer(
                  width: width,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Theme.of(context).colorScheme.primaryContainer.withOpacity(.2),
                  ),
                  curve: Curves.linear,
                  duration: const Duration(seconds: 2),
                  child: Center(
                    child: Container(
                      width: 250,
                      height: 250,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 5,
                          color: Theme.of(context).colorScheme.primaryContainer,
                        ),
                        shape: BoxShape.circle,
                      ),
                      padding: const EdgeInsets.all(35),
                      child: Image.asset(
                        'assets/images/upload.png',
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 25),
                Text(
                  'Apenas arquivos PDF de até 10MB',
                  style: Theme.of(context).textTheme.bodyMedium,
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 12),
                OwButton(
                  onPressed: () {
                    selectAndUploadFile(
                      companyId: widget.companyId,
                      category: category,
                    );
                  },
                  labelText: 'Selecionar e enviar documento',
                  leading: EvaIcons.uploadOutline,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}