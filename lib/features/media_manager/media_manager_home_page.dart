import 'dart:convert';

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:image_editor_plus/image_editor_plus.dart';
import 'package:image_picker/image_picker.dart' as ip;
import 'package:image_picker/image_picker.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class MediaManagerHomePage extends StatefulWidget {
  const MediaManagerHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  static String route = '/media-manager/home';
  final bool automaticallyImplyLeading;

  @override
  _MediaManagerHomePageState createState() => _MediaManagerHomePageState();
}

class _MediaManagerHomePageState extends State<MediaManagerHomePage> {
  List<FileMediaManagerEntity>? _files;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  void dispose() {
    super.dispose();
    _files?.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const Text('Imagens WIX'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            GridView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: _files?.length ?? 0,
              cacheExtent: (MediaQuery.sizeOf(context).width / 3) - 25,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1,
                crossAxisCount: 3,
                mainAxisExtent: (MediaQuery.sizeOf(context).width / 3) - 25,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
              ),
              itemBuilder: ((context, index) {
                return InkWell(
                  onLongPress: () {
                    Clipboard.setData(ClipboardData(
                      text: '${_files![index].fileUrl}',
                    )).then((value) {
                      OwBotToast.toast('Link da imagem copiado.');
                    });
                  },
                  child: AspectRatio(
                    aspectRatio: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.secondaryContainer,
                      ),
                      child: Image.network(
                        '${_files![index].fileUrl}',
                      ),
                    ),
                  ),
                );
              }),
            ),
            Visibility(
              visible: _files != null,
              child: OwButton(
                margin: const EdgeInsets.all(25),
                labelText: 'Carregar mais',
                onPressed: () {
                  callApi();
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: null,
        child: const Icon(
          Icons.add,
        ),
        onPressed: () async {
          _files ??= [];
          final retorno = await openModalPage(context, const AlterarLogo());
          if (retorno is FileMediaManagerEntity) {
            _files!.insert(0, retorno);
            setState(() {});
          }
        },
      ),
    );
  }

  void callApi() async {
    OwBotToast.loading();
    try {
      final filesTemp = await Api.mediaManager.listMediaWix(
        skip: _files?.length ?? 0,
      );
      _files ??= [];
      _files!.addAll(filesTemp);
      OwBotToast.close();
    } catch (_) {
      OwBotToast.close();
    }
    setState(() {});
  }
}

class AlterarLogo extends StatefulWidget {
  const AlterarLogo({super.key});

  @override
  _AlterarLogoState createState() => _AlterarLogoState();
}

class _AlterarLogoState extends State<AlterarLogo> {
  XFile? pickedFile;
  final picker = ip.ImagePicker();

  void _camera() async {
    pickedFile = await picker.pickImage(source: ip.ImageSource.camera);
    _upload();
  }

  void _galeria() async {
    pickedFile = await picker.pickImage(source: ip.ImageSource.gallery);
    _upload();
  }

  Future<void> _upload() async {
    if (pickedFile == null) {
      showOwDialog(
          context: context,
          title: 'Erro ao adicionar imagem',
          buttons: [
            OwButton.text(
              labelText: 'Fechar',
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ]);
    } else {
      final Uint8List fileData = await pickedFile!.readAsBytes();
      String base64Image;
      final resultado = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ImageEditor(
            image: fileData,
          ),
        ),
      );
      OwBotToast.loading();
      if (resultado != null) {
        if (resultado is Uint8List) {
          base64Image = base64Encode(resultado);
        } else {
          base64Image = base64Encode(fileData);
        }
        final Map enviarImagem = {
          'image': 'data:image/jpeg;base64,$base64Image'
        };
        try {
          final retorno = await Api.mediaManager.insertImageWix(
            data: enviarImagem,
          );
          context.pop(retorno);
          OwBotToast.close();
        } catch (e) {
          OwBotToast.close();
          await showOwDialog(
              context: context,
              title: 'Estamos adicionando sua imagem',
              buttons: [
                OwButton.text(
                  labelText: 'Fechar',
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.pop(context);
                    openLinkPage(
                      context,
                      OpenLinkPageParams.basic(child: const MediaManagerHomePage(automaticallyImplyLeading: true)),
                    );
                  },
                ),
              ]);
        }
      } else {
        OwBotToast.close();
        showOwDialog(
            context: context,
            title: 'Erro ao obter a imagem',
            buttons: [
              OwButton.text(
                labelText: 'Fechar',
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ]);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.primaryContainer,
          borderRadius: const BorderRadius.all(Radius.circular(15)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 25, 30, 15),
              child: Text(
                'Upload de Imagem',
                textAlign: TextAlign.left,
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: Theme.of(context).colorScheme.onPrimaryContainer,
                    ),
              ),
            ),
            const OwDivider(),
            const SizedBox(height: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 26,
                    vertical: 4,
                  ),
                  leading: Icon(
                    EvaIcons.cameraOutline,
                    color: Theme.of(context).colorScheme.onPrimaryContainer,
                  ),
                  title: const Text(
                    'Tirar uma foto',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: _camera,
                ),
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 26,
                    vertical: 4,
                  ),
                  leading: Icon(
                    EvaIcons.imageOutline,
                    color: Theme.of(context).colorScheme.onPrimaryContainer,
                  ),
                  title: const Text(
                    'Enviar da galeria',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: _galeria,
                ),
              ],
            ),
            const SizedBox(height: 10)
          ],
        ),
      ),
    );
  }
}
