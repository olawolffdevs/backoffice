import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ContactsHomePage extends StatefulWidget {
  const ContactsHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  static String route = '/contacts';
  final bool automaticallyImplyLeading;
  @override
  State<ContactsHomePage> createState() => _ContactsHomePageState();
}

class _ContactsHomePageState extends State<ContactsHomePage> {
  List<ContactEntity>? _contacts;
  String? msgError;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  OwText(
                    'Contatos',
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const OwText(
                    'Gerencie os contatos dos seus cliente e fornecedores.',
                  ),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 25),
              child: OwDivider(),
            ),
            Visibility(
              visible: !Valid.text(msgError),
              replacement: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    OwText(
                      'Ocorreu um erro',
                      style: Theme.of(context).textTheme.titleMedium,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 16),
                    OwText(
                      '$msgError',
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 16),
                    OwButton(
                      labelText: 'Voltar',
                      onPressed: () {
                        context.pop();
                      },
                    ),
                    OwButton.elevated(
                      labelText: 'Tentar novamente',
                      onPressed: () {
                        setState(() {
                          msgError = null;
                          _contacts = null;
                        });
                        callApi();
                      },
                    ),
                  ],
                ),
              ),
              child: Visibility(
                visible: _contacts != null,
                replacement: const Center(child: CircularProgressIndicator()),
                child: Visibility(
                  visible: _contacts?.isNotEmpty ?? false,
                  replacement: Container(
                    padding: const EdgeInsets.fromLTRB(25, 0, 25, 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          'Você ainda não cadastrou nenhum contato.',
                          style: Theme.of(context).textTheme.titleMedium,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  child: OwGrid.builder(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    shrinkWrap: true,
                    spacing: 15,
                    runSpacing: 15,
                    itemCount: _contacts?.length ?? 0,
                    numbersInRowAccordingToWidgth: const [
                      680,
                      1050,
                      1400,
                      2900
                    ],
                    itemBuilder: (context, index) {
                      return Ink(
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.secondaryContainer,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(15),
                          ),
                        ),
                        child: InkWell(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(15),
                          ),
                          onTap: () async {
                            final result = await context.push(
                              '/${WhiteLabelEntity.current?.id}/dashboard/contacts/${_contacts![index].id}/details',
                              extra: _contacts![index],
                            );
                            if (result is ContactEntity) {
                              _contacts![index] = result;
                              setState(() {});
                            } else if (result == true) {
                              _contacts!.removeAt(index);
                              setState(() {});
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(25),
                            child: Row(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).colorScheme.primary,
                                    shape: BoxShape.circle,
                                  ),
                                  padding: const EdgeInsets.all(12),
                                  child: OwText(
                                    '${_contacts![index].name?.substring(0, 1)}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleLarge
                                        ?.copyWith(
                                          color: Theme.of(context).colorScheme.onPrimary,
                                        ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: [
                                      OwText(
                                        '${_contacts![index].name}',
                                        style: Theme.of(context).textTheme.titleLarge,
                                      ),
                                      const SizedBox(
                                        height: 4,
                                      ),
                                      OwText(
                                        "${_contacts![index].phone?.length} ${_contacts![index].phone?.length == 1 ? "telefone" : "telefones"}",
                                        style: Theme.of(context).textTheme.bodyMedium,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: _contacts == null
          ? const SizedBox()
          : FloatingActionButton.extended(
              heroTag: null,
              label: const Text('Novo contato'),
              icon: const Icon(
                Icons.add,
              ),
              onPressed: () async {
                final ContactEntity? retorno = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/contacts/add',
                );
                if (retorno is ContactEntity) {
                  _contacts?.insert(0, retorno);
                  setState(() {});
                }
              },
            ),
    );
  }

  void callApi() async {
    try {
      _contacts = await Api.contacts.listByCompany();
    } catch (e) {
      msgError = '$e';
    }
    setState(() {});
  }
}
