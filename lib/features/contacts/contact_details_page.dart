import 'package:brasil_fields/brasil_fields.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ContactDetailsPage extends StatefulWidget {
  const ContactDetailsPage({Key? key, this.contact}) : super(key: key);
  static String route = '/contacts-details';
  final ContactEntity? contact;

  @override
  State<ContactDetailsPage> createState() => _ContactDetailsPageState();
}

class _ContactDetailsPageState extends State<ContactDetailsPage> {
  TextEditingController description = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController labels = TextEditingController();

  List<TextEditingController> _phones = [];

  @override
  void initState() {
    super.initState();
    if (widget.contact != null) {
      description.text = widget.contact!.description ?? '';
      name.text = widget.contact!.name ?? '';
      email.text = widget.contact!.email ?? '';
      widget.contact!.phone?.map((e) {
        _phones.add(new TextEditingController(text: e));
      }).toList();
    } else {
      _phones.add(new TextEditingController());
    }
  }

  @override
  void dispose() {
    description.dispose();
    name.dispose();
    email.dispose();
    labels.dispose();
    _phones.map((e) => e.dispose()).toList();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        actions: [
          Visibility(
            visible: widget.contact != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar este contato?',
                  description: 'Essa ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton(
                      labelText: 'Sim',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                    OwButton(
                      labelText: 'Cancelar',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.contacts.delete(id: widget.contact!.id!);
                    OwBotToast.close();
                    context.pop(true);
                  } catch (e) {
                    OwBotToast.close();
                    showOwDialog(
                      context: context,
                      title: 'Ocorreu um erro ao remover o contato.',
                      description: '$e',
                      buttons: [
                        OwButton(
                          labelText: 'Fechar',
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    );
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 25,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwText(
              'Detalhes do contato',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            const SizedBox(
              height: 10,
            ),
            OwText(
              'Classifique e acompanhe as pessoas que interagem com seu site.',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            const SizedBox(
              height: 25,
            ),
            OwTextField(
              controller: name,
              labelText: 'Nome',
            ),
            const SizedBox(
              height: 25,
            ),
            OwTextField(
              controller: email,
              labelText: 'Email',
              keyboardType: TextInputType.emailAddress,
            ),
            ListView.builder(
              itemCount: _phones.length,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return OwTextField(
                  margin: const EdgeInsets.only(top: 25),
                  controller: _phones[index],
                  keyboardType: TextInputType.phone,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                    TelefoneInputFormatter(),
                  ],
                  labelText: 'Telefone ${index + 1}',
                );
              },
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CardAtalhos(
                  title: 'Add outro número de telefone',
                  icon: EvaIcons.phoneOutline,
                  selecionado: true,
                  onPressed: () {
                    setState(() {
                      _phones.add(new MaskedTextController(mask: '(00) 00000 0000'));
                    });
                  },
                ),
                Visibility(
                  visible: _phones.length > 1,
                  child: CardAtalhos(
                    title: 'Remover último',
                    icon: EvaIcons.phoneOutline,
                    onPressed: () {
                      setState(() {
                        _phones.removeLast();
                      });
                    },
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            OwTextField(
              controller: description,
              labelText: 'Descrição',
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            OwButton(
              expanded: true,
              margin: const EdgeInsets.all(25),
              labelText: 'Salvar',
              onPressed: () async {
                if (description.text.trim().isEmpty || name.text.trim().isEmpty) {
                  showOwDialog(
                    context: context,
                    title: 'Existem um ou mais campos que precisam ser preenchidos.',
                    buttons: [
                      OwButton(
                        labelText: 'Fechar',
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  );
                } else {
                  if (widget.contact != null) {
                    final ContactEntity module = widget.contact!.copyWith(
                      description: description.text.trim(),
                      name: name.text.trim(),
                      phone: _phones.map((e) {
                        return OwFormat.removerCaracteres(
                          e.text.trim(),
                          '() ',
                        );
                      }).toList(),
                      email: email.text.trim(),
                      empresa: companySM.company?.id,
                    );
                    try {
                      OwBotToast.loading();
                      await Api.contacts.update(id: widget.contact!.id!, data: module);
                      OwBotToast.close();
                      context.pop(module);
                      showOwDialog(
                        context: context,
                        title: 'Contato atualizado com sucesso.',
                      );
                    } catch (e) {
                      OwBotToast.close();
                      showOwDialog(
                        context: context,
                        title: 'Ocorreu um erro ao atualizar o contato.',
                        description: '$e',
                        buttons: [
                          OwButton(
                            labelText: 'Fechar',
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      );
                    }
                  } else {
                    try {
                      final ContactEntity module = ContactEntity(
                        description: description.text.trim(),
                        name: name.text.trim(),
                        phone: _phones.map((e) {
                          return e.text.trim();
                        }).toList(),
                        email: email.text.trim(),
                        empresa: companySM.company?.id,
                      );
                      OwBotToast.loading();
                      final response = await Api.contacts.insert(data: module);
                      context.pop(response);
                      showOwDialog(
                        context: context,
                        title: 'Contato adicionado com sucesso.',
                      );
                    } catch (e) {
                      OwBotToast.close();
                      showOwDialog(
                        context: context,
                        title: 'Ocorreu um erro ao adicionar o contato.',
                        description: '$e',
                        buttons: [
                          OwButton(
                            labelText: 'Fechar',
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      );
                    }
                  }
                }
              },
            ),
            SizedBox(height: BS.bottomPadding(context))
          ],
        ),
      ),
    );
  }
}
