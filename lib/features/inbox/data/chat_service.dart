import 'package:audioplayers/audioplayers.dart';
import 'package:backoffice/features/inbox/data/chat_params.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import '../../../settings/web/web_app_updater.dart';

final player = AudioPlayer();

class ChatService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Stream<List<QueryDocumentSnapshot>> getUserChats(String userId) {
    return _firestore
      .collection('chat-dm')
      .where('participantsList', arrayContainsAny: [userId])
      .orderBy('updatedDate', descending: false)
      .snapshots()
      .map((snapshot) {
        int unreadCount = 0;
        for (var doc in snapshot.docs) {
          final tmp = ChatEntity.fromMap(doc.data());
          if(!tmp.lastMessage!.readBy.toString().contains(userId)) {
            unreadCount++;
          }
        }
        if(unreadCount > appSM.countMessages) {
          if(appSM.soundEnabled) {
            try {
              player.play(AssetSource('sound/notification.wav'), volume: 1);
            } catch (e) {
              //
            }
          }
        }
        if(unreadCount > 0) {
          WebAppUpdater.updateTitle('Você tem $unreadCount conversas não lidas || Backoffice: ${WhiteLabelEntity.current?.name}');
          WebAppUpdater.updateFavicon('/favicon2.png');
        } else {
          WebAppUpdater.updateTitle('Backoffice: ${WhiteLabelEntity.current?.name}');
          WebAppUpdater.updateFavicon('/favicon.png');
        }
        appSM.setCountMessages(unreadCount);
        return snapshot.docs;
      });
  }

  Stream<List<QueryDocumentSnapshot>> getMessagesByChat(String chatId) {
    return _firestore
      .collection('chat-dm')
      .doc(chatId)
      .collection('messages')
      .orderBy('createdDate', descending: false)
      .snapshots()
      .map((snapshot) => snapshot.docs);
  }

  Future<ChatEntity> startChat({
    required ChatParams params,
  }) async {

    late ChatEntity chatTemp;

    final db = _firestore.collection('chat-dm');
    final timestamp = Timestamp.fromDate(DateTime.now());
    final DocumentSnapshot chatFB = await db.doc(params.chatId).get();

    if (!chatFB.exists) {

      chatTemp = createChat(
        params: params,
        timestamp: timestamp,
      );
      
      db.doc(params.chatId).set(chatTemp.toMap()..addAll({
        'participantsList': params.participants != null
          ? params.participants!.map((e) => e.id).toList()
          : [params.current.id]
      }));

    } else {

      final Map<String, dynamic> temp = chatFB.data() as Map<String, dynamic>;
      chatTemp = ChatEntity.fromMap(temp);
      if(chatTemp.lastMessage != null) {
        if(!chatTemp.lastMessage!.readBy.contains(params.current.id)) {
          final readBy = chatTemp.lastMessage?.readBy ?? <ParticipantChat>[];
          readBy.add(params.current);
          db.doc(params.chatId).update({
            'lastMessage': chatTemp.lastMessage?.copyWith(
              readBy: readBy,
            ).toMap() ?? null,
          });
        }
      }
    }

    print(chatTemp.participants);

    return chatTemp;
  }

  ChatEntity createChat({
    required ChatParams params,
    required Timestamp timestamp,
  }) {
    return ChatEntity(
      origin: params.origin ?? 'inbox',
      createdBy: params.current,
      createdDate: timestamp,
      updatedDate: timestamp,
      administrators: [params.current],
      participants: params.participants ?? [
        params.current,
      ],
      isPublic: false,
      archived: false,
      canMembersEditGroupData: false,
      membersCanMessage: true,
      chatDescription: 'Chat privado - DM',
      chatTitle: params.title,
      id: params.chatId,
      additional: params.additionalChat,
    );
  }

}