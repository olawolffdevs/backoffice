import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ChatParams {
  const ChatParams({
    required this.chatId,
    this.additionalChat,
    this.origin = 'inbox',
    this.title,
    required this.current,
    this.participants = const [],
  });
  final String chatId;
  final String? origin;
  final String? title;
  final ParticipantChat current;
  final AdditionalChat? additionalChat;
  final List<ParticipantChat>? participants;
}