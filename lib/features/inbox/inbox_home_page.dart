import 'package:backoffice/features/inbox/data/chat_params.dart';
import 'package:backoffice/features/inbox/ui/chat/chat_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:flutter/scheduler.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class InboxHomePage extends StatefulWidget {
  const InboxHomePage({Key? key, this.bottom = 0, this.type = 'company', this.automaticallyImplyLeading = true}) : super(key: key);
  final double bottom;
  final String type;
  final bool automaticallyImplyLeading;
  static String route = '/inbox';

  @override
  State<InboxHomePage> createState() => _InboxHomePageState();
}

class _InboxHomePageState extends State<InboxHomePage> {
  int notRead = 0;

  List<DocumentSnapshot>? documents;
  List<String> ids = [
    '${WhiteLabelEntity.current?.setting.companyId}',
    '${WhiteLabelEntity.current?.setting.ecommerceId}',
  ];
  ChatEntity? chat;

  @override
  void initState() {
    super.initState();
    _listenToData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _listenToData() {
    FirebaseFirestore.instance
        .collection('chat-dm')
        .where('participantsList', arrayContainsAny: ids)
        .snapshots()
        .listen((snap) {
      documents = snap.docs;
      documents?.sort((a, b) {
        final Map chatA = a.data() as Map;
        final Map chatB = b.data() as Map;
        return chatB['updatedDate'].compareTo(chatA['updatedDate']);
      });
      try {
        SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
          if (mounted) {
            setState(() {});
          }
        });
      } catch (ee) {
        //
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Scaffold(
              appBar: OwAppBar(
                automaticallyImplyLeading: widget.automaticallyImplyLeading,
                titleSpacing: 25,
                title: const OwText('Conversas'),
                centerTitle: true,
              ),
              body: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                      child: documents == null
                          ? const Center(child: CircularProgressIndicator())
                          : documents!.isEmpty
                              ? isEmpty()
                              : listChats()),
                ],
              ),
            ),
          ),
          Visibility(
            visible: (MediaQuery.sizeOf(context).width > 840) &&
                documents != null &&
                documents!.isNotEmpty,
            child: Container(
              margin: const EdgeInsets.all(25),
              width: MediaQuery.sizeOf(context).width * .6,
              height: MediaQuery.sizeOf(context).height,
              constraints: const BoxConstraints(minWidth: 260, maxWidth: 840),
              child: LayoutBuilder(
                builder: (context, constraints) {
                  final int now = DateTime.now().millisecondsSinceEpoch;
                  final Timestamp timestamp =
                      Timestamp.fromMillisecondsSinceEpoch(now);
                  return MediaQuery(
                    data: MediaQueryData(
                      padding: MediaQuery.viewPaddingOf(context),
                      size: Size(
                        constraints.maxWidth,
                        constraints.maxHeight,
                      ),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 2,
                          color:
                              Theme.of(context).colorScheme.secondaryContainer,
                        ),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(33),
                        ),
                      ),
                      child: chat != null
                          ? ClipRRect(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(33),
                              ),
                              child: new ChatPage(
                                chat: chat!,
                                params: ChatParams(
                                  chatId: '${chat?.id}',
                                  current: ParticipantChat(
                                    name: '${WhiteLabelEntity.current?.name}',
                                    id: '${WhiteLabelEntity.current?.setting.ecommerceId}',
                                    type: chat?.origin == 'order'
                                      ? 'ecommerce'
                                      : 'support',
                                    createdDate: timestamp,
                                  ),
                                ),
                                automaticallyImplyLeading: false,
                                key: ValueKey<Object>(Key('${chat?.id}')),
                              ),
                            )
                          : ClipRRect(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(33),
                              ),
                              child: Center(
                                child: isNotSelected(),
                              ),
                            ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget isEmpty() {
    return Center(
      child: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(25, 0, 25, widget.bottom),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image.asset(
              'assets/images/chat.png',
              height: 250,
            ),
            const SizedBox(height: 25),
            const OwText(
              'Poxa, nenhuma conversa ainda',
              style: TextStyle(fontWeight: FontWeight.w800, fontSize: 20),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 15),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 35),
              child: OwText(
                'Quando você tiver uma conversa privada ou em grupo, você as verá aqui. Você também pode iniciar uma conversa privada ou conversar com nosso suporte clicando nos botões localizado no topo da tela.',
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 25)
          ],
        ),
      ),
    );
  }

  Widget isNotSelected() {
    return Container(
      color: Theme.of(context).colorScheme.background,
      child: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Image.asset(
                'assets/images/chat.png',
                height: 250,
              ),
              const SizedBox(height: 30),
              const OwText(
                'Nenhuma mensagem selecionada',
                style: TextStyle(fontWeight: FontWeight.w800, fontSize: 20),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 15),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 35),
                child: OwText(
                  'Selecione uma mensagem ao lado.',
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(height: 10)
            ],
          ),
        ),
      ),
    );
  }

  Widget listChats() {
    return ListView.separated(
      shrinkWrap: true,
      physics: const BouncingScrollPhysics(),
      padding: EdgeInsets.fromLTRB(
        MediaQuery.sizeOf(context).width > 840 ? 25 : 5,
        10,
        MediaQuery.sizeOf(context).width > 840 ? 25 : 5,
        10 + widget.bottom,
      ),
      itemCount: documents!.length,
      separatorBuilder: (context, index) {
        return const SizedBox(height: 0);
      },
      itemBuilder: (context, index) {
        final Map<String, dynamic> temp =
            documents![index].data() as Map<String, dynamic>;
        final ChatEntity chatTemp = ChatEntity.fromMap(temp).copyWith(
          id: documents![index].id,
        );
        String horario = '';
        final DateTime agora = chatTemp.updatedDate.toDate();
        final DateTime now = DateTime.now();
        if (agora.day == now.day &&
            agora.month == now.month &&
            agora.year == now.year) {
          horario = DateFormat('HH:mm')
              .format(chatTemp.updatedDate.toDate().toLocal());
        } else {
          horario = DateFormat('dd/MM/yyyy')
              .format(chatTemp.updatedDate.toDate().toLocal());
        }

        String titleChat = '';
        final List<ParticipantChat> participantsTemp = chatTemp.participants;
        participantsTemp.removeWhere((element) =>
            element.id == WhiteLabelEntity.current?.setting.ecommerceId);
        titleChat = chatTemp.chatTitle ??
            participantsTemp
                .map((e) => e.name)
                .toList()
                .toString()
                .replaceAll('[', '')
                .replaceAll(']', '');

        if (index == 0) {
          notRead = 0;
        }
        if (!(chatTemp.lastMessage?.readBy
                .toString()
                .contains('${WhiteLabelEntity.current?.setting.ecommerceId}') ??
            true)) {
          notRead = notRead + 1;
        }
        return Ink(
          decoration: BoxDecoration(
            color: '${chatTemp.id}' == '${chat?.id}'
                ? Theme.of(context).colorScheme.primaryContainer
                : Theme.of(context).colorScheme.background,
            borderRadius: const BorderRadius.all(
              Radius.circular(15),
            ),
          ),
          child: InkWell(
            borderRadius: const BorderRadius.all(
              Radius.circular(15),
            ),
            onTap: () async {
              if (MediaQuery.sizeOf(context).width <= 950) {
                final int now = DateTime.now().millisecondsSinceEpoch;
                final timestamp = Timestamp.fromMillisecondsSinceEpoch(now);
                context.push(
                  '/${WhiteLabelEntity.current?.id}/inbox/chat/${chatTemp.id}',
                  extra: <String, dynamic>{
                    'chatId': chatTemp.id,
                    'current': ParticipantChat(
                      name: '${WhiteLabelEntity.current?.name}',
                      id: '${WhiteLabelEntity.current?.setting.ecommerceId}',
                      type: chatTemp.origin == 'order' ? 'ecommerce' : 'support',
                      createdDate: timestamp,
                    ),
                    'chat': chatTemp,
                    'key': ValueKey<Object>(Key(chatTemp.id ?? '')),
                  },
                );
              } else {
                setState(() {
                  chat = chatTemp;
                });
              }
            },
            child: Container(
              padding: const EdgeInsets.all(15),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondaryContainer,
                      shape: BoxShape.circle,
                    ),
                    constraints: const BoxConstraints(
                      minWidth: 60,
                      minHeight: 60,
                    ),
                    child: Center(
                      child: chatTemp.origin == 'support'
                          ? Image.network(
                              '${WhiteLabelEntity.current!.style.image.icon}',
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSecondaryContainer,
                              height: 50,
                            )
                          : Padding(
                              padding: const EdgeInsets.all(20),
                              child: Icon(
                                getIconByOrigin(chatTemp.origin),
                                color: Theme.of(context)
                                    .colorScheme
                                    .onSecondaryContainer,
                              ),
                            ),
                    ),
                  ),
                  const SizedBox(width: 15),
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: OwText(
                                '$titleChat',
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium
                                    ?.copyWith(
                                      color: '${chatTemp.id}' == '${chat?.id}'
                                          ? Theme.of(context)
                                              .colorScheme
                                              .onPrimaryContainer
                                          : Theme.of(context)
                                              .colorScheme
                                              .onBackground,
                                    ),
                                maxLines: 1,
                              ),
                            ),
                            const SizedBox(width: 10),
                            OwText('$horario'),
                          ],
                        ),
                        const SizedBox(height: 4),
                        Row(
                          children: [
                            Visibility(
                              visible: chatTemp.lastMessage?.owner?.id ==
                                  WhiteLabelEntity.current?.setting.ecommerceId,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 6),
                                child: Icon(
                                  Icons.done_all_outlined,
                                  color: '${chatTemp.id}' == '${chat?.id}'
                                      ? Theme.of(context)
                                          .colorScheme
                                          .onPrimaryContainer
                                      : Theme.of(context)
                                          .colorScheme
                                          .onBackground,
                                ),
                              ),
                            ),
                            Expanded(
                              child: OwText(
                                Bidi.stripHtmlIfNeeded(
                                    chatTemp.lastMessage?.value ?? ''),
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium
                                    ?.copyWith(
                                      color: '${chatTemp.id}' == '${chat?.id}'
                                          ? Theme.of(context)
                                              .colorScheme
                                              .onPrimaryContainer
                                          : Theme.of(context)
                                              .colorScheme
                                              .onBackground,
                                    ),
                                maxLines: 1,
                              ),
                            ),
                            Visibility(
                              visible: !(chatTemp.lastMessage?.readBy
                                      .toString()
                                      .contains(
                                          '${WhiteLabelEntity.current?.setting.ecommerceId}') ??
                                  true),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 6),
                                child: HighlightMessageCard(
                                  text: 'Não lido',
                                  color: ColorSystem(context: context),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

IconData getIconByOrigin(origin) {
  switch (origin) {
    case 'scheduling':
      return EvaIcons.calendarOutline;
    case 'order':
      return EvaIcons.shoppingBagOutline;
    case 'paginas_verde':
      return EvaIcons.link2Outline;
    case 'support':
      return EvaIcons.questionMarkCircleOutline;
    case 'contacts':
      return EvaIcons.phoneCallOutline;
    default:
      return EvaIcons.messageCircleOutline;
  }
}

IconData getIconByParticipant(origin) {
  switch (origin) {
    case 'store':
      return Icons.storefront_outlined;
    case 'ecommerce':
      return Icons.storefront_outlined;
    case 'company':
      return Icons.store_outlined;
    case 'people':
      return EvaIcons.personOutline;
    case 'user':
      return EvaIcons.personOutline;
    case 'support':
      return EvaIcons.questionMarkCircleOutline;
    default:
      return EvaIcons.questionMark;
  }
}
