import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class LinkScreen extends StatefulWidget {
  const LinkScreen({super.key});

  @override
  State<LinkScreen> createState() => _LinkScreenState();
}

class _LinkScreenState extends State<LinkScreen> {

  bool _isExternalLink = true;
  final TextEditingController _linkController = TextEditingController();
  final TextEditingController _labelButton = TextEditingController();

  final List<Map<String, String>> _appScreens = [
    {'name': 'levar para Inicio da PF', 'link': '/dashboard-pf'},
    {'name': 'levar para Inicio da PJ', 'link': '/empresa/#companyId/dashboard'},
    {'name': 'levar para Perfil da PF', 'link': '/pf/perfil/home'},
    {'name': 'levar para Perfil da PJ', 'link': '/empresa/#companyId/perfil'},
    {'name': 'levar para Me ajuda', 'link': '/me-ajuda'},
    {'name': 'levar para Indicar app', 'link': '/empresa/#companyId/indicate'},
    {'name': 'levar para Certificado MEI', 'link': '/empresa/#companyId/gestao-do-mei/ccmei'},
    {'name': 'levar para Cartão CNPJ', 'link': '/empresa/#companyId/cnpj'},
    {'name': 'levar para DAS', 'link': '/empresa/#companyId/gestao-do-mei/das'},
    {'name': 'levar para DASN', 'link': '/empresa/#companyId/gestao-do-mei/dasn'},
  ];

  String? _selectedScreen;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Adicionar link'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SingleChildScrollView(
              padding: const EdgeInsets.only(left: 16, right: 8),
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CardAtalhos(
                    title: 'Link Externo',
                    selecionado: _isExternalLink,
                    onPressed: () {
                      setState(() {
                        _isExternalLink = true;
                        _linkController.clear();
                        _selectedScreen = null;
                      });
                    },
                  ),
                  CardAtalhos(
                    title: 'Link do aplicativo',
                    selecionado: !_isExternalLink,
                    onPressed: () {
                      setState(() {
                        _isExternalLink = false;
                        _linkController.clear();
                        _selectedScreen = null;
                      });
                    },
                  ),
                ],
              ),
            ),
            Visibility(
              visible: _isExternalLink,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 8,
                    ),
                    child: OwTextField(
                      controller: _linkController,
                      labelText: 'Link externo',
                      hintText: 'Digite o link externo aqui',
                    ),
                  ),
                ],
              ),
              replacement: ListView.builder(
                padding: const EdgeInsets.only(top: 8),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _appScreens.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 20),
                    title: Text(_appScreens[index]['name']!),
                    trailing: _selectedScreen == _appScreens[index]['link']
                      ? Icon(Icons.check_circle, color: Theme.of(context).primaryColor)
                      : const Icon(Icons.radio_button_unchecked),
                    onTap: () {
                      setState(() {
                        _selectedScreen = _appScreens[index]['link'];
                        _linkController.text = _appScreens[index]['link']!;
                      });
                    },
                  );
                },
              ),
            ),
            const SizedBox(height: 24),
            const OwDivider(),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 8,
              ),
              child: OwTextField(
                controller: _labelButton,
                labelText: 'Texto do botão',
                hintText: 'Ex.: Saiba mais',
              ),
            ),
            const SizedBox(height: 24),
            const OwDivider(),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        padding: const EdgeInsets.all(25),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwButton(
              onPressed: () {
                Navigator.pop(context, {
                  'type': 'link',
                  'value': _linkController.text,
                  'extra': Valid.text(_labelButton.text) ? _labelButton.text : 'Acessar link',
                });
              },
              labelText: 'Enviar mensagem com link',
              leading: EvaIcons.paperPlaneOutline,
              height: 55,
            ),
            SizedBox(height: BS.bottomPadding(context)),
          ],
        ),
      ),
    );
  }
}