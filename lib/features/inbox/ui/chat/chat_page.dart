import 'package:backoffice/features/inbox/data/chat_service.dart';
import 'package:backoffice/features/inbox/ui/widgets/app_bar_chat.dart';
import 'package:backoffice/features/inbox/ui/widgets/card/card_alert_chat.dart';
import 'package:backoffice/features/inbox/ui/widgets/card/card_message_chat.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:backoffice/features/inbox/ui/widgets/card/card_section_chat.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import '../../data/chat_params.dart';
import '../widgets/bottom_bar_chat.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({
    Key? key,
    this.automaticallyImplyLeading = true,
    required this.params,
    required this.chat,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  final ChatParams params;
  final ChatEntity chat;

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {

  final _scrollController = ScrollController();
  ChatEntity? chat;

  String mensagemDia = 'Começo da Conversa';

  final FocusNode _focusNode = FocusNode();
  final TextSelectionControls _selectionControls = materialTextSelectionControls;

  @override
  void initState() {
    super.initState();
    ChatService().startChat(params: widget.params).then((result) {
      setState(() {
        chat = result;
        print(chat?.participants);
      });
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(chat == null) {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Scaffold(
      appBar: AppBarChat(
        params: widget.params,
        chat: chat!,
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
      ),
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const CardAlertChat(label: 'Para garantir a sua segurança, evite compartilhar números de cartão ou quaisquer dados pessoais através deste chat. Lembre-se de que nossas conversas não são protegidas por criptografia de ponta a ponta.'),
            const CardSectionChat(label: 'Você chegou ao início da conversa'),
            StreamBuilder<List<QueryDocumentSnapshot>>(
              stream: ChatService().getMessagesByChat('${chat!.id}'),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: CircularProgressIndicator());
                }
                final messages = snapshot.data!.map((doc) => MessageChatEntity.fromMap(doc.data() as Map<String, dynamic>)).toList();
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: messages.length,
                      itemBuilder: (context, index) {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            boxDate(messages[index], index),
                            SelectableRegion(
                              focusNode: _focusNode,
                              selectionControls: _selectionControls,
                              child: CardMessageChat(
                                message: messages[index],
                                params: widget.params,
                                extra: messages[index].extra,
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                );
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomBarChat(
        scrollController: _scrollController,
        params: widget.params,
        chat: chat!,
      ),
    );
  }

  Widget boxDate(MessageChatEntity message, int index) {
    final DateTime agora = DateTime.fromMillisecondsSinceEpoch(
      (message.createdDate).millisecondsSinceEpoch,
    );
    final List data2 = agora.toString().substring(0, 10).split('-');
    final String dia = '${data2[2]}/${data2[1]}/${data2[0]}';
    String aux = mensagemDia;
    if (aux == 'Começo da Conversa') aux = dia;
    mensagemDia = dia;
    if (mensagemDia != aux || index == 0) {
      return CardSectionChat(
        label: DateFormat('dd/MM/yyyy').format(
          DateTime.fromMillisecondsSinceEpoch(
            message.createdDate.millisecondsSinceEpoch,
          ).toLocal(),
        ),
      );
    } else {
      return const SizedBox();
    }
  }
  
}
