import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import '../../inbox_home_page.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:intl/intl.dart';

class DetailsChatPage extends StatefulWidget {
  const DetailsChatPage({
    Key? key,
    required this.chat,
    required this.current,
  }) : super(key: key);
  final ChatEntity chat;
  final ParticipantChat current;
  @override
  State<DetailsChatPage> createState() => _DetailsChatPageState();
}

class _DetailsChatPageState extends State<DetailsChatPage> {
  late ChatEntity chat;
  bool loading = true;

  final db = FirebaseFirestore.instance.collection('chat-dm');

  List shortcuts = [];
  String? titleChat;
  String createdBy = 'usuário';

  void initChat() async {
    chat = widget.chat;
    final DocumentSnapshot chatDocument = await db.doc(chat.id).get();
    final Map<String, dynamic> chatMap = chatDocument.data() as Map<String, dynamic>;
    final ChatEntity chatTemp = ChatEntity.fromMap(chatMap);
    chat = chatTemp;

    final List<ParticipantChat> participantsTemp = [];
    for (final element in chat.participants) {
      participantsTemp.add(element);
    }
    participantsTemp.removeWhere((element) => element.id == widget.current.id);
    titleChat = chat.chatTitle ?? participantsTemp.map((e) => e.name).toList().toString().replaceAll('[', '').replaceAll(']', '');
    createdBy = chat.createdBy.name;

    if (Valid.text(chat.additional?.orderId)) {
      shortcuts.add(RoundCard(
        title: 'Visualizar pedido',
        child: Icon(IconAdapter.getIcon('EvaIcons.fileTextOutline')),
        color: ColorSystem(context: context),
        spotlight: const SizedBox(),
        onPressed: () async {
          try {
            OwBotToast.loading();
            final result = await Api.ecommerce.orders.find(
              id: '${chat.additional?.orderId}',
            );
            OwBotToast.close();
            await context.push('/${WhiteLabelEntity.current?.id}/ecommerce/order/${result.id}', extra: result);
          } catch (e) {
            OwBotToast.close();
            showOwDialog(
              context: context,
              title: 'Ocorreu um erro.',
              description: 'Não conseguimos obter as informações do pedido.',
              buttons: [
                OwButton(
                  labelText: 'Fechar',
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            );
          }
        },
      ));
    }
    loading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    initChat();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: loading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              padding: const EdgeInsets.only(top: 10),
              physics: const BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(height: 25),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 150,
                        height: 150,
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.secondaryContainer,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(40),
                          ),
                        ),
                        child: Center(
                          child: Icon(
                            getIconByOrigin(chat.origin),
                            color: Theme.of(context).colorScheme.onSecondaryContainer,
                            size: 60,
                          ),
                        ),
                      ),
                    ],
                  ),
                  OwText(
                    '$titleChat',
                    style: Theme.of(context).textTheme.headlineMedium,
                    padding: const EdgeInsets.fromLTRB(25, 25, 25, 10),
                    textAlign: TextAlign.center,
                  ),
                  OwText(
                    '${chat.chatDescription}',
                    style: Theme.of(context).textTheme.bodyMedium,
                    padding: const EdgeInsets.fromLTRB(25, 0, 25, 30),
                    textAlign: TextAlign.center,
                  ),
                  Visibility(
                    visible: shortcuts.isNotEmpty,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const OwDivider(),
                        Padding(
                          padding: const EdgeInsets.only(top: 30, bottom: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: shortcuts.map((e) => Padding(
                              child: e,
                              padding: const EdgeInsets.only(right: 5, left: 5),
                            )).toList(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  OwVisibility(
                    visible: true,
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.symmetric(vertical: 30),
                          child: OwDivider(),
                        ),
                        OwText(
                          "Criado por $createdBy em ${DateFormat("dd/MM/yyyy").format(chat.createdDate.toDate().toLocal())}",
                          style: Theme.of(context).textTheme.titleMedium,
                          padding: const EdgeInsets.fromLTRB(25, 0, 25, 5),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 25),
                    child: OwDivider(),
                  ),
                  OwText(
                    'Participantes (${chat.participants.length})',
                    style: Theme.of(context).textTheme.headlineSmall,
                    padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                  ),
                  ListView.builder(
                    itemCount: chat.participants.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Ink(
                        child: InkWell(
                          onTap: () async {
                            if(getRoleByParticipant(chat.participants[index].type) == 'Usuário') {
                              try {
                                OwBotToast.loading();
                                final user = await Api.user.find(id: chat.participants[index].id);
                                OwBotToast.close();
                                context.push(
                                  '/${WhiteLabelEntity.current?.id}/dashboard/users/${user.id}',
                                  extra: user,
                                );
                              } catch (e) {
                                OwBotToast.close();
                                showOwDialog(
                                  context: context,
                                  title: 'Ocorreu um erro',
                                  description: '$e',
                                  buttons: [
                                    OwButton(
                                      labelText: 'Fechar',
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    ),
                                  ],
                                );
                              }
                            } else if (getRoleByParticipant(chat.participants[index].type) == 'Empresa') {
                              try {
                                OwBotToast.loading();
                                final user = await Api.company.find(id: chat.participants[index].id);
                                OwBotToast.close();
                                context.push(
                                  '/${WhiteLabelEntity.current?.id}/dashboard/company/${user.id}',
                                  extra: user,
                                );
                              } catch (e) {
                                OwBotToast.close();
                                showOwDialog(
                                  context: context,
                                  title: 'Ocorreu um erro',
                                  description: '$e',
                                  buttons: [
                                    OwButton(
                                      labelText: 'Fechar',
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    ),
                                  ],
                                );
                              }
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 25,
                              vertical: 20,
                            ),
                            child: Row(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).colorScheme.primary,
                                    shape: BoxShape.circle,
                                  ),
                                  padding: const EdgeInsets.all(12),
                                  child: Center(
                                    child: Icon(
                                      getIconByParticipant(chat.participants[index].type),
                                      color: Theme.of(context).colorScheme.onPrimary,
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 15),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: [
                                      Row(
                                        children: [
                                          Flexible(
                                            child: Text(
                                              '${chat.participants[index].name}',
                                              style: Theme.of(context).textTheme.titleMedium,
                                            ),
                                          ),
                                          Visibility(
                                            visible: chat.participants[index].id == widget.current.id,
                                            child: Padding(
                                              padding: const EdgeInsets.only(left: 8),
                                              child: Container(
                                                padding: const EdgeInsets.symmetric(
                                                  vertical: 1,
                                                  horizontal: 8,
                                                ),
                                                decoration: BoxDecoration(
                                                  color: Theme.of(context).colorScheme.secondaryContainer,
                                                  borderRadius: const BorderRadius.all(
                                                    Radius.circular(6),
                                                  ),
                                                ),
                                                child: Text(
                                                  'Você',
                                                  style: Theme.of(context).textTheme.bodySmall?.copyWith(
                                                    color: Theme.of(context).colorScheme.onSecondaryContainer,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Visibility(
                                            visible: chat.administrators.toString().contains(chat.participants[index].id),
                                            child: Padding(
                                              padding: const EdgeInsets.only(left: 8),
                                              child: Container(
                                                padding: const EdgeInsets.symmetric(
                                                  vertical: 1,
                                                  horizontal: 8,
                                                ),
                                                decoration: BoxDecoration(
                                                  color: Theme.of(context).primaryColor,
                                                  borderRadius: const BorderRadius.all(
                                                    Radius.circular(6),
                                                  ),
                                                ),
                                                child: Text(
                                                  'ADM',
                                                  style: Theme.of(context).textTheme.bodySmall?.copyWith(
                                                    color: Theme.of(context).colorScheme.onPrimary,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 4),
                                      Text(
                                        "Entrou em: ${DateFormat("dd/MM/yyyy").format(chat.participants[index].createdDate.toDate().toLocal())}",
                                        style: Theme.of(context).textTheme.bodyMedium,
                                      ),
                                      const SizedBox(height: 4),
                                      Text(
                                        '${getRoleByParticipant(chat.participants[index].type)}',
                                        style: Theme.of(context).textTheme.bodyMedium,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                  const SizedBox(height: 25),
                  const OwDivider(),
                  Visibility(
                    visible: chat.participants.toString().contains(widget.current.id) && chat.participants.length > 2,
                    child: InkWell(
                      onTap: () async {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            actionsPadding: const EdgeInsets.fromLTRB(16, 10, 16, 10),
                            title: const Text('Sair da conversa?'),
                            content: const Text('Você não terá mais acesso ao histórico de mensagens. Você pode entrar novamente caso seja convidado.'),
                            actions: [
                              TextButton(
                                child: const Text('Cancelar'),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                              TextButton(
                                child: const Text('Sair'),
                                onPressed: () async {
                                  // verificar se é adm
                                  if (chat.administrators.toString().contains(widget.current.id)) {
                                    // entrou por ser adm
                                    // verificar se tem mais algum adm
                                    if (chat.administrators.length > 1) {
                                      // tem outro adm, pode remover
                                      await removerAdm(widget.current);
                                      await removerUsuario(widget.current);
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    } else {
                                      // n tem outro adm, verificar se possui outro usuário
                                      if (chat.participants.length > 1) {
                                        // tem outro usuario, dar adm pra ele e depois remover usuario
                                        final listTemp = chat.participants.where((element) => element.id != widget.current.id).toList();
                                        await atribuirAdmParaOutroUsuario(listTemp[0]);
                                        await removerAdm(widget.current);
                                        await removerUsuario(widget.current);
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      } else {
                                        // n tem outro usuario, remover conversa
                                        await removerUsuario(widget.current);
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      }
                                    }
                                  } else {
                                    removerUsuario(widget.current);
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                  }
                                },
                              ),
                            ],
                          ),
                        );
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 25,
                          vertical: 20,
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: OwText(
                                'Sair da conversa',
                                style: Theme.of(context).textTheme.titleLarge?.copyWith(
                                  color: Theme.of(context).colorScheme.error,
                                ),
                              ),
                            ),
                            const SizedBox(width: 15),
                            Icon(
                              Icons.keyboard_arrow_right_outlined,
                              color: Theme.of(context).colorScheme.error,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  Future<void> removerUsuario(ParticipantChat participant) async {
    try {
      OwBotToast.loading();
      final DocumentSnapshot chatDocument = await db.doc(chat.id).get();
      final Map<String, dynamic> chatMap = chatDocument.data() as Map<String, dynamic>;
      final ChatEntity chatTemp = ChatEntity.fromMap(chatMap);
      chatTemp.participants.removeWhere((element) => element.id == participant.id);
      db.doc(chat.id).update({
        'participants': chatTemp.participants.map((e) => e.toMap()).toList(),
      });
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
    }
  }

  Future<void> removerAdm(ParticipantChat participant) async {
    try {
      final DocumentSnapshot chatDocument = await db.doc(chat.id).get();
      final Map<String, dynamic> chatMap = chatDocument.data() as Map<String, dynamic>;
      final ChatEntity chatTemp = ChatEntity.fromMap(chatMap);
      chatTemp.administrators.removeWhere((element) => element.id == participant.id);
      db.doc(chat.id).update({
        'administrators': chatTemp.administrators.map((e) => e.toMap()).toList(),
      });
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
    }
  }

  Future<void> atribuirAdmParaOutroUsuario(ParticipantChat participant) async {
    try {
      final DocumentSnapshot chatDocument = await db.doc(chat.id).get();
      final Map<String, dynamic> chatMap = chatDocument.data() as Map<String, dynamic>;
      final ChatEntity chatTemp = ChatEntity.fromMap(chatMap);
      chatTemp.administrators.add(participant);
      db.doc(chat.id).update({
        'administrators': chatTemp.administrators.map((e) => e.toMap()).toList(),
      });
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
    }
  }

  String filterAndFormat() {
    if(Valid.text(chat.chatTitle)) {
      return chat.chatTitle!;
    }
    final list = chat.participants.map((e) => e.name).toList();
    final wordToRemove = widget.current.name;
    final List<String> filteredList = list.where((word) => word != wordToRemove).toList();
    if (filteredList.length > 1) {
      String formattedText = filteredList.sublist(0, filteredList.length - 1).join(', ');
      formattedText += ' e ${filteredList.last}';
      return formattedText;
    } else {
      return filteredList.join();
    }
  }

  String participantCount() {
    final count = chat.participants.length;
    final result = "$count ${count == 1 ? 'participante' : 'participantes'} (com você)";
    return result;
  }

  String getRoleByParticipant(origin) {
    switch (origin) {
      case 'store':
      case 'ecommerce':
      case 'company':
        return 'Empresa';
      case 'people':
      case 'user':
        return 'Usuário';
      case 'support':
        return 'Suporte da plataforma';
      default:
        return '';
    }
  }
}
