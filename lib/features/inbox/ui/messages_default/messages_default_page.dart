import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class SuggestionsMessagesChat extends StatefulWidget {
  const SuggestionsMessagesChat({super.key});

  @override
  State<SuggestionsMessagesChat> createState() => _SuggestionsMessagesChatState();
}

class _SuggestionsMessagesChatState extends State<SuggestionsMessagesChat> {

  List<ChatDefaultEntity>? chatDataList;
  Set<String> categories = {};
  Map<String, List<ChatDefaultEntity>> messagesByCategory = {};

  String? selectedCategory;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  void separateCategoriesAndGroupMessages() {
    categories.add('Todos');
    for (var chat in chatDataList ?? []) {
      categories.add(chat.category);
      messagesByCategory.putIfAbsent(chat.category, () => []).add(chat);
    }
    messagesByCategory['Todos'] = List.from(chatDataList ?? []);
    selectedCategory = 'Todos';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Sugestões de mensagens'),
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Visibility(
        visible: chatDataList != null,
        replacement: const Center(
          child: CircularProgressIndicator(),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 60,
              child: ListView(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                scrollDirection: Axis.horizontal,
                children: categories.map((category) => Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: ChoiceChip(
                    label: Text(category),
                    selected: selectedCategory == category,
                    onSelected: (bool selected) {
                      setState(() {
                        selectedCategory = category;
                      });
                    },
                  ),
                )).toList(),
              ),
            ),
            const SizedBox(height: 8),
            Expanded(
              child: ListView.separated(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                itemCount: messagesByCategory[selectedCategory]?.length ?? 0,
                separatorBuilder: (context, index) {
                  return const SizedBox(height: 8);
                },
                itemBuilder: (context, index) {
                  final chat = messagesByCategory[selectedCategory]![index];
                  return Ink(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(15)),
                      color: Theme.of(context).colorScheme.secondaryContainer,
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(15)),
                      onTap: () async {
                        await showOwDialog(
                          context: context,
                          title: 'Opções',
                          description: 'O que você deseja fazer?',
                          buttons: [
                            OwButton.outline(
                              labelText: 'Deletar',
                              onPressed: () async {
                                Navigator.pop(context);
                                try {
                                  OwBotToast.loading();
                                  await Api.chat.deleteMessageDefauilt(id: chat.id);
                                  OwBotToast.close();
                                  chatDataList?.removeWhere((element) => element.id == chat.id);
                                  separateCategoriesAndGroupMessages();
                                  setState(() {});
                                } catch (e) {
                                  OwBotToast.close();
                                  OwBotToast.toast('$e');
                                }
                              },
                            ),
                            OwButton(
                              labelText: 'Enviar mensagem',
                              onPressed: () {
                                Navigator.pop(context);
                                Navigator.pop(context, '${chat.message}');
                              },
                            ),
                          ],
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              '${Bidi.stripHtmlIfNeeded(chat.message)}',
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            const SizedBox(height: 6),
                            Text('${chat.category}'),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: null,
        child: const Icon(Icons.add),
        onPressed: () async {
          final result = await openLinkPage(
            context,
            OpenLinkPageParams.basic(
              child: const InsertMessageDefaultPage(),
            ),
          );
          if (result is ChatDefaultEntity) {
            chatDataList?.add(result);
            separateCategoriesAndGroupMessages();
            setState(() {});
          }
        },
      ),
    );
  }

  Future<void> callApi() async {
    try {
      chatDataList = await Api.chat.listMessagesDefault();
      separateCategoriesAndGroupMessages();
    } catch (e) {
      OwBotToast.toast('$e');
    }
    setState(() {});
  }
}

class InsertMessageDefaultPage extends StatefulWidget {
  const InsertMessageDefaultPage({super.key});

  @override
  State<InsertMessageDefaultPage> createState() => _InsertMessageDefaultPageState();
}

class _InsertMessageDefaultPageState extends State<InsertMessageDefaultPage> {
  TextEditingController category = TextEditingController();
  TextEditingController message = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Inserir mensagem padrão'),
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            OwTextField(
              controller: category,
              labelText: 'Categoria da mensagem',
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 25),
              helperText: 'Ex.: Boas vindas, Dúvidas',
            ),
            const OwDivider(),
            BasicHorizontalCard(
              color: ColorSystem(context: context),
              title: 'Resposta',
              description: Bidi.stripHtmlIfNeeded(message.text),
              onPressed: () async {
                final result = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/html-editor',
                  extra: message.text,
                );
                if (result is String) {
                  message.text = result;
                  setState(() {});
                }
              },
            ),
            const OwDivider(),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: EdgeInsets.fromLTRB(
            25,
            25,
            25,
            MediaQuery.viewInsetsOf(context).bottom + 25,
          ),
          labelText: 'Salvar',
          onPressed: () async {
            if (category.text.trim().isEmpty || message.text.trim().isEmpty) {
              showOwDialog(
                context: context,
                title: 'Ops, calma lá',
                description: 'Existem um ou mais campos que precisam ser preenchidos.',
              );
            } else {
              try {
                final response = await Api.chat.insertMessageDefault(params: {
                  'category': category.text,
                  'message': message.text,
                });
                if (mounted) {
                  Navigator.pop(context, response);
                  showOwDialog(
                    context: context,
                    title: 'Cadastrado com sucesso',
                  );
                }
              } catch (e) {
                if (mounted) {
                  showOwDialog(
                    context: context,
                    title: 'Ops',
                    description: '$e',
                  );
                }
              }
            }
          },
        ),
      ),
    );
  }
}
