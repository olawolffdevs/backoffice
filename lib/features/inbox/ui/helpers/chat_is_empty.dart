import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ChatIsEmpty extends StatelessWidget {
  const ChatIsEmpty({
    Key? key,
    required this.bottom,
  }) : super(key: key);
  final double bottom;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(25, 0, 25, bottom),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image.asset(
              'assets/images/chat.png',
              height: 250,
            ),
            const SizedBox(height: 25),
            const OwText(
              'Poxa, nenhuma conversa ainda',
              style: TextStyle(fontWeight: FontWeight.w800, fontSize: 20),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 15),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 35),
              child: OwText(
                'Quando você tiver uma conversa privada ou em grupo, você as verá aqui. Você também pode iniciar uma conversa privada ou conversar com nosso suporte clicando nos botões localizado no topo da tela.',
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 25)
          ],
        ),
      ),
    );
  }
}
