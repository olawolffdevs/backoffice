import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ChatNotSelected extends StatelessWidget {
  const ChatNotSelected({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.background,
      child: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Image.asset(
                'assets/images/chat.png',
                height: 250,
              ),
              const SizedBox(height: 30),
              const OwText(
                'Nenhuma mensagem selecionada',
                style: TextStyle(fontWeight: FontWeight.w800, fontSize: 20),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 15),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 35),
                child: OwText(
                  'Selecione uma mensagem ao lado.',
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(height: 10)
            ],
          ),
        ),
      ),
    );
  }
}