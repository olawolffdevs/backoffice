import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:http_parser/http_parser.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

enum MediaTypeCategory { audio, video, image, document, text, any }

class UploadFilePage extends StatefulWidget {

  const UploadFilePage({Key? key, this.isPublic = false, this.mediaTypeCategory}) : super(key: key);
  final bool isPublic;
  final MediaTypeCategory? mediaTypeCategory;

  @override
  _UploadFilePageState createState() => _UploadFilePageState();
}

class _UploadFilePageState extends State<UploadFilePage> {

  double? width;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      width = 250;
      animated(true);
    });
  }

  void animated([t]) {
    try {
      setState(() {
        if (width == 250) {
          width = 180;
        } else {
          width = 250;
        }
      });
      Future.delayed(
          t ? const Duration(milliseconds: 500) : const Duration(seconds: 2),
          () {
        animated(false);
      });
    } catch (e) {}
  }

  static const fileUploadLimits = {
    'image/jpeg': 5 * 1024 * 1024,
    'image/jpg': 5 * 1024 * 1024,
    'image/png': 5 * 1024 * 1024,
    'application/pdf': 5 * 1024 * 1024,
    'video/mp4': 9 * 1024 * 1024,
    'audio/mpeg': 5 * 1024 * 1024,
    'audio/wav': 9 * 1024 * 1024,
    'audio/mp3': 5 * 1024 * 1024,
    'text/plain': 1 * 1024 * 1024,
    'application/msword': 5 * 1024 * 1024,
    'application/vnd.ms-excel': 5 * 1024 * 1024,
    'application/zip': 9 * 1024 * 1024,
  };

  final Map<String, String> extensionToMimeType = {
    'jpeg': 'image/jpeg',
    'jpg': 'image/jpeg',
    'png': 'image/png',
    'pdf': 'application/pdf',
    'mp4': 'video/mp4',
    'mpeg': 'audio/mpeg',
    'wav': 'audio/wav',
    'mp3': 'audio/mp3',
    'txt': 'text/plain',
    'doc': 'application/msword',
    'xls': 'application/vnd.ms-excel',
    'zip': 'application/zip',
  };

  static const Map<MediaTypeCategory, List<String>> mediaTypeExtensions = {
    MediaTypeCategory.audio: ['mp3', 'mpeg', 'wav'],
    MediaTypeCategory.video: ['mp4'],
    MediaTypeCategory.image: ['jpeg', 'jpg', 'png'],
    MediaTypeCategory.document: ['pdf', 'doc', 'xls', 'txt'],
    MediaTypeCategory.text: ['txt'],
    MediaTypeCategory.any: [],
  };

  static const Map<MediaTypeCategory, String> mediaTypeDescriptions = {
    MediaTypeCategory.audio: 'Arquivos de áudio',
    MediaTypeCategory.video: 'Arquivos de vídeo',
    MediaTypeCategory.image: 'Imagens',
    MediaTypeCategory.document: 'Documentos',
    MediaTypeCategory.text: 'Textos',
    MediaTypeCategory.any: 'Todos os tipos de arquivo',
  };

  MediaType _getMediaType(String? extension) {
    final String? mimeType = extensionToMimeType[extension?.toLowerCase()];

    if (mimeType != null) {
      final parts = mimeType.split('/');
      return MediaType(parts[0], parts[1]);
    } else {
      // Retorne um tipo padrão ou lance uma exceção se o tipo de arquivo não for suportado
      return MediaType('application', 'octet-stream');
    }
  }

  Future<void> selectAndUploadFile({
    required String companyId,
  }) async {
    final List<String>? extensions = widget.mediaTypeCategory != null ? mediaTypeExtensions[widget.mediaTypeCategory] : null;
    final FileType fileType = extensions == null || extensions.isEmpty ? FileType.any : FileType.custom;

    final FilePickerResult? pickedFile = await FilePicker.platform.pickFiles(
      type: fileType,
      allowedExtensions: extensions,
      withData: true,
    );

    if (pickedFile != null) {
      final Uint8List? fileBytes = pickedFile.files.single.bytes;
      final String filename = pickedFile.files.single.name;
      final String? fileExtension = pickedFile.files.single.extension;

      if (fileBytes != null) {
        final MediaType contentType = _getMediaType(fileExtension);
        final String mimeType = '${contentType.type}/${contentType.subtype}';

        if (fileUploadLimits.containsKey(mimeType) && fileBytes.length <= fileUploadLimits[mimeType]!) {
          try {
            OwBotToast.loading();
            final fileUploaded = await Api.upload.upload(
              fileBytes,
              contentType: contentType,
              filename: filename,
              isPublic: widget.isPublic,
            );
            OwBotToast.close();
            Navigator.pop(context, fileUploaded.url);
          } catch (e) {
            OwBotToast.close();
            showOwDialog(
              context: context,
              title: 'Ocorreu um erro ao enviar arquivo',
              description: '$e',
              buttons: [
                OwButton(
                  labelText: 'Fechar',
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            );
          }
        } else {
          OwBotToast.toast('Limite de tamanho ou tipo de arquivo não suportado');
        }
      }
    } else {
      OwBotToast.toast('Nenhum arquivo selecionado');
    }
  }

  @override
  Widget build(BuildContext context) {
    String displayText = 'Selecionar e enviar arquivo';
    if (widget.mediaTypeCategory != null) {
      displayText = '${mediaTypeDescriptions[widget.mediaTypeCategory]} de até ${_getMaxFileSizeDisplayText(widget.mediaTypeCategory)}';
    }

    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(EvaIcons.close),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AnimatedContainer(
                    width: width,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Theme.of(context).colorScheme.primaryContainer.withOpacity(.2),
                    ),
                    curve: Curves.linear,
                    duration: const Duration(seconds: 2),
                    child: Center(
                      child: Container(
                        width: 250,
                        height: 250,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 5,
                            color: Theme.of(context).colorScheme.primaryContainer,
                          ),
                          shape: BoxShape.circle,
                        ),
                        padding: const EdgeInsets.all(35),
                        child: const Icon(
                          EvaIcons.cloudUploadOutline,
                          size: 100,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 25),
                  Text(
                    displayText,
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 6),
                  Text(
                    '${filterAndFormat(mediaTypeExtensions[widget.mediaTypeCategory]!)}',
                    style: Theme.of(context).textTheme.bodyMedium,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 12),
                  OwButton(
                    onPressed: () => selectAndUploadFile(
                      companyId: '${WhiteLabelEntity.current?.setting.companyId}',
                    ),
                    labelText: 'Escolher arquivo',
                    leading: EvaIcons.uploadOutline,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String filterAndFormat(List<String> list) {
    // Filter the list to remove the specified word
    final List<String> filteredList = list;
    
    // Check if the filtered list contains more than one word to format correctly
    if (filteredList.length > 1) {
      // Get all words except the last, and join them with a comma
      String formattedText = filteredList.sublist(0, filteredList.length - 1).join(', ');
      // Add " and " and the last word
      formattedText += ' e ${filteredList.last}';
      return formattedText;
    } else {
      // If the filtered list contains only one word, return it directly
      return filteredList.join();
    }
  }

  String _getMaxFileSizeDisplayText(MediaTypeCategory? category) {
    if (category == null) return 'tamanho variável';

    int maxSize = 0;
    mediaTypeExtensions[category]?.forEach((ext) {
      final String mimeType = _getMimeType(ext);
      final int? limit = fileUploadLimits[mimeType];
      if (limit != null && limit > maxSize) {
        maxSize = limit;
      }
    });

    return '${(maxSize / (1024 * 1024)).toStringAsFixed(0)}MB';
  }

  String _getMimeType(String ext) {
    return extensionToMimeType[ext.toLowerCase()] ?? 'application/octet-stream';
  }
}