import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdfx/pdfx.dart';
import 'package:internet_file/internet_file.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ViewPdf extends StatefulWidget {
  const ViewPdf({Key? key, required this.url}) : super(key: key);
  final String url;

  @override
  _ViewPdfState createState() => _ViewPdfState();
}

class _ViewPdfState extends State<ViewPdf> {
  late PdfControllerPinch pdfController;

  @override
  void initState() {
    super.initState();
    pdfController = PdfControllerPinch(
      document: PdfDocument.openData(InternetFile.get(widget.url)),
    );
  }

  @override
  void dispose() {
    pdfController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Visualizar PDF'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () => Navigator.pop(context),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Clipboard.setData(ClipboardData(
                text: '${widget.url}',
              )).then((value) {
                OwBotToast.toast('Link do documento copiado.');
              });
            },
            icon: const Icon(EvaIcons.copyOutline),
          ),
        ],
      ),
      body: PdfViewPinch(
        controller: pdfController,
        onDocumentLoaded: (document) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text('Páginas totais: ${document.pagesCount}')),
          );
        },
        onPageChanged: (page) {},
      ),
    );
  }
}