import 'dart:math';

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ViewImage extends StatefulWidget {
  const ViewImage({super.key, required this.url});
  final String url;

  @override
  State<ViewImage> createState() => _ViewImageState();
}

class _ViewImageState extends State<ViewImage> {

  double rotationAngle = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Imagem'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: InteractiveViewer(
              child: RotatedBox(
                quarterTurns: (rotationAngle / (pi / 2)).round(),
                child: Image.network(widget.url),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    rotationAngle -= pi / 2;
                  });
                },
                child: const Text('Girar Esquerda'),
              ),
              const SizedBox(width: 20),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    rotationAngle += pi / 2;
                  });
                },
                child: const Text('Girar Direita'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}