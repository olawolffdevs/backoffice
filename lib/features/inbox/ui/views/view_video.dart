import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:video_player/video_player.dart';

class ViewVideo extends StatefulWidget {
  const ViewVideo({
    Key? key,
    required this.url,
  }) : super(key: key);
  final String url;

  @override
  State<ViewVideo> createState() => _ViewVideoState();
}

class _ViewVideoState extends State<ViewVideo> {

  late VideoPlayerController _videoPlayerController1;
  ChewieController? _chewieController;

  @override
  void initState() {
    super.initState();
    initializePlayer();
  }

  @override
  void dispose() {
    try {
      _videoPlayerController1.dispose();
      _chewieController?.dispose();
    } catch (e) {
      // e
    }
    super.dispose();
  }

  Future<void> initializePlayer() async {
    try {
      _videoPlayerController1 = VideoPlayerController.networkUrl(Uri.parse(widget.url));
      await _videoPlayerController1.initialize();
    } catch (e) {
      // e
    }
    _createChewieController();
    setState(() {});
  }

  void _createChewieController() {
    try {
      _chewieController = ChewieController(
        videoPlayerController: _videoPlayerController1,
        autoPlay: false,
        looping: false,
      );
    } catch (e) {
      // e
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(),
      body: _video(),
    );
  }

  bool isValidUrl(String url) {
    try {
      Uri.parse(url);
      return true;
    } catch (e) {
      return false;
    }
  }

  Widget _video() {
    if (!Valid.text(widget.url)) {
      return const SizedBox();
    }
    return Padding(
      padding: const EdgeInsets.all(25),
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: Container(
          constraints: const BoxConstraints(
            maxWidth: 600,
            maxHeight: 1000,
            minHeight: 230,
          ),
          height: MediaQuery.sizeOf(context).height * 0.7,
          child: _chewieController != null && _chewieController!.videoPlayerController.value.isInitialized
            ? ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                child: Chewie(
                  controller: _chewieController!,
                ),
              )
            : const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(height: 20),
                  OwText('Carregando'),
                ],
              ),
        ),
      ),
    );
  }
}