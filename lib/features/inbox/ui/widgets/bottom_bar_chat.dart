// import 'package:backoffice/features/inbox/insert_item_modal.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import 'package:backoffice/features/inbox/ui/widgets/item_option_chat.dart';

import '../../data/chat_params.dart';
import '../../link_page.dart';
import '../messages_default/messages_default_page.dart';
import '../upload/upload_chat_file.dart';
import 'modal/insert_item_modal.dart';

class BottomBarChat extends StatefulWidget {
  const BottomBarChat({
    Key? key,
    required this.params,
    required this.chat,
    required this.scrollController,
  }) : super(key: key);
  final ChatParams params;
  final ChatEntity chat;
  final ScrollController scrollController;

  @override
  State<BottomBarChat> createState() => _BottomBarChatState();
}

class _BottomBarChatState extends State<BottomBarChat> {
  final _msg = TextEditingController();
  final FocusNode myFocusNode = FocusNode();

  bool showMic = true;
  bool showOptions = false;
  bool emoji = false;

  @override
  void initState() {
    super.initState();
    _msg.addListener(() {
      if (Valid.text(_msg.text)) {
        if (showMic) {
          setState(() {
            showMic = false;
          });
        }
      } else {
        if (!showMic) {
          setState(() {
            showMic = true;
          });
        }
      }
    });
    myFocusNode.addListener(() {
      if (myFocusNode.hasFocus) {
        if(emoji || showOptions) {
          setState(() {
            emoji = false;
            showOptions = false;
          });
        }
      }
    });
  }

  @override
  void dispose() {
    _msg.dispose();
    myFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 4,
            vertical: 8,
          ),
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.surfaceVariant,
          ),
          child: Row(
            children: [
              IconButton(
                icon: showOptions
                  ? const Icon(Icons.keyboard_alt_outlined)
                  : const Icon(EvaIcons.plusCircleOutline),
                onPressed: () {
                  setState(() {
                    emoji = false;
                    showOptions = !showOptions;
                  });
                },
              ),
              IconButton(
                icon: const Icon(EvaIcons.smilingFaceOutline),
                onPressed: () {
                  setState(() {
                    showOptions = false;
                    emoji = !emoji;
                  });
                },
              ),
              Expanded(
                child: TextField(
                  controller: _msg,
                  focusNode: myFocusNode,
                  minLines: 1,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Theme.of(context).colorScheme.onPrimary,
                    hintStyle: Theme.of(context).textTheme.bodyLarge,
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 1,
                    ),
                    focusedBorder: _border(),
                    enabledBorder: _border(),
                    hintText: 'Digitar mensagem...',
                    counterText: '',
                  ),
                  style: Theme.of(context).textTheme.bodyLarge,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  maxLines: 5,
                  maxLength: 800,
                  onTap: () {},
                ),
              ),
              Visibility(
                visible: false,
                child: IconButton(
                  icon: const Icon(EvaIcons.micOutline),
                  onPressed: () {
                    // sendMessage('audio', 'https://static.wixstatic.com/mp3/082347_afab6bb4ce8b4b0caaac69fce37885e1.wav');
                  },
                ),
                replacement: IconButton(
                  icon: const Icon(EvaIcons.paperPlaneOutline),
                  onPressed: () {
                    if(Valid.text(_msg.text)) {
                      sendMessage('text', _msg.text);
                    }
                  },
                ),
              ),
            ],
          ),
        ),
        _options(),
        _emoji(),
        SizedBox(height: MediaQuery.viewInsetsOf(context).bottom),
      ],
    );
  }

  InputBorder? _border() {
    return OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(24)),
      borderSide: BorderSide(
        color: Theme.of(context).colorScheme.surface,
        width: 1,
      ),
    );
  }

  Widget _options() {
    return AnimatedCrossFade(
      firstCurve: Curves.bounceIn,
      crossFadeState: showOptions ? CrossFadeState.showSecond : CrossFadeState.showFirst,
      duration: const Duration(milliseconds: 600),
      firstChild: const SizedBox(),
      secondChild: Container(
        color: Theme.of(context).colorScheme.surfaceVariant,
        padding: const EdgeInsets.all(24),
        child: GridView(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: MediaQuery.of(context).size.width > 650 ? 6 : 3,
            crossAxisSpacing: 24,
            mainAxisSpacing: 24,
          ),
          children: [
            ItemOptionChat(
              title: 'Mensagens',
              icon: EvaIcons.saveOutline,
              onTap: () async {
                final result = await openLinkPage(
                  context,
                  OpenLinkPageParams.basic(
                    child: const SuggestionsMessagesChat(),
                  ),
                );
                if (result is String) {
                  sendMessage('text', result);
                }
              },
            ),
            ItemOptionChat(
              title: 'Avançado',
              icon: EvaIcons.colorPaletteOutline,
              onTap: () async {
                final result = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/html-editor',
                  extra: _msg.text,
                );
                if (result is String) {
                  sendMessage('text', result);
                }
              },
            ),
            ItemOptionChat(
              title: 'Link',
              icon: EvaIcons.link2Outline,
              onTap: () async {
                final result = await openModalPage(
                  context,
                  const LinkScreen(),
                );
                if(result is Map<String, dynamic>) {
                  sendMessage(
                    result['type'],
                    result['value'],
                    extra: result['extra'],
                  );
                }
              },
            ),
            ItemOptionChat(
              title: 'Documento',
              icon: EvaIcons.fileTextOutline,
              onTap: () async {
                await sendMessageFunction(MediaTypeCategory.document);
              },
            ),
            ItemOptionChat(
              title: 'Vídeo',
              icon: EvaIcons.filmOutline,
              onTap: () async {
                await sendMessageFunction(MediaTypeCategory.video);
              },
            ),
            ItemOptionChat(
              title: 'Imagem',
              icon: EvaIcons.imageOutline,
              onTap: () async {
                await sendMessageFunction(MediaTypeCategory.image);
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> sendMessageFunction(MediaTypeCategory mediaTypeCategory) async {
    final result = await openModalPage(
      context,
      InsertItemModal(mediaTypeCategory: mediaTypeCategory),
    );
    if(result is Map<String, dynamic>) {
      sendMessage(
        result['type'],
        result['value'],
        extra: result['extra'],
      );
    }
  }

  Widget _emoji() {
    return AnimatedCrossFade(
      firstCurve: Curves.bounceIn,
      crossFadeState: emoji ? CrossFadeState.showSecond : CrossFadeState.showFirst,
      duration: const Duration(milliseconds: 600),
      firstChild: const SizedBox(),
      secondChild: Container(
        color: Theme.of(context).colorScheme.surfaceVariant,
        height: MediaQuery.sizeOf(context).height * 0.4,
        constraints: const BoxConstraints(minHeight: 190, maxHeight: 320),
        child: EmojiPicker(
          textEditingController: _msg,
          config: Config(
            searchViewConfig: SearchViewConfig(
              backgroundColor: Theme.of(context).colorScheme.background,
              buttonIconColor: Theme.of(context).colorScheme.primary,
            ),
            skinToneConfig: SkinToneConfig(
              dialogBackgroundColor: Theme.of(context).colorScheme.background,
              indicatorColor: Theme.of(context).colorScheme.onBackground,
              enabled: true,
            ),
            bottomActionBarConfig: BottomActionBarConfig(
              backgroundColor: Theme.of(context).colorScheme.background,
              buttonIconColor: Theme.of(context).colorScheme.primary,
            ),
            categoryViewConfig: CategoryViewConfig(
              backgroundColor: Theme.of(context).colorScheme.background,
              initCategory: Category.RECENT,
              indicatorColor: Theme.of(context).colorScheme.primary,
              iconColor: Theme.of(context).colorScheme.onBackground,
              iconColorSelected: Theme.of(context).colorScheme.primary,
              backspaceColor: Theme.of(context).colorScheme.primary,
              tabIndicatorAnimDuration: kTabScrollDuration,
              categoryIcons: const CategoryIcons(),
              recentTabBehavior: RecentTabBehavior.RECENT,
            ),
            emojiViewConfig: EmojiViewConfig(
              backgroundColor: Theme.of(context).colorScheme.background,
              columns: 7,
              emojiSizeMax: 28 *
                  (foundation.defaultTargetPlatform == TargetPlatform.iOS
                      ? 1.20
                      : 1.0),
              verticalSpacing: 0,
              horizontalSpacing: 0,
              gridPadding: EdgeInsets.zero,
              recentsLimit: 28,
              replaceEmojiOnLimitExceed: false,
              noRecents: OwText(
                'Nenhum emoji recente',
                style: Theme.of(context).textTheme.titleLarge,
                textAlign: TextAlign.center,
              ),
              loadingIndicator: const SizedBox.shrink(),
              buttonMode: ButtonMode.MATERIAL,
            ),
            checkPlatformCompatibility: true,
          ),
        ),
      ),
    );
  }

  Future<void> sendMessage(String type, String value, {String? extra}) async {
    final db = FirebaseFirestore.instance.collection('chat-dm');
    final timestamp = Timestamp.fromDate(DateTime.now());

    // create local msg
    final msg = MessageChatEntity(
      createdDate: timestamp,
      updatedDate: timestamp,
      readBy: [widget.params.current],
      type: type,
      value: value,
      reactions: [],
      owner: widget.params.current,
      extra: extra,
    );

    final newMsg = msg.toMap()..addAll({'servedBy': ''});

    // update chat with last message
    db.doc(widget.params.chatId).update({
      'lastMessage': newMsg,
      'updatedDate': timestamp,
    });

    // create message in chat
    db.doc(widget.params.chatId).collection('messages').doc().set(newMsg);

    // clear textfield and reset config
    _msg.clear();
    emoji = false;
    showOptions = false;
    showMic = true;

    // scrool to bottom
    widget.scrollController.animateTo(
      widget.scrollController.position.maxScrollExtent + 300,
      duration: const Duration(seconds: 1),
      curve: Curves.fastOutSlowIn,
    );
  }
}