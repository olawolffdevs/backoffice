import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import '../../../orders/order_detail_page.dart';
import '../../data/chat_params.dart';
import '../chat/details_chat_page.dart';

class AppBarChat extends StatelessWidget implements PreferredSizeWidget {
  const AppBarChat({
    Key? key,
    this.preferredSize = const Size.fromHeight(kToolbarHeight),
    required this.automaticallyImplyLeading,
    required this.params,
    required this.chat,
  }) : super(key: key);

  @override
  final Size preferredSize;
  final bool automaticallyImplyLeading;
  
  final ChatParams params;
  final ChatEntity chat;  

  @override
  Widget build(BuildContext context) {
    return OwAppBar(
      automaticallyImplyLeading: automaticallyImplyLeading,
      titleSpacing: automaticallyImplyLeading ? 8 : 24,
      centerTitle: false,
      title: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            filterAndFormat(),
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            participantCount(),
            style: Theme.of(context).textTheme.bodyMedium,
          ),
        ],
      ),
      actions: [
        Visibility(
          visible: Valid.text('${chat.additional?.orderId}'),
          child: IconButton(
            tooltip: 'Detalhes do pedido',
            icon: const Icon(Icons.list_alt_outlined),
            onPressed: () async {
              try {
                OwBotToast.loading();
                final result = await Api.ecommerce.orders.find(
                  id: '${chat.additional?.orderId}',
                );
                OwBotToast.close();
                await openLinkPage(
                  context,
                  OpenLinkPageParams.basic(
                    child: OrderDetailPage(
                      result,
                      () {},
                    ),
                  ),
                );
              } catch (e) {
                OwBotToast.close();
                showOwDialog(
                  context: context,
                  title: 'Ocorreu um erro.',
                  description: 'Não conseguimos obter as informações do pedido.',
                  buttons: [
                    OwButton(
                      labelText: 'Fechar',
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                );
              }
            },
          ),
        ),
        IconButton(
          icon: const Icon(EvaIcons.infoOutline),
          onPressed: () async {
            await openLinkPage(
              context,
              OpenLinkPageParams.basic(
                child: DetailsChatPage(
                  chat: chat,
                  current: params.current,
                ),
              ),
            );
          },
        ),
      ]
    );
  }

  String filterAndFormat() {
    if(Valid.text(chat.chatTitle)) {
      return chat.chatTitle!;
    }
    final list = chat.participants.map((e) => e.name).toList();
    final wordToRemove = params.current.name;

    // Filter the list to remove the specified word
    final List<String> filteredList = list.where((word) => word != wordToRemove).toList();
    
    // Check if the filtered list contains more than one word to format correctly
    if (filteredList.length > 1) {
      // Get all words except the last, and join them with a comma
      String formattedText = filteredList.sublist(0, filteredList.length - 1).join(', ');
      // Add " and " and the last word
      formattedText += ' e ${filteredList.last}';
      return formattedText;
    } else {
      // If the filtered list contains only one word, return it directly
      return filteredList.join();
    }
  }

  String participantCount() {
    final count = chat.participants.length;
    final result = "$count ${count == 1 ? 'participante' : 'participantes'} (com você)";
    return result;
  }
}
