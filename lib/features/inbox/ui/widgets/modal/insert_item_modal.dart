import 'package:backoffice/features/inbox/ui/upload/upload_chat_file.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';


class InsertItemModal extends StatefulWidget {
  const InsertItemModal({super.key, this.mediaTypeCategory});
  final MediaTypeCategory? mediaTypeCategory;
  @override
  _InsertItemModalState createState() => _InsertItemModalState();
}

class _InsertItemModalState extends State<InsertItemModal> {
  TextEditingController name = new TextEditingController();
  TextEditingController value = new TextEditingController();

  void dispose() {
    name.dispose();
    value.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          borderRadius: appSM.isWeb
            ? const BorderRadius.all(Radius.circular(15))
            : const BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
              ),
        ),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Center(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.primaryContainer,
                          borderRadius: const BorderRadius.all(Radius.circular(16)),
                        ),
                        height: 5,
                        width: 30,
                      ),
                    ),
                    const SizedBox(height: 15),
                    OwTextField(
                      autofocus: false,
                      controller: name,
                      keyboardType: TextInputType.visiblePassword,
                      textInputAction: TextInputAction.done,
                      hintText: 'Escreva aqui...',
                      helperText: 'Aparece junto com o arquivo',
                      labelText: 'Título ${getType(widget.mediaTypeCategory).$2} (Opcional)',
                      maxLength: 32,
                      textCapitalization: TextCapitalization.none,
                    ),
                    const SizedBox(height: 25),
                    IntrinsicHeight(
                      child: Container(
                        child: Row(
                          children: [
                            Expanded(
                              child: OwTextField(
                                autofocus: false,
                                controller: value,
                                keyboardType: TextInputType.visiblePassword,
                                textInputAction: TextInputAction.done,
                                labelText: 'Link ${getType(widget.mediaTypeCategory).$2}',
                                hintText: 'Cole o link ou selecione o arquivo',
                                textCapitalization: TextCapitalization.none,
                              ),
                            ),
                            const SizedBox(width: 16),
                            IconButton(
                              onPressed: () async {
                                final result = await openModalPage(
                                  context,
                                  UploadFilePage(
                                    isPublic: true,
                                    mediaTypeCategory: widget.mediaTypeCategory,
                                  ),
                                );
                                if(result is String) {
                                  value.text = result;
                                  setState(() {});
                                }
                              },
                              icon: const Icon(EvaIcons.uploadOutline),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 25),
                    OwButton(
                      onPressed: () {
                        autenticar();
                      },
                      labelText: 'Enviar mensagem',
                      leading: EvaIcons.paperPlaneOutline,
                      height: 55,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20 + BS.bottomPadding(context)),
            ],
          ),
        ),
      ),
    );
  }

  void autenticar() async {
    if(Valid.text(value.text)) {
      Navigator.pop(context, {
        'type': getType(widget.mediaTypeCategory).$1,
        'value': value.text,
        'extra': name.text,
      });
    }
  }

  (String, String) getType(MediaTypeCategory? media) {
    switch (media) {
      case MediaTypeCategory.audio:
        return ('audio', 'do áudio');
      case MediaTypeCategory.document:
        return ('document', 'do documento');
      case MediaTypeCategory.image:
        return ('image', 'da imagem');
      case MediaTypeCategory.text:
        return ('text', 'do texto');
      case MediaTypeCategory.video:
        return ('video', 'do vídeo');
      default:
        return ('text', 'do texto');
    }
  }
}