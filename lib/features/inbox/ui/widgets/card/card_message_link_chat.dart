import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:url_launcher/url_launcher_string.dart';

class CardMessageLinkChat extends StatelessWidget {
  const CardMessageLinkChat({
    Key? key,
    required this.url,
    this.extra,
    required this.send,
    required this.createdDate,
  }) : super(key: key);
  final String url;
  final String? extra;
  final Timestamp createdDate;
  final bool send;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 12),
          child: Row(
            mainAxisAlignment: send ? MainAxisAlignment.start : MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                constraints: BoxConstraints(
                  minWidth: 10,
                  maxWidth: MediaQuery.sizeOf(context).width * 0.65,
                ),
                padding: const EdgeInsets.all(12),
                decoration: BoxDecoration(
                  color: send
                    ? Theme.of(context).colorScheme.tertiaryContainer
                    : Theme.of(context).colorScheme.secondaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                ),
                child: Column(
                  crossAxisAlignment: send
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.end,
                  children: [
                    OwButton(
                      labelText: extra ?? 'Acessar link',
                      onPressed: () async {
                        final urlValid = await canLaunchUrlString(url);
                        if(urlValid) {
                          launchUrlString(
                            url,
                            mode: LaunchMode.externalApplication,
                          );
                        } else if (url.startsWith('/')) {
                          context.push(url);
                        } else {
                          showOwDialog(
                            context: context,
                            title: 'Link inválido',
                            buttons: [
                              OwButton(
                                labelText: 'Fechar',
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ]
                          );
                        }
                      },
                    ),
                    const SizedBox(height: 5),
                    OwText(
                      DateFormat('HH:mm').format(DateTime.fromMillisecondsSinceEpoch(
                        createdDate.millisecondsSinceEpoch,
                      ).toLocal()),
                      style: Theme.of(context).textTheme.bodySmall?.copyWith(
                        color: send
                          ? Theme.of(context).colorScheme.onTertiaryContainer
                          : Theme.of(context).colorScheme.onSecondaryContainer,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
