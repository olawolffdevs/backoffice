import 'package:backoffice/features/inbox/ui/views/view_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class CardMessageImageChat extends StatelessWidget {
  const CardMessageImageChat({
    Key? key,
    required this.url,
    this.extra,
    required this.send,
    required this.createdDate,
  }) : super(key: key);
  final String url;
  final String? extra;
  final Timestamp createdDate;
  final bool send;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 12),
          child: Row(
            mainAxisAlignment: send ? MainAxisAlignment.start : MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                constraints: BoxConstraints(
                  minWidth: 10,
                  maxWidth: MediaQuery.sizeOf(context).width * 0.65,
                ),
                padding: const EdgeInsets.all(12),
                decoration: BoxDecoration(
                  color: send
                    ? Theme.of(context).colorScheme.tertiaryContainer
                    : Theme.of(context).colorScheme.secondaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                ),
                child: Column(
                  crossAxisAlignment: send
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.end,
                  children: [
                    Visibility(
                      visible: Valid.text(extra),
                      child: OwText(
                        '$extra',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          color: send
                            ? Theme.of(context).colorScheme.onTertiaryContainer
                            : Theme.of(context).colorScheme.onSecondaryContainer,
                        ),
                        padding: const EdgeInsets.only(bottom: 8),
                      ),
                    ),
                    Material(
                      type: MaterialType.transparency,
                      color: Colors.transparent,
                      child: Ink(
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.surfaceVariant,
                          borderRadius: const BorderRadius.all(Radius.circular(16)),
                          border: Border.all(
                            width: 1,
                            color: send
                              ? Theme.of(context).colorScheme.onTertiaryContainer
                              : Theme.of(context).colorScheme.onSecondaryContainer,
                          ),
                        ),
                        child: InkWell(
                          borderRadius: const BorderRadius.all(Radius.circular(16)),
                          onTap: () {
                            openLinkPage(
                              context,
                              OpenLinkPageParams.basic(
                                child: ViewImage(
                                  url: url,
                                ),
                                dynamicResponsivity: false,
                              ),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(6),
                            child: ClipRRect(
                              borderRadius: const BorderRadius.all(Radius.circular(16)),
                              child: Image.network(
                                url,
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 12),
                    OwText(
                      DateFormat('HH:mm').format(DateTime.fromMillisecondsSinceEpoch(
                        createdDate.millisecondsSinceEpoch,
                      ).toLocal()),
                      style: Theme.of(context).textTheme.bodySmall?.copyWith(
                        color: send
                          ? Theme.of(context).colorScheme.onTertiaryContainer
                          : Theme.of(context).colorScheme.onSecondaryContainer,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
