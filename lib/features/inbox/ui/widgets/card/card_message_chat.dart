import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import '../../../data/chat_params.dart';
import 'card_message_audio_chat.dart';
import 'card_message_image_chat.dart';
import 'card_message_link_chat.dart';
import 'card_message_pdf_chat.dart';
import 'card_message_text_chat.dart';
import 'card_message_video_chat.dart';

class CardMessageChat extends StatelessWidget {
  const CardMessageChat({
    Key? key,
    required this.message,
    this.extra,
    required this.params,
  }) : super(key: key);
  final MessageChatEntity message;
  final String? extra;
  final ChatParams params;

  @override
  Widget build(BuildContext context) {
    if(message.type == 'text') {
      return CardMessageTextChat(
        message: message.value,
        send: message.owner?.id != '${params.current.id}',
        createdDate: message.createdDate,
      );
    } else if (message.type == 'audio') {
      return CardMessageAudioChat(
        url: message.value,
        extra: extra,
        send: message.owner?.id != '${params.current.id}',
        createdDate: message.createdDate,
      );
    } else if (message.type == 'pdf') {
      return CardMessagePdfChat(
        url: message.value,
        extra: extra,
        send: message.owner?.id != '${params.current.id}',
        createdDate: message.createdDate,
      );
    } else if (message.type == 'document') {
      return CardMessagePdfChat(
        url: message.value,
        extra: extra,
        send: message.owner?.id != '${params.current.id}',
        createdDate: message.createdDate,
      );
    } else if (message.type == 'image') {
      return CardMessageImageChat(
        url: message.value,
        extra: extra,
        send: message.owner?.id != '${params.current.id}',
        createdDate: message.createdDate,
      );
    } else if (message.type == 'video') {
      return CardMessageVideoChat(
        url: message.value,
        extra: extra,
        send: message.owner?.id != '${params.current.id}',
        createdDate: message.createdDate,
      );
    } else if (message.type == 'link') {
      return CardMessageLinkChat(
        url: message.value,
        extra: extra,
        send: message.owner?.id != '${params.current.id}',
        createdDate: message.createdDate,
      );
    }
    return const ListTile(
      title: Text('Mensagem não suportada'),
      subtitle: Text('Pode ser necessário uma atualização'),
    );
  }
}
