import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:voice_message_package/voice_message_package.dart';

class CardMessageAudioChat extends StatelessWidget {
  const CardMessageAudioChat({
    Key? key,
    required this.url,
    this.extra,
    required this.send,
    required this.createdDate,
  }) : super(key: key);
  final String url;
  final String? extra;
  final Timestamp createdDate;
  final bool send;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        LayoutBuilder(
          builder: (context, constraints) {
            return MediaQuery(
              data: MediaQueryData(
                padding: MediaQuery.of(context).viewPadding,
                size: Size(
                  constraints.maxWidth,
                  constraints.maxHeight,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12, 0, 12, 12),
                child: Row(
                  mainAxisAlignment: send ? MainAxisAlignment.start : MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      constraints: BoxConstraints(
                        minWidth: 10,
                        maxWidth: MediaQuery.sizeOf(context).width * 0.65,
                      ),
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: send
                          ? Theme.of(context).colorScheme.tertiaryContainer
                          : Theme.of(context).colorScheme.secondaryContainer,
                        borderRadius: const BorderRadius.all(Radius.circular(15)),
                      ),
                      child: Column(
                        crossAxisAlignment: send ? CrossAxisAlignment.start : CrossAxisAlignment.end,
                        children: [
                          LayoutBuilder(
                            builder: (context, constraints) {
                              return MediaQuery(
                                data: MediaQueryData(
                                  padding: MediaQuery.of(context).viewPadding,
                                  size: Size(
                                    constraints.maxWidth,
                                    constraints.maxHeight,
                                  ),
                                ),
                                child: VoiceMessageView(
                                  circlesColor: Theme.of(context).colorScheme.primary,
                                  backgroundColor: Colors.transparent,
                                  notActiveSliderColor: Colors.transparent,
                                  activeSliderColor: Theme.of(context).colorScheme.primary,
                                  controller: VoiceController(
                                    audioSrc: url,
                                    maxDuration: const Duration(seconds: 10),
                                    isFile: false,
                                    onComplete: () {
                                      /// do something on complete
                                    },
                                    onPause: () {
                                      /// do something on pause
                                    },
                                    onPlaying: () {
                                      /// do something on playing
                                    },
                                    onError: (err) {
                                      /// do somethin on error
                                    },
                                  ),
                                  innerPadding: 0,
                                  cornerRadius: 20,
                                ),
                              );
                            }
                          ),
                          const SizedBox(height: 5),
                          OwText(
                            DateFormat('HH:mm').format(DateTime.fromMillisecondsSinceEpoch(
                              createdDate.millisecondsSinceEpoch,
                            ).toLocal()),
                            style: Theme.of(context).textTheme.bodySmall?.copyWith(
                              color: send
                                ? Theme.of(context).colorScheme.onTertiaryContainer
                                : Theme.of(context).colorScheme.onSecondaryContainer,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        ),
      ],
    );
  }
}
