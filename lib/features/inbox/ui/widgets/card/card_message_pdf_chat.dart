import 'dart:math';

import 'package:backoffice/features/inbox/ui/views/view_pdf.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:http/http.dart' as http;

class CardMessagePdfChat extends StatefulWidget {
  const CardMessagePdfChat({
    Key? key,
    required this.url,
    this.extra,
    required this.send,
    required this.createdDate,
  }) : super(key: key);
  final String url;
  final String? extra;
  final Timestamp createdDate;
  final bool send;

  @override
  State<CardMessagePdfChat> createState() => _CardMessagePdfChatState();
}

class _CardMessagePdfChatState extends State<CardMessagePdfChat> {

  String _fileSize = 'Carregando...';

  @override
  void initState() {
    super.initState();
    _fetchFileSize();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 12),
          child: Row(
            mainAxisAlignment: widget.send ? MainAxisAlignment.start : MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                constraints: BoxConstraints(
                  minWidth: 10,
                  maxWidth: MediaQuery.sizeOf(context).width * 0.65,
                ),
                padding: const EdgeInsets.all(12),
                decoration: BoxDecoration(
                  color: widget.send
                    ? Theme.of(context).colorScheme.tertiaryContainer
                    : Theme.of(context).colorScheme.secondaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                ),
                child: Column(
                  crossAxisAlignment: widget.send
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.end,
                  children: [
                    Material(
                      type: MaterialType.transparency,
                      color: Colors.transparent,
                      child: Ink(
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.surfaceVariant,
                          borderRadius: const BorderRadius.all(Radius.circular(16)),
                          border: Border.all(
                            width: 1,
                            color: widget.send
                              ? Theme.of(context).colorScheme.onTertiaryContainer
                              : Theme.of(context).colorScheme.onSecondaryContainer,
                          ),
                        ),
                        child: InkWell(
                          borderRadius: const BorderRadius.all(Radius.circular(16)),
                          onTap: () {
                            openLinkPage(
                              context,
                              OpenLinkPageParams.basic(
                                child: ViewPdf(
                                  url: widget.url,
                                ),
                                dynamicResponsivity: false,
                              ),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(6),
                            child: Row(
                              children: [
                                IgnorePointer(
                                  child: IconButton(
                                    icon: const Icon(EvaIcons.fileTextOutline),
                                    onPressed: () {},
                                  ),
                                ),
                                const SizedBox(width: 8),
                                Flexible(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        Valid.text(widget.extra) ? '${widget.extra}' : 'PDF',
                                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                          fontWeight: FontWeight.bold,
                                          color: Theme.of(context).colorScheme.onSurfaceVariant,
                                        ),
                                      ),
                                      Text(
                                        _fileSize,
                                        style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                                          color: Theme.of(context).colorScheme.onSurfaceVariant,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 8),
                    OwText(
                      DateFormat('HH:mm').format(DateTime.fromMillisecondsSinceEpoch(
                        widget.createdDate.millisecondsSinceEpoch,
                      ).toLocal()),
                      style: Theme.of(context).textTheme.bodySmall?.copyWith(
                        color: widget.send
                          ? Theme.of(context).colorScheme.onTertiaryContainer
                          : Theme.of(context).colorScheme.onSecondaryContainer,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<void> _fetchFileSize() async {
    final response = await http.head(Uri.parse(widget.url));
    final contentLength = response.headers['content-length'];

    if(mounted) {
      if (contentLength != null) {
        final fileSizeInBytes = int.parse(contentLength);
        setState(() {
          _fileSize = _formatBytes(fileSizeInBytes, 2);
        });
      } else {
        setState(() {
          _fileSize = 'Tamanho desconhecido';
        });
      }
    }
  }

  String _formatBytes(int bytes, int decimals) {
    if (bytes <= 0) return '0 Bytes';
    const suffixes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    final i = (log(bytes) / log(1024)).floor();
    return ((bytes / pow(1024, i)).toStringAsFixed(decimals)) + ' ' + suffixes[i];
  }
}
