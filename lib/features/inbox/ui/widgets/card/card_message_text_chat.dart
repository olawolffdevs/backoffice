import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:intl/intl.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class CardMessageTextChat extends StatelessWidget {
  const CardMessageTextChat({
    Key? key,
    required this.message,
    required this.send,
    required this.createdDate,
  }) : super(key: key);
  final String message;
  final Timestamp createdDate;
  final bool send;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 12),
          child: Row(
            mainAxisAlignment: send ? MainAxisAlignment.start : MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                constraints: BoxConstraints(
                  minWidth: 10,
                  maxWidth: MediaQuery.sizeOf(context).width * 0.65,
                ),
                padding: const EdgeInsets.all(12),
                decoration: BoxDecoration(
                  color: send
                    ? Theme.of(context).colorScheme.tertiaryContainer
                    : Theme.of(context).colorScheme.secondaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                ),
                child: Column(
                  crossAxisAlignment: send
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.end,
                  children: [
                    HtmlWidget(
                      message,
                      onErrorBuilder: (context, element, error) => OwText('$element error: $error'),
                      onLoadingBuilder: (context, element, loadingProgress) => const CircularProgressIndicator(),
                      customWidgetBuilder: (element) {
                        if (element.localName == 'das') {
                          return OwButton(
                            labelText: 'Acessar DAS',
                            onPressed: () {},
                          );
                        }
                        if (element.localName == 'dasn') {
                          return OwButton(
                            labelText: 'Acessar DASN',
                            onPressed: () {},
                          );
                        }
                        if (element.localName == 'cnpj') {
                          return OwButton(
                            labelText: 'Cartão CNPJ',
                            onPressed: () {},
                          );
                        }
                        if (element.localName == 'ccmei') {
                          return OwButton(
                            labelText: 'Certificado MEI',
                            onPressed: () {},
                          );
                        }
                        return null;
                      },
                      renderMode: RenderMode.column,
                      textStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(
                        color: send
                          ? Theme.of(context).colorScheme.onTertiaryContainer
                          : Theme.of(context).colorScheme.onSecondaryContainer,
                      ),
                    ),
                    const SizedBox(height: 5),
                    OwText(
                      DateFormat('HH:mm').format(DateTime.fromMillisecondsSinceEpoch(
                        createdDate.millisecondsSinceEpoch,
                      ).toLocal()),
                      style: Theme.of(context).textTheme.bodySmall?.copyWith(
                        color: send
                          ? Theme.of(context).colorScheme.onTertiaryContainer
                          : Theme.of(context).colorScheme.onSecondaryContainer,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

