import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:backoffice/ui/components/alerta_simples.dart';

class FinancialEducationVideoCrudPage extends StatefulWidget {
  const FinancialEducationVideoCrudPage({
    Key? key,
    this.video,
    required this.category,
  }) : super(key: key);
  final VideoFinancialEducationEntity? video;
  final CategoryFinancialEducationEntity category;
  @override
  _FinancialEducationVideoCrudPageState createState() =>
      _FinancialEducationVideoCrudPageState();
}

class _FinancialEducationVideoCrudPageState
    extends State<FinancialEducationVideoCrudPage> {
  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController urlVideo = TextEditingController();
  TextEditingController urlCover = TextEditingController();
  TextEditingController index = TextEditingController();

  List<FocusNode> focus = FN.initialilzeFocusNodeList(7);

  @override
  void initState() {
    super.initState();
    if (widget.video != null) {
      title.text = widget.video!.title ?? '';
      description.text = widget.video!.description ?? '';
      urlVideo.text = widget.video!.url ?? '';
      urlCover.text = widget.video!.cover ?? '';
      index.text = widget.video!.index.toString();
    }
  }

  @override
  void dispose() {
    title.dispose();
    description.dispose();
    urlVideo.dispose();
    urlCover.dispose();
    index.dispose();
    FN.disposeFocusNodeList(focus);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes do vídeo'),
        actions: [
          Visibility(
            visible: widget.video != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Apagar vídeo',
                  description:
                      'Essa ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.financialEducation.videos.delete(
                      id: widget.video!.id!,
                    );
                    OwBotToast.close();
                    if (mounted) {
                      context.go('/${WhiteLabelEntity.current?.id}/dashboard');
                    }
                  } catch (_) {
                    OwBotToast.close();
                    OwBotToast.toast('Erro ao remover vídeo.');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(
          horizontal: 25,
          vertical: 10,
        ),
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            RawKeyboardListener(
              focusNode: focus[0],
              onKey: (RawKeyEvent event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FN.nextFn(context, focus[1]);
                }
              },
              child: OwTextField(
                controller: title,
                labelText: 'Título do vídeo',
                hintText: 'Escreva aqui...',
                focusNode: focus[0],
                onFieldSubmitted: (_) {
                  FN.nextFn(context, focus[1]);
                },
                onChanged: (valor) {
                  setState(() {});
                },
              ),
            ),
            RawKeyboardListener(
              focusNode: focus[1],
              onKey: (RawKeyEvent event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FN.nextFn(context, focus[2]);
                }
              },
              child: OwTextField(
                margin: const EdgeInsets.only(
                  top: 15,
                ),
                controller: description,
                labelText: 'Descrição do vídeo',
                hintText: 'Escreva aqui...',
                focusNode: focus[1],
                onFieldSubmitted: (_) {
                  FN.nextFn(context, focus[2]);
                },
                onChanged: (valor) {
                  setState(() {});
                },
              ),
            ),
            RawKeyboardListener(
              focusNode: focus[2],
              onKey: (RawKeyEvent event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FN.nextFn(context, focus[3]);
                }
              },
              child: OwTextField(
                margin: const EdgeInsets.only(
                  top: 15,
                ),
                controller: urlVideo,
                labelText: 'URL do vídeo (MP4)',
                hintText: 'Cole aqui...',
                focusNode: focus[2],
                onFieldSubmitted: (_) {
                  FN.nextFn(context, focus[3]);
                },
                onChanged: (valor) {
                  setState(() {});
                },
              ),
            ),
            RawKeyboardListener(
              focusNode: focus[3],
              onKey: (RawKeyEvent event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FN.nextFn(context, focus[4]);
                }
              },
              child: OwTextField(
                margin: const EdgeInsets.only(
                  top: 15,
                ),
                controller: urlCover,
                labelText: 'URL da capa do vídeo',
                hintText: 'Cole aqui...',
                focusNode: focus[3],
                onFieldSubmitted: (_) {
                  FN.nextFn(context, focus[5]);
                },
                onChanged: (valor) {
                  setState(() {});
                },
              ),
            ),
            const SizedBox(height: 25),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0),
              child: Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.remove),
                    onPressed: () {
                      setState(() {
                        final value = int.tryParse(index.text) ?? 2;
                        if (value > 1) {
                          index.text = '${value - 1}';
                        }
                      });
                    },
                  ),
                  Expanded(
                    child: RawKeyboardListener(
                      focusNode: focus[3],
                      onKey: (RawKeyEvent event) {
                        if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                          FN.nextFn(context, focus[4]);
                        }
                      },
                      child: OwTextField(
                        margin: const EdgeInsets.symmetric(horizontal: 16),
                        hintText: 'Digite aqui...',
                        controller: index,
                        focusNode: focus[3],
                        onFieldSubmitted: (_) {
                          FN.nextFn(context, focus[4]);
                        },
                        onChanged: (valor) {
                          setState(() {});
                        },
                      ),
                    ),
                  ),
                  IconButton(
                    icon: const Icon(Icons.add),
                    onPressed: () {
                      setState(() {
                        final value = int.tryParse(index.text) ?? 1;
                        index.text = '${value + 1}';
                      });
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          labelText: 'Salvar vídeo',
          onPressed: () async {
            if (title.text.trim().isEmpty ||
                description.text.trim().isEmpty ||
                urlCover.text.trim().isEmpty ||
                urlVideo.text.trim().isEmpty ||
                index.text.trim().isEmpty) {
              showDialog(
                context: context,
                builder: (context) => const AlertaSimples(
                  'Existem um ou mais campos que precisam ser preenchidos.',
                ),
              );
            } else {
              if (widget.video != null) {
                final VideoFinancialEducationEntity video =
                    widget.video!.copyWith(
                  title: title.text.trim(),
                  description: description.text.trim(),
                  url: urlVideo.text.trim(),
                  cover: urlCover.text.trim(),
                  aspectRatio: 1,
                  index: int.tryParse(index.text) ?? 0,
                  categoryId: widget.category.id,
                );
                try {
                  OwBotToast.loading();
                  final response = await Api.financialEducation.videos.update(
                    id: widget.video!.id!,
                    data: video,
                  );
                  OwBotToast.close();
                  if (mounted) {
                    context.pop(response);
                    OwBotToast.toast('Atualizado com sucesso');
                  }
                } catch (e) {
                  OwBotToast.close();
                  if (mounted) {
                    showOwDialog(
                      context: context,
                      title: 'Ops, ocorreu um erro',
                      description: '$e',
                      buttons: [
                        OwButton.text(
                          labelText: 'Fechar',
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    );
                  }
                }
              } else {
                final VideoFinancialEducationEntity video =
                    VideoFinancialEducationEntity(
                  title: title.text.trim(),
                  description: description.text.trim(),
                  url: urlVideo.text.trim(),
                  cover: urlCover.text.trim(),
                  aspectRatio: 1,
                  index: int.tryParse(index.text) ?? 0,
                  categoryId: widget.category.id,
                );
                try {
                  OwBotToast.loading();
                  final response = await Api.financialEducation.videos.add(
                    data: video,
                  );
                  OwBotToast.close();
                  if (mounted) {
                    context.pop(response);
                    OwBotToast.toast('Cadastrado com sucesso');
                  }
                } catch (e) {
                  OwBotToast.close();
                  if (mounted) {
                    showOwDialog(
                      context: context,
                      title: 'Ops, ocorreu um erro',
                      description: '$e',
                      buttons: [
                        OwButton.text(
                          labelText: 'Fechar',
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    );
                  }
                }
              }
            }
          },
        ),
      ),
    );
  }
}
