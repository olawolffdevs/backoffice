import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ListVideoPage extends StatefulWidget {
  const ListVideoPage({
    Key? key,
    required this.category,
  }) : super(key: key);
  final CategoryFinancialEducationEntity category;
  @override
  _ListVideoPageState createState() => _ListVideoPageState();
}

class _ListVideoPageState extends State<ListVideoPage> {
  List<VideoFinancialEducationEntity>? videos;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: Text('Vídeos de ${widget.category.title}'),
      ),
      body: SingleChildScrollView(
        child: Visibility(
          visible: videos != null,
          replacement: const Center(
            child: CircularProgressIndicator(),
          ),
          child: Visibility(
            visible: videos?.isNotEmpty ?? false,
            replacement: Container(
              margin: appSM.isWeb
                  ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                  : const EdgeInsets.fromLTRB(25, 10, 25, 25),
              padding: const EdgeInsets.all(25),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).colorScheme.outlineVariant,
                ),
                borderRadius: const BorderRadius.all(Radius.circular(15)),
              ),
              child: const Center(
                child: Text(
                    'Você ainda não cadastrou nenhum vídeo. Adicione o primeiro para adicionar os conteúdos.'),
              ),
            ),
            child: OwGrid.builder(
              padding: appSM.isWeb
                  ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                  : const EdgeInsets.fromLTRB(25, 10, 25, 25),
              itemCount: videos?.length ?? 0,
              numbersInRowAccordingToWidgth: const [400, 850, 1200, 1600],
              itemBuilder: (context, index) {
                final VideoFinancialEducationEntity video = videos![index];
                return Ink(
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.secondaryContainer,
                    borderRadius: const BorderRadius.all(Radius.circular(16)),
                  ),
                  child: InkWell(
                    onTap: () async {
                      final retorno = await context.push(
                        '/${WhiteLabelEntity.current?.id}/dashboard/financial-education/category/${widget.category.id}/videos/${video.id}',
                        extra: {'video': video, 'category': widget.category},
                      );
                      if (retorno is VideoFinancialEducationEntity) {
                        videos![index] = retorno;
                        setState(() {});
                      } else if (retorno == true) {
                        videos!.removeAt(index);
                        setState(() {});
                      }
                    },
                    borderRadius: const BorderRadius.all(Radius.circular(16)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 20,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          AspectRatio(
                            aspectRatio: 16 / 9,
                            child: ClipRRect(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(16)),
                              child: Image.network(
                                '${videos?[index].cover}',
                                width: MediaQuery.of(context).size.width,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          const SizedBox(height: 15),
                          Text(
                            '${videos?[index].title}',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSecondaryContainer,
                            ),
                          ),
                          const SizedBox(
                            height: 6,
                          ),
                          Text(
                            '${videos?[index].description}',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        label: const Text('Adicionar vídeo'),
        icon: const Icon(Icons.add),
        onPressed: () async {
          final retorno = await context.push(
            '/${WhiteLabelEntity.current?.id}/dashboard/financial-education/category/${widget.category.id}/videos/new',
            extra: {'category': widget.category},
          );
          if (retorno is VideoFinancialEducationEntity) {
            videos ??= [];
            videos!.insert(0, retorno);
            setState(() {});
          }
        },
      ),
    );
  }

  void callApi() async {
    try {
      videos = await Api.financialEducation.videos.listByCategory(
        id: widget.category.id ?? '',
        query: {'skip': videos?.length ?? 0},
      );
      setState(() {});
    } catch (e) {
      OwBotToast.close();
      OwBotToast.toast(e.toString());
    }
  }
}
