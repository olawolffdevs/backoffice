import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class FinancialEducationHomePage extends StatefulWidget {
  const FinancialEducationHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<FinancialEducationHomePage> createState() => _FinancialEducationHomePageState();
}

class _FinancialEducationHomePageState extends State<FinancialEducationHomePage> {
  List<CategoryFinancialEducationEntity>? categories;

  @override
  void initState() {
    super.initState();
    callApiCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const Text('Educação financeira'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: appSM.isWeb
                  ? const EdgeInsets.fromLTRB(50, 50, 50, 0)
                  : const EdgeInsets.fromLTRB(25, 25, 25, 0),
              child: Text(
                'Categorias',
                style: appSM.isWeb
                    ? Theme.of(context).textTheme.titleLarge
                    : Theme.of(context).textTheme.titleMedium,
              ),
            ),
            Visibility(
              visible: categories != null,
              replacement: Container(
                constraints: const BoxConstraints(
                  minHeight: 300,
                ),
                child: const Center(child: CircularProgressIndicator()),
              ),
              child: Visibility(
                visible: categories?.isNotEmpty ?? false,
                replacement: Container(
                  margin: appSM.isWeb
                      ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                      : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  padding: const EdgeInsets.all(25),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Theme.of(context).colorScheme.outlineVariant,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                  ),
                  child: const Center(
                    child: Text(
                        'Você ainda não cadastrou nenhuma categoria. Adicione a primeira para adicionar os conteúdos.'),
                  ),
                ),
                child: OwGrid.builder(
                  padding: appSM.isWeb
                      ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                      : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  itemBuilder: (context, index) {
                    return Ink(
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.secondaryContainer,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(16)),
                      ),
                      child: InkWell(
                        onTap: () async {
                          final result = await context.push(
                            '/${WhiteLabelEntity.current?.id}/dashboard/financial-education/category/${categories![index].id}',
                            extra: categories![index],
                          );
                          if (result is CategoryFinancialEducationEntity) {
                            categories![index] = (result);
                            setState(() {});
                          } else if (result == true) {
                            categories!.removeAt(index);
                            setState(() {});
                          }
                        },
                        borderRadius:
                            const BorderRadius.all(Radius.circular(16)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 20,
                            horizontal: 20,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              AspectRatio(
                                aspectRatio: 16 / 9,
                                child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(16)),
                                  child: Image.network(
                                    '${categories?[index].cover}',
                                    width: MediaQuery.of(context).size.width,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 15),
                              Text(
                                '${categories?[index].title}',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onSecondaryContainer,
                                ),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Text(
                                '${categories?[index].description}',
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  Expanded(
                                    child: IgnorePointer(
                                      child: CardAtalhos(
                                        title: 'Editar',
                                        icon: EvaIcons.edit2Outline,
                                        onPressed: () {},
                                        selecionado: true,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: CardAtalhos(
                                      title: 'Vídeos',
                                      icon: EvaIcons.videoOutline,
                                      onPressed: () {
                                        context.push(
                                          '/${WhiteLabelEntity.current?.id}/dashboard/financial-education/category/${categories?[index].id}/videos',
                                          extra: categories?[index],
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: categories?.length ?? 0,
                  numbersInRowAccordingToWidgth: const [400, 850, 1200, 1600],
                ),
              ),
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),
      floatingActionButton: categories == null
          ? null
          : FloatingActionButton.extended(
              heroTag: null,
              label: const Text('Adicionar categoria'),
              icon: const Icon(Icons.add),
              onPressed: () async {
                final result = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/financial-education/category/new',
                );
                if (result is CategoryFinancialEducationEntity) {
                  categories ??= [];
                  categories!.add(result);
                  setState(() {});
                }
                await callApiCategories();
              },
            ),
    );
  }

  Future<void> callApiCategories() async {
    try {
      categories = await Api.financialEducation.categories.list(query: {});
      setState(() {});
    } catch (e) {
      OwBotToast.close();
      OwBotToast.toast(e.toString());
    }
  }
}
