import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class FinancialEducationCategoryCrudPage extends StatefulWidget {
  const FinancialEducationCategoryCrudPage({this.category, Key? key})
      : super(key: key);
  final CategoryFinancialEducationEntity? category;
  @override
  _FinancialEducationCategoryCrudPageState createState() =>
      _FinancialEducationCategoryCrudPageState();
}

class _FinancialEducationCategoryCrudPageState
    extends State<FinancialEducationCategoryCrudPage> {
  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController image = TextEditingController();
  MaskedTextController index = MaskedTextController(mask: '000');
  TextEditingController tags = TextEditingController();
  List<ActionChip> items = [];

  List<FocusNode> focus = FN.initialilzeFocusNodeList(5);

  @override
  void initState() {
    super.initState();
    if (widget.category != null) {
      title.text = widget.category!.title ?? '';
      description.text = widget.category!.description ?? '';
      image.text = widget.category!.cover ?? '';
      index.text = widget.category!.index.toString();
      widget.category!.tags?.map((e) {
        items.add(ActionChip(
          label: Text(e),
          tooltip: '$e',
          onPressed: () {},
        ));
      }).toList();
    }
  }

  @override
  void dispose() {
    FN.disposeFocusNodeList(focus);
    title.dispose();
    description.dispose();
    image.dispose();
    index.dispose();
    tags.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes da categoria'),
        actions: [
          Visibility(
            visible: widget.category != null,
            child: IconButton(
              icon: const Icon(EvaIcons.videoOutline),
              onPressed: () {
                context.push(
                    '/${WhiteLabelEntity.current?.id}/dashboard/financial-education/category/${widget.category?.id}/videos',
                    extra: widget.category!);
              },
            ),
          ),
          Visibility(
            visible: widget.category != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Apagar categoria',
                  description:
                      'Essa ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.financialEducation.categories.delete(
                      id: widget.category!.id!,
                    );
                    OwBotToast.close();
                    if (mounted) {
                      context.go('/${WhiteLabelEntity.current?.id}/dashboard');
                    }
                  } catch (e) {
                    OwBotToast.close();
                    OwBotToast.toast('$e');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            sectionTitle(
              title: 'Título da categoria',
              description:
                  'O nome dessa categoria irá aparecer para os usuários finais',
            ),
            RawKeyboardListener(
              focusNode: focus[0],
              onKey: (RawKeyEvent event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FN.nextFn(context, focus[1]);
                }
              },
              child: OwTextField(
                controller: title,
                margin: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                hintText: 'Escreva aqui...',
                maxLength: 62,
                focusNode: focus[0],
                onFieldSubmitted: (_) {
                  FN.nextFn(context, focus[1]);
                },
                onChanged: (valor) {
                  setState(() {});
                },
              ),
            ),
            sectionTitle(
              title: 'Descrição da categoria',
              description:
                  'Descreva informações relavantes dessa categoria, ela irá aparecer para os usuários finais',
            ),
            RawKeyboardListener(
              focusNode: focus[1],
              onKey: (RawKeyEvent event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FN.nextFn(context, focus[2]);
                }
              },
              child: OwTextField(
                controller: description,
                margin: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                hintText: 'Escreva aqui...',
                maxLength: 250,
                maxLines: 8,
                focusNode: focus[1],
                onFieldSubmitted: (_) {
                  FN.nextFn(context, focus[2]);
                },
                onChanged: (valor) {
                  setState(() {});
                },
              ),
            ),
            sectionTitle(
              title: 'Imagem de capa',
              description:
                  'Adicione um link para a imagem de capa do seu vídeo.',
            ),
            RawKeyboardListener(
              focusNode: focus[2],
              onKey: (RawKeyEvent event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FN.nextFn(context, focus[3]);
                }
              },
              child: OwTextField(
                controller: image,
                margin: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                hintText: 'Escreva aqui...',
                helperText: 'Recomendamos a proporção 16x9',
                focusNode: focus[2],
                onFieldSubmitted: (_) {
                  FN.nextFn(context, focus[3]);
                },
                onChanged: (valor) {
                  setState(() {});
                },
              ),
            ),
            sectionTitle(
              title: 'Posição na tela',
              description:
                  'Defina um index - número inteiro - para definir a posição em que o módulo irá aparecer na tela.',
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.remove),
                    onPressed: () {
                      setState(() {
                        final value = int.tryParse(index.text) ?? 2;
                        if (value > 1) {
                          index.text = '${value - 1}';
                        }
                      });
                    },
                  ),
                  Expanded(
                    child: RawKeyboardListener(
                      focusNode: focus[3],
                      onKey: (RawKeyEvent event) {
                        if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                          FN.nextFn(context, focus[4]);
                        }
                      },
                      child: OwTextField(
                        margin: const EdgeInsets.symmetric(horizontal: 16),
                        hintText: 'Digite aqui...',
                        controller: index,
                        focusNode: focus[3],
                        onFieldSubmitted: (_) {
                          FN.nextFn(context, focus[4]);
                        },
                        onChanged: (valor) {
                          setState(() {});
                        },
                      ),
                    ),
                  ),
                  IconButton(
                    icon: const Icon(Icons.add),
                    onPressed: () {
                      setState(() {
                        final value = int.tryParse(index.text) ?? 1;
                        index.text = '${value + 1}';
                      });
                    },
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            const OwDivider(),
            const SizedBox(height: 25),
            sectionTitle(
              title: 'Hastags da categoria',
              description:
                  'Adicione tags para ajudar o usuário achar a categoria.',
            ),
            RawKeyboardListener(
              focusNode: focus[4],
              onKey: (RawKeyEvent event) {
                if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                  FN.unfocusFn(context);
                }
              },
              child: OwTextField(
                controller: tags,
                margin: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                hintText: 'Ex.: PJ, PF, Dica',
                helperText: 'Pressione Enter para adicionar a tag',
                focusNode: focus[4],
                onChanged: (valor) {
                  setState(() {});
                },
                onFieldSubmitted: (str) {
                  setState(() {
                    items.add(ActionChip(
                      label: Text(str),
                      tooltip: '$str',
                      onPressed: () {},
                    ));
                    tags.clear();
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(25, 25, 25, 10),
              child: Wrap(
                spacing: 8,
                runSpacing: 8,
                children: items
                    .map((e) => ActionChip(
                          label: e.label,
                          onPressed: () {
                            items.remove(e);
                            setState(() {});
                          },
                        ))
                    .toList(),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          labelText: 'Salvar',
          onPressed: () async {
            if (title.text.trim().isEmpty ||
                description.text.trim().isEmpty ||
                image.text.trim().isEmpty ||
                index.text.trim().isEmpty) {
              showOwDialog(
                context: context,
                title: 'Ops, faltando informações',
                description:
                    'Existem um ou mais campos que precisam ser preenchidos.',
                buttons: [
                  OwButton.text(
                    labelText: 'Fechar',
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            } else {
              if (widget.category != null) {
                final CategoryFinancialEducationEntity category =
                    widget.category!.copyWith(
                  title: title.text.trim(),
                  description: description.text.trim(),
                  cover: image.text.trim(),
                  index: int.tryParse(index.text) ?? 0,
                  tags: items.map((e) => '${e.tooltip}').toList(),
                );
                try {
                  OwBotToast.loading();
                  final response = Api.financialEducation.categories.update(
                    id: widget.category!.id!,
                    data: category,
                  );
                  OwBotToast.close();
                  if (mounted) {
                    context.pop(response);
                    OwBotToast.toast('Atualizado com sucesso');
                  }
                } catch (e) {
                  OwBotToast.close();
                  if (mounted) {
                    showOwDialog(
                      context: context,
                      title: 'Ops, ocorreu um erro',
                      description: '$e',
                      buttons: [
                        OwButton.text(
                          labelText: 'Fechar',
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    );
                  }
                }
              } else {
                final CategoryFinancialEducationEntity category =
                    CategoryFinancialEducationEntity(
                  title: title.text.trim(),
                  description: description.text.trim(),
                  cover: image.text.trim(),
                  index: int.tryParse(index.text) ?? 0,
                  tags: items.map((e) => '${e.tooltip}').toList(),
                );
                try {
                  OwBotToast.loading();
                  final response = await Api.financialEducation.categories.add(
                    data: category,
                  );
                  OwBotToast.close();
                  if (mounted) {
                    context.pop(response);
                    OwBotToast.toast('Cadastrado com sucesso');
                  }
                } catch (e) {
                  OwBotToast.close();
                  if (mounted) {
                    showOwDialog(
                      context: context,
                      title: 'Ops, ocorreu um erro',
                      description: '$e',
                      buttons: [
                        OwButton.text(
                          labelText: 'Fechar',
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    );
                  }
                }
              }
            }
          },
        ),
      ),
    );
  }

  Widget sectionTitle({required String title, String? description}) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 25, 25, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          OwText(
            title,
            style: Theme.of(context).textTheme.titleLarge?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          Visibility(
            visible: Valid.text(description),
            child: OwText(
              '$description',
              padding: const EdgeInsets.only(top: 6),
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          ),
        ],
      ),
    );
  }
}
