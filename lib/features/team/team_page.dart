import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class TeamPage extends StatefulWidget {
  const TeamPage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<TeamPage> createState() => _TeamPageState();
}

class _TeamPageState extends State<TeamPage> {

  TextEditingController search = TextEditingController();

  List<UserEntity>? users;
  int? sortColumnIndex;
  bool isAscending = false;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const Text('Equipe'),
        actions: [
          IconButton(
            tooltip: 'Adicionar membro',
            icon: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6),
              child: Row(
                children: [
                  const Icon(Icons.add),
                  const SizedBox(width: 4),
                  Text(
                    'Novo',
                    style: Theme.of(context).textTheme.titleSmall?.copyWith(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                ],
              ),
            ),
            onPressed: () async {
              context.push(
                '/${WhiteLabelEntity.current?.id}/dashboard/white-label/user/add',
              );
            },
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await callApi();
        },
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 24, top: 12),
                    constraints: const BoxConstraints(
                      maxWidth: 290,
                    ),
                    child: OwTextField(
                      widthBorder: 2,
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(16)),
                        ),
                        fillColor: Theme.of(context).colorScheme.primaryContainer,
                        hintText: 'Buscar...',
                        suffixIcon: IconButton(
                          icon: const Icon(EvaIcons.searchOutline),
                          onPressed: () {
                            setState(() {});
                          },
                        )
                      ),
                      outlined: true,
                      controller: search,
                      onFieldSubmitted: (value) {
                        setState(() {});
                      },
                    ),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.all(24),
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.surface,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16),
                  ),
                  border: Border.all(
                    width: 1,
                    color: Theme.of(context).colorScheme.surfaceVariant,
                  ),
                ),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16),
                  ),
                  child: DataTable(
                    border: const TableBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(16)),
                    ),
                    headingRowColor: MaterialStatePropertyAll(
                      Theme.of(context).colorScheme.primaryContainer,
                    ),
                    sortAscending: isAscending,
                    sortColumnIndex: sortColumnIndex,
                    columns: getColumns(['Nome', 'Email', 'Permissão']),
                    rows: getRows(Valid.text(search.text) ? searchData() : users),
                  ),
                ),
              ),
              const SizedBox(height: 100),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      users = await Api.whiteLabel.listTeamByWhiteLabel(
        id: '${WhiteLabelEntity.current?.id}',
      );
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: 'Tente novamente mais tarde.',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }

  List<DataColumn> getColumns(List<String> columns) => columns.map((column) {
    return DataColumn(
      label: Text(column),
      onSort: onSort, 
    );
  }).toList();

  List<DataRow> getRows(List<UserEntity>? users) => users?.map((UserEntity user) {
    final cells = [
      user.name,
      user.email,
      'Administrador'
    ];
    return DataRow(
      cells: getCells(cells, user),
    );
  }).toList() ?? <DataRow>[];

  List<DataCell> getCells(List<dynamic> cells, UserEntity user) => cells.map((cell) {
    return DataCell(
      Text(cell),
      onTap: () async {
        try {
          final result = await showOwDialog(
            context: context,
            title: 'Remover ${user.name.split(" ")[0]}?',
            description: 'Essa ação não poderá ser desfeita. Você tem certeza disso?',
            buttons: [
              OwButton.text(
                labelText: 'Não, manter',
                onPressed: () {
                  Navigator.pop(context, false);
                },
              ),
              OwButton.text(
                labelText: 'Sim, excluir',
                onPressed: () {
                  Navigator.pop(context, true);
                },
              ),
            ],
          );
          if (result == true) {
            OwBotToast.loading();
            await Api.whiteLabel.deleteUser(
              whiteLabelId: '${WhiteLabelEntity.current?.id}',
              userId: '${user.id}',
            );
            OwBotToast.close();
            callApi();
          }
        } catch (e) {
          OwBotToast.close();
          await showOwDialog(
            context: context,
            title: 'Ocorreu um erro',
            description: '$e',
            buttons: [
              OwButton.text(
                labelText: 'Fechar',
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
      }
    );
  }).toList();

  List<UserEntity> searchData() {

    final s = normalizarTexto(search.text);

    return users!.where((usuario) {
      final n = normalizarTexto(usuario.name);
      final e = normalizarTexto(usuario.email!);

      return n.contains(s) || e.contains(s);
    }).toList();
  }

  void onSort(int columnIndex, bool ascending) {

    if(columnIndex == 0) {
      users!.sort((user1, user2) => compareString(ascending, user1.name, user2.name));
    } else if (columnIndex == 1) {
      users!.sort((user1, user2) => compareString(ascending, user1.email!, user2.email!));
    }

    setState(() {
      this.sortColumnIndex = columnIndex;
      this.isAscending = ascending;
    });
  }

  int compareString(bool ascending, String value1, String value2) {
    return ascending ? value1.compareTo(value2) : value2.compareTo(value1);
  }

  String normalizarTexto(String texto) {
    final semAcentos = texto.toLowerCase().replaceAll(RegExp(r'[áàãâä]'), 'a')
        .replaceAll(RegExp(r'[éèêë]'), 'e')
        .replaceAll(RegExp(r'[íìîï]'), 'i')
        .replaceAll(RegExp(r'[óòõôö]'), 'o')
        .replaceAll(RegExp(r'[úùûü]'), 'u')
        .replaceAll(RegExp(r'[ç]'), 'c')
        .replaceAll(RegExp(r'[^a-z0-9]'), ''); // Remove pontuações e mantém apenas letras e números
    return semAcentos;
  }
}
