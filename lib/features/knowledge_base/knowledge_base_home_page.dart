import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class KnowledgeBaseHomePage extends StatefulWidget {
  const KnowledgeBaseHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<KnowledgeBaseHomePage> createState() => _KnowledgeBaseHomePageState();
}

class _KnowledgeBaseHomePageState extends State<KnowledgeBaseHomePage> {
  List<HelpCategoriesFaqEntity>? categories;

  @override
  void initState() {
    super.initState();
    callApiCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const Text('Base de conhecimento'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: appSM.isWeb
                  ? const EdgeInsets.fromLTRB(50, 50, 50, 0)
                  : const EdgeInsets.fromLTRB(25, 25, 25, 0),
              child: Text(
                'Categorias',
                style: appSM.isWeb
                    ? Theme.of(context).textTheme.titleLarge
                    : Theme.of(context).textTheme.titleMedium,
              ),
            ),
            Visibility(
              visible: categories != null,
              replacement: Container(
                constraints: const BoxConstraints(
                  minHeight: 300,
                ),
                child: const Center(child: CircularProgressIndicator()),
              ),
              child: Visibility(
                visible: categories?.isNotEmpty ?? false,
                replacement: Container(
                  margin: appSM.isWeb
                      ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                      : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  padding: const EdgeInsets.all(25),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Theme.of(context).colorScheme.outlineVariant,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                  ),
                  child: const Center(
                    child: Text(
                        'Você ainda não cadastrou nenhuma categoria. Adicione a primeira para adicionar as perguntas e respostas.'),
                  ),
                ),
                child: OwGrid.builder(
                  padding: appSM.isWeb
                      ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                      : const EdgeInsets.fromLTRB(25, 10, 25, 25),
                  itemBuilder: (context, index) {
                    return Ink(
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.secondaryContainer,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(16)),
                      ),
                      child: InkWell(
                        onTap: () async {
                          final result = await context.push(
                            '/${WhiteLabelEntity.current?.id}/dashboard/knowledge-base/category/${categories![index].id}',
                            extra: categories![index],
                          );
                          if (result is HelpCategoriesFaqEntity) {
                            categories![index] = (result);
                            setState(() {});
                          }
                        },
                        borderRadius:
                            const BorderRadius.all(Radius.circular(16)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 20,
                            horizontal: 20,
                          ),
                          child: Row(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onSecondaryContainer,
                                  shape: BoxShape.circle,
                                ),
                                child: Icon(
                                  IconAdapter.getIcon(
                                    '${categories?[index].icon}',
                                  ),
                                  color: Theme.of(context)
                                      .colorScheme
                                      .secondaryContainer,
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Text(
                                      '${categories?[index].title}',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onSecondaryContainer,
                                      ),
                                    ),
                                    const SizedBox(height: 4),
                                    Text(
                                      '${categories?[index].description}',
                                    ),
                                    OwButton.outline(
                                      margin: const EdgeInsets.only(top: 4),
                                      labelText: 'Ver perguntas e respostas',
                                      onPressed: () async {
                                        context.push(
                                          '/${WhiteLabelEntity.current?.id}/dashboard/knowledge-base/category/${categories?[index].id}/questions',
                                          extra: categories?[index],
                                        );
                                      },
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: categories?.length ?? 0,
                  numbersInRowAccordingToWidgth: const [450, 950, 1200, 1600],
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: categories == null
          ? null
          : FloatingActionButton.extended(
              heroTag: null,
              label: const Text('Adicionar categoria'),
              icon: const Icon(Icons.add),
              onPressed: () async {
                final result = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/knowledge-base/category/new',
                );
                if (result is HelpCategoriesFaqEntity) {
                  categories ??= [];
                  categories!.add(result);
                  setState(() {});
                }
              },
            ),
    );
  }

  void callApiCategories() async {
    try {
      categories = await Api.knowledgeBase.listCategories(params: {});
      setState(() {});
    } catch (e) {
      OwBotToast.close();
      OwBotToast.toast(e.toString());
    }
  }
}
