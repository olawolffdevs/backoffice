import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class FaqQuestionCrudPage extends StatefulWidget {
  const FaqQuestionCrudPage({
    Key? key,
    this.question,
    required this.category,
  }) : super(key: key);
  final HelpQuestionFaqEntity? question;
  final HelpCategoriesFaqEntity category;
  @override
  _FaqQuestionCrudPageState createState() => _FaqQuestionCrudPageState();
}

class _FaqQuestionCrudPageState extends State<FaqQuestionCrudPage> {
  TextEditingController titulo = TextEditingController();
  TextEditingController resumo = TextEditingController();
  TextEditingController descricao = TextEditingController();
  TextEditingController index = TextEditingController();
  TextEditingController categoryId = TextEditingController();
  TextEditingController tags = TextEditingController();

  HelpCategoriesFaqEntity? category;
  List<ActionChip> items = [];

  @override
  void initState() {
    super.initState();
    if (widget.question != null) {
      titulo.text = widget.question!.title;
      resumo.text = widget.question!.summary;
      descricao.text = widget.question!.description;
      categoryId.text = widget.question!.category?.id ?? '';
      index.text = widget.question!.index.toString();
      category = widget.question!.category;
      widget.question!.type.map((e) {
        items.add(ActionChip(
          label: Text(e),
          tooltip: e,
        ));
      }).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Detalhes da pergunta'),
        actions: [
          Visibility(
            visible: widget.question != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                    context: context,
                    title: 'Deseja apagar esta pergunta?',
                    description:
                        'Essa ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                    buttons: [
                      OwButton.text(
                        labelText: 'Sim, apagar',
                        onPressed: () {
                          Navigator.pop(context, true);
                        },
                      ),
                      OwButton.text(
                        labelText: 'Não, manter',
                        onPressed: () {
                          Navigator.pop(context, false);
                        },
                      )
                    ]);
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.knowledgeBase.deleteQuestion(
                      id: widget.question!.id,
                    );
                    if (mounted) {
                      context.pop(true);
                    }
                  } catch (_) {
                    OwBotToast.toast('Erro ao remover pergunta.');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 25,
        ),
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              controller: titulo,
              labelText: 'Título da pergunta',
              hintText: 'Escreva aqui...',
            ),
            OwTextField(
              controller: descricao,
              margin: const EdgeInsets.only(top: 20),
              labelText: 'Resumo da resposta',
              hintText: 'Escreva aqui...',
            ),
            OwTextField(
              controller: resumo,
              margin: const EdgeInsets.only(top: 20),
              labelText: 'Resposta completa >',
              hintText: 'Escreva aqui...',
              readOnly: true,
              onTap: () async {
                final result = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/html-editor',
                  extra: resumo.text,
                );
                if (result is String) {
                  resumo.text = result;
                  setState(() {});
                }
              },
            ),
            sectionTitle(
              title: 'Posição na tela',
              description:
                  'Defina um index - número inteiro - para definir a posição em que o módulo irá aparecer na tela.',
            ),
            Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.remove),
                  onPressed: () {
                    setState(() {
                      final value = int.tryParse(index.text) ?? 2;
                      if (value > 1) {
                        index.text = '${value - 1}';
                      }
                    });
                  },
                ),
                Expanded(
                  child: OwTextField(
                    margin: const EdgeInsets.symmetric(horizontal: 16),
                    hintText: 'Digite aqui...',
                    controller: index,
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.add),
                  onPressed: () {
                    setState(() {
                      final value = int.tryParse(index.text) ?? 1;
                      index.text = '${value + 1}';
                    });
                  },
                ),
              ],
            ),
            const SizedBox(height: 35),
            // OwTextField(
            //   controller: tags,
            //   margin: const EdgeInsets.only(
            //     top: 15,
            //   ),
            //   labelText: 'Adicione tags (hashtags)',
            //   hintText: 'Ex.: PJ, PF, Dica',
            //   helperText: 'Pressione Enter para adicionar a tag',
            //   onFieldSubmitted: (str) {
            //     setState(() {
            //       items.add(ActionChip(
            //         label: Text(str),
            //         tooltip: str,
            //         onPressed: () {},
            //       ));
            //       tags.clear();
            //     });
            //   },
            // ),
            // Container(
            //   margin: const EdgeInsets.only(top: 15, bottom: 10),
            //   child: Wrap(
            //     children: items,
            //   ),
            // ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: EdgeInsets.fromLTRB(
            20,
            20,
            20,
            MediaQuery.viewInsetsOf(context).bottom + 20,
          ),
          labelText: 'Salvar',
          onPressed: () async {
            final List<String> t = items.map((e) => e.tooltip ?? '').toList();
            if (titulo.text.trim().isEmpty ||
                descricao.text.trim().isEmpty ||
                resumo.text.trim().isEmpty ||
                index.text.trim().isEmpty) {
              showOwDialog(
                context: context,
                title: 'Ops, calma lá',
                description:
                    'Existem um ou mais campos que precisam ser preenchidos.',
              );
            } else {
              if (widget.question != null) {
                final HelpQuestionFaqEntity question =
                    widget.question!.copyWith(
                  index: int.tryParse(index.text) ?? 0,
                  type: t,
                  title: titulo.text.trim(),
                  description: descricao.text.trim(),
                  summary: resumo.text.trim(),
                  category: widget.category,
                );
                try {
                  OwBotToast.loading();
                  final response = await Api.knowledgeBase.updateQuestion(
                    id: widget.question!.id,
                    data: question,
                  );
                  if (mounted) {
                    context.pop(response);
                    showOwDialog(
                      context: context,
                      title: 'Pergunta atualizada com sucesso.',
                    );
                  }
                } catch (e) {
                  OwBotToast.close();
                  if (mounted) {
                    showOwDialog(
                      context: context,
                      title: 'Ops',
                      description: '$e',
                    );
                  }
                }
              } else {
                final HelpQuestionFaqEntity question = HelpQuestionFaqEntity(
                  index: int.tryParse(index.text) ?? 0,
                  type: t,
                  title: titulo.text.trim(),
                  description: descricao.text.trim(),
                  summary: resumo.text.trim(),
                  category: widget.category,
                  id: '',
                );
                try {
                  final response = await Api.knowledgeBase.insertQuestion(
                    data: question,
                  );
                  if (mounted) {
                    context.pop(response);
                    showOwDialog(
                      context: context,
                      title: 'Cadastrado com sucesso',
                    );
                  }
                } catch (e) {
                  if (mounted) {
                    showOwDialog(
                      context: context,
                      title: 'Ops',
                      description: '$e',
                    );
                  }
                }
              }
            }
          },
        ),
      ),
    );
  }

  Widget sectionTitle({required String title, String? description}) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 25, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          OwText(
            title,
            style: Theme.of(context).textTheme.titleLarge?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          Visibility(
            visible: Valid.text(description),
            child: OwText(
              '$description',
              padding: const EdgeInsets.only(top: 6),
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          ),
        ],
      ),
    );
  }
}
