import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:quill_html_editor/quill_html_editor.dart';

class AnswerPage extends StatefulWidget {
  const AnswerPage({
    Key? key,
    required this.text,
  }) : super(key: key);
  final String text;

  @override
  State<AnswerPage> createState() => _AnswerPageState();
}

class _AnswerPageState extends State<AnswerPage> {
  final QuillEditorController controller = QuillEditorController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: OwAppBar(
          title: const Text('Edição'),
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(EvaIcons.closeCircleOutline),
            onPressed: () async {
              context.pop(widget.text);
            },
          ),
          actions: [
            IconButton(
              icon: const Row(
                children: [
                  Icon(EvaIcons.saveOutline),
                  SizedBox(width: 8),
                  Text('Salvar'),
                  SizedBox(width: 8),
                ],
              ),
              onPressed: () async {
                try {
                  final String text = await controller.getText();
                  context.pop(text);
                } catch (e) {
                  context.pop(widget.text);
                }
              },
            ),
          ],
        ),
        resizeToAvoidBottomInset: true,
        body: Column(
          children: [
            ToolBar(
              padding: const EdgeInsets.all(8),
              iconSize: 25,
              toolBarColor: Theme.of(context).colorScheme.background,
              activeIconColor: Theme.of(context).colorScheme.primary,
              iconColor: Theme.of(context).colorScheme.onBackground,
              controller: controller,
              crossAxisAlignment: WrapCrossAlignment.start,
              direction: Axis.horizontal,
            ),
            const OwDivider(),
            Expanded(
              child: QuillHtmlEditor(
                text: widget.text,
                hintText: 'Digite aqui...',
                controller: controller,
                isEnabled: true,
                ensureVisible: false,
                minHeight: 500,
                autoFocus: true,
                hintTextAlign: TextAlign.start,
                padding: const EdgeInsets.only(left: 10, top: 10),
                hintTextPadding: const EdgeInsets.only(left: 20),
                backgroundColor: Theme.of(context).colorScheme.background,
                textStyle: Theme.of(context).textTheme.bodyMedium,
                inputAction: InputAction.newline,
                loadingBuilder: (context) {
                  return const Center(
                    child: CircularProgressIndicator(
                    strokeWidth: 3,
                  ));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
