import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ListQuestionsFaqPage extends StatefulWidget {
  const ListQuestionsFaqPage({
    Key? key,
    required this.category,
  }) : super(key: key);
  final HelpCategoriesFaqEntity category;
  @override
  _ListQuestionsFaqPageState createState() => _ListQuestionsFaqPageState();
}

class _ListQuestionsFaqPageState extends State<ListQuestionsFaqPage> {
  List<HelpQuestionFaqEntity>? questions;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(title: const Text('Perguntas e respostas')),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        label: const Text('Adicionar pergunta'),
        icon: const Icon(Icons.add),
        onPressed: () async {
          final retorno = await context.push(
            '/${WhiteLabelEntity.current?.id}/dashboard/knowledge-base/category/${widget.category.id}/questions/new',
            extra: widget.category,
          );
          if (retorno is HelpQuestionFaqEntity) {
            questions!.insert(0, retorno);
            setState(() {});
          }
        },
      ),
      body: Visibility(
        visible: questions != null,
        replacement: Container(
          constraints: const BoxConstraints(
            minHeight: 300,
          ),
          child: const Center(child: CircularProgressIndicator()),
        ),
        child: Visibility(
          visible: questions?.isNotEmpty ?? false,
          replacement: Container(
            margin: appSM.isWeb
                ? const EdgeInsets.fromLTRB(50, 35, 50, 50)
                : const EdgeInsets.fromLTRB(25, 10, 25, 25),
            padding: const EdgeInsets.all(25),
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                color: Theme.of(context).colorScheme.outlineVariant,
              ),
              borderRadius: const BorderRadius.all(Radius.circular(15)),
            ),
            child: const Center(
              child: Text(
                'Você ainda não cadastrou nenhuma pergunta. Adicione a primeira para vincular as respostas.',
              ),
            ),
          ),
          child: ListView.builder(
            padding: const EdgeInsets.all(25),
            itemCount: questions?.length ?? 0,
            itemBuilder: (context, index) {
              final HelpQuestionFaqEntity question = questions![index];
              return Ink(
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.secondaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(16)),
                ),
                child: InkWell(
                  onTap: () async {
                    final retorno = await context.push(
                      '/${WhiteLabelEntity.current?.id}/dashboard/knowledge-base/category/${widget.category.id}/questions/${question.id}',
                      extra: <String, dynamic>{
                        'question': question,
                        'category': widget.category,
                      },
                    );
                    if (retorno is HelpQuestionFaqEntity) {
                      questions![index] = retorno;
                      setState(() {});
                    } else if (retorno == true) {
                      questions!.remove(index);
                      setState(() {});
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 15,
                      horizontal: 25,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          question.title,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text('${question.category?.title}'),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void callApi() async {
    try {
      questions = await Api.knowledgeBase.listQuestions(
        params: {
          'skip': questions?.length ?? 0,
          'categoryId': widget.category.id
        },
      );
      setState(() {});
    } catch (e) {
      OwBotToast.close();
      OwBotToast.toast(e.toString());
    }
  }
}
