import 'package:backoffice/ui/components/alerta_simples.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class KnowledgeBaseCategoryCrudPage extends StatefulWidget {
  const KnowledgeBaseCategoryCrudPage({
    Key? key,
    this.category,
  }) : super(key: key);
  final HelpCategoriesFaqEntity? category;

  @override
  State<KnowledgeBaseCategoryCrudPage> createState() =>
      _KnowledgeBaseCategoryCrudPageState();
}

class _KnowledgeBaseCategoryCrudPageState
    extends State<KnowledgeBaseCategoryCrudPage> {
  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController icon = TextEditingController();
  MaskedTextController index = MaskedTextController(mask: '000');

  final List<String> _typeList = ['PJ', 'PF'];
  List<String> _filters = [];

  @override
  void initState() {
    super.initState();
    if (widget.category != null) {
      title.text = widget.category!.title ?? '';
      description.text = widget.category!.description ?? '';
      icon.text = widget.category!.icon ?? '';
      index.text = (widget.category!.index ?? 0).toString();
      _filters = widget.category!.type ?? [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Categoria'),
        actions: [
          Visibility(
            visible: widget.category != null,
            child: IconButton(
              icon: const Row(
                children: [
                  Icon(EvaIcons.questionMarkCircleOutline),
                  SizedBox(width: 8),
                  Text('Perguntas'),
                  SizedBox(width: 8),
                ],
              ),
              onPressed: () {
                context.push(
                    '/${WhiteLabelEntity.current?.id}/dashboard/knowledge-base/category/${widget.category?.id}/questions',
                    extra: widget.category!);
              },
            ),
          ),
          Visibility(
            visible: widget.category != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar esta categoria?',
                  description:
                      'Esta ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.knowledgeBase
                        .deleteCategory(id: widget.category!.id!);
                    if (mounted) {
                      context.go('/${WhiteLabelEntity.current?.id}/dashboard');
                    }
                  } catch (_) {
                    OwBotToast.toast('Erro ao remover categoria.');
                  }
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(
          vertical: 15,
          horizontal: 25,
        ),
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwTextField(
              controller: title,
              labelText: 'Nome da categoria',
              hintText: 'Escreva aqui...',
              maxLength: 50,
            ),
            OwTextField(
              controller: description,
              margin: const EdgeInsets.only(top: 25),
              labelText: 'Descrição da categoria',
              hintText: 'Escreva aqui...',
              maxLength: 50,
            ),
            OwTextField(
              controller: index,
              margin: const EdgeInsets.only(top: 25),
              labelText: 'Index (Ordem de exibição na tela)',
              keyboardType: TextInputType.phone,
              hintText: '0',
              maxLength: 3,
            ),
            OwTextField(
              controller: icon,
              margin: const EdgeInsets.only(top: 20),
              enableInteractiveSelection: false,
              labelText: 'Ícone da categoria',
              hintText: 'Clique para escolher um ícone',
              readOnly: true,
              onTap: () async {
                final result = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/selected-icon',
                );
                if (result != null && result is String) {
                  icon.text = result;
                  setState(() {});
                }
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 25,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Selecione para quais ambientes essa categoria ficará visível:',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  const SizedBox(height: 10),
                  Wrap(
                    spacing: 5.0,
                    children: _typeList.map((item) {
                      return OwChipWidget.filter(
                        label: item,
                        selected: _filters.contains(item),
                        showCheckmark: false,
                        onSelected: (bool value) {
                          setState(() {
                            if (value) {
                              if (!_filters.contains(item)) {
                                _filters.add(item);
                              }
                            } else {
                              _filters.removeWhere((String name) {
                                return name == item;
                              });
                            }
                          });
                        },
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: EdgeInsets.fromLTRB(
            15,
            15,
            15,
            MediaQuery.viewInsetsOf(context).bottom + 15,
          ),
          labelText: 'Salvar',
          onPressed: () async {
            final List<String> t = _filters;
            if (title.text.trim().isEmpty || index.text.trim().isEmpty) {
              showDialog(
                context: context,
                builder: (context) => const AlertaSimples(
                  'Existem um ou mais campos que precisam ser preenchidos.',
                ),
              );
            } else {
              if (widget.category != null) {
                final HelpCategoriesFaqEntity category =
                    widget.category!.copyWith(
                  index: int.tryParse(index.text) ?? 0,
                  type: t,
                  title: title.text.trim(),
                  description: description.text,
                  icon: icon.text,
                );
                try {
                  OwBotToast.loading();
                  final response = await Api.knowledgeBase.updateCategory(
                    id: widget.category!.id!,
                    data: category,
                  );
                  if (mounted) {
                    context.pop(response);
                    showOwDialog(
                      context: context,
                      title: 'Categoria atualizada com sucesso.',
                    );
                  }
                } catch (e) {
                  OwBotToast.close();
                  if (mounted) {
                    showOwDialog(
                      context: context,
                      title: 'Ocorreu um erro',
                      description: '$e',
                    );
                  }
                }
              } else {
                final HelpCategoriesFaqEntity category =
                    HelpCategoriesFaqEntity(
                  index: int.tryParse(index.text) ?? 0,
                  type: t,
                  title: title.text.trim(),
                  description: description.text,
                  icon: icon.text,
                );
                try {
                  final response =
                      await Api.knowledgeBase.insertCategory(data: category);
                  if (mounted) {
                    context.pop(response);
                    showOwDialog(
                      context: context,
                      title: 'Categoria cadastrada com sucesso.',
                    );
                  }
                } catch (e) {
                  if (mounted) {
                    showOwDialog(
                      context: context,
                      title: 'Ocorreu um erro',
                      description: '$e',
                    );
                  }
                }
              }
            }
          },
        ),
      ),
    );
  }
}
