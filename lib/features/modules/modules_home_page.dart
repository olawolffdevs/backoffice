import 'package:backoffice/features/modules/module_crud_page.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

import 'module_view_page.dart';

class ModulesHomePage extends StatefulWidget {
  const ModulesHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<ModulesHomePage> createState() => _ModulesHomePageState();
}

class _ModulesHomePageState extends State<ModulesHomePage> with AutomaticKeepAliveClientMixin<ModulesHomePage> {
  @override
  bool get wantKeepAlive => true;

  List<ModulePjmei>? modules;
  List<String> pagesGenerate = [];
  TextEditingController search = TextEditingController();

  int? sortColumnIndex;
  bool isAscending = false;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        title: const OwText('Módulos'),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await callApi();
        },
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              _pagesListHorizontal(
                title: 'Telas do app',
                list: pages,
                showTitle: true,
              ),
              _pagesListHorizontal(
                title: 'Telas geradas',
                list: pagesGenerate,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 24, top: 12),
                    constraints: const BoxConstraints(
                      maxWidth: 290,
                    ),
                    child: OwTextField(
                      widthBorder: 2,
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(16)),
                        ),
                        fillColor: Theme.of(context).colorScheme.primaryContainer,
                        hintText: 'Buscar...',
                        suffixIcon: IconButton(
                          icon: const Icon(EvaIcons.searchOutline),
                          onPressed: () {
                            setState(() {});
                          },
                        ),
                      ),
                      outlined: true,
                      controller: search,
                      onFieldSubmitted: (value) {
                        setState(() {});
                      },
                    ),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.all(24),
                constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width,
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.surface,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16),
                  ),
                  border: Border.all(
                    width: 1,
                    color: Theme.of(context).colorScheme.surfaceVariant,
                  ),
                ),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16),
                  ),
                  child: ScrollConfiguration(
                    behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                      PointerDeviceKind.touch,
                      PointerDeviceKind.mouse,
                    }),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(
                        border: const TableBorder(
                          borderRadius: const BorderRadius.all(Radius.circular(16)),
                        ),
                        headingRowColor: MaterialStatePropertyAll(
                          Theme.of(context).colorScheme.primaryContainer,
                        ),
                        sortAscending: isAscending,
                        sortColumnIndex: sortColumnIndex,
                        columns: getColumns(['Nome', 'Tela', 'Visual', 'Data', 'Tipo Visual', 'Visual']),
                        rows: getRows(Valid.text(search.text) ? searchData() : modules),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 100),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        icon: const Icon(
          Icons.add,
        ),
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.onPrimary,
        label: Text(
          'Novo módulo',
          style: TextStyle(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        onPressed: () async {
          final result = await openLinkPage(
            context,
            OpenLinkPageParams.basic(
              child: ModuleCrudPage(
                routes: pages,
              ),
            ),
          );
          if (result is ModulePjmei) {
            modules ??= [];
            modules!.add(result);
            setState(() {});
          }
        },
      ),
    );
  }

  Future<void> callApi() async {
    try {
      OwBotToast.loading();
      modules = await Api.module.listAll(params: {'skip': modules?.length ?? 0});
      OwBotToast.close();
      pagesGenerate = getNewPages(modules ?? [], pages);
    } catch (e) {
      OwBotToast.close();
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: 'Tente novamente mais tarde.',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }

  List<String> getNewPages(List<ModulePjmei> modules, List<String> pages) {
  return modules.where((module) => !pages.contains(module.typeScreen)).map((module) => module.typeScreen).toSet().toList();
}

  Widget _pagesListHorizontal({
    required String title,
    required List<String> list,
    bool showTitle = false,
  }) {
    if(list.isEmpty) {
      return const SizedBox();
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        OwText(
          '$title',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(
            fontWeight: FontWeight.bold,
          ),
          padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
        ),
        ScrollConfiguration(
          behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
            PointerDeviceKind.touch,
            PointerDeviceKind.mouse,
          }),
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(left: 24, right: 16),
            scrollDirection: Axis.horizontal,
            child: Row(
              children: list.map((e) => Container(
                margin: const EdgeInsets.only(right: 8),
                constraints: const BoxConstraints(
                  maxWidth: 300,
                ),
                child: Ink(
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.primaryContainer,
                    borderRadius: const BorderRadius.all(Radius.circular(16)),
                  ),
                  child: InkWell(
                    onTap: () {
                      openLinkPage(
                        context,
                        OpenLinkPageParams.basic(
                          child: ModuleViewPage(
                            modules: (modules ?? []).where((value) => value.typeScreen == e).toList(),
                          ),
                        ),
                      );
                    },
                    borderRadius: const BorderRadius.all(Radius.circular(16)),
                    child: Padding(
                      padding: const EdgeInsets.all(24),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Visibility(
                            visible: showTitle,
                            child: OwText(
                              convertEnumsPages(e),
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                              padding: const EdgeInsets.only(bottom: 4),
                            ),
                          ),
                          Text(e),
                        ],
                      ),
                    ),
                  ),
                ),
              )).toList(),
            ),
          ),
        ),
        const SizedBox(height: 24),
        const OwDivider(),
        const SizedBox(height: 12),
      ],
    );
  }

  List<DataColumn> getColumns(List<String> columns) => columns.map((column) {
    return DataColumn(
      label: Flexible(child: Text(column)),
      onSort: onSort, 
    );
  }).toList();

  List<DataRow> getRows(List<ModulePjmei>? users) => users?.map((ModulePjmei user) {
    final cells = [
      user.title,
      convertEnumsPages(user.typeScreen),
      user.displayType,
      user.createdAt,
      user.image?['type'] ?? '',
      user.image?['value'] ?? '',
    ];
    return DataRow(
      cells: getCells(cells, user),
    );
  }).toList() ?? <DataRow>[];

  List<DataCell> getCells(List<dynamic> cells, ModulePjmei user) => cells.map((cell) {
    return DataCell(
      Text('$cell'),
      onTap: () async {
        final result = await openLinkPage(
          context,
          OpenLinkPageParams.basic(
            child: ModuleCrudPage(
              module: user,
              routes: pages,
            ),
          ),
        );
        if (result is ModulePjmei) {
          final int value = modules?.indexWhere((element) {
            return element.id == user.id;
          }) ?? -1;
          if (value != -1) {
            modules![value] = result.copyWith();
          }
          setState(() {});
        }
      }
    );
  }).toList();

  List<ModulePjmei> searchData() {

    final s = normalizarTexto(search.text);

    return modules!.where((usuario) {
      final t = normalizarTexto('${usuario.title}');
      final d = normalizarTexto('${usuario.description}');
      final ts = normalizarTexto('${usuario.typeScreen}');
      final dt = normalizarTexto('${usuario.displayType}');

      return t.contains(s) || d.contains(s) || ts.contains(s) || dt.contains(s);
    }).toList();
  }

  void onSort(int columnIndex, bool ascending) {

    if(columnIndex == 0) {
      modules!.sort((user1, user2) => compareString(ascending, '${user1.title}', '${user2.title}'));
    } else if (columnIndex == 1) {
      modules!.sort((user1, user2) => compareString(ascending, '${user1.typeScreen}', '${user2.typeScreen}'));
    } else if (columnIndex == 2) {
      modules!.sort((user1, user2) => compareString(ascending, '${user1.displayType}', '${user2.displayType}'));
    } else if (columnIndex == 3) {
      modules!.sort((user1, user2) => compareString(ascending, '${user1.createdAt}', '${user2.createdAt}'));
    } else if (columnIndex == 4) {
      modules!.sort((user1, user2) => compareString(ascending, '${user1.image?['type']}', '${user2.image?['type']}'));
    } else if (columnIndex == 5) {
      modules!.sort((user1, user2) => compareString(ascending, '${user1.image?['value']}', '${user2.image?['value']}'));
    }

    setState(() {
      this.sortColumnIndex = columnIndex;
      this.isAscending = ascending;
    });
  }

  int compareString(bool ascending, String value1, String value2) {
    return ascending ? value1.compareTo(value2) : value2.compareTo(value1);
  }

  String normalizarTexto(String texto) {
    final semAcentos = texto.toLowerCase().replaceAll(RegExp(r'[áàãâä]'), 'a')
        .replaceAll(RegExp(r'[éèêë]'), 'e')
        .replaceAll(RegExp(r'[íìîï]'), 'i')
        .replaceAll(RegExp(r'[óòõôö]'), 'o')
        .replaceAll(RegExp(r'[úùûü]'), 'u')
        .replaceAll(RegExp(r'[ç]'), 'c')
        .replaceAll(RegExp(r'[^a-z0-9]'), ''); // Remove pontuações e mantém apenas letras e números
    return semAcentos;
  }

  String convertEnumsPages(String? value) {
    return switch (value) {
      'dashboard_pf_page' => 'INICIO DA PF',
      'home_company_page' => 'INICIO DA PJ',
      'home_ecommerce_page' => 'INICIO LOJA VIRTUAL',
      'profile_user_page' => 'PERFIL PF',
      'profile_company_page' => 'PERFIL PJ',
      'profile_ecommerce_page' => 'PERFIL LOJA VIRTUAL',
      'register_company_home_page' => 'PQ CADASTRAR EMPRESA NO APP',
      'open_company' => 'ABRIR CNPJ',
      'support_company_page' => 'ME AJUDA',
      'indicate_app_page' => 'INDICAR APP',
      'mei_management_company_page' => 'GESTÃO DO MEI',
      'nota_fiscal' => 'NOTA FISCAL',
      'paginas_verde_company_page' => 'PAGINAS VERDE',
      'tax_center_company_page' => 'CENTRO DE TAXAS DA PJ',
      _ => value ?? 'Inválido',
    };
  }

  List<String> pages = [
    'dashboard_pf_page', // TELA INICIAL DA PESSOA FISICA ATUAL
    'home_company_page', // TELA INICIAL DA EMPRESA ATUAL
    'home_ecommerce_page', // TELA INICIAL DA LOJA VIRTUAL ATUAL
    'profile_user_page', // TELA DE PERFIL DA PESSOA FISICA ATUAL
    'profile_company_page', // TELA DE PERFIL DA EMPRESA ATUAL
    'profile_ecommerce_page', // TELA DE PERFIL DA LOJA VIRTUAL ATUAL
    'support_company_page', // TELA DE ME AJUDA
    'indicate_app_page', // TELA DE INDICACAO DO APP
    'register_company_home_page', // TELA DE PQ CADASTRAR A EMPRESA
    'open_company', // TELA DE TIPOS DE ABERTURA DE EMPRESA
    'mei_management_company_page', // TELA GESTAO DO MEI
    'nota_fiscal', // TELA DE NOTA FISCAL
    'paginas_verde_company_page', // PAGINAS VERDE DA EMPRESA ATUAL
    'tax_center_company_page', // CENTRAL DE TAXAS DA EMPRESA ATUAL
  ];

  List<String> widgets = [
    'LGM01',
    'LGM02',
    'LGM03',
    'LGM04',
    'LHM01',
    'LHM02',
    'LHM03',
    'LHM04',
    'LHM05',
    'LHM06',
    'LHM07',
    'LVM01',
    'LVM02',
    'LVM03',
    'LVM04',
    'LNBM',
    'LFABM',
    'LAC',
    'DEVELOPMENT',
    '_',
  ];
}