import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ModuleCrudPage extends StatefulWidget {
  const ModuleCrudPage({
    Key? key,
    this.module,
    this.routes = const [],
  }) : super(key: key);
  final ModulePjmei? module;
  final List<String> routes;

  @override
  State<ModuleCrudPage> createState() => _ModuleCrudPageState();
}

class _ModuleCrudPageState extends State<ModuleCrudPage> {
  int selectedIndex = 0;
  String typeUrl = 'link externo';

  // dynamic page
  bool dynamicPage = false;
  TextEditingController titleDynamicPage = TextEditingController();
  TextEditingController descriptionDynamicPage = TextEditingController();
  TextEditingController idDynamicPage = TextEditingController();
  TextEditingController titleAppBarDynamicPage = TextEditingController();

  // basic
  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController index = TextEditingController();
  TextEditingController route = TextEditingController();

  // disponibilidade
  TextEditingController minimalVersion = TextEditingController();
  TextEditingController maximalVersion = TextEditingController();
  bool largeScreen = true;
  bool smallScreen = true;
  bool isWeb = true;
  bool isAndroid = true;
  bool isIos = true;

  // visual
  String? displayTypeValue;
  String? spotlightValue;
  String? styleWidgetValue;
  String elementVisual = 'icon';
  TextEditingController screenValue = TextEditingController();
  TextEditingController elementVisualText = TextEditingController();

  // opcionais
  bool webview = false;
  bool openLink = false;
  bool openModal = false;
  bool openModalSelectUserAccountWidget = false;
  bool openModalSelectCompanieWidget = false;
  bool switchHideValueGlobal = false;
  bool buttonSelected = false;
  bool isBorder = false;
  bool loading = false;
  String? usecase;
  TextEditingController idUsecase = TextEditingController();
  TextEditingController labelButton = TextEditingController();

  List<String> hideOnPlans = [];
  List<String> activeOnPlans = [];
  List<String> groups = [];
  List<String> suggestionsList = [];

  List<String> reservados = [
    '#companyId',
    '#companyFantasyName',
    '#companyCorporateName',
    '#companyDocument',
    '#userId',
    '#userName',
    '#userFirstName',
    '#userDocument',
    '#storeId',
    '#storeName',
    '#moduleId',
  ];

  List<FocusNode> focus = FN.initialilzeFocusNodeList(5);
  List<FocusNode> focus2 = FN.initialilzeFocusNodeList(2);

  @override
  void initState() {
    super.initState();
    if (widget.module != null) {
      // dynamic page
      dynamicPage = widget.module!.params?['isDynamicPage'] ?? false;
      titleDynamicPage.text = widget.module!.params?['titleDynamicPage'] ?? '';
      descriptionDynamicPage.text =
          widget.module!.params?['descriptionDynamicPage'] ?? '';
      idDynamicPage.text = widget.module!.params?['idDynamicPage'] ?? '';
      titleAppBarDynamicPage.text =
          widget.module!.params?['titleAppBarDynamicPage'] ?? '';

      // basic
      title.text = widget.module!.title ?? '';
      description.text = widget.module!.description ?? '';
      index.text = widget.module!.index.toString();
      route.text = widget.module!.route ?? '';

      // disponibilidade
      minimalVersion.text = widget.module!.minimalVersion.toString();
      if (widget.module!.maximalVersion != null) {
        maximalVersion.text = widget.module!.maximalVersion.toString();
      }
      largeScreen = widget.module!.params?['largeScreen'] ?? true;
      smallScreen = widget.module!.params?['smallScreen'] ?? true;
      isWeb = widget.module!.params?['isWeb'] ?? true;
      isAndroid = widget.module!.params?['isAndroid'] ?? true;
      isIos = widget.module!.params?['isIos'] ?? true;

      groups = widget.module!.groups ?? [];
      activeOnPlans = widget.module!.activeOnPlans ?? [];
      hideOnPlans = widget.module!.hideOnPlans ?? [];

      // visual
      displayTypeValue = widget.module!.displayType;

      if (!displayType.toString().contains(displayTypeValue ?? '')) {
        displayTypeValue = '_';
      }
      spotlightValue = widget.module!.spotlight;
      styleWidgetValue = widget.module!.params?['styleWidget'] ?? 'background';
      screenValue.text = widget.module!.typeScreen;
      if (displayTypeValue?.isEmpty ?? true) {
        displayTypeValue = '_';
      }
      elementVisualText.text = widget.module!.image?['value'] ?? '';
      elementVisual = widget.module!.image?['type'] ?? 'icon';
      elementVisual = elementVisual.toString().toLowerCase();

      // opcionais
      labelButton.text = widget.module!.params?['labelButton'] ?? '';
      webview = widget.module!.params?['webview'] ?? false;
      openLink = widget.module!.params?['openLink'] ?? false;
      openModal = widget.module!.params?['openModal'] ?? false;
      openModalSelectUserAccountWidget =
          widget.module!.params?['openModalSelectUserAccountWidget'] ?? false;
      openModalSelectCompanieWidget =
          widget.module!.params?['openModalSelectCompanieWidget'] ?? false;
      switchHideValueGlobal =
          widget.module!.params?['switchHideValueGlobal'] ?? false;
      buttonSelected = widget.module!.params?['buttonSelected'] ?? false;
      isBorder = widget.module!.params?['isBorder'] ?? false;
      loading = widget.module!.params?['loading'] ?? false;
      usecase = widget.module!.params?['usecase'];
      idUsecase.text = widget.module!.params?['id'] ?? '';

      if (dynamicPage) {
        typeUrl = 'tela dinâmica';
      } else if (route.text.contains('https://') ||
          route.text.contains('http://')) {
        typeUrl = 'link externo';
      } else if (route.text == 'null' || route.text == '/off') {
        typeUrl = 'nenhum lugar';
      } else if (widget.routes.contains(route.text)) {
        typeUrl = 'tela do aplicativo';
      } else if (openModalSelectUserAccountWidget ||
          openModalSelectCompanieWidget ||
          switchHideValueGlobal) {
        typeUrl = 'função dentro do app';
      } else {
        typeUrl = 'tela do aplicativo';
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    screenValue.dispose();
    elementVisualText.dispose();
    titleDynamicPage.dispose();
    descriptionDynamicPage.dispose();
    idDynamicPage.dispose();
    titleAppBarDynamicPage.dispose();
    minimalVersion.dispose();
    maximalVersion.dispose();
    idUsecase.dispose();
    labelButton.dispose();
    title.dispose();
    description.dispose();
    index.dispose();
    route.dispose();
    FN.disposeFocusNodeList(focus);
    FN.disposeFocusNodeList(focus2);
  }

  @override
  Widget build(BuildContext context) {
    print(widget.module);
    return Scaffold(
      appBar: OwAppBar(
        elevation: 0,
        scrolledUnderElevation: 0,
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Visibility(
            visible: widget.module != null,
            child: IconButton(
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final result = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar este módulo?',
                  description: 'Esta ação não pode ser desfeita. Uma vez excluída, serão perdidos todos os dados.',
                  buttons: [
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                    OwButton(
                      labelText: 'Apagar',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                  ],
                );
                if (result == true) {
                  OwBotToast.loading();
                  try {
                    await Api.module.delete(id: widget.module!.id!);
                    if (mounted) {
                      OwBotToast.close();
                      context.go('/${WhiteLabelEntity.current?.id}/dashboard');
                    }
                  } catch (e) {
                    OwBotToast.close();
                    OwBotToast.toast('$e');
                  }
                }
              },
            ),
          ),
        ],
        title: const Text('Módulo'),
      ),
      body: IndexedStack(
        children: [
          basicScreen(),
          navigationScreen(),
          designScreen(),
          moreScreen(),
        ],
        index: selectedIndex,
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwButton(
              radius: 0,
              labelText: 'Salvar módulo',
              height: 55,
              onPressed: _onPressed,
            ),
            NavigationBar(
              onDestinationSelected: (value) {
                setState(() {
                  selectedIndex = value;
                });
              },
              destinations: const [
                NavigationDestination(
                  icon: Icon(EvaIcons.edit2Outline),
                  label: 'Básico',
                ),
                NavigationDestination(
                  icon: Icon(EvaIcons.compassOutline),
                  label: 'Destino',
                ),
                NavigationDestination(
                  icon: Icon(EvaIcons.colorPaletteOutline),
                  label: 'Visual',
                ),
                NavigationDestination(
                  icon: Icon(EvaIcons.browserOutline),
                  label: 'Mais',
                ),
              ],
              selectedIndex: selectedIndex,
            ),
          ],
        ),
      ),
    );
  }

  Widget basicScreen() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          sectionTitle(
            title: 'Título do módulo',
          ),
          RawKeyboardListener(
            focusNode: focus[0],
            onKey: (RawKeyEvent event) {
              if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                FN.nextFn(context, focus[1]);
              }
            },
            child: OwTextField(
              margin: const EdgeInsets.symmetric(horizontal: 25),
              hintText: 'Digite aqui...',
              helperText: 'Nome que o usuário irá ver na tela do aplicativo',
              controller: title,
              focusNode: focus[0],
              onFieldSubmitted: (_) {
                FN.nextFn(context, focus[1]);
              },
              onChanged: (valor) {
                setState(() {});
              },
            ),
          ),
          const SizedBox(height: 25),
          sectionTitle(
            title: 'Descrição do módulo',
          ),
          RawKeyboardListener(
            focusNode: focus[1],
            onKey: (RawKeyEvent event) {
              if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                FN.nextFn(context, focus[2]);
              }
            },
            child: OwTextField(
              margin: const EdgeInsets.symmetric(horizontal: 25),
              hintText: 'Digite aqui...',
              controller: description,
              maxLines: 4,
              textInputAction: TextInputAction.newline,
              keyboardType: TextInputType.multiline,
            ),
          ),
          const SizedBox(height: 25),
          sectionTitle(
            title: 'Posição na tela',
            description:
                'Defina um index - número inteiro - para definir a posição em que o módulo irá aparecer na tela.',
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.remove),
                  onPressed: () {
                    setState(() {
                      final value = int.tryParse(index.text) ?? 2;
                      if (value > 1) {
                        index.text = '${value - 1}';
                      }
                    });
                  },
                ),
                Expanded(
                  child: RawKeyboardListener(
                    focusNode: focus[3],
                    onKey: (RawKeyEvent event) {
                      if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                        FN.nextFn(context, focus[4]);
                      }
                    },
                    child: OwTextField(
                      margin: const EdgeInsets.symmetric(horizontal: 16),
                      hintText: 'Digite aqui...',
                      controller: index,
                      focusNode: focus[3],
                      onFieldSubmitted: (_) {
                        FN.nextFn(context, focus[4]);
                      },
                      onChanged: (valor) {
                        setState(() {});
                      },
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.add),
                  onPressed: () {
                    setState(() {
                      final value = int.tryParse(index.text) ?? 1;
                      index.text = '${value + 1}';
                    });
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 25),
          sectionTitle(
              title: 'Versão mínima',
              description:
                  'Qual a versão mínima do aplicativo para que este módulo apareça para o usuário? A partir da versão (x) o módulo aparecerá na tela do usuário.'),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.remove),
                  onPressed: () {
                    setState(() {
                      final value = int.tryParse(minimalVersion.text) ?? 2;
                      if (value > 1) {
                        minimalVersion.text = '${value - 1}';
                      }
                    });
                  },
                ),
                Expanded(
                  child: RawKeyboardListener(
                    focusNode: focus[4],
                    onKey: (RawKeyEvent event) {
                      if (event.isKeyPressed(LogicalKeyboardKey.tab)) {
                        FN.unfocusFn(context);
                      }
                    },
                    child: OwTextField(
                      margin: const EdgeInsets.symmetric(horizontal: 16),
                      hintText: 'Digite aqui...',
                      controller: minimalVersion,
                      focusNode: focus[4],
                      onFieldSubmitted: (_) {
                        FN.unfocusFn(context);
                      },
                      onChanged: (valor) {
                        setState(() {});
                      },
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.add),
                  onPressed: () {
                    setState(() {
                      final value = int.tryParse(minimalVersion.text) ?? 1;
                      minimalVersion.text = '${value + 1}';
                    });
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 25),
          sectionTitle(
              title: 'Versão máxima (opc)',
              description:
                  'Versão máxima do aplicativo para o módulo deixar de funcionar. A partir da versão * o módulo não aparecerá na tela do usuário.'),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.remove),
                  onPressed: () {
                    setState(() {
                      final value = int.tryParse(maximalVersion.text) ?? 2;
                      if (value > 1) {
                        maximalVersion.text = '${value - 1}';
                      }
                    });
                  },
                ),
                Expanded(
                  child: OwTextField(
                    margin: const EdgeInsets.symmetric(horizontal: 16),
                    hintText: 'Digite aqui...',
                    controller: maximalVersion,
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.add),
                  onPressed: () {
                    setState(() {
                      final value = int.tryParse(maximalVersion.text) ?? 1;
                      maximalVersion.text = '${value + 1}';
                    });
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 60),
        ],
      ),
    );
  }

  Widget navigationScreen() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          sectionTitle(
            title: 'Em qual tela vai aparecer esse módulo?',
          ),
          OwTextField(
            margin: const EdgeInsets.symmetric(horizontal: 25),
            hintText: 'Digite aqui...',
            controller: screenValue,
            focusNode: focus2[0],
            onFieldSubmitted: (_) {
              FN.nextFn(context, focus2[1]);
            },
            onChanged: (valor) {
              setState(() {});
            },
          ),
          const SizedBox(height: 25),
          sectionTitle(
            title: 'Ao clicar, o usuário vai ser levado para',
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Wrap(
              spacing: 8,
              runSpacing: 2,
              children: [
                'link externo',
                'tela do aplicativo',
                'tela dinâmica',
                'função dentro do app',
                'nenhum lugar',
              ].map((e) {
                return FilterChip(
                  label: Text(e),
                  selected: typeUrl == e,
                  onSelected: (value) {
                    typeUrl = e;
                    if (typeUrl == 'nenhum lugar') {
                      route.text = 'null';
                    } else if (typeUrl == 'função dentro do app') {
                      route.text = 'null';
                    } else if (typeUrl == 'tela dinâmica') {
                      route.text = '/dynamic-page';
                    } else {
                      route.text = ' ';
                    }
                    setState(() {});
                  },
                );
              }).toList(),
            ),
          ),
          Visibility(
            visible: route.text != 'null',
            child: Visibility(
              visible: typeUrl == 'tela dinâmica',
              child: Container(
                padding: const EdgeInsets.all(25),
                margin: const EdgeInsets.symmetric(vertical: 15),
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.primaryContainer,
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    OwText(
                      'Detalhes da tela dinâmica',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    OwTextField(
                      controller: idDynamicPage,
                      margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                      labelText: 'ID da tela dinâmica',
                      hintText: 'Digite aqui...',
                    ),
                    OwTextField(
                      controller: titleDynamicPage,
                      margin: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                      labelText: 'Título da tela dinâmica',
                      hintText: 'Digite aqui...',
                    ),
                    OwTextField(
                      controller: titleAppBarDynamicPage,
                      margin: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                      labelText: 'Título da appBar na tela dinâmica',
                      hintText: 'Digite aqui...',
                    ),
                    OwTextField(
                      controller: descriptionDynamicPage,
                      margin: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                      labelText: 'Descrição da tela dinâmica',
                      hintText: 'Digite aqui...',
                    ),
                  ],
                ),
              ),
              replacement: OwTextField.withSuggestions(
                controller: route,
                margin: const EdgeInsets.fromLTRB(25, 10, 25, 0),
                labelText: typeUrl,
                hintText: 'Digite aqui...',
                suggestionsList: suggestionsList,
                onChanged: (valor) {
                  setState(() {});
                },
              ),
            ),
          ),
          Visibility(
            visible: typeUrl == 'link externo',
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 0,
                      vertical: 10,
                    ),
                    title: Text(
                      'Abrir link no webview',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    subtitle: const Text(
                        'O link será aberto no próprio app em caso de link externo. O usuário poderá abrir no navegador se preferir.'),
                    trailing: Switch(
                      value: webview,
                      onChanged: ((value) {
                        setState(() {
                          webview = value;
                          if (value) {
                            openModalSelectCompanieWidget = false;
                            openModalSelectUserAccountWidget = false;
                          }
                        });
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: (typeUrl != 'link externo' && route.text != 'null') ||
                (typeUrl == 'link externo' && !webview),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 0,
                      vertical: 10,
                    ),
                    title: Text(
                      'Abrir link no openLink',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    subtitle: const Text(
                      'O link será aberto em um modal no meio da tela caso esteja em uma tela grande. Caso contrário, abrirá em tela inteira.',
                    ),
                    trailing: Switch(
                      value: openLink,
                      onChanged: ((value) {
                        setState(() {
                          openLink = value;
                          if (value) {
                            openModal = false;
                          }
                        });
                      }),
                    ),
                  ),
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 0,
                      vertical: 10,
                    ),
                    title: Text(
                      'Abrir no openModal',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    subtitle: const Text(
                        'O link será aberto em um modal(dialog) no meio da tela.'),
                    trailing: Switch(
                      value: openModal,
                      onChanged: ((value) {
                        setState(() {
                          openModal = value;
                          if (value) {
                            openLink = false;
                          }
                        });
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: typeUrl == 'função dentro do app',
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 0,
                      vertical: 10,
                    ),
                    title: Text(
                      'Abrir modal do usuário',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    subtitle: const Text(
                      'Será aberto uma tela para o usuário ver as principais opções e trocar ou sair da conta.',
                    ),
                    trailing: Switch(
                      value: openModalSelectUserAccountWidget,
                      onChanged: ((value) {
                        setState(() {
                          openModalSelectUserAccountWidget = value;
                          if (value) {
                            openModalSelectCompanieWidget = false;
                            switchHideValueGlobal = false;
                          }
                        });
                      }),
                    ),
                  ),
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 0,
                      vertical: 10,
                    ),
                    title: Text(
                      'Abrir modal da empresa',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    subtitle: const Text(
                      'Será aberto uma tela para o usuário ver as principais ações referente a empresa atual.',
                    ),
                    trailing: Switch(
                      value: openModalSelectCompanieWidget,
                      onChanged: ((value) {
                        setState(() {
                          openModalSelectCompanieWidget = value;
                          if (value) {
                            openModalSelectUserAccountWidget = false;
                            switchHideValueGlobal = false;
                          }
                        });
                      }),
                    ),
                  ),
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 0,
                      vertical: 10,
                    ),
                    title: Text(
                      'Olho mágico',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    subtitle: const Text(
                      'Quando o usuário clicar nesse módulo será alterado a visibilidade do olho mágico.',
                    ),
                    trailing: Switch(
                      value: switchHideValueGlobal,
                      onChanged: ((value) {
                        setState(() {
                          switchHideValueGlobal = value;
                          if (value) {
                            openModalSelectUserAccountWidget = false;
                            openModalSelectCompanieWidget = false;
                          }
                        });
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 25),
        ],
      ),
    );
  }

  Widget designScreen() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          sectionTitle(
            title: 'Estilo visual',
          ),
          ScrollConfiguration(
            behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
              PointerDeviceKind.touch,
              PointerDeviceKind.mouse,
            }),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              padding: const EdgeInsets.only(left: 25, right: 17),
              child: Row(
                children: [
                  'icon',
                  'network',
                ].map((e) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: FilterChip(
                      label: Text(styleVisualEnum(e)),
                      selected: elementVisual == e,
                      onSelected: (value) {
                        setState(() {
                          elementVisual = e;
                        });
                      },
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
          Visibility(
            visible: elementVisual == 'icon',
            child: OwButton.outline(
              margin: const EdgeInsets.fromLTRB(25, 10, 25, 0),
              height: 45,
              labelText: 'Escolher ícone pela lista',
              onPressed: () async {
                final result = await context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/selected-icon',
                );
                if (result != null && result is String) {
                  elementVisualText.text = result;
                  setState(() {});
                }
              },
            ),
          ),
          OwTextField(
            controller: elementVisualText,
            margin: const EdgeInsets.fromLTRB(25, 10, 25, 0),
            labelText: 'Ícone ou url da imagem aqui',
          ),
          const SizedBox(height: 25),
          sectionTitle(
            title: 'Texto de destaque',
          ),
          OwDropdown(
            margin: const EdgeInsets.fromLTRB(25, 10, 25, 0),
            value: spotlightValue,
            optionsList: spotlight,
            onChanged: (String? item) {
              setState(() {
                spotlightValue = item;
              });
            },
          ),
          const SizedBox(height: 25),
          sectionTitle(
            title: 'Tipo visual',
          ),
          OwDropdown(
            margin: const EdgeInsets.fromLTRB(25, 10, 25, 0),
            value: displayTypeValue,
            optionsList: displayType,
            onChanged: (String? item) {
              setState(() {
                displayTypeValue = item;
              });
            },
          ),
          const SizedBox(height: 25),
          sectionTitle(
            title: 'Cor do módulo',
          ),
          OwGrid.builder(
            padding: const EdgeInsets.fromLTRB(25, 10, 25, 0),
            numbersInRowAccordingToWidgth: [
              170,
              220,
              270,
              320,
              370,
              420,
              470,
              520,
              570,
              620
            ],
            itemCount: styleWidget.length,
            itemBuilder: (context, index) {
              return CardAtalhos(
                onPressed: () {
                  setState(() {
                    styleWidgetValue = styleWidget[index];
                  });
                },
                selecionado: true,
                labelColor: Colors.white,
                border: BorderSide(
                  width: styleWidget[index] == styleWidgetValue ? 8 : 1,
                  color: Theme.of(context).colorScheme.onBackground,
                ),
                selectedColor: returnColor(styleWidget[index], context),
              );
            },
          ),
          const SizedBox(height: 25),
          sectionTitle(
            title: 'Visual do módulo',
          ),
          Visibility(
            visible: displayTypeValue == 'LAC' ||
                displayTypeValue == 'LFABM' ||
                displayTypeValue == 'LNBM' ||
                displayTypeValue == 'DEVELOPMENT' ||
                displayTypeValue == '-',
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 25),
              child: Text(
                  'Não é possível visualizar o visual deste módulo. O \'tipo visual\' escolhido será mostrado corretamente, porém você não pode visualizar ele aqui.'),
            ),
            replacement: IgnorePointer(
              child: ListModulesWidget(
                modules: [
                  ModulePjmei(
                    title: Valid.text(title.text) ? title.text.trim() : null,
                    description: Valid.text(description.text)
                        ? description.text.trim()
                        : null,
                    index: int.tryParse(index.text.trim()) ?? 0,
                    image: {
                      'type': elementVisual.toString().toUpperCase(),
                      'value': elementVisualText.text,
                    },
                    route: route.text.trim(),
                    minimalVersion:
                        int.tryParse(minimalVersion.text.trim()) ?? 9999,
                    maximalVersion: int.tryParse(maximalVersion.text.trim()),
                    displayType: displayTypeValue ?? 'LGM01',
                    typeScreen: screenValue.text.trim(),
                    spotlight: spotlightValue == '' ? null : spotlightValue,
                    activeOnPlans: activeOnPlans.isEmpty ? null : activeOnPlans,
                    groups: groups.isEmpty ? null : groups,
                    hideOnPlans: hideOnPlans.isEmpty ? null : hideOnPlans,
                    whiteLabel: '${WhiteLabelEntity.current?.id}',
                    params: {
                      'webview': webview,
                      'labelButton': labelButton.text,
                      'titleDynamicPage': titleDynamicPage.text,
                      'descriptionDynamicPage': descriptionDynamicPage.text,
                      'idDynamicPage': idDynamicPage.text,
                      'isDynamicPage': dynamicPage,
                      'titleAppBarDynamicPage': titleAppBarDynamicPage.text,
                      'buttonSelected': buttonSelected,
                      'isBorder': isBorder,
                      'isWeb': isWeb,
                      'isAndroid': isAndroid,
                      'isIos': isIos,
                      'smallScreen': smallScreen,
                      'largeScreen': largeScreen,
                      'styleWidget': styleWidgetValue,
                      'openLink': openLink,
                      'openModal': openModal,
                      'openModalSelectUserAccountWidget':
                          openModalSelectUserAccountWidget,
                      'openModalSelectCompanieWidget':
                          openModalSelectCompanieWidget,
                      'switchHideValueGlobal': switchHideValueGlobal,
                      'usecase': usecase,
                      'loading': loading,
                      'id': idUsecase.text,
                    },
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 40),
        ],
      ),
    );
  }

  Widget moreScreen() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.all(25),
            child: box(
              title: 'Visibilidade',
              children: [
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 0,
                    vertical: 10,
                  ),
                  title: Text(
                    'Visível em tela grande',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  subtitle: const Text('Quando tiver na versão de tela grande'),
                  trailing: Switch(
                    value: largeScreen,
                    onChanged: ((value) {
                      setState(() {
                        largeScreen = value;
                      });
                    }),
                  ),
                ),
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 0,
                    vertical: 10,
                  ),
                  title: Text(
                    'Visível em tela pequena',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  subtitle:
                      const Text('Quando tiver na versão de tela pequena'),
                  trailing: Switch(
                    value: smallScreen,
                    onChanged: ((value) {
                      setState(() {
                        smallScreen = value;
                      });
                    }),
                  ),
                ),
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 0,
                    vertical: 10,
                  ),
                  title: Text(
                    'Visível no Web',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  subtitle: const Text('Quando tiver na versão de navegador'),
                  trailing: Switch(
                    value: isWeb,
                    onChanged: ((value) {
                      setState(() {
                        isWeb = value;
                      });
                    }),
                  ),
                ),
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 0,
                    vertical: 10,
                  ),
                  title: Text(
                    'Visível no Android',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  subtitle: const Text('Quando tiver na versão de celular'),
                  trailing: Switch(
                    value: isAndroid,
                    onChanged: ((value) {
                      setState(() {
                        isAndroid = value;
                      });
                    }),
                  ),
                ),
                ListTile(
                  contentPadding: const EdgeInsets.only(top: 10),
                  title: Text(
                    'Visível no iOS',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  subtitle: const Text('Quando tiver na versão de celular'),
                  trailing: Switch(
                    value: isIos,
                    onChanged: ((value) {
                      setState(() {
                        isIos = value;
                      });
                    }),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 25),
            child: box(
              title: 'Opcionais',
              children: [
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 0,
                  ),
                  title: Text(
                    'Exibir borda?',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  subtitle: const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        'O módulo terá uma borda ao redor para diferenciar em relação ao fundo.',
                      ),
                    ],
                  ),
                  trailing: Switch(
                    value: isBorder,
                    onChanged: ((value) {
                      setState(() {
                        isBorder = value;
                      });
                    }),
                  ),
                ),
                const SizedBox(height: 15),
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 0,
                  ),
                  title: Text(
                    'Botão em destaque?',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  subtitle: const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        'Em alguns widgets específicos podem aparecer um botão extra. Ao marcar, você mostrará uma cor diferente dos demais.',
                      ),
                    ],
                  ),
                  trailing: Switch(
                    value: buttonSelected,
                    onChanged: ((value) {
                      setState(() {
                        buttonSelected = value;
                      });
                    }),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget sectionTitle({required String title, String? description}) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 25, 25, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          OwText(
            title,
            style: Theme.of(context).textTheme.titleLarge?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          Visibility(
            visible: Valid.text(description),
            child: OwText(
              '$description',
              padding: const EdgeInsets.only(top: 6),
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          ),
        ],
      ),
    );
  }

  List<String> displayType = [
    'LGM01',
    'LGM02',
    'LGM03',
    'LGM04',
    'LHM01',
    'LHM02',
    'LHM03',
    'LHM04',
    'LHM05',
    'LHM06',
    'LHM07',
    'LVM01',
    'LVM02',
    'LVM03',
    'LVM04',
    'LNBM',
    'LFABM',
    'LAC',
    'DEVELOPMENT',
    '_',
  ];

  List<String> spotlight = [
    'Em breve',
    'Novo',
    'Beta',
    'Dica',
    'Premium',
    'Desabilitado',
    'Manutenção',
    'AD',
    'Pra você',
    'Desenvolvimento',
    'Em testes',
    '',
  ];

  List<String> styleWidget = [
    'background',
    'primary',
    'primaryContainer',
    'secondary',
    'secondaryContainer',
    'tertiary',
    'tertiaryContainer',
    'error',
    'errorContainer',
    'surfaceVariant',
    'inversePrimary',
  ];

  List<String> screen = [
    'company_page',
    'dashboard_pf_page',
    'dashboard_pj_page',
    'individual_page',
    'home_company_page',
    'home_ecommerce_page',
    'home_individual_page',
    'finances_company_page',
    'finances_ecommerce_page',
    'finances_individual_page',
    'profile_company_page',
    'profile_ecommerce_page',
    'profile_user_page',
    'profile_user_page_global',
    'support_company_page',
    'support_ecommerce_page',
    'support_individual_page',
    'indicate_app_page',
    'open_company_page',
    'mei_management_company_page',
    'nota_fiscal_page',
    'paginas_verde_company_page',
    'tax_center_company_page',
  ];

  Widget box({required String title, required List<Widget> children}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.primaryContainer,
            borderRadius: const BorderRadius.vertical(
              top: Radius.circular(10),
            ),
          ),
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
            vertical: 10,
          ),
          child: Text(
            title,
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                  color: Theme.of(context).colorScheme.onPrimaryContainer,
                ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.secondaryContainer,
            borderRadius: const BorderRadius.vertical(
              bottom: Radius.circular(10),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: children,
            ),
          ),
        )
      ],
    );
  }

  Color returnColor(String color, BuildContext context) {
    switch (color) {
      case 'background':
        return Theme.of(context).colorScheme.background;
      case 'primary':
        return Theme.of(context).colorScheme.primary;
      case 'primaryContainer':
        return Theme.of(context).colorScheme.primaryContainer;
      case 'secondary':
        return Theme.of(context).colorScheme.secondary;
      case 'secondaryContainer':
        return Theme.of(context).colorScheme.secondaryContainer;
      case 'tertiary':
        return Theme.of(context).colorScheme.tertiary;
      case 'tertiaryContainer':
        return Theme.of(context).colorScheme.tertiaryContainer;
      case 'error':
        return Theme.of(context).colorScheme.error;
      case 'errorContainer':
        return Theme.of(context).colorScheme.errorContainer;
      case 'surfaceVariant':
        return Theme.of(context).colorScheme.surfaceVariant;
      case 'inversePrimary':
        return Theme.of(context).colorScheme.inversePrimary;
      default:
        return Theme.of(context).colorScheme.background;
    }
  }

  String styleVisualEnum(String style) {
    switch (style) {
      case 'icon':
        return 'Ícone';
      case 'network':
        return 'Imagem da internet';
      default:
        return 'Inválido';
    }
  }

  void _onPressed() async {
    if (
        index.text.trim().isEmpty ||
        minimalVersion.text.trim().isEmpty ||
        displayTypeValue == null ||
        styleWidgetValue == null ||
        screenValue.text.trim().isEmpty) {
          print(route.text);
          print(index.text);
          print(minimalVersion.text);
          print(displayTypeValue);
          print(styleWidgetValue);
          print(screenValue.text);
          print('oi');
      showOwDialog(
        context: context,
        title: 'Ops, faltou preencher algumas coisinhas',
        description: 'Existe um ou mais campos que precisam ser marcados.',
        buttons: [
          OwButton.text(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    } else {
      if (typeUrl == 'nenhum lugar') {
        route.text = 'null';
      }

      if (typeUrl != 'link externo') {
        webview = false;
      }

      if (typeUrl != 'função dentro do app') {
        openModalSelectCompanieWidget = false;
        openModalSelectUserAccountWidget = false;
        switchHideValueGlobal = false;
      }

      if (typeUrl == 'tela dinâmica') {
        dynamicPage = true;
        route.text = '/dynamic-page';
      } else {
        dynamicPage = false;
      }

      final Map<String, String> imageData = {
        'type': elementVisual.toString().toUpperCase(),
        'value': elementVisualText.text,
      };
      if (widget.module != null) {
        final ModulePjmei module = widget.module!.copyWith(
          title: Valid.text(title.text) ? title.text.trim() : null,
          description:
              Valid.text(description.text) ? description.text.trim() : null,
          index: int.tryParse(index.text.trim()) ?? 0,
          image: imageData,
          route: route.text.trim(),
          minimalVersion: int.tryParse(minimalVersion.text.trim()) ?? 9999,
          maximalVersion: int.tryParse(maximalVersion.text.trim()),
          displayType: displayTypeValue!,
          typeScreen: screenValue.text.trim(),
          spotlight: spotlightValue == '' ? null : spotlightValue,
          activeOnPlans: activeOnPlans.isEmpty ? null : activeOnPlans,
          groups: groups.isEmpty ? null : groups,
          hideOnPlans: hideOnPlans.isEmpty ? null : hideOnPlans,
          whiteLabel: WhiteLabelEntity.current?.id,
          params: {
            'webview': webview,
            'labelButton': labelButton.text,
            'titleDynamicPage': titleDynamicPage.text,
            'descriptionDynamicPage': descriptionDynamicPage.text,
            'idDynamicPage': idDynamicPage.text,
            'isDynamicPage': dynamicPage,
            'titleAppBarDynamicPage': titleAppBarDynamicPage.text,
            'buttonSelected': buttonSelected,
            'isBorder': isBorder,
            'isWeb': isWeb,
            'isAndroid': isAndroid,
            'isIos': isIos,
            'smallScreen': smallScreen,
            'largeScreen': largeScreen,
            'styleWidget': styleWidgetValue,
            'openLink': openLink,
            'openModal': openModal,
            'openModalSelectUserAccountWidget':
                openModalSelectUserAccountWidget,
            'openModalSelectCompanieWidget': openModalSelectCompanieWidget,
            'switchHideValueGlobal': switchHideValueGlobal,
            'usecase': usecase,
            'loading': loading,
            'id': idUsecase.text,
          },
        );

        try {
          OwBotToast.loading();
          final temp =
              await Api.module.update(id: '${module.id}', data: module);
          if (mounted) {
            Navigator.pop(context, temp);
            await showOwDialog(
              context: context,
              title: 'Módulo atualizado com sucesso.',
            );
          }
        } catch (e) {
          OwBotToast.close();
          if (mounted) {
            showOwDialog(
              context: context,
              title: 'Ocorreu um erro',
              description: '$e',
            );
          }
        }
      } else {
        final Map<String, dynamic> newT = {
          'description':
              Valid.text(description.text) ? description.text.trim() : null,
          'index': int.tryParse(index.text.trim()) ?? 0,
          'route': route.text.trim(),
          'title': Valid.text(title.text) ? title.text.trim() : null,
          'displayType': displayTypeValue!,
          'image': imageData,
          'minimalVersion': int.tryParse(minimalVersion.text.trim()) ?? 9999,
          'maximalVersion': int.tryParse(maximalVersion.text.trim()),
          'type_screen': screenValue.text.trim(),
          'spotlight': spotlightValue,
          'activeOnPlans': activeOnPlans.isEmpty ? null : activeOnPlans,
          'groups': groups.isEmpty ? null : groups,
          'hideOnPlans': hideOnPlans.isEmpty ? null : hideOnPlans,
          'whiteLabel': WhiteLabelEntity.current?.id,
          'params': {
            'webview': webview,
            'labelButton': labelButton.text,
            'titleDynamicPage': titleDynamicPage.text,
            'descriptionDynamicPage': descriptionDynamicPage.text,
            'idDynamicPage': idDynamicPage.text,
            'isDynamicPage': dynamicPage,
            'titleAppBarDynamicPage': titleAppBarDynamicPage.text,
            'buttonSelected': buttonSelected,
            'isBorder': isBorder,
            'isWeb': isWeb,
            'isAndroid': isAndroid,
            'isIos': isIos,
            'smallScreen': smallScreen,
            'largeScreen': largeScreen,
            'styleWidget': styleWidgetValue,
            'openLink': openLink,
            'openModal': openModal,
            'openModalSelectUserAccountWidget':
                openModalSelectUserAccountWidget,
            'openModalSelectCompanieWidget': openModalSelectCompanieWidget,
            'switchHideValueGlobal': switchHideValueGlobal,
            'usecase': usecase,
            'loading': loading,
            'id': idUsecase.text,
          }
        };
        try {
          OwBotToast.loading();
          final response = await Api.module.insert(data: newT);
          if (mounted) {
            if (Navigator.canPop(context)) {
              Navigator.pop(context, response);
            } else {
              context.go('/${WhiteLabelEntity.current?.id}/dashboard');
            }
            showOwDialog(
              context: context,
              title: 'Módulo adicionado com sucesso.',
            );
          }
        } catch (e) {
          OwBotToast.close();
          if (mounted) {
            showOwDialog(
              context: context,
              title: 'Ocorreu um erro',
              description: '$e',
            );
          }
        }
      }
    }
  }
}
