import 'dart:ui';

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ModuleViewPage extends StatefulWidget {
  const ModuleViewPage({
    Key? key,
    required this.modules,
  }) : super(key: key);
  final List<ModulePjmei> modules;

  @override
  State<ModuleViewPage> createState() => _ModuleViewPageState();
}

class _ModuleViewPageState extends State<ModuleViewPage> {

  List<TabModuleWidget> _screens = [];

  @override
  void initState() {
    super.initState();
    _screens.add(TabModuleWidget(
      title: 'Início',
      icon: EvaIcons.homeOutline,
      page: const SizedBox(),
    ));
    widget.modules.forEach((element) {
      if (element.displayType == 'TAB' || element.displayType == 'LNBM') {
        _screens.add(
          TabModuleWidget(
            title: element.title ?? '',
            icon: IconAdapter.getIcon(element.image?['value'] ?? ''),
            page: const SizedBox(),
          ),
        );
      }
      moduleSM.addModule(element);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      appBar: OwAppBar(
        title: const Text('Demonstração (não funcional)'),
        leading: IconButton(
          icon: const Icon(EvaIcons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          CardActionListWidget(
            list: (widget.modules).where((value) => value.displayType == 'LAC').toList(),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            IgnorePointer(
              child: ListModulesWidget(
                modules: widget.modules,
              ),
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),
      floatingActionButton: IgnorePointer(
        child: BoxFloatingButtonWidget(
          list: (widget.modules).where((value) => value.displayType == 'LFABM').toList(),
        ),
      ),
      bottomNavigationBar: _screens.length > 1 ? IgnorePointer(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 5,
            vertical: 20,
          ),
          child: Container(
            alignment: Alignment.bottomCenter,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(80),
              ),
              
            ),
            constraints: const BoxConstraints(
              maxWidth: 350,
              maxHeight: 104,
            ),
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(80)),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                child: Container(
                  padding: const EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    borderRadius:const  BorderRadius.all(
                      Radius.circular(80),
                    ),
                    color: Theme.of(context).colorScheme.background.withOpacity(.88),
                    border: Border.all(
                      width: 1,
                      color: Theme.of(context).colorScheme.outlineVariant,
                    ),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: _screens.asMap().map((i, e) => MapEntry(i, NavigationDestinationButton(
                      title: e.title,
                      icon: e.icon,
                      selected: _screens[0].title == e.title,
                      onDestinationSelected: () {},
                    ))).values.toList(),
                  ),
                ),
              ),
            ),
          ),
        ),
      ) : null,
    );
  }
}