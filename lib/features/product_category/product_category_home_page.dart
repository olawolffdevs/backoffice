import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ProductCategoryHomePage extends StatefulWidget {
  const ProductCategoryHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<ProductCategoryHomePage> createState() => _ProductCategoryHomePageState();
}

class _ProductCategoryHomePageState extends State<ProductCategoryHomePage> {
  List<ProductCategoryEcommerceEntity> _productCategories = [];

  @override
  void initState() {
    super.initState();
    callApiCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
        centerTitle: false,
        titleSpacing: 25,
        title: const Text('Categorias dos produtos'),
        actions: [
          IconButton(
            tooltip: 'Adicionar novo item',
            icon: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6),
              child: Row(
                children: [
                  const Icon(Icons.add),
                  const SizedBox(width: 4),
                  Text(
                    'Novo',
                    style: Theme.of(context).textTheme.titleSmall?.copyWith(
                          color: Theme.of(context).colorScheme.primary,
                        ),
                  ),
                ],
              ),
            ),
            onPressed: () async {
              await context.push(
                  '/${WhiteLabelEntity.current?.id}/ecommerce/product/category/add');
              await callApiCategories();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwGrid.builder(
              shrinkWrap: true,
              itemCount: _productCategories.length,
              padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
              spacing: 25,
              runSpacing: 25,
              numbersInRowAccordingToWidgth: [500, 850, 1150, 1450],
              itemBuilder: (context, index) {
                return Material(
                  type: MaterialType.transparency,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.primaryContainer,
                      borderRadius: const BorderRadius.all(Radius.circular(14)),
                    ),
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(14)),
                      onTap: () async {
                        await context.push(
                          '/${WhiteLabelEntity.current?.id}/ecommerce/product/category/add',
                          extra: {
                            'category': _productCategories[index],
                          },
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 24,
                          vertical: 20,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              '${_productCategories[index].name}',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> callApiCategories() async {
    try {
      OwBotToast.loading();
      _productCategories = await Api.ecommerce.productCategory.listByEcommerce(
          id: '${WhiteLabelEntity.current?.setting.ecommerceId}');
      OwBotToast.close();
    } catch (e) {
      OwBotToast.close();
    }
    if (mounted) {
      setState(() {});
    }
  }
}
