import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ProductCategoryCrudPage extends StatefulWidget {
  const ProductCategoryCrudPage(this.category, {super.key});
  final ProductCategoryEcommerceEntity? category;

  @override
  State<ProductCategoryCrudPage> createState() => _ProductCategoryCrudPageState(this.category);
}

class _ProductCategoryCrudPageState extends State<ProductCategoryCrudPage> {
  _ProductCategoryCrudPageState(this.category);

  final ProductCategoryEcommerceEntity? category;

  List<FocusNode> fnListAtualizar = FN.initialilzeFocusNodeList(2);

  TextEditingController categoryName = TextEditingController();
  TextEditingController categoryIndex = TextEditingController();

  @override
  void initState(){
    super.initState();
    categoryName.text = category?.name ?? '';
    categoryIndex.text = (category?.index ?? 0).toString();
  }

  @override
  void dispose(){
    super.dispose();
    FN.disposeFocusNodeList(fnListAtualizar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: OwText('${category != null ? 'Editar' : 'Criar'} categoria'.toUpperCase()),
        actions: [
          Visibility(
            visible: category != null,
            child: IconButton(
              tooltip: 'Excluir',
              icon: const Icon(EvaIcons.trash2Outline),
              onPressed: () async {
                final confirmar = await showOwDialog(
                  context: context,
                  title: 'Deseja apagar esaa categoria?',
                  description: 'Essa ação não poderá ser desfeita',
                  buttons: [
                    OwButton.text(
                      labelText: 'Remover',
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    ),
                    OwButton.text(
                      labelText: 'Manter',
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                    ),
                  ],
                );
                if (confirmar == true) {
                  try{
                    OwBotToast.loading();
                    Api.ecommerce.productCategory.delete(id: category?.id ?? '');
                    context.pop();
                    OwBotToast.close();
                  }
                  catch(e){
                    OwBotToast.close();
                  }          
                }
              },
            ),
          ),
        ],
      ),
      body: _body(),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          onPressed: () async {
            if (category != null){
              await callUpdate();
              context.pop();
            }
            else{
              await callCreate();
              context.pop();
            }
          },
          margin: EdgeInsets.fromLTRB(
            25,
            25,
            25,
            MediaQuery.viewInsetsOf(context).bottom + 25,
          ),
          labelText: '${category != null ? 'Atualizar' : 'Criar'} categoria',
        ),
      ),
    );
  }

  Widget _body(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        OwText(
          'Categorias',
          style: Theme.of(context).textTheme.titleLarge?.copyWith(
            fontWeight: FontWeight.bold,
          ),
          padding: const EdgeInsets.fromLTRB(24, 24, 24, 10),
        ),

        const SizedBox(height: 20,),

        RawKeyboardListener(
          focusNode: fnListAtualizar[0],
          onKey: (RawKeyEvent event) {
            if (event.isKeyPressed(LogicalKeyboardKey.tab)){
              FN.nextFn(context, fnListAtualizar[1]);
            }
          },
          child: OwTextField(
            margin: const EdgeInsets.symmetric(
              horizontal: 25,
              vertical: 10,
            ),
            controller: categoryName,
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            labelText: 'Nome da categoria',
            maxLines: 12,
            focusNode: fnListAtualizar[0],
            onFieldSubmitted: (_) {
              FN.nextFn(context, fnListAtualizar[1]);
            },
            onChanged: (valor) {
              setState(() {});
            },
          ),
        ),

        RawKeyboardListener(
          focusNode: fnListAtualizar[1],
          onKey: (RawKeyEvent event) {
            if (event.isKeyPressed(LogicalKeyboardKey.tab)){
              FN.unfocusFn(context);
            }
          },
          child: OwTextField(
            margin: const EdgeInsets.symmetric(
              horizontal: 25,
              vertical: 10,
            ),
            controller: categoryIndex,
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            labelText: 'Index',
            maxLines: 12,
            focusNode: fnListAtualizar[1],
            onFieldSubmitted: (_) {
              FN.unfocusFn(context);
            },
            onChanged: (valor) {
              setState(() {});
            },
          ),
        ),
      ],
    );
  }

  Future<void> callCreate() async {
    try{
      OwBotToast.loading();
      await Api.ecommerce.productCategory.insert(name: '${categoryName.text.toString()}', index: int.parse(categoryIndex.text.toString()), ecommerceId: '${WhiteLabelEntity.current?.setting.ecommerceId}');
      OwBotToast.close();
    }
    catch(e){
      OwBotToast.close();
    }
  }

  Future<void> callUpdate() async {
    try{
      OwBotToast.loading();
      final ProductCategoryEcommerceEntity newCategory = ProductCategoryEcommerceEntity(
        id: category?.id ?? '',
        name: categoryName.text,
        index: int.parse(categoryName.text),
      );
      Api.ecommerce.productCategory.update(id: category?.id ?? '', data: newCategory);
      OwBotToast.close();
    }
    catch(e){
      OwBotToast.close();
    }
  }
}