// import 'package:backoffice/domain/entities/group_entity.dart';
// import '../../../../../plugin_pjmei_components/lib/main/factories/usecases/groups/list_groups_factory.dart';
// import 'package:flutter/material.dart';
// import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

// class SelectGroupsPage extends StatefulWidget {
//   final List<String> groups;
//   const SelectGroupsPage({
//     Key? key,
//     required this.groups,
//   }) : super(key: key);

//   @override
//   State<SelectGroupsPage> createState() => SelectGroupsPageState();
// }

// class SelectGroupsPageState extends State<SelectGroupsPage> {

//   List<GroupEntity>? groups;
//   List<String> groupsSelected = [];

//   @override
//   void initState() {
//     super.initState();
//     callApi();
//     groupsSelected = widget.groups;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: SingleChildScrollView(
//           physics: const BouncingScrollPhysics(),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: [
//               const SizedBox(height: 25),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   horizontal: 25,
//                   vertical: 10,
//                 ),
//                 child: Text(
//                   "Selecione os grupos",
//                   style: Theme.of(context).textTheme.headlineLarge,
//                 ),
//               ),
//               const SizedBox(height: 15),
//                 Visibility(
//                   visible: groups != null,
//                   replacement: const Center(
//                     child: CircularProgressIndicator(),
//                   ),
//                   child: ListView.separated(
//                     shrinkWrap: true,
//                     physics: const NeverScrollableScrollPhysics(),
//                     padding: const EdgeInsets.symmetric(horizontal: 25),
//                     itemCount: groups?.length ?? 0,
//                     separatorBuilder: (context, index) {
//                       return const SizedBox(height: 25);
//                     },
//                     itemBuilder: (context, index) {
//                       return Ink(
//                         decoration: BoxDecoration(
//                           borderRadius: const BorderRadius.all(Radius.circular(15)),
//                           color: Theme.of(context).colorScheme.secondaryContainer,
//                         ),
//                         child: InkWell(
//                           borderRadius: const BorderRadius.all(Radius.circular(15)),
//                           onTap: () async {
//                             setState(() {
//                               if(groupsSelected.toString().contains(groups![index].id!)) {
//                                 groupsSelected.remove(groups![index].id!);
//                               } else {
//                                 groupsSelected.add(groups![index].id!);
//                               }
//                             });
//                           },
//                           child: Padding(
//                             padding: const EdgeInsets.fromLTRB(
//                               25,
//                               25,
//                               25,
//                               25,
//                             ),
//                             child: Row(
//                               children: [
//                                 Expanded(
//                                   child: Column(
//                                     crossAxisAlignment: CrossAxisAlignment.stretch,
//                                     children: [
//                                       Text(
//                                         groups![index].name,
//                                         style: const TextStyle(
//                                           fontWeight: FontWeight.bold,
//                                           fontSize: 16,
//                                         ),
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                                 Checkbox(
//                                   value: groupsSelected.toString().contains(groups![index].id!), 
//                                   onChanged: (value) {
//                                     setState(() {
//                                       if(groupsSelected.toString().contains(groups![index].id!)) {
//                                         groupsSelected.remove(groups![index].id!);
//                                       } else {
//                                         groupsSelected.add(groups![index].id!);
//                                       }
//                                     });
//                                   },
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                       );
//                     },
//                   ),
//                 ),
//             ],
//           ),
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         child: const Icon(EvaIcons.saveOutline),
//         onPressed: () {
//           Navigator.pop(context, groupsSelected);
//         },
//       ),
//     );
//   }

//   void callApi([skip = 0]) async {
//     try {
//       groups = await makeRemoteListGroups({"skip": groups?.length ?? 0}).exec();
//     } catch (_) {}
//     setState(() {});
//   }
// }