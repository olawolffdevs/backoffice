import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class SelectPlanPage extends StatefulWidget {
  const SelectPlanPage({super.key});

  @override
  State<SelectPlanPage> createState() => _SelectPlanPageState();
}

class _SelectPlanPageState extends State<SelectPlanPage> {
  List<PlanEntity>? plans;

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        title: const Text('Selecionar plano'),
      ),
      body: plans == null
          ? const Center(child: CircularProgressIndicator())
          : ListView.separated(
              physics: const BouncingScrollPhysics(),
              padding: const EdgeInsets.symmetric(horizontal: 25),
              itemCount: plans?.length ?? 0,
              separatorBuilder: (context, index) {
                return const SizedBox(height: 25);
              },
              itemBuilder: (context, index) {
                return Ink(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                    color: Theme.of(context).colorScheme.secondaryContainer,
                  ),
                  child: InkWell(
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                    onTap: () async {
                      Navigator.pop(context, plans![index]);
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(
                        25,
                        25,
                        0,
                        25,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            plans![index].name,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          const SizedBox(
                            height: 6,
                          ),
                          Text(
                            '${plans![index].description}',
                          ),
                          const SizedBox(
                            height: 6,
                          ),
                          Text(
                            OwFormat.toRealCurrency(plans![index].price, isInt: true),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
    );
  }

  void callApi([skip = 0]) async {
    try {
      plans = await Api.plans.list(query: {'skip': plans?.length ?? 0});
    } catch (_) {}
    setState(() {});
  }
}
