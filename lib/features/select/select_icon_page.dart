import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class ViewIconsListPage extends StatefulWidget {
  const ViewIconsListPage({super.key});

  @override
  State<ViewIconsListPage> createState() => _ViewIconsListPageState();
}

class _ViewIconsListPageState extends State<ViewIconsListPage> {
  List _icons = [];

  @override
  void initState() {
    super.initState();
    _icons = IconAdapter.map.keys.toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 25,
        ),
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Ícones',
              style: Theme.of(context).textTheme.displayLarge,
            ),
            const SizedBox(
              height: 25,
            ),
            OwGrid.builder(
              itemCount: _icons.length,
              numbersInRowAccordingToWidgth: const [
                150,
                300,
                450,
                600,
                750,
                900
              ],
              itemBuilder: (context, index) {
                return Ink(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(
                        15,
                      ),
                    ),
                    color: Theme.of(context).colorScheme.secondaryContainer,
                  ),
                  child: InkWell(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(
                        15,
                      ),
                    ),
                    onTap: () {
                      context.pop(_icons[index].toString());
                    },
                    child: Container(
                      padding: const EdgeInsets.all(
                        15,
                      ),
                      child:
                          Icon(IconAdapter.getIcon(_icons[index].toString())),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
