// import 'package:backoffice/domain/entities/plan_entity.dart';
// import 'package:backoffice/main/factories/usecases/plans/list_plans_factory.dart';
// import 'package:flutter/material.dart';
// import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

// class SelectPlansPage extends StatefulWidget {
//   final List<String> plans;
//   const SelectPlansPage({
//     Key? key,
//     required this.plans,
//   }) : super(key: key);

//   @override
//   State<SelectPlansPage> createState() => SelectPlansPageState();
// }

// class SelectPlansPageState extends State<SelectPlansPage> {

//   List<PlanEntity>? plans;
//   List<String> plansSelected = [];

//   @override
//   void initState() {
//     super.initState();
//     callApi();
//     plansSelected = widget.plans;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: SingleChildScrollView(
//           physics: const BouncingScrollPhysics(),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: [
//               const SizedBox(height: 25),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   horizontal: 25,
//                   vertical: 10,
//                 ),
//                 child: Text(
//                   "Selecione os planos",
//                   style: Theme.of(context).textTheme.headlineLarge,
//                 ),
//               ),
//               const SizedBox(height: 15),
//               Visibility(
//                 visible: plans != null,
//                 replacement: const Center(
//                   child: CircularProgressIndicator(),
//                 ),
//                 child: ListView.separated(
//                   shrinkWrap: true,
//                   physics: const NeverScrollableScrollPhysics(),
//                   padding: const EdgeInsets.symmetric(horizontal: 25),
//                   itemCount: plans?.length ?? 0,
//                   separatorBuilder: (context, index) {
//                     return const SizedBox(height: 25);
//                   },
//                   itemBuilder: (context, index) {
//                     return Ink(
//                       decoration: BoxDecoration(
//                         borderRadius: const BorderRadius.all(Radius.circular(15)),
//                         color: Theme.of(context).colorScheme.secondaryContainer,
//                       ),
//                       child: InkWell(
//                         borderRadius: const BorderRadius.all(Radius.circular(15)),
//                         onTap: () async {
//                           setState(() {
//                             if(plansSelected.toString().contains(plans![index].id!)) {
//                               plansSelected.remove(plans![index].id!);
//                             } else {
//                               plansSelected.add(plans![index].id!);
//                             }
//                           });
//                         },
//                         child: Padding(
//                           padding: const EdgeInsets.fromLTRB(
//                             25,
//                             25,
//                             25,
//                             25,
//                           ),
//                           child: Row(
//                             children: [
//                               Expanded(
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.stretch,
//                                   children: [
//                                     Text(
//                                       plans![index].name,
//                                       style: const TextStyle(
//                                         fontWeight: FontWeight.bold,
//                                         fontSize: 16,
//                                       ),
//                                     ),
//                                     const SizedBox(
//                                       height: 6,
//                                     ),
//                                     Text(
//                                       OwFormat.toRealCurrency(plans![index].amount,
//                                           isInt: true),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               Checkbox(
//                                 value: plansSelected.toString().contains(plans![index].id!), 
//                                 onChanged: (value) {
//                                   setState(() {
//                                     if(plansSelected.toString().contains(plans![index].id!)) {
//                                       plansSelected.remove(plans![index].id!);
//                                     } else {
//                                       plansSelected.add(plans![index].id!);
//                                     }
//                                   });
//                                 },
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     );
//                   },
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         child: const Icon(EvaIcons.saveOutline),
//         onPressed: () {
//           Navigator.pop(context, plansSelected);
//         },
//       ),
//     );
//   }

//   void callApi([skip = 0]) async {
//     try {
//       plans = await makeRemoteListPlans({"skip": plans?.length ?? 0}).exec();
//     } catch (_) {}
//     setState(() {});
//   }
// }