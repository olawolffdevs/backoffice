import 'package:flutter/gestures.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:flutter/material.dart';

class RoadmapHomePage extends StatefulWidget {
  const RoadmapHomePage({super.key});

  @override
  State<RoadmapHomePage> createState() => _RoadmapHomePageState();
}

class _RoadmapHomePageState extends State<RoadmapHomePage> {

  List<RoadmapEntity>? roadmap;
  List<RoadmapEntity> roadmapSelected = [];
  String selected = '';

  @override
  void initState() {
    super.initState();
    callApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(),
      body: roadmap == null
        ? const Center(child: CircularProgressIndicator())
        : SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.all(24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      'Roteiro do produto',
                      style: Theme.of(context).textTheme.headlineLarge?.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 10),
                    Text(
                      'Mantenha-se atualizado sobre o que estamos trabalhando, os lançamentos planejados e nossos recursos mais recentes.',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                  ],
                ),
              ),
              ScrollConfiguration(
                behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                  PointerDeviceKind.touch,
                  PointerDeviceKind.mouse,
                }),
                child: SingleChildScrollView(
                  padding: const EdgeInsets.only(left: 24, right: 16),
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      CardAtalhos(
                        title: 'Todos',
                        selecionado: selected == '',
                        onPressed: () {
                          filter('');
                        },
                      ),
                      CardAtalhos(
                        title: 'Planejado',
                        selecionado: selected == 'planned',
                        onPressed: () {
                          filter('planned');
                        },
                      ),
                      CardAtalhos(
                        title: 'Trabalhando nisso',
                        selecionado: selected == 'working',
                        onPressed: () {
                          filter('working');
                        },
                      ),
                      CardAtalhos(
                        title: 'Em testes',
                        selecionado: selected == 'pre_launch',
                        onPressed: () {
                          filter('pre_launch');
                        },
                      ),
                      CardAtalhos(
                        title: 'Lançado',
                        selecionado: selected == 'launched',
                        onPressed: () {
                          filter('launched');
                        },
                      )
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 24),
              const OwDivider(),
              ListView.separated(
                itemBuilder: (context, index) {
                  return Ink(
                    padding: const EdgeInsets.all(24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          '${roadmapSelected[index].name}',
                          style: Theme.of(context).textTheme.titleLarge?.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 10),
                        Text(
                          '${roadmapSelected[index].description}',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        const SizedBox(height: 20),
                        Row(
                          children: [
                            HighlightMessageCard(
                              color: ColorSystem(context: context),
                              text: '${convertEnumsStatus(roadmapSelected[index].status)}',
                            ),
                            const SizedBox(width: 10),
                            Visibility(
                              visible: Valid.text(roadmapSelected[index].prevision),
                              child: HighlightMessageCard(
                                color: ColorSystem(context: context),
                                text: '${convertEnumsStatus(roadmapSelected[index].prevision)}',
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const OwDivider();
                },
                itemCount: roadmapSelected.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
              ),
            ],
          ),
        ),
    );
  }

  Future<void> callApi() async {
    try {
      roadmap = await Api.roadmap.list();
      roadmapSelected = roadmap ?? [];
    } catch (e) {
      context.pop();
      showOwDialog(
        context: context,
        title: 'Ocorreu um erro',
        description: 'Tente novamente mais tarde.',
        buttons: [
          OwButton(
            labelText: 'Fechar',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    }
    setState(() {});
  }

  void filter(String value) {
    selected = value;
    if(value == '') {
      roadmapSelected = roadmap ?? [];
    } else {
      roadmapSelected = roadmap?.where((element) => element.status == value).toList() ?? [];
    }
    setState(() {});
  }

  String convertEnumsStatus(String? value) {
    return switch (value) {
      'launched' => 'Lançado',
      'pre_launch' => 'Em breve',
      'working' => 'Trabalhando nisso',
      'planned' => 'Planejado',
      _ => value ?? 'Inválido',
    };
  }

}