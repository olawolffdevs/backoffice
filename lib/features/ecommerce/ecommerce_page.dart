import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class PanelEcommerceHomePage extends StatefulWidget {
  const PanelEcommerceHomePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<PanelEcommerceHomePage> createState() => _PanelEcommerceHomePageState();
}

class _PanelEcommerceHomePageState extends State<PanelEcommerceHomePage> {
  List<ModulePjmei> modules = [
    ModulePjmei(
      whiteLabel: '${WhiteLabelEntity.current?.id}',
      displayType: 'LGM01',
      title: 'Horário de funcionamento',
      route: '/${WhiteLabelEntity.current?.id}/dashboard/ecommerce/schedules',
      image: {
        'type': 'ICON',
        'value': 'EvaIcons.clockOutline',
      },
    ),
    ModulePjmei(
      whiteLabel: '${WhiteLabelEntity.current?.id}',
      displayType: 'LGM01',
      title: 'Quant. atendimento por vez',
      route: '/${WhiteLabelEntity.current?.id}/dashboard/ecommerce/max-participants-by-schedule',
      image: {
        'type': 'ICON',
        'value': 'EvaIcons.personDoneOutline',
      },
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      if (ecommerceSM.establishment?.name == null) {
        return const Material(
          child: Center(child: CircularProgressIndicator()),
        );
      }
      return Scaffold(
        appBar: OwAppBar(
          automaticallyImplyLeading: widget.automaticallyImplyLeading,
          title: const Text('Gestão da lojinha'),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Observer(builder: (context) {
                return Padding(
                  padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      OwText(
                        'Gerenciando: ${ecommerceSM.establishment?.name}',
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                      const SizedBox(height: 6),
                      OwText(
                        '${OwFormat.cpfCnpj(ecommerceSM.establishment?.documentNumber)}',
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                    ],
                  ),
                );
              }),
              ListModulesWidget(
                modules: modules,
              ),
            ],
          ),
        ),
      );
    });
  }
}
