import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class MaxParticipantsBySchedulePage extends StatefulWidget {
  const MaxParticipantsBySchedulePage({
    Key? key,
    required this.automaticallyImplyLeading,
  }) : super(key: key);
  final bool automaticallyImplyLeading;
  @override
  State<MaxParticipantsBySchedulePage> createState() => _MaxParticipantsBySchedulePageState();
}

class _MaxParticipantsBySchedulePageState extends State<MaxParticipantsBySchedulePage> {

  final limit = TextEditingController();
  bool delivery = false;
  bool online = false;
  bool presential = false;
  bool availableForScheduling = false;

  @override
  void initState() {
    super.initState();
    availableForScheduling = ecommerceSM.establishment?.availableForScheduling ?? false;
    limit.text = (ecommerceSM.establishment?.maxParticipantsBySchedule ?? 1).toString();
    if(ecommerceSM.establishment!.serviceMethods.contains('online')) {
      online = true;
    }
    if(ecommerceSM.establishment!.serviceMethods.contains('delivery')) {
      delivery = true;
    }
    if(ecommerceSM.establishment!.serviceMethods.contains('takeout')) {
      presential = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: OwAppBar(
        automaticallyImplyLeading: widget.automaticallyImplyLeading,
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            OwText(
              'Atendimento',
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                fontWeight: FontWeight.bold,
              ),
              padding: const EdgeInsets.symmetric(horizontal: 25),
            ),
            const SizedBox(height: 10),
            OwText(
              'Defina as regras de atendimento da sua lojinha. Como sua capacidade máxima de atendimento e modo de operação',
              style: Theme.of(context).textTheme.bodyLarge,
              padding: const EdgeInsets.symmetric(horizontal: 25),
            ),
            const SizedBox(height: 25),
            ListTile(
              contentPadding: const EdgeInsets.symmetric(horizontal: 25),
              title: const OwText('Receber agendamentos?'),
              trailing: Switch(
                value: availableForScheduling,
                onChanged: (valor) {
                  setState(() {
                    availableForScheduling = valor;
                  });
                },
              ),
            ),
            const SizedBox(height: 25),
            const OwDivider(),
            const SizedBox(height: 25),
            Visibility(
              visible: availableForScheduling,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  OwTextField(
                    controller: limit,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    hintText: '1',
                    labelText: 'Limite de atendimentos',
                    helperText: 'Quantos agendamentos você consegue atender por vez?',
                    margin: const EdgeInsets.symmetric(horizontal: 25),
                  ),
                  const SizedBox(height: 45),
                  OwText(
                    'Modo de operação',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                  ),
                  const SizedBox(height: 10),
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 25),
                    title: const OwText('Atendimento online'),
                    trailing: Switch(
                      value: online,
                      onChanged: (valor) {
                        setState(() {
                          online = valor;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 25),
                    title: const OwText('Atendimento no cliente'),
                    trailing: Switch(
                      value: delivery,
                      onChanged: (valor) {
                        setState(() {
                          delivery = valor;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 25),
                    title: const OwText('Atendimento no meu endereço'),
                    trailing: Switch(
                      value: presential,
                      onChanged: (valor) {
                        setState(() {
                          presential = valor;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
          ],
        ),
      ),
      bottomNavigationBar: OwBottomAppBar(
        child: OwButton(
          margin: const EdgeInsets.all(25),
          labelText: 'Atualizar',
          onPressed: () async {
            try {
              final serviceMethods = <String>[];
              if(online) serviceMethods.add('online');
              if(presential) serviceMethods.add('takeout');
              if(delivery) serviceMethods.add('delivery');
              OwBotToast.loading();
              ecommerceSM.establishment = await Api.ecommerce.update(
                id: '${WhiteLabelEntity.current?.setting.ecommerceId}',
                data: ecommerceSM.establishment!.copyWith(
                  availableForScheduling: availableForScheduling,
                  serviceMethods: serviceMethods,
                ),
              );
              OwBotToast.close();
              OwBotToast.toast('Atualizado com sucesso');
            } catch (e) {
              OwBotToast.close();
              showOwDialog(
                context: context,
                title: 'Ops',
                description: '$e',
                buttons: [
                  OwButton(
                    labelText: 'Fechar',
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}