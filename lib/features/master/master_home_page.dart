import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class MasterHomePage extends StatefulWidget {
  const MasterHomePage({Key? key}) : super(key: key);
  @override
  MasterHomePageState createState() => MasterHomePageState();
}

class MasterHomePageState extends State<MasterHomePage>
    with AutomaticKeepAliveClientMixin<MasterHomePage> {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: OwAppBar(
        title: const OwText(
          'Meu White Label',
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w500,
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            BasicHorizontalCard(
              title: 'Editar White Label atual',
              description: 'Editar dados da plataforma',
              leading: const Icon(EvaIcons.colorPaletteOutline),
              color: ColorSystem(context: context, type: 'secondaryContainer'),
              onPressed: () {
                context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/white-label/edit',
                  extra: WhiteLabelEntity.current!,
                );
              },
            ),
            const OwDivider(),
            BasicHorizontalCard(
              title: 'Adicionar colaborador',
              description: 'Adicione pessoas para te ajudar',
              leading: const Icon(EvaIcons.personAddOutline),
              color: ColorSystem(context: context, type: 'secondaryContainer'),
              onPressed: () {
                context.push(
                  '/${WhiteLabelEntity.current?.id}/dashboard/white-label/user/add',
                );
              },
            ),
            const OwDivider(),
          ],
        ),
      ),
    );
  }
}
