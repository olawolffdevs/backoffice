import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);
  @override
  SplashPageState createState() => SplashPageState();
}

class SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    _verifyUser();
    checkUserNotifier.addListener(() async {
      if (userSM.user == null) {
        final UserEntity? user = await UserDB.ler();
        if (user != null) {
          await UserDB.remover();
          if (mounted) {
            context.go('/');
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(child: CircularProgressIndicator()),
    );
  }

  void _verifyUser() async {
    final UserEntity? user = await UserDB.ler();
    if ((user != null && (user.name.isNotEmpty))) {
      try {
        userSM.setUser(user);
        OwBotToast.loading();
        final UserEntity temp = await Api.auth.newRefreshToken();
        userSM.setUser(temp);
        await UserDB.atualizar(temp);
        if (mounted) {
          context.go('/home');
        }
      } catch (e) {
        OwBotToast.toast('Erro ao atualizar informações do usuário');
        await UserDB.remover();
        if (mounted) {
          context.go('/login');
        }
      }
    } else {
      if (mounted) {
        context.go('/login');
      }
    }
  }
}
