import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class SettingsModel {
  
  SettingsModel({
    required this.name,
    required this.menu,
  });
  final String name;
  final List<MenuItemEntity> menu;
}