import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'routes.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    if(WhiteLabelEntity.current != null) {
      appSM.setWhiteLabel(backoffice);
    }
  }

  @override
  Widget build(BuildContext context) {
    return DynamicColorBuilder(
      builder: (lightColorScheme, darkColorScheme) {
        return Observer(
          builder: (context) {
            return MaterialApp.router(
              themeMode: appSM.themeMode,
              title: Valid.text(WhiteLabelEntity.current?.name) ? 'Backoffice: ${WhiteLabelEntity.current?.name}' : 'Backoffice',
              routerDelegate: routesBackoffice.routerDelegate,
              routeInformationParser: routesBackoffice.routeInformationParser,
              routeInformationProvider: routesBackoffice.routeInformationProvider,
              builder: ComponentsInit(
                webWidth: 600,
                isWeb: true,
                hidePrint: false,
                routers: {},
              ),
              shortcuts: {
                LogicalKeySet(LogicalKeyboardKey.space): const ActivateIntent(),
              },
              theme: AppTheme.getLightThemeApp(
                colorScheme: ColorScheme.fromSeed(seedColor: colorConvert(appSM.whiteLabel?.style.lightColorScheme ?? '#001d0d')),
                visualDensity: VisualDensity.standard,
                platform: TargetPlatform.iOS,
              ),
              darkTheme: AppTheme.getDarkThemeApp(
                colorScheme: ColorScheme.fromSeed(seedColor: colorConvert(appSM.whiteLabel?.style.darkColorScheme ?? '#001d0d'), brightness: Brightness.dark),
                visualDensity: VisualDensity.compact,
                platform: TargetPlatform.iOS,
              ),
              debugShowCheckedModeBanner: false,
              localizationsDelegates: const [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: const [
                Locale('pt', 'BR'),
              ],
              locale: const Locale('pt', 'BR'),
            );
          }
        );
      },
    );
  }
}

WhiteLabelEntity backoffice = WhiteLabelEntity(
  name: 'Backoffice',
  description: 'Gestão de marcas do White Label.',
  id: '8653819c-2072-4a5a-91fe-25166883d0c7',
  setting: SettingWhiteLabelEntity(
    companyId: '936900bf-28e0-4445-8932-310b94d19f7a',
    ecommerceId: '9dd3f999-0453-44df-b254-96192075137c',
    disabledPrintScreen: false,
    forceAuthenticationStart: false,
    redirectOrigin: 'https://portal.pjmei.app',
    urlWebApplication: 'https://portal.pjmei.app',
    link: LinkWhiteLabelEntity(
      termsOfUse: 'https://www.pjmei.app/termos-de-uso',
      privacyPolicy: 'https://www.pjmei.app/politica-de-privacidade',
      lgpd: 'https://www.pjmei.app/lgpd',
      website: 'https://www.pjmei.app',
    ),
    environment: EnvironmentWhiteLabelEntity(
      individual: ItemEnvironmentWhiteLabelEntity(
        enabled: true,
        limit: 1,
      ),
      company: ItemEnvironmentWhiteLabelEntity(
        enabled: true,
        limit: 10,
      ),
    ),
    functionality: FunctionalityWhiteLabelEntity(
      companies: true,
      financialEducation: true,
      groups: true,
      inbox: true,
      knowledgeBase: true,
      master: true,
      modules: true,
      notifications: true,
      plans: true,
      users: true,
      ecommerce: true,
    ),
    planRequired: false,
  ),
  style: StyleWhiteLabelEntity(
    lightColorScheme: '#ff001d0d',
    darkColorScheme: '#ff001d0d',
    image: ImageWhiteLabelEntity(
      favicon: 'assets/images/logo-backoffice.png',
      icon: 'assets/images/logo-backoffice.png',
      logoMenuMobile: 'assets/images/logo-backoffice.png',
      logoMenuWeb: 'assets/images/logo-backoffice.png',
      logoSplash: 'assets/images/logo-backoffice.png',
    ),
  ),
  validations: [],
);