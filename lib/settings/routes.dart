import 'package:backoffice/features/auth/login_page.dart';
import 'package:backoffice/features/company/company_crud_page.dart';
import 'package:backoffice/features/contacts/contacts_home_page.dart';
import 'package:backoffice/features/ecommerce/max_participants_by_schedule_page.dart';
import 'package:backoffice/features/financial_education/category/financial_education_category_crud_page.dart';
import 'package:backoffice/features/financial_education/financial_education_home_page.dart';
import 'package:backoffice/features/financial_education/video/financial_education_video_crud_page.dart';
import 'package:backoffice/features/financial_education/video/list_video_page.dart';
import 'package:backoffice/features/groups/groups_crud_page.dart';
import 'package:backoffice/features/groups/groups_home_page.dart';
import 'package:backoffice/features/groups/select_users_page.dart';
import 'package:backoffice/features/inbox/data/chat_params.dart';
import 'package:backoffice/features/inbox/ui/chat/chat_page.dart';
import 'package:backoffice/features/inbox/ui/chat/details_chat_page.dart';
import 'package:backoffice/features/inbox/inbox_home_page.dart';
import 'package:backoffice/features/company/company_home_page.dart';
import 'package:backoffice/features/dashboard/dashboard_page.dart';
import 'package:backoffice/features/knowledge_base/categories/faq_category_crud_page.dart';
import 'package:backoffice/features/knowledge_base/knowledge_base_home_page.dart';
import 'package:backoffice/features/knowledge_base/questions/answer_page.dart';
import 'package:backoffice/features/knowledge_base/questions/faq_question_crud_page.dart';
import 'package:backoffice/features/knowledge_base/questions/list_questions_page.dart';
import 'package:backoffice/features/master/master_home_page.dart';
import 'package:backoffice/features/media_manager/media_manager_home_page.dart';
import 'package:backoffice/features/modules/modules_home_page.dart';
import 'package:backoffice/features/orders/order_detail_page.dart';
import 'package:backoffice/features/product_category/product_category_crud_page.dart';
import 'package:backoffice/features/product_category/product_category_home_page.dart';
import 'package:backoffice/features/products/add_product.dart';
import 'package:backoffice/features/products/products_home_page.dart';
import 'package:backoffice/features/schedule/add_product_schedule_page.dart';
import 'package:backoffice/features/schedule/product_scheduling_page.dart';
import 'package:backoffice/features/products/update_product.dart';
import 'package:backoffice/features/select/select_icon_page.dart';
import 'package:backoffice/features/notifications/notifications_crud_page.dart';
import 'package:backoffice/features/notifications/notifications_home_page.dart';
import 'package:backoffice/features/plans/itens_plans/itens_plans_crud_page.dart';
import 'package:backoffice/features/plans/itens_plans/itens_plans_home_page.dart';
import 'package:backoffice/features/plans/plans_crud_page.dart';
import 'package:backoffice/features/plans/plans_home_page.dart';
import 'package:backoffice/features/vouchers/vouchers_home_page.dart';
import 'package:backoffice/features/statistics/categories_most_schedule_page.dart';
import 'package:backoffice/features/statistics/news_companies_by_period_page.dart';
import 'package:backoffice/features/statistics/news_users_by_period_page.dart';
import 'package:backoffice/features/statistics/sales_by_period_page.dart';
import 'package:backoffice/features/statistics/products_most_schedule_page.dart';
import 'package:backoffice/features/statistics/most_booked_weekdays_page.dart';
import 'package:backoffice/features/statistics/service_location_most_schedule_page.dart';
import 'package:backoffice/features/tasks/tasks_home_page.dart';
import 'package:backoffice/features/users/users_crud_page.dart';
import 'package:backoffice/features/users/users_home_page.dart';
import 'package:backoffice/features/white_label/add_user_in_white_label_page.dart';
import 'package:backoffice/features/white_label/edit_white_label_page.dart';
import 'package:backoffice/features/white_label/select_white_label_page.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:backoffice/features/splash/splash_page.dart';
import '../features/contacts/contact_details_page.dart';
import '../features/documents/documents_home_page.dart';
import '../features/modules/module_crud_page.dart';
import '../features/notifications/send_notification_page.dart';
import '../features/orders/orders_home_page.dart';
import '../features/roadmap/roadmap_home_page.dart';
import '../features/settings/settings_page.dart';
import '../features/tasks/contacts_task_page.dart';
import '../features/tasks/task_details_page.dart';
import '../features/tutorial/tutorial_home_page.dart';
import '../features/tutorial/tutorial_pdf_page.dart';
import '../features/tutorial/tutorial_video_page.dart';
import '../features/vouchers/vouchers_crud_page.dart';

Future<String?> checkWhiteLabel(context, GoRouterState state) async {
  if (state.pathParameters.containsKey('whiteLabelId')) {
    if (WhiteLabelEntity.current?.id == null) {
      try {
        final temp = await Api.whiteLabel
            .find(id: '${state.pathParameters['whiteLabelId']}');
        appSM.setWhiteLabel(temp);
        WhiteLabelEntity.current = temp;
        return null;
      } catch (e) {
        return '/';
      }
    } else if (WhiteLabelEntity.current?.id !=
        '${state.pathParameters['whiteLabelId']}') {
      try {
        final temp = await Api.whiteLabel
            .find(id: '${state.pathParameters['whiteLabelId']}');
        appSM.setWhiteLabel(temp);
        WhiteLabelEntity.current = temp;
        return null;
      } catch (e) {
        return '/';
      }
    } else {
      return null;
    }
  } else {
    return '/';
  }
}

final routesBackoffice = GoRouter(
  navigatorKey: navigatorKey,
  initialLocation: '/',
  redirect: (context, state) async {
    if (state.path == '/' || state.path == '/login') {
      return null;
    }
    if (userSM.user?.id == null) {
      final UserEntity? user = await UserDB.ler();
      if (user != null) {
        try {
          final UserEntity temp = await Api.auth.newRefreshToken();
          userSM.setUser(temp);
          await UserDB.atualizar(temp);
          return null;
        } catch (e) {
          await UserDB.remover();
          userSM.user = null;
          return '/login';
        }
      } else {
        return '/login';
      }
    } else {
      return null;
    }
  },
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const SplashPage(),
    ),
    GoRoute(
      path: '/login',
      builder: (context, state) => const LoginPage(),
    ),
    GoRoute(
      path: '/settings',
      builder: (context, state) => const SettingsPage(),
    ),
    GoRoute(
      path: '/home',
      builder: (context, state) => const SelectWhiteLabelPage(),
    ),
    GoRoute(
      path: '/white-label/add',
      builder: (context, state) => const CrudWhiteLabelPage(),
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard',
      builder: (context, state) => const DashboardPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/company',
      builder: (context, state) => const CompanyHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/company/:companyId',
      builder: (context, state) => CompanyCrudPage(
        company: state.extra as CompanyEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/white-label/user/add',
      builder: (context, state) => const AddUserInWhiteLabelPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/white-label/add',
      builder: (context, state) => const CrudWhiteLabelPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/white-label/edit',
      builder: (context, state) => CrudWhiteLabelPage(
        whiteLabel: state.extra as WhiteLabelEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/statistics/most/booked-weekdays',
      builder: (context, state) => const MostBookedWeekdaysPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path:
          '/:whiteLabelId/dashboard/statistics/most/service-location-most-schedule',
      builder: (context, state) => const ServiceLocationMostSchedulePage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/statistics/most/categories-most-schedule',
      builder: (context, state) => const CategoriesMostSchedulePage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/statistics/most/products-most-schedule',
      builder: (context, state) => const ProductsMostSchedulePage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/statistics/basic/by-period/users',
      builder: (context, state) => const UsersByPeriodPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/statistics/basic/by-period/companies',
      builder: (context, state) => const CompaniesByPeriodPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/statistics/basic/by-period/sales',
      builder: (context, state) => const SalesByPeriodPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/user',
      builder: (context, state) => const UsersHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/users/:userId',
      builder: (context, state) => UsersCrudPage(
        user: state.extra as UserEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/select/users',
      builder: (context, state) => SelectUsersPage(
        users: state.extra is List<String> ? (state.extra as List<String>) : [],
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/vouchers',
      builder: (context, state) => const VouchersHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/vouchers/new',
      builder: (context, state) => const VouchersCrudPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/vouchers/:id',
      builder: (context, state) => VouchersCrudPage(
        voucher: state.extra as VoucherEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/groups',
      builder: (context, state) => const GroupsHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/groups/new',
      builder: (context, state) => const GroupsCrudPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/groups/:groupId',
      builder: (context, state) => GroupsCrudPage(
        group: state.extra as GroupEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/plans',
      builder: (context, state) => const PlansHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/plans/new',
      builder: (context, state) => const PlansCrudPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/plans/vouchers',
      builder: (context, state) => const VouchersHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/plans/:planId',
      builder: (context, state) => PlansCrudPage(
        plan: state.extra as PlanEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/plans/:planId/items',
      builder: (context, state) => ItensPlansHomePage(
        plan: state.extra as PlanEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/plans/:planId/items/new',
      builder: (context, state) => ItensPlansCrudPage(
        planId: state.pathParameters['planId']!,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/plans/:planId/items/:itemId',
      builder: (context, state) => ItensPlansCrudPage(
        planId: state.pathParameters['planId']!,
        item: state.extra as PlanItemEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/financial-education',
      builder: (context, state) => const FinancialEducationHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/financial-education/category/new',
      builder: (context, state) => const FinancialEducationCategoryCrudPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/financial-education/category/:categoryId',
      builder: (context, state) => FinancialEducationCategoryCrudPage(
        category: state.extra as CategoryFinancialEducationEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path:
          '/:whiteLabelId/dashboard/financial-education/category/:categoryId/videos',
      builder: (context, state) => ListVideoPage(
        category: state.extra as CategoryFinancialEducationEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path:
          '/:whiteLabelId/dashboard/financial-education/category/:categoryId/videos/new',
      builder: (context, state) => FinancialEducationVideoCrudPage(
        video: (state.extra as Map<String, dynamic>)['video'] != null
            ? (state.extra as Map<String, dynamic>)['video']
                as VideoFinancialEducationEntity
            : null,
        category: (state.extra as Map<String, dynamic>)['category']
            as CategoryFinancialEducationEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path:
          '/:whiteLabelId/dashboard/financial-education/category/:categoryId/videos/:videoId',
      builder: (context, state) => FinancialEducationVideoCrudPage(
        video: (state.extra as Map<String, dynamic>)['video']
            as VideoFinancialEducationEntity,
        category: (state.extra as Map<String, dynamic>)['category']
            as CategoryFinancialEducationEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/knowledge-base',
      builder: (context, state) => const KnowledgeBaseHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/knowledge-base/category/new',
      builder: (context, state) => const KnowledgeBaseCategoryCrudPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/knowledge-base/category/:categoryId',
      builder: (context, state) => KnowledgeBaseCategoryCrudPage(
        category: state.extra as HelpCategoriesFaqEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path:
          '/:whiteLabelId/dashboard/knowledge-base/category/:categoryId/questions',
      builder: (context, state) => ListQuestionsFaqPage(
        category: state.extra as HelpCategoriesFaqEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path:
          '/:whiteLabelId/dashboard/knowledge-base/category/:categoryId/questions/new',
      builder: (context, state) => FaqQuestionCrudPage(
        category: state.extra as HelpCategoriesFaqEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path:
          '/:whiteLabelId/dashboard/knowledge-base/category/:categoryId/questions/:questionId',
      builder: (context, state) => FaqQuestionCrudPage(
        question: (state.extra as Map<String, dynamic>)['question']
            as HelpQuestionFaqEntity,
        category: (state.extra as Map<String, dynamic>)['category']
            as HelpCategoriesFaqEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/html-editor',
      builder: (context, state) => AnswerPage(
        text: state.extra as String,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/master',
      builder: (context, state) => const MasterHomePage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/selected-icon',
      builder: (context, state) => const ViewIconsListPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/contacts',
      builder: (context, state) => const ContactsHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/contacts/add',
      builder: (context, state) => const ContactDetailsPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/contacts/:contactId/details',
      builder: (context, state) {
        return ContactDetailsPage(
          contact: state.extra as ContactEntity,
        );
      },
      redirect: (context, state) async {
        if (state.extra is ContactEntity) {
          return null;
        } else {
          return checkWhiteLabel(context, state);
        }
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/roadmap',
      builder: (context, state) => const RoadmapHomePage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/tutorial',
      builder: (context, state) => const TutorialHomePage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/tutorial/item',
      builder: (context, state) {
        if ((state.extra as TutorialEntity).type == 'video') {
          return TutorialVideoDetail(
            tutorial: state.extra as TutorialEntity,
          );
        } else if ((state.extra as TutorialEntity).type == 'pdf') {
          return TutorialPdfPage(
            tutorial: state.extra as TutorialEntity,
          );
        } else {
          return const TutorialHomePage();
        }
      },
      redirect: (context, state) async {
        if (state.extra is TutorialEntity) {
          return null;
        } else {
          return '/${WhiteLabelEntity.current?.id}/dashboard';
        }
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/documents',
      builder: (context, state) => const DocumentsHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/empresa/:id/documents',
      builder: (context, state) => DocumentsHomePage(
        automaticallyImplyLeading: true,
        companyId: state.pathParameters['id'].toString(),
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/media-manager',
      builder: (context, state) => const MediaManagerHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/tasks',
      builder: (context, state) => const TasksHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/tasks/add',
      builder: (context, state) => const TaskDetailsPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/tasks/:taskId/details',
      builder: (context, state) {
        return TaskDetailsPage(
          task: state.extra as TaskEntity,
        );
      },
      redirect: (context, state) async {
        if (state.extra is TaskEntity) {
          return null;
        } else {
          return checkWhiteLabel(context, state);
        }
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/tasks/contacts',
      builder: (context, state) {
        return ContactsTaskPage(
          contacts: state.extra as List<ContactEntity>,
        );
      },
      redirect: (context, state) async {
        if (state.extra is List<ContactEntity>) {
          return null;
        } else {
          return '/empresa/${WhiteLabelEntity.current?.id}/tasks';
        }
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/modules',
      builder: (context, state) => const ModulesHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/modules/new',
      builder: (context, state) => const ModuleCrudPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/modules/:moduleId',
      builder: (context, state) => ModuleCrudPage(
        module: state.extra as ModulePjmei,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/notifications',
      builder: (context, state) => const NotificationHomePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/notifications/new',
      builder: (context, state) => const NotificationCrudPage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/notifications/:notificationId',
      builder: (context, state) => NotificationCrudPage(
        notification: state.extra as NotificationEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/notifications/:notificationId/send',
      builder: (context, state) => SendNotificationPage(
        notification: state.extra as NotificationEntity,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/notifications/:notificationId/users',
      builder: (context, state) => const AddUserInWhiteLabelPage(
        selectUsers: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/inbox',
      builder: (context, state) => const InboxHomePage(),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/empresa/:companyId/inbox/:idChat/details',
      builder: (context, state) => DetailsChatPage(
        chat: (state.extra as Map<String, dynamic>)['chat'],
        current: (state.extra as Map<String, dynamic>)['current'],
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/inbox/chat/:idChat',
      builder: (context, state) {
        if (state.extra != null && state.extra is Map<String, dynamic>) {
          return ChatPage(
            params: ChatParams(
              chatId: (state.extra! as Map<String, dynamic>)['chatId'] as String,
              origin: (state.extra! as Map<String, dynamic>)['origin'] as String?,
              title: (state.extra! as Map<String, dynamic>)['title'] as String?,
              current: (state.extra! as Map<String, dynamic>)['current'] as ParticipantChat,
              participants: (state.extra! as Map<String, dynamic>)['participants'] as List<ParticipantChat>?,
              additionalChat: (state.extra! as Map<String, dynamic>)['additionalChat'] as AdditionalChat?,
            ),
            chat: (state.extra! as Map<String, dynamic>)['chat'] as ChatEntity,
            key: (state.extra! as Map<String, dynamic>)['key'] as Key,
          );
        } else {
          return const InboxHomePage();
        }
      },
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/inbox/:idChat/details',
      builder: (context, state) => DetailsChatPage(
        chat: (state.extra as Map<String, dynamic>)['chat'],
        current: (state.extra as Map<String, dynamic>)['current'],
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/ecommerce/max-participants-by-schedule',
      builder: (context, state) => const MaxParticipantsBySchedulePage(
        automaticallyImplyLeading: true,
      ),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/ecommerce/product/add',
      builder: (context, state) => AddProductPage(() {}),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/ecommerce/orders/home',
      builder: (context, state) => const OrdersHomePage(
        automaticallyImplyLeading: true,
      ),
    ),
    GoRoute(
      path: '/:whiteLabelId/ecommerce/product/home',
      builder: (context, state) => const ProductsHomePage(
        automaticallyImplyLeading: true,
      ),
    ),
    GoRoute(
      path: '/:whiteLabelId/ecommerce/product/category',
      builder: (context, state) => const ProductCategoryHomePage(
        automaticallyImplyLeading: true,
      ),
    ),
    GoRoute(
      path: '/:whiteLabelId/dashboard/ecommerce/schedules',
      builder: (context, state) => const ProductSchedulingPage(null),
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/product/schedule/add',
      builder: (context, state) {
        if (state.extra != null && state.extra is ProductEcommerceEntity) {
          return AddProductSchedulePage(
            state.extra! as ProductEcommerceEntity,
          );
        } else if (state.extra != null && state.extra is Map) {
          return AddProductSchedulePage(
            (state.extra! as Map)['product'] as ProductEcommerceEntity,
          );
        } else {
          return const AddProductSchedulePage(null);
        }
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/ecommerce/product/category/add',
      builder: (context, state) {
        if (state.extra != null &&
            state.extra is ProductCategoryEcommerceEntity) {
          return ProductCategoryCrudPage(
            state.extra! as ProductCategoryEcommerceEntity,
          );
        } else if (state.extra != null && state.extra is Map) {
          return ProductCategoryCrudPage(
            (state.extra! as Map)['category'],
          );
        } else {
          return const ProductCategoryCrudPage(null);
        }
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/ecommerce/product/:productId',
      builder: (context, state) {
        if (state.extra != null && state.extra is ProductEcommerceEntity) {
          return UpdateProductPage(
            state.extra! as ProductEcommerceEntity,
            () {},
          );
        } else if (state.extra != null && state.extra is Map) {
          return UpdateProductPage(
            (state.extra! as Map)['product'],
            (state.extra! as Map)['notifyParent'],
          );
        } else {
          return const DashboardPage();
        }
      },
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
    GoRoute(
      path: '/:whiteLabelId/ecommerce/order/:itemId',
      builder: (context, state) {
        if (state.extra != null && state.extra is OrderEcommerceEntity) {
          return OrderDetailPage(
            state.extra! as OrderEcommerceEntity,
            () {},
          );
        } else if (state.extra != null && state.extra is Map) {
          return OrderDetailPage(
            (state.extra! as Map)['dadosPedido'],
            (state.extra! as Map)['notifyParent'],
            key: (state.extra! as Map)['key'],
          );
        } else {
          return const DashboardPage();
        }
      },
      redirect: (context, state) async {
        return checkWhiteLabel(context, state);
      },
    ),
  ],
);
