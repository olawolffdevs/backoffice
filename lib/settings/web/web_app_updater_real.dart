import 'dart:html' as html;
import 'package:flutter/foundation.dart' show kIsWeb;

class WebAppUpdater {
  static void updateTitle(String newTitle) {
    if (kIsWeb) {
      html.document.title = newTitle;
    }
  }

  static void updateFavicon(String newFaviconURL) {
    if (kIsWeb) {
      // Adiciona um timestamp ao URL para evitar caching
      final String faviconURLWithTimestamp = '$newFaviconURL?_=${DateTime.now().millisecondsSinceEpoch}';
      
      var link = html.document.querySelector("link[rel*='icon']") as html.LinkElement?;
      if (link == null) {
        link = html.document.createElement('link') as html.LinkElement;
        link.rel = 'icon';
        html.document.head!.append(link);
      }
      link.href = faviconURLWithTimestamp;
    }
  }
}