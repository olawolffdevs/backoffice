import 'package:flutter/material.dart';

class CardFiltros extends StatelessWidget {

  const CardFiltros(
    this.title, {
    this.icon,
    super.key,
    this.group,
    this.value,
    this.onChanged,
    this.selected,
  }) : down = false;

  const CardFiltros.down(
    this.title,
    this.icon, {
    super.key,
    this.group,
    this.value,
    this.onChanged,
    this.selected,
  }) : down = true;
  final String title;
  final IconData? icon;
  final String? group;
  final String? value;
  final bool? selected;
  final bool? down;
  final ValueChanged<String>? onChanged;

  @override
  Widget build(BuildContext context) {
    bool? selectedTemp = selected;
    if (value == null) {
      selectedTemp = selectedTemp ?? group == title;
    } else {
      selectedTemp = selectedTemp ?? group == value;
    }

    return GestureDetector(
      onTap: onChanged != null
          ? () {
              if (onChanged != null) {
                if (value != null) {
                  onChanged!(value!);
                } else {
                  onChanged!(title);
                }
              }
            }
          : null,
      child: Container(
        decoration: BoxDecoration(
          color: selectedTemp
              ? Theme.of(context).colorScheme.primary
              : Theme.of(context).colorScheme.secondaryContainer,
          borderRadius: const BorderRadius.all(Radius.circular(50)),
        ),
        margin: const EdgeInsets.only(right: 5),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Center(
          child: title != ''
              ? Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    _icon(context, selectedTemp),
                    Text(
                      title,
                      style: Theme.of(context).textTheme.labelLarge!.copyWith(
                        color: selectedTemp
                          ? Theme.of(context).colorScheme.onPrimary
                          : Theme.of(context).colorScheme.onSecondaryContainer,
                      ),
                    ),
                    down ?? false
                        ? Row(
                            children: [
                              Container(
                                  height: 21,
                                  width: 1,
                                  margin:
                                      const EdgeInsets.only(left: 8, right: 5),
                                  color: selectedTemp
                                ? Theme.of(context).colorScheme.onPrimary
                                : Theme.of(context).colorScheme.onSecondaryContainer,
                              ),
                              Icon(Icons.keyboard_arrow_down,
                                  size: 21,
                                  color: selectedTemp
                                ? Theme.of(context).colorScheme.onPrimary
                                : Theme.of(context).colorScheme.onSecondaryContainer,
                              ),
                            ],
                          )
                        : const SizedBox(),
                  ],
                )
              : Center(child: _icon(context, selectedTemp)),
        ),
      ),
    );
  }

  Widget _icon(BuildContext context, bool selected) {
    if (icon == null) {
      return const SizedBox();
    }
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: Icon(
        icon,
        size: 21,
        color: selected
          ? Theme.of(context).colorScheme.onPrimary
          : Theme.of(context).colorScheme.onSecondaryContainer,
      ),
    );
  }
}
