import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:flutter/material.dart';

class BoxMessageNotRead extends StatelessWidget {
  const BoxMessageNotRead({super.key});

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(16)),
        color: Theme.of(context).colorScheme.surface,
        border: Border.all(
          width: 1,
          color: Theme.of(context).colorScheme.surfaceVariant,
        ),
      ),
      child: InkWell(
        borderRadius: const BorderRadius.all(Radius.circular(16)),
        onTap: () {
          appSM.setSelectedIndex(10);
        },
        child: Padding(
          padding: const EdgeInsets.all(24),
          child: Observer(
            builder: (context) {
              if (appSM.countMessages > 0) {
                return Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Conversas não lidas',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    HighlightMessageCard(
                      color: ColorSystem(context: context),
                      text: '${appSM.countMessages} conversas',
                    ),
                  ],
                );
              } else {
                return Text(
                  'Parabéns, você não possui mensagens não lidas.',
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
