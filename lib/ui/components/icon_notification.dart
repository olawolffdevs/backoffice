import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class IconNotification extends StatelessWidget {
  const IconNotification({super.key});

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        if (appSM.countMessages > 0) {
          return Stack(
            alignment: Alignment.bottomRight,
            children: [
              IconButton(
                icon: const Icon(EvaIcons.bellOutline),
                onPressed: () {
                  appSM.setSelectedIndex(10);
                },
              ),
              IgnorePointer(
                child: Container(
                  padding: const EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.primary,
                    borderRadius: const BorderRadius.all(Radius.circular(24)),
                  ),
                  child: Text(
                    '${appSM.countMessages}',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.onPrimary,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          );
        } else {
          return IconButton(
            icon: const Icon(EvaIcons.bellOutline),
            onPressed: () {
              appSM.setSelectedIndex(10);
            },
          );
        }
      },
    );
  }
}