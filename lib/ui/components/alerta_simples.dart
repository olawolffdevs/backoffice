import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class AlertaSimples extends StatelessWidget {

  const AlertaSimples(
    this.mensagem, {
    Key? key,
    this.conteudo,
    this.textoBotaoPrimario = 'OK',
    this.funcaoBotaoPrimario,
    this.textoBotaoSecundario,
    this.funcaoBotaoSecundario,
    this.botaoSecundarioNaDireita = true,
  }) : super(key: key);
  final String mensagem;
  final String? conteudo;
  final String? textoBotaoSecundario;
  final String textoBotaoPrimario;
  final Function()? funcaoBotaoPrimario;
  final Function()? funcaoBotaoSecundario;
  final bool botaoSecundarioNaDireita;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.transparent,
      content: Container(
        padding: const EdgeInsets.all(22),
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              mensagem,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: conteudo != null ? FontWeight.bold : null,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            conteudo != null
                ? Text(
                    conteudo!,
                    textAlign: TextAlign.center,
                  )
                : const SizedBox(),
            const SizedBox(height: 20),
            textoBotaoSecundario == null
                ? botaoPrimario(context)
                : Row(
                    children: [
                      !botaoSecundarioNaDireita
                          ? Expanded(child: botaoSecundario(context))
                          : Expanded(child: botaoPrimario(context)),
                      const SizedBox(
                        width: 10,
                      ),
                      botaoSecundarioNaDireita
                          ? Expanded(child: botaoSecundario(context))
                          : Expanded(child: botaoPrimario(context)),
                    ],
                  ),
          ],
        ),
      ),
    );
  }

  Widget botaoPrimario(BuildContext context) {
    return OwButton(
      height: 45,
      onPressed: funcaoBotaoPrimario ??
          () {
            context.pop();
          },
      labelText: textoBotaoPrimario,
    );
  }

  Widget botaoSecundario(BuildContext context) {
    return OwButton(
      height: 45,
      onPressed: funcaoBotaoSecundario ??
          () {
            context.pop();
          },
      labelText: textoBotaoSecundario ?? '',
    );
  }
}
