import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:go_router/go_router.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:flutter/material.dart';

class MenuWidget extends StatelessWidget implements PreferredSizeWidget {
  const MenuWidget({required this.title, Key? key, this.appBarWidget}) : super(key: key);
  final String title;
  final Widget? appBarWidget;

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
        color: Colors.transparent,
        child: PreferredSize(
          preferredSize: const Size.fromHeight(80.0),
          child: Container(
            width: MediaQuery.of(context).size.width,
            color: Theme.of(context)
                .colorScheme
                .secondaryContainer
                .withOpacity(.2),
            child: SafeArea(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                    child: GestureDetector(
                      onTap: () {
                        if (Navigator.canPop(context)) {
                          context.pop();
                        } else {
                          context.go(
                              '/${Environment.current?.environmentType}/${WhiteLabelEntity.current?.name}/dashboard/home');
                        }
                      },
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                          horizontal: 25,
                          vertical: 12,
                        ),
                        decoration: BoxDecoration(
                          color:
                              Theme.of(context).colorScheme.secondaryContainer,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(
                              15,
                            ),
                          ),
                        ),
                        padding: const EdgeInsets.all(15),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(
                              EvaIcons.gridOutline,
                              color:
                                  Theme.of(context).textTheme.titleLarge!.color,
                            ),
                            const SizedBox(width: 10),
                            Flexible(
                              child: Text(
                                title,
                                style: Theme.of(context).textTheme.titleLarge,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 1),
                  if (appBarWidget != null) appBarWidget!,
                  const SizedBox(width: 15),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
