import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';

class CardProductionEnvironmentAlertWidget extends StatelessWidget {
  const CardProductionEnvironmentAlertWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(
          Radius.circular(
            15,
          ),
        ),
        color: Theme.of(context).colorScheme.secondaryContainer,
      ),
      padding: const EdgeInsets.all(25),
      child: Text(
        'Os itens listados nesta tela se referem ao ambiente ${WhiteLabelEntity.current?.name}. Fique atento ao ambiente, tendo cuidado ao postar algo em produção.',
        style: Theme.of(context).textTheme.bodyLarge,
      ),
    );
  }
}
