import 'package:backoffice/settings/firebase_options.dart';
import 'package:backoffice/settings/start_app.dart';
import 'package:flutter/material.dart';
import 'package:plugin_pjmei_components/plugin_pjmei_components.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => Main.local(
  whiteLabel: backoffice,
  environment: EnvironmentType.development,
  listenUser: _updateUserListener,
  minimalVersionApp: 11,
  home: const MyApp(),
  logoutPathRedirect: '/login',
  pathProfileUser: '/user',
  firebaseOptions: DefaultFirebaseOptions.currentPlatform,
  preload: () async {
    await _getTheme();
    checkUserNotifier.addListener(() async {
      await UserDB.atualizar(userSM.user!);
    });
  },
);

Future<void> _updateUserListener() async {
  await UserDB.atualizar(userSM.user!);
}

Future<void> _getTheme() async {
  try {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('ativo')) {
      if (prefs.getBool('ativo') ?? false) {
        appSM.setThemeMode(ThemeMode.dark);
      } else {
        appSM.setThemeMode(ThemeMode.light);
      }
    } else {
      appSM.setThemeMode(ThemeMode.system);
    }

    if (prefs.containsKey('soundEnabled')) {
      appSM.setSoundEnabled(prefs.getBool('soundEnabled')!);
    } else {
      appSM.setSoundEnabled(true);
    }
  } catch(e) {
    //
  }

  if (userSM.user?.id == null) {
    final UserEntity? user = await UserDB.ler();
    if (user != null) {
      userSM.setUser(user);
    }
  }
}